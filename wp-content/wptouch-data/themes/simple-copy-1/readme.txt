Theme Name: Simple Copy #1
Theme URI: http://www.bravenewcode.com/products/wptouch-pro/
Description: CUSTOMIZED FRONT PAGE TO ADD SECOND DEALER LOCATION.
Version: 1.4.2
Stable tag: 1.4.2
Depends on: 3.7.3
Author: MEDIAGISTIC
Parent: Foundation
Tags: smartphone

= Version 1.4.2 (March 3, 2015) =

* Changed: More robust icon naming

= Version 1.4.1 (December 11, 2014) =

* Changed: Icon font renamed from fontello to wptouch-icons

= Version 1.4 (October, 2014) =

* Changed: Overhauled blog styling drawing on design used in MobileStore