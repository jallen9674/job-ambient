<?php get_header(); ?>

	<div id="content">
		<?php simple_homepage_content(); ?>

		<?php if ( wptouch_has_menu( 'secondary_menu' ) ) { ?>
			<nav id="homepage-menu-list" class="homepage-menu show-hide-menu">
				<?php wptouch_show_menu( 'secondary_menu' ); ?>
			</nav>
		<?php } ?>
		
		<?php if ( simple_has_phone_number() ) { ?>
			<nav class="homepage-menu">
				<a href="tel:<?php simple_phone_number(); ?>" class="phone-number"><?php _e( 'Call Us', 'wptouch-pro' ); ?></a>
			</nav>
		<?php } ?>

		<?php if ( simple_has_map() ) { ?>
			<nav class="homepage-menu">
				<a href="javascript:return false;" class="map-address" data-effect-target="map"><?php _e( 'Kingman, AZ', 'wptouch-pro' ); ?></a>
			</nav>			
		<?php } ?>
		
		<div id="map" class="start">
			<?php simple_map_display(); ?>
		</div>
        
        <nav class="homepage-menu"> 
        <a href="javascript:return false;" class="map-address2 tappable" data-effect-target="map2">Henderson, NV</a> 
        </nav>
  <div id="map2" class="start hide">
    <p>110 Corporate Park Dr Ste 111, Henderson, NV 89074</p>
    <br>
    <div class="simple-map">
      <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="//maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=110 Corporate Park Dr Ste 111, Henderson, NV 89074&amp;ie=UTF8&amp;z=12&amp;t=m&amp;iwloc=near&amp;output=embed"></iframe>
    </div>
  </div>

	</div><!-- #content -->

<?php get_footer(); ?>