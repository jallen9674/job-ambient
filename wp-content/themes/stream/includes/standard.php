<?php 
if( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) {
global $post_id; ?>
	<div class="postimg">
        <figure>
			<a title="<?php printf(__('Permanent Link to %s', 'THEME_FRONT_SITE'), get_the_title()); ?>" href="<?php the_permalink(); ?>">
				<?php echo atp_resize($post_id,'','510','250','imgborder','');?>
			</a>
		</figure>
	</div>
<?php } ?>