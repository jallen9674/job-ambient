<?php $link = get_post_meta($post->ID, 'link_url', TRUE); ?>
<?php if( is_singular() ) { ?>
    <h2 class="entry-title"><?php the_title(); ?></h1>
	<?php } else { ?>
	<h2 class="entry-title"><a href="<?php echo esc_url($link); ?>"><?php the_title(); ?></a></h2>
<?php } ?>
