<?php get_header(); ?>
<div class="pagemid">
	<div class="inner">

		<div id="main">
			<div class="entry-content">

				<?php get_template_part( 'loop' ); ?>

			</div>
			<!-- /entrycontent -->
		</div>
		<!-- /main -->

		<?php get_sidebar(); ?>

		<div class="clear"></div>
			
	</div>
	<!-- /inner -->
</div>
<!-- /pagemid -->
<?php get_footer(); ?>