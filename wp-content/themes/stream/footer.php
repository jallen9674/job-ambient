	</div>
	<div class="clear"></div>
	<!-- /- #layout_wrapper -->
	
	<footer id="footer">
		<?php if( get_option( 'atp_footer_sidebar' ) != 'on' ) { ?>
		<div class="inner">
			<?php get_template_part( 'sidebar', 'footer' ); ?>
		</div>
		<?php } ?>
	</footer>
	<!-- /footer -->
	

	<div class="copyright">
		<div class="inner">
			<div class="copyright_left">
				<?php echo stripslashes(do_shortcode(get_option('atp_copy_left')));?>
			</div>
			<div class="copyright_right">
				<?php echo stripslashes(do_shortcode(get_option('atp_copy_right'))); ?>
			</div>
		</div>
		<!-- /inner -->
	</div>
	<!-- /copyright -->

</div>
<!-- /wrapper -->

</div>
<!-- /layout -->
<?php 
	$googleanalytics = get_option( 'atp_googleanalytics' );
	if( $googleanalytics ){
		echo stripslashes($googleanalytics); 
	}
?>
<?php wp_footer();?>
</body>
</html>