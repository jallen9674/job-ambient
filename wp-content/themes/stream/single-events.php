<?php
/*
Template Name: Events
*/
?>
<?php get_header(); ?>
	<div class="pagemid">
		<?php echo atp_generator( 'breadcrumb', $post->ID ); ?>			
		<div class="inner">	

			<div id="main">
				<div class="entry-content">						
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div <?php post_class('event-single');?> id="post-<?php the_ID(); ?>">
				<?php
				$event_date=get_post_meta($post->ID, "event_date", true);
				$event_starttime=get_post_meta($post->ID, "event_starttime", true);
				$event_endtime=get_post_meta($post->ID, "event_endtime", true);
				$event_venue=get_post_meta($post->ID, "event_venue", true);
				$event_location=get_post_meta($post->ID,"event_location",true);
				$date_event=explode(' ',$event_date);
				if( atp_generator( 'sidebaroption',$post->ID ) != "fullwidth" ){ $width='625'; }else{ $width='960';  }	
				?>
				<?php if( has_post_thumbnail()){ ?>
				<!-- .postimg -->
				<div class="event_thumb">
					<figure>
						<?php
						$post_thumbnail_id = get_post_thumbnail_id($post->ID);
						echo atp_resize($post->ID,'',$width,'','imageborder','');?>
					</figure>
				</div>
				<!-- .postimg -->
				<?php } ?>	
					
				<h2 class="entry-title title-large"><?php the_title(); ?></h2>
				<div class="event_entry">
					<div class="event_details">
						<ul>
							<?php if( isset( $event_date ) && $event_date != '' ) { ?>
							<li>
								<strong><?php echo $datetxt; ?></strong> <?php echo $event_date; ?>
							</li>
							<?php } ?>
							<?php if(isset($event_starttime) && $event_starttime !='') { ?>
							<li>
								<strong><?php echo $starttimetxt; ?></strong> <?php echo date('g:i a', strtotime($event_starttime)); ?>
							</li>
							<?php } ?>
							<?php if(isset($event_endtime) && $event_endtime !='') { ?>
							<li>
								<strong><?php echo $endtimetxt; ?></strong> <?php echo  date('g:i a', strtotime($event_endtime)); ?>
							</li>
							<?php } ?>
							<?php if(isset($event_venue) && $event_venue !='') { ?>
							<li>
								<strong><?php echo $venuetxt; ?></strong> <?php echo $event_venue; ?>
							</li>
							<?php } ?>	
							<?php if(isset($event_location) && $event_location !='') { ?>
							<li>
								<strong><?php echo $locationtxt; ?></strong> <?php echo $event_location; ?>
							</li>
							<?php } ?>												
						</ul>
					</div>
				</div>
				<div class="divider_line"></div>
				<?php the_content(); ?>
				<?php if($event_location != '') {
					echo sysgooglemap(array(
					'width'     => '',
					'height'    => '300',
					'address'   => $event_location,
					'controls'  => '[]',
					'longitude' => '',
					'latitude'	=> '',
					'html'      => '',
					'popup'     => 'false',
					'zoom'      => 12,
					'align'     => false));
				}?>
				<div class="clear"></div>
				<div id="nav-below" class="navigation">
				<div class="nav-previous"><?php previous_post_link('&larr; %link') ?></div>
				<div class="nav-next"><?php next_post_link('%link &rarr;') ?></div>
						</div>
						<?php
						
							echo atp_generator('relatedposts',get_the_id());
						?>

						</div>
							
						<!-- start:post pagination / metadata -->
						<?php edit_post_link(__('Edit', 'THEME_FRONT_SITE'), '<span class="edit-link">', '</span>'); ?>

						<div class="clear"></div>
						<?php
						if(get_option('atp_portfoliocomments')!='on') { 
							comments_template('', true); 
						}?>
						
						<?php endwhile; else: ?>
						<?php '<p>'.__('Sorry, no projects matched your criteria.', 'THEME_FRONT_SITE').'</p>';?>
						<?php endif; ?>
					</div>
					<!-- /entry-content -->
				</div>
				<!-- /main -->
				<?php if(atp_generator('sidebaroption',$post->ID) != "fullwidth"){ get_sidebar(); } ?>
				<div class="clear"></div>
		</div>
		<!-- /inner -->
	</div>
	<!-- /pagemid -->
	<?php get_footer(); ?>