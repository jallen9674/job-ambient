<?php

$path = __FILE__;
$pathwp = explode( 'wp-content', $path );
$wp_url = $pathwp[0];
// Loading the wp core functions and variables
require_once( $wp_url.'/wp-load.php' );

function atp_style() {
$atp_option_var = array(
'atp_themecolor',
'atp_stickybarcolor',
'atp_bodyp',
'atp_h1',
'atp_h2',
'atp_h3',
'atp_h4',
'atp_h5',
'atp_h4',
'atp_h6',
'atp_sidebartitle',
'atp_footertitle',
'atp_copyrights',
'atp_entrytitle',
'atp_entrytitlelinkhover',
'atp_bodyproperties',
'atp_sliderbgproup',
'atp_logotitle',
'atp_tagline',
'atp_breadcrumbtext',
'atp_breadcrumblink',
'atp_breadcrumblinkhover',
'atp_breadcrumbbg',
'atp_footerlinkcolor',
'atp_footerlinkhovercolor',
'atp_footerbg',
'atp_copybgcolor',
'atp_footertextcolor',
'atp_subheaderproperties',
'atp_topmenu',
'atp_topmenu_linkhoverbg',
'atp_topmenu_linkhover',
'atp_topmenu_sub_bg',
'atp_topmenu_bg',
'atp_primarymenu_bg',
'atp_topmenu_sub_link',
'atp_topmenu_active_linkbg',
'atp_topmenu_active_link',
'atp_topmenu_sub_linkhover',
'atp_topmenu_sub_hoverbg',
'atp_topmenu_bordercolor',
'atp_entrytitle',
'atp_overlayimages',
'atp_wrapbg',
'atp_link',
'atp_linkhover',
'atp_subheaderlink',
'atp_subheaderlinkhover',
'atp_subheader_pt',
'atp_subheader_pb',
'atp_logo_ml',
'atp_logo_mt',
'atp_subheader_textcolor',
'atp_extracss',
'atp_headerbg',
'atp_headertxt',
'atp_teaser_properties',
'atp_footerheadingcolor',
'atp_teaser_text',
'atp_fontwoff',
'atp_fontttf',
'atp_fontsvg',
'atp_fonteot',
'atp_fontname',
'atp_fontclass',
'atp_copylinkcolor',

'atp_border',
'atpbodyfont',
'atp_headings',
'atp_sidebarTitleFace','atp_footerTitleFace','atp_mainmenufont','atp_footertext','atp_copyright','atp_footertitle_color'
);

foreach($atp_option_var as $value){
	$$value = get_option($value);
}

//------------------------------------------------------------------------------------------------------
// B O D Y  B G  P R O P E R T I E S
//------------------------------------------------------------------------------------------------------
$overlayimages = '';
$bodyimage =  generate_imagecss($atp_bodyproperties,array('background-color'=>'color'));
if( $atp_overlayimages != '' ) {
	$overlayimages =  generate_css(array('background-image'=>'url('.THEME_PATTDIR.$atp_overlayimages.')'));
}
$themecolor		= $atp_themecolor	? $atp_themecolor : '';
$bodyp = generate_fontcss($atp_bodyp);

//------------------------------------------------------------------------------------------------------
// S L I D E R   B A C K G R O U N D   P R O P E R T I E S
//------------------------------------------------------------------------------------------------------
$sliderbg =  generate_imagecss($atp_sliderbgproup,array('background-color'=>'color'));
$subheaderbg =  generate_imagecss($atp_subheaderproperties,array('background-color'=>'color'));
$teaserbg =  generate_imagecss($atp_teaser_properties,array('background-color'=>'color'));
$logotitle = generate_fontcss($atp_logotitle);
$subheader_pt = $atp_subheader_pt ? 'padding-top:'.$atp_subheader_pt.'px;': '';
$subheader_pb = $atp_subheader_pb ? 'padding-bottom:'.$atp_subheader_pb.'px;': '';
$subheader_textcolor = $atp_subheader_textcolor ? 'color:'.$atp_subheader_textcolor.';': '';
$footerbg = generate_imagecss($atp_footerbg,array('background-color'=>'color'));
//$footertextcolor = $atp_footertextcolor ? 'color:'.$atp_footertextcolor.';': '';
$footerheadingcolor = $atp_footerheadingcolor ? 'color:'.$atp_footerheadingcolor.';': '';
$copybgcolor = $atp_copybgcolor ? 'background-color:'.$atp_copybgcolor.';': '';
$breadcrumbbg = $atp_breadcrumbbg ? 'background-color:'.$atp_breadcrumbbg.';': '';
$breadcrumbtext = $atp_breadcrumbtext ? 'color:'.$atp_breadcrumbtext.';': '';
$breadcrumblink = $atp_breadcrumblink ? 'color:'.$atp_breadcrumblink.';': '';
$atp_copylinkcolor = $atp_copylinkcolor ? 'color:'.$atp_copylinkcolor.';': '';
$breadcrumblinkhover = $atp_breadcrumblinkhover ? 'color:'.$atp_breadcrumblinkhover.';': '';
$footerlinkcolor = $atp_footerlinkcolor ? 'color:'.$atp_footerlinkcolor.';':'';
$footerlinkhovercolor = $atp_footerlinkhovercolor ? 'color:'.$atp_footerlinkhovercolor.';':'';
$atp_border =  $atp_border ? 'border-color:'.$atp_border.';':'';
$copyrights = generate_fontcss($atp_copyrights);
$bodyfont = $atpbodyfont ? 'font-family:'.$atpbodyfont.';': '';
$headings = $atp_headings ? 'font-family:'.$atp_headings.';': '';
$sidebarTitleFace = $atp_sidebarTitleFace ? 'font-family:'.$atp_sidebarTitleFace.';': '';
$footerTitleFace = $atp_footerTitleFace ? 'font-family:'.$atp_footerTitleFace.';': '';
$copyrightProperties = generate_fontcss( $atp_copyright);
$atp_footertitle_color =$atp_footertitle_color ? 'color:'.$atp_footertitle_color.';':''; 
//------------------------------------------------------------------------------------------------------
// L O G O   T A G L I N E 
//------------------------------------------------------------------------------------------------------
$tagline = generate_fontcss($atp_tagline);
$logo_ml = $atp_logo_ml ? 'margin-left:'.$atp_logo_ml.'px;': '';
$logo_mt = $atp_logo_mt ? 'margin-top:'.$atp_logo_mt.'px;': '';
$stickybgcolor	= $atp_stickybarcolor? 'background:'.$atp_stickybarcolor.';': '';;

//------------------------------------------------------------------------------------------------------
//  M E N U 
//------------------------------------------------------------------------------------------------------
$primarymenubg = $atp_primarymenu_bg ? 'background-color:'.$atp_primarymenu_bg.';':'';
$topmenubg = $atp_topmenu_bg ? 'background-color:'.$atp_topmenu_bg.';':'';
$topmenufont = generate_fontcss($atp_topmenu);
$topmenu_linkhover	= $atp_topmenu_linkhover?'color:'.$atp_topmenu_linkhover.';': '';
$topmenu_linkhoverbg	= $atp_topmenu_linkhoverbg?'background-color:'.$atp_topmenu_linkhoverbg.';': '';
$topmenu_sub_bg	= $atp_topmenu_sub_bg? 'background:'.$atp_topmenu_sub_bg.';': '';
$topmenu_sub_link	= $atp_topmenu_sub_link? 'color:'.$atp_topmenu_sub_link.';': '';
$topmenu_sub_linkhover	= $atp_topmenu_sub_linkhover? 'color:'.$atp_topmenu_sub_linkhover.';': '';
$topmenu_sub_hoverbg	= $atp_topmenu_sub_hoverbg? 'background:'.$atp_topmenu_sub_hoverbg.';': '';
$topmenu_bordercolor	= $atp_topmenu_bordercolor? 'border-color:'.$atp_topmenu_bordercolor.';': '';
$topmenu_active_linkbg   = $atp_topmenu_active_linkbg? 'background-color:'.$atp_topmenu_active_linkbg.';': '';
$topmenu_active_link     = $atp_topmenu_active_link? 'color:'.$atp_topmenu_active_link.';': '';
$footerText			= generate_fontcss( $atp_footertext);
//------------------------------------------------------------------------------------------------------
// HEADINGS
//------------------------------------------------------------------------------------------------------
$h1font = generate_fontcss($atp_h1);
$h2font = generate_fontcss($atp_h2);
$h3font = generate_fontcss($atp_h3);
$h4font = generate_fontcss($atp_h4);
$h5font = generate_fontcss($atp_h5);
$h6font = generate_fontcss($atp_h6);

//------------------------------------------------------------------------------------------------------
// GENERAL PROPERTIES
//------------------------------------------------------------------------------------------------------
$entrytitlefont = generate_fontcss($atp_entrytitle);
$entrytitlelinkhover= $atp_entrytitlelinkhover? 'color:'.$atp_entrytitlelinkhover.';': '';

$sidebartitlefont = generate_fontcss($atp_sidebartitle);
$footertitlefont = generate_fontcss($atp_footertitle);

$wrapbg	= $atp_wrapbg ? 'background-color:'.$atp_wrapbg.';': '';
$headerbg	= $atp_headerbg ? 'background-color:'.$atp_headerbg.';': '';
$headertxt = $atp_headertxt ? 'color:'.$atp_headertxt.';': '';
$linkcolor	= $atp_link ? 'color:'.$atp_link.';': '';
$linkhovercolor	= $atp_linkhover ? 'color:'.$atp_linkhover.';': '';
$subheaderlink	= $atp_subheaderlink ? 'color:'.$atp_subheaderlink.';': '';
$subheaderlinkhover	= $atp_subheaderlinkhover ? 'color:'.$atp_subheaderlinkhover.';': '';
$teaser_text = $atp_teaser_text ? 'color:'.$atp_teaser_text.';': '';

?>
<style>
<?php if ($themecolor != '') { ?>
.topbar,
#subheader,
.progress_bar,
#featured_slider,
.hover_type a:hover,
table.fancy_table th,
.post_list:hover .content { background-color:<?php echo $themecolor;?> }

a,
a:focus { color:<?php echo $themecolor;?> }

#footer,
.tabs li.current,
#featured_slider,
.sf-menu ul.sub-menu,
.widget-title span { border-color:<?php echo $themecolor;?> }
<?php } ?>

body {
	<?php echo $bodyimage; ?>
	<?php echo $bodyp; ?>
	<?php echo $bodyfont; ?>
	}
.bodyoverlay {
	<?php echo $overlayimages; ?>
}

h3.widget-title { <?php echo $sidebarTitleFace; ?> }

#footer h3.widget-title { <?php echo $footerTitleFace; ?> }

#header {
	<?php echo $headerbg; ?>
	<?php echo $headertxt; ?>
}
#featured_slider { 
	<?php echo $sliderbg; ?>
	}
.frontpage_teaser {
	<?php echo $teaserbg; ?> 
	<?php echo $teaser_text; ?> 
}

#subheader {
	<?php echo $subheaderbg; ?> 
	<?php echo $subheader_pt; ?> 
	<?php echo $subheader_pb; ?> 
}

#subheader {
	<?php echo $subheader_textcolor; ?> 
}

h1#site-title a{
	<?php echo $logotitle; ?> 
}

h2#site-description {
	<?php echo $tagline; ?> 
}
.logo {
	<?php echo $logo_ml; ?>
	<?php echo $logo_mt; ?>
}
#sticky { <?php echo $stickybgcolor; ?> }
.primarymenu { <?php echo $topmenubg; ?> }
.sf-menu a { <?php echo $topmenufont; ?> }

.sf-menu li:hover, .sf-menu li.sfHover,
.sf-menu a:focus, .sf-menu a:hover, 
.sf-menu a:active {
	<?php echo $topmenu_linkhoverbg; ?> 
	<?php echo $topmenu_linkhover; ?> 
}

.sf-menu ul { <?php echo $topmenu_sub_bg; ?> }
.sf-menu ul a { <?php echo $topmenu_sub_link; ?> }

.sf-menu li li:hover, .sf-menu li li.sfHover,
.sf-menu li li a:focus, .sf-menu li li a:hover, .sf-menu li li a:active {
	<?php echo $topmenu_sub_linkhover; ?> 
	<?php echo $topmenu_sub_hoverbg; ?> 
}

.sf-menu li li a { <?php echo $topmenu_bordercolor; ?> }

.sf-menu li.current-cat > a, 
.sf-menu li.current_page_item > a,
.sf-menu li.current-page-ancestor > a {
	<?php echo $topmenu_active_linkbg; ?>
	<?php echo $topmenu_active_link; ?>
}
h1 { <?php echo $h1font; ?> }

h2 { <?php echo $h2font; ?> }
h3 { <?php echo $h3font; ?> }
h4 { <?php echo $h4font; ?> }
h5 { <?php echo $h5font; ?> }
h6 { <?php echo $h6font; ?> }
h1, h2, h3, h4, h5, h6 { <?php echo $headings; ?> }

h2.entry-title a { <?php echo $entrytitlefont; ?> }
h2.entry-title a:hover { <?php echo $entrytitlelinkhover; ?> }

.widget-title { <?php echo $sidebartitlefont; ?><?php echo $atp_footertitle_color?> }

#footer h3 .widget-title { <?php echo $footertitlefont; ?> <?php echo $atp_footertitle_color?> }
.pagemid { <?php echo $wrapbg; ?> }
a, .entry-title a { <?php echo $linkcolor; ?> }

a:hover,
.entry-title a:hover { <?php echo $linkhovercolor; ?> }

#subheader a { <?php echo $subheaderlink; ?> }
#subheader a:hover { <?php echo $subheaderlinkhover; ?> }

#footer { 
	<?php echo $footerbg; ?> 
	<?php echo $footerText; ?>
}
.copyright { 
	<?php echo $copyrights; ?> 
	<?php echo $copybgcolor; ?> 
	<?php echo $copyrightProperties ?>
	
}

.copyright_left > a{
	<?php echo $atp_copylinkcolor; ?> 
}
#footer .widget-title span {
	<?php echo $atp_border; ?> 
}

#footer  .widget-title {
	<?php echo $footerheadingcolor; ?> 
	<?php echo $footertextcolor; ?> 	
}

.breadcrumbs {
	<?php echo $breadcrumbbg; ?> 
}
.breadcrumb .breadcrumbs-plus {
	<?php echo $breadcrumbtext; ?> 
}

.breadcrumbs a { <?php echo $breadcrumblink; ?> }
.breadcrumbs a:hover{ <?php echo $breadcrumblinkhover ?> }
#footer a { <?php echo $footerlinkcolor; ?> }
#footer a:hover { <?php echo $footerlinkhovercolor; ?> }
<?php 
if($atp_extracss != '') {
	echo $atp_extracss;
}
?>
<?php

if(($atp_fontwoff !='') && ($atp_fontttf !='') && ($atp_fontsvg !='') && ($atp_fonteot !='')) {
$fontclass=$atp_fontclass ? $atp_fontclass :'h1,h2,h3,h4,h5,h6,#atp_menu a,.splitter ul li a,.pagination';

?>
@font-face {
    font-family: '<?php echo $atp_fontname; ?>';
    src: url('<?php echo $atp_fontwoff; ?>');
    src: url('<?php echo $atp_fonteot; ?>?#iefix') format('embedded-opentype'),
         url('<?php echo $atp_fontwoff; ?>') format('woff'),
         url('<?php echo $atp_fontttf; ?>') format('truetype'),
         url('<?php echo $atp_fontsvg; ?>#<?php echo $atp_fontname; ?>') format('svg');
    font-weight: normal;
    font-style: normal;

}
<?php echo $fontclass; ?> {
font-family: '<?php echo $atp_fontname; ?>';
}
<?php
define('CUSTOMFONT',true);
}
?>
</style>
<?php
}
add_action('wp_head','atp_style',100);


//for font css attributes
function generate_fontcss($arr_var) {
	$font			= $arr_var['face'] 			? 'font-family:'.$arr_var['face'].';': ''; 
	$size			= $arr_var['size'] 			? 'font-size:'.$arr_var['size'].';': '';
	$color			= $arr_var['color'] 		? 'color:'.$arr_var['color'].';': '';
	$lineheight		= $arr_var['lineheight']	? 'line-height:'.$arr_var['lineheight'].';': '';
	$style			= $arr_var['style'] 		? 'font-style:'.$arr_var['style'].';': '';
	$variant		= $arr_var['fontvariant'] 	? 'font-weight:'.$arr_var['fontvariant'].';': '';
	
	return "{$font} {$size} {$color} {$lineheight} {$style} {$variant}";
}

//for background image css attributes
function generate_imagecss($arr,$include_others) {

	$str='';
	if($arr['image']!='') {
	
		$str .='background-image:url('.$arr['image'].');
		background-repeat:'.$arr['style'].';
		background-position:'.$arr['position'].';
		background-attachment:'.$arr['attachment'].';';
	}
	
	if(is_array($include_others)) {
		foreach($include_others as $key => $val) {
			if($arr[$val]!='')
				$str .=$key.':'.$arr[$val].';';
		}
	}
	return $str;
}

//forkey value css pairs
function  generate_css($arr) {
	$str='';
	if(is_array($arr)) {
		foreach($arr as $key => $val) {
			$str .=$key.':'.$val.';';
		}
	}
	return $str;
}
?>