<?php
/*
Template Name: Events
*/
?>
<?php get_header(); ?>

	<div class="pagemid">
		<?php echo atp_generator( 'breadcrumb', $post->ID ); ?>	
		<div class="inner">

			<div id="main">
				<div class="entry-content">
				<?php
	
				if ( get_query_var('paged') ) {
					$paged = get_query_var('paged');
				}
				
				elseif ( get_query_var('page') ) {
					$paged = get_query_var('page');
				} else {
					$paged = 1;  
				}
				query_posts("post_type=events&paged=$paged&meta_key=event_date&orderby=meta_value&order=asc");

				?>

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php
						$event_date=get_post_meta($post->ID, "event_date", true);
						$event_starttime=get_post_meta($post->ID, "event_starttime", true);
						$event_endtime=get_post_meta($post->ID, "event_endtime", true);
						$event_location=get_post_meta($post->ID,"event_location",true);
						$event_venue=get_post_meta($post->ID,"event_venue",true);
						$date_event=explode('-',$event_date);	
					?>
					<div class="events">
						<?php
						if( has_post_thumbnail()){ 
							
						?>
						<!-- .postimg -->
						<div class="event_thumb">
							<figure>
								<?php
								$post_thumbnail_id = get_post_thumbnail_id($post->ID);
								echo atp_resize($post->ID,'','200','150','imageborder','');?>
							</figure>
						</div>
						<!-- .postimg -->
						<?php } ?>	
						<div class="event_entry">
						<div class="event_details">
						<h2 class="entry-title title-large"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( __( "Permanent Link to %s", 'THEME_FRONT_SITE' ), esc_attr( get_the_title() ) ); ?>"><?php the_title(); ?></a></h2>
						<ul>
							<?php if( isset( $event_date ) && $event_date != '' ) { ?>
							<li>
								<strong><?php echo $datetxt; ?></strong> <?php echo $event_date; ?>
							</li>
							<?php } ?>
							<?php if(isset($event_starttime) && $event_starttime !='') { ?>
							<li>
								<strong><?php echo $starttimetxt; ?></strong> <?php echo date('g:i a', strtotime($event_starttime)); ?>
							</li>
							<?php } ?>
							<?php if(isset($event_endtime) && $event_endtime !='') { ?>
							<li>
								<strong><?php echo $endtimetxt; ?></strong> <?php echo date('g:i a', strtotime($event_endtime));; ?>
							</li>
							<?php } ?>
							<?php if(isset($event_venue) && $event_venue !='') { ?>
							<li>
								<strong><?php echo $venuetxt; ?></strong> <?php echo $event_venue; ?>
							</li>
							<?php } ?>	
							<?php if(isset($event_location) && $event_location !='') { ?>
							<li>
								<strong><?php echo $locationtxt; ?></strong> <?php echo $event_location; ?>
							</li>
							<?php } ?>												
						</ul>
						</div>
						<!-- /evententry -->
					</div>
					</div>
					<div class="divider_line"></div>
					<!-- /post-<?php the_ID();?> -->

					<?php 
					endwhile; 
					wp_reset_query();
					?>
					
					<?php edit_post_link( __( 'Edit', 'THEME_FRONT_SITE' ), '<span class="edit-link">', '</span>' ); ?>

					<?php
					if(function_exists('atp_pagination')){ 
						atp_pagination(); 
					}?>
					
					<?php else : ?>
					<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'THEME_FRONT_SITE' ); ?></p>
					<?php get_search_form(); ?>
					<?php endif;?>
				</div>
				<!-- .event -->
			</div>
			<!-- /main-->
			
			<?php if(atp_generator('sidebaroption',$post->ID) != "fullwidth"){ get_sidebar(); } ?>
			<!-- /sidebar -->

			<div class="clear"></div>
		</div>
		<!-- /inner -->
	</div>
	<!-- /pagemid -->
	<?php get_footer(); ?>