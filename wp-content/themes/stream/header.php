<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'THEME_FRONT_SITE' ), max( $paged, $page ) );

?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo THEME_URI; ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<!--[if IE 7]>
  <link rel="stylesheet" href="<?php echo THEME_URI; ?>/css/fortawesome/font-awesome-ie7.css">
<![endif]-->
<?php if(get_option('atp_custom_favicon')) { ?>
	<link rel="shortcut icon" href="<?php echo get_option('atp_custom_favicon'); ?>" type="image/x-icon" /> 
<?php } ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>

<body <?php body_class('gradient'); ?>>
	<?php if ( get_post_meta($post->ID, 'page_bg_image', true) ) : ?>
	<img id="pagebg" src="<?php echo get_post_meta($post->ID, "page_bg_image", true); ?>" />
	<?php endif; ?>

	<div id="<?php echo get_option('atp_layoutoption');?>" class="<?php echo atp_generator('sidebaroption',$post->ID);?>"><div class="bodyoverlay"></div>
	<?php if(get_option('atp_stickybar') != "on" &&  get_option('atp_stickycontent') !='') { ?>
	<div id="sticky">
	<?php echo  stripslashes(get_option('atp_stickycontent')); ?>
	</div>
	<div id="trigger" class="tarrow"></div>
	<!-- #sticky -->
	<?php }?>
	<div id="wrapper">

		<div class="topbar"></div>
		<!-- /- .topbar -->
		<div id="header">
			<div class="inner">
				<div class="logo">
				<?php atp_generator('logo'); ?>
				</div>
				<!-- /logo -->

				<div class="header_right">
				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('headerright') ) : endif;  ?>
				</div>
				<!-- /headright-->
			</div>
			<!-- /inner-->
		</div>
		<!-- /header -->
		<?php if ( get_option('atp_menu_position') != "on" ) { ?>
		<div class="primarymenu"><nav><?php atp_generator('atp_primary_menu'); ?></nav></div>
		<?php } ?>
		<?php 
		$pageslider = get_post_meta($post->ID,'page_slider', true);
		if(is_front_page() || $pageslider != "" ) {
			atp_generator('teaser_option');
		}else{
			echo atp_generator('subheader',$post->ID);
		}?>
		<!-- /- .frontpage_teaser -->
		
		<div id="layout_wrapper">
			<?php if ( get_option('atp_menu_position') == "on" ) { ?><div class="primarymenu"><nav><?php get_option('atp_menu_position') ? atp_generator('atp_primary_menu') : ''; ?></nav></div>	<?php } ?>
			<div class="clear"></div>
		<?php		
		if(is_front_page() || $pageslider != "" ) {
		
			// Get Slider based on the option selected in theme options panel
			if(get_option("atp_slidervisble") != "on") {
				if($pageslider == "") {
					$chooseslider=get_option('atp_slider');
				} else {
					$chooseslider = $pageslider;
				}
				switch ($chooseslider):
					case 'static_image':
										get_template_part( 'slider/static', 'slider' );   	
										break;
					case 'flexslider':
										get_template_part( 'slider/flex', 'slider' );
										break;
					case 'eislider':
										get_template_part('slider/ei','slider'); 	
										break;
					case 'nivoslider':
										get_template_part('slider/nivo','slider');
										break;
					case 'videoslider':
										get_template_part( 'slider/video', 'slider' );   	
										break;
					case 'planbox':
										get_template_part( 'slider/planbox', 'slider' );   	
										break;
					case 'custom_slider':
										get_template_part( 'slider/custom', 'slider' );   	
										break;
					default:
										get_template_part( 'slider/default', 'slider' );
				endswitch;
			}
			wp_reset_query();
		} 
?>