<?php get_header(); ?>
	<div class="pagemid">
	<?php echo atp_generator( 'breadcrumb', $post->ID ); ?>	1

		<div class="inner">
			<?php echo atp_generator( 'breadcrumb', $post->ID ); ?>	
			<div id="main">	
			
				<ul class="list-square">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<li <?php post_class('taglist');?> id="post-<?php the_ID(); ?>">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( __( "Permanent Link to %s", 'THEME_FRONT_SITE' ), esc_attr( get_the_title() ) ); ?>"><?php the_title(); ?></a>
				</li>
				<?php endwhile; endif;?>
				</ul>
				<?php 
				if(function_exists('atp_pagination')) {
					atp_pagination(); 
				} ?>
				<!-- #pagination -->
		
			</div>

			<?php if(atp_generator('sidebaroption',$post->ID) != "fullwidth"){ get_sidebar(); } ?>
			<!-- /sidebar -->

			<div class="clear"></div>

		</div>
		<!-- .inner -->
	</div>
	<!-- .pagemid -->
	
	<?php get_footer(); ?>