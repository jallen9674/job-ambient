<?php get_header(); ?>

	<div class="pagemid">

		<div class="inner">

			<div id="main">

				<div class="entry-content">
					<?php
					if ( get_query_var('paged') ) {
						$paged = get_query_var('paged');
					}
					elseif ( get_query_var('page') ) {
						$paged = get_query_var('page');
					} else {
						$paged = 1;  
					}
					query_posts( array_merge( array(
						'posts_per_page' => -1,
						'paged'=>$paged
						), $wp_query->query ) );
					$columns=4;
					$column_index = 0; 
					if (have_posts()) : while (have_posts()) : the_post(); 
					$project_desc = get_post_meta($post->ID,'project_desc',TRUE);
					$column_index++;
					$last = ($column_index == $columns && $columns != 1) ? 'last ' : '';
				?>
			
				<div class="one_fourth <?php echo $last; ?>">
				<?php
					$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );
					echo '<a href="'.$src['0'].'" data-rel="prettyPhoto[mixed]">';
						echo atp_resize($post->ID,'',210,120,'imgborder','');
						echo '</a>';
					?>
					<h5><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
				</div>
			<?php	
				if($column_index == $columns){
					$column_index = 0;
				}
			 endwhile; ?>
			
				<?php 
				if(function_exists('atp_pagination')) { 
					atp_pagination(); 
				} ?>
				<!-- #pagination -->
			
				<?php else :
				if ( is_category() ) { // If this is a category archive
					printf("<h2 class='center'>Sorry, but there aren't any posts in the %s category yet.</h2>", single_cat_title('',false));
				} else if ( is_date() ) { // If this is a date archive
				echo("<h2>Sorry, but there aren't any posts with this date.</h2>");
				} else if ( is_author() ) { // If this is a category archive
					$userdata = get_userdatabylogin(get_query_var('author_name'));
					printf("<h2 class='center'>Sorry, but there aren't any posts by %s yet.</h2>", $userdata->display_name);
				} else {
					echo("<h2 class='center'>No posts found.</h2>");
					}
				get_search_form();
				endif;
			?>

				</div>
				<!-- entry .content -->
			
				</div>
			<!-- /main-->
			
			<div class="clear"></div>
		</div>
		<!-- /inner -->
	</div>
	<!-- /pagemid -->
	<?php get_footer(); ?>