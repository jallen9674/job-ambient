<?php

	$options = '';
	/* Get Options from Theme Options */
	$atp_style = get_option('atp_style');
	$readmoretxt = get_option('atp_readmoretxt') ? get_option('atp_readmoretxt') : 'Readmore:';
	$starttimetxt = get_option('atp_starttime') ? get_option('atp_starttime') : 'Start Time:';
	$endtimetxt = get_option('atp_endtime') ? get_option('atp_endtime') : 'End Time:';
	$datetxt = get_option('atp_date') ? get_option('atp_date') : 'Date:';	
	$venuetxt = get_option('atp_venue') ? get_option('atp_venue') : 'Venue:';
	$locationtxt = get_option('atp_location') ? get_option('atp_location') : 'Location:';
	$projectdatetxt = get_option('atp_project_date') ? get_option('atp_project_date') : 'Project Date:';
	$projecturltxt = get_option('atp_project_url') ? get_option('atp_project_url') : 'Project Url:';
	$skillstxt = get_option('atp_skills') ? get_option('atp_skills') : 'Skills:';
	$visitsitetxt = get_option('atp_visitsitetxt') ? get_option('atp_visitsitetxt'):'Visit Site';
	$categoriestxt = get_option('atp_category') ? get_option('atp_category') : 'Category:';
	$breadcrumb_separator = get_option('atp_breadcrumbsep') ? get_option('atp_breadcrumbsep') : '';

	// Awesomefonts
	require_once( get_template_directory() . '/framework/includes/awesomefont_array.php' );
	// Corner RibbonS  Files
	require_once( get_template_directory() . '/framework/includes/ribbons_array.php' );

	// Array of Sociables
	$staff_social = array(
		''           => 'Select Sociable',
		'blogger'    =>'Blogger',
		'delicious'  =>'Delicious',
		'digg'       =>'Digg',
		'facebook'   =>'Facebook',
		'flickr'     =>'Flickr',
		'forrst'     =>'Forrst',
		'google'     =>'Google',
		'html5'      =>'Html5',
		'lastfm'     =>'Lastfm',
		'linkedin'   =>'Linkedin',
		'paypal'     =>'Paypal',
		'picasa'     =>'Picasa',
		'pinterest'  =>'Pinterest',
		'rss'        =>'Rss',
		'skype'      =>'Skype',
		'stumbleupon'=>'Stumbleupon',
		'technorati' =>'Technorati',
		'twitter'    =>'Twitter',
		'twitter3'   =>'Twitter',
		'dribbble'   =>'Dribbble',
		'vimeo'      =>'Vimeo',
		'windows'    =>'Windows',
		'wordpress'  =>'Wordpress',
		'yahoo'      =>'Yahoo',
		'yelp'       =>'Yelp',
		'youtube'    =>'Youtube'
	);
	ksort($staff_social); // Sort Variables by Alphabetical Order

	
	/* Load the Theme class. */
	if(!class_exists('atp_theme')){
		// class start
		class atp_theme 
		{
			public $theme_name;
			public $meta_box;		
			public function __construct()
			{
				$this->atp_constant();
				$this->atp_themesupport();
				$this->atp_head();
				$this->atp_customfont();
				$this->atp_themepanel();
				$this->atp_widgets();
				$this->atp_post_types();
				$this->atp_custom_meta();
				$this->atp_meta_generators();
				$this->atp_shortcodes();
				$this->atp_common();
			}

			function atp_constant()
			{
				/**
				 * Framework General Variables and directory paths
				 */
				if (function_exists('wp_get_theme')){
					$theme_data = wp_get_theme();
					$themeversion = $theme_data->Version;
					$theme_name = $theme_data->Name;
				} else {
					$theme_data = (object) get_theme_data(get_template_directory() . '/style.css');
					$themeversion = $theme_data->Version;
					$theme_name = $theme_data->Name;
				}
				define('FRAMEWORK', '2.0'); //  Framework Version
				define('THEMENAME',$theme_name);
				define('THEMEVERSION',$themeversion);	

				/**
				 * Set the file path based on whether the Options Framework is in a parent theme or child theme
				 * Directory Structure
				 */
				define( 'THEME_URI', get_template_directory_uri() );	
				define( 'THEME_DIR',get_template_directory() );
				
				define( 'FRAMEWORK_DIR', get_template_directory() . '/framework/' );
				define( 'FRAMEWORK_URI', get_template_directory_uri() . '/framework/' );
				define( 'CUSTOM_META', FRAMEWORK_DIR . '/custom-meta/' );
				define( 'CUSTOM_PLUGINS', FRAMEWORK_DIR . '/custom-plugins/' );
				define( 'CUSTOM_POST', FRAMEWORK_DIR . '/custom-post/' );
				
				define( 'THEME_JS', THEME_URI . '/js' );
				define( 'THEME_CSS', THEME_URI . '/css' );

				define( 'THEME_SHORTCODES', FRAMEWORK_DIR . 'shortcode/' );
				define( 'THEME_WIDGETS', FRAMEWORK_DIR . 'widgets/' );
				define( 'THEME_PLUGINS', FRAMEWORK_DIR . 'plugins/' );
				define( 'THEME_POSTTYPE',FRAMEWORK_DIR .'custom-post/' );
				define( 'THEME_CUSTOMMETA',FRAMEWORK_DIR.'custom-meta/' );
				define( 'THEME_PATTDIR', get_template_directory_uri().'/images/patterns/' );

			}


			// widgets
			function atp_widgets()
			{
				$atp_widgts = array( 'register_widget','contactform','contactinfo','flickr','twitter','sociable','popularpost','recentpost');
				foreach( $atp_widgts as $widget )	{
					require_once( THEME_WIDGETS .$widget.'.php' );
				}
			}
			
			/***
			 * Header loads
			 */
			function atp_head()
			{
				require_once( FRAMEWORK_DIR . 'common/head.php' );
			}

			/***
			 * Custom Function
			 */
			function atp_customfont()
			{
				require_once( FRAMEWORK_DIR . 'common/atp_googlefont.php' );
			}
			
			/***
			 * Shortcodes
			 */
			function atp_shortcodes()
			{
				$atp_short = array('accordion','boxes','buttons','contactform','contactinfo','flickr','general','gallery','image','layout','lightbox','messageboxes','flexslider','popular','portfolio','recent','tabs_toggles','planbox','twitter','testimonial','sociable','videos','carousel_list','eventslist','staff','progressbar','gmap' );
				foreach( $atp_short as $short ){
					require_once( THEME_SHORTCODES .$short.'.php' );
				}
			}

			/***
			 * Theme Supports
			 */
			function atp_themesupport()
			{
				// Add support for a variety of post formats
				//add_theme_support( 'post-formats', array( 'aside','audio','link', 'image', 'gallery', 'quote','status','video') );
				/**
				 * Add Theme Support for 
				 * post thumbnails and automatic feed links
				 */
				//add_theme_support( 'post-thumbnails', array( 'post', 'page', 'portfoliotype', 'slider', 'testimonialtype', 'events') );
				add_theme_support( 'post-thumbnails' );
				add_theme_support( 'automatic-feed-links' );
				//add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
				/**
				 * function register_my_menus - Registers Menus
				 */
				add_theme_support( 'editor-style' );
				add_theme_support( 'menus' );
				
				register_nav_menus( array(
					'primary-menu' => __( 'Primary Menu','ATP_ADMIN_SITE' )
				));
				
			
				if ( ! isset( $content_width ) ) $content_width = 900;
				
			}

			/***
			 * Common functions/pagination/sociables/image resizing and breadcrumb plus
			 */
			function atp_common()
			{
				require_once( THEME_DIR . '/css/skin.php' );
				require_once( FRAMEWORK_DIR . 'common/class_twitter.php' );
				require_once( FRAMEWORK_DIR . 'common/atp_generator.php' );
				require_once( FRAMEWORK_DIR . 'common/pagination.php' );
				require_once( FRAMEWORK_DIR . 'common/sociable-bookmark.php' );
				require_once( FRAMEWORK_DIR . 'includes/image_resize.php' );
				require_once( FRAMEWORK_DIR . 'includes/class-activation.php' );
			}

			/***
			 * Custom Post types slider/portfolio/testimonials
			 */
			function atp_post_types()
			{
				require_once( THEME_POSTTYPE . '/slider.php' );
				require_once( THEME_POSTTYPE . '/events.php' );
				require_once( THEME_POSTTYPE . '/portfolio.php' );
				require_once( THEME_POSTTYPE . '/testimonial.php' );
			}

			/***
			 * Custom meta options and generator
			 */
			function atp_custom_meta()
			{
				require_once( THEME_CUSTOMMETA . '/page-meta.php' );
				require_once( THEME_CUSTOMMETA . '/post-meta.php' );
				require_once( THEME_CUSTOMMETA . '/slider-meta.php' );
				require_once( THEME_CUSTOMMETA . '/testimonial-meta.php' );
				require_once( THEME_CUSTOMMETA . '/portfolio-meta.php' );
				require_once( THEME_CUSTOMMETA . '/events-meta.php' );
				
			}
			
			function atp_meta_generators() {
				require_once( THEME_CUSTOMMETA . '/meta-generator.php' );
				require_once( THEME_CUSTOMMETA . '/shortcode-meta.php' );
				require_once( THEME_CUSTOMMETA . '/shortcode-generator.php' );
			}
		
			/***
			 * Admin functions/interface
			 */
			function atp_themepanel()
			{
				// These files build out the options interface.  
				require_once( FRAMEWORK_DIR . 'admin/admin-interface.php' );
				require_once( FRAMEWORK_DIR . 'admin/theme-options.php' );
				if( isset( $_GET['page'] ) == 'advance' ) {
					require_once( FRAMEWORK_DIR . 'admin/advance-options.php' );
				}
			}
			// class end
		/** 
			 * Custom Switch case for fetching categories
			 * posts/post-types/custom-taxonomies
			 */

			function atp_variable($type)
			{
				$options = array();
				switch($type){
					case 'pages':
								$atp_entries = get_pages('sort_column=post_parent,menu_order');
								foreach ($atp_entries as $atppage) {
									$options[$atppage->ID] = $atppage->post_title;
								}
								break;
					case 'slider':
								$atp_entries = get_terms('slider_cat','orderby=name&hide_empty=true');
								foreach ($atp_entries as $slider) {
								$options[$slider->slug] = $slider->name;
								$slider_ids[] = $slider->slug;
								}
								break;
					case 'portfolio':
								$atp_entries = get_terms('portfolio_cat','orderby=name&hide_empty=true');
								foreach ($atp_entries as $menuvalue) {
								$options[$menuvalue->slug] = $menuvalue->name;
								$menuvalue_ids[] = $menuvalue->slug;
								}
								break;
					case 'posts':
								$atp_entries = get_categories('hide_empty=true');
								foreach ($atp_entries as $atp_posts) {
								$options[$atp_posts->slug] = $atp_posts->name;
								$atp_posts_ids[] = $atp_posts->slug;
								}
								break;
					case 'categories':
								$atp_entries = get_categories('hide_empty=true');
								foreach ($atp_entries as $atp_posts) {
								$options[$atp_posts->term_id] = $atp_posts->name;
								$atp_posts_ids[] = $atp_posts->term_id;
								}
								break;
					case 'events':
								$atp_entries = get_terms('events_cat','orderby=name&hide_empty=true');
								foreach ($atp_entries as $eventsvalue) {
								$options[$eventsvalue->slug] = $eventsvalue->name;
								$eventsvalue_id[] = $eventsvalue->slug;
								}
								break;
					case 'testimonial':
								$atp_entries =get_terms('testimonial_cat','orderby=name&hide_empty=true');
								foreach ($atp_entries as $testimonialvalue) {
								$options[$testimonialvalue->slug] = $testimonialvalue->name;
								$testimonialvalue_id[] = $testimonialvalue->slug;
								}
								break;
					case 'tags':
								$atp_entries = get_tags( array( 'taxonomy' => 'post_tag' ));
								foreach ($atp_entries as $tagsvalue) {
								$options[$tagsvalue->slug] = $tagsvalue->name;
								}
								break;
					case 'slider_type':
								$options = array(
									''				=> 'Select Slider',
									'flexslider'	=> 'Flex Slider',
									'eislider'		=> 'Ei Slider',
									'nivoslider'	=> 'Nivo Slider',
									'planbox'		=> 'Planbox Slider',
									'videoslider'	=> 'Single Video',
									'static_image'	=> 'Static Image',
									'custom_slider'	=> 'Custom Slider'
								);
								break;
				}
				
				return $options;
			}
		}
	}
	$atp_theme = new atp_theme();
	$shortname = "atp";
	$url =  FRAMEWORK_URI . 'admin/images/';

	add_action( 'after_setup_theme', 'atp_theme_setup' );
	function atp_theme_setup()
	{
		load_theme_textdomain( 'THEME_FRONT_SITE', get_template_directory() . '/languages' );
		load_theme_textdomain( 'ATP_ADMIN_SITE', get_template_directory() . '/languages' );

		add_filter( 'the_content', 'pfix' );
		add_filter( 'widget_text', 'do_shortcode' );
		add_filter( 'posts_where', 'multi_tax_terms');
		add_filter( 'wp_trim_excerpt', 'new_excerpt_more' );
		add_filter( 'upload_mimes', 'atp_custom_upload_mimes');
		
	}

	/***
	 * Shortcodes p tag Fix
	 */
	function pfix( $content )
	{
		$array = array (
			'<p>[' => '[', 
			']</p>' => ']', 
			']<br />' => ']'
		);
		$content = strtr( $content, $array );
		return $content;
	}
	
	/**
	 * Multiple taxonomies
	 */
	function multi_tax_terms($where) {
		global $wp_query, $wpdb;

		if (isset($wp_query->query_vars['term']) && (strpos($wp_query->query_vars['term'], ',') !== false && strpos($where, "AND 0") !== false)) {
			//Get the terms
			$term_arr = explode(",", $wp_query->query_vars['term']);
			foreach ($term_arr as $term_item) {
				$terms[] = get_terms($wp_query->query_vars['taxonomy'], array(
					'slug' => $term_item
				));
			} //$term_arr as $term_item
				
			//Get the id of posts with that term in that taxonomy
			foreach ($terms as $term) {
				$term_ids[] = $term[0]->term_id;
			} //$terms as $term
			
			$post_ids = get_objects_in_term($term_ids, $wp_query->query_vars['taxonomy']);
			
			if (!is_wp_error($post_ids) && count($post_ids)) {
				// Build the new query
				$new_where = " AND $wpdb->posts.ID IN (" . implode(', ', $post_ids) . ") ";
				$where     = str_replace("AND 0", $new_where, $where);
			}else {
			}
		} //$wp_query
		return $where;
	}
	
	// 
	function new_excerpt_more( $excerpt ) {
		return str_replace( '[...]', '...', $excerpt );
	}

	//  Custom Upload file extension
	function atp_custom_upload_mimes($existing_mimes)
	{
		// add the file extension to the array
		$existing_mimes['eot'] = 'font/eot';
		$existing_mimes['ttf'] = 'font/ttf';
		$existing_mimes['woff'] = 'font/woff';
		$existing_mimes['svg'] = 'font/svg';
		return $existing_mimes;
	}
	
	$atp_singlenavigation = get_option( 'atp_singlenavigation' );
	
	/***
	 * code that executes when theme is being activated
	 */
	if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' && get_option( 'atp_default_template_option_values','defaultoptionsnotexists' ) == 'defaultoptionsnotexists' ){
		
		$default_option_values = '';
		add_option( 'atp_default_template_option_values', $default_option_values, '', 'yes' );
		atp_options();
		update_option_values( $options,unserialize( base64_decode( $default_option_values ) ) );
	}
	
?>