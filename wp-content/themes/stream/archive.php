<?php get_header(); ?>

	<div class="pagemid">
		<?php echo atp_generator( 'breadcrumb', $post->ID ); ?>	
		<div class="inner">	
			<div id="main">
				<div class="entry-content">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
				<div <?php post_class();?> id="post-<?php the_ID(); ?>">
						<?php echo atp_generator('postmetaStyle'); ?>						
						<!-- /postmetadata -->	
					
					<div class="post-right">
						<h2 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( __( "Permanent Link to %s", 'THEME_FRONT_SITE' ), esc_attr( get_the_title() ) ); ?>"><?php the_title(); ?></a></h2>
						<?php
						if( has_post_thumbnail()){ 
						?>
						<!-- .postimg -->
						<div class="postimg">
							<figure>
							<?php
								$post_thumbnail_id = get_post_thumbnail_id($post->ID);
								echo atp_resize($post->ID,'','580','300','imageborder','');?>
							
							</figure>
						</div>
						<!-- .postimg -->
						<?php } ?>
						<div class="post-entry">
						<?php 
							global $more; $more = 0;
							the_excerpt();
							?>
						<a href="<?php the_permalink() ?>" class="more-link"><?php echo _e('Continue Reading','THEME_FRONT_SITE');?></a>
						</div>
						<!-- /post-entry -->
					</div>
					
				
					</div>
					<div class="divider"></div>
					<!-- /post-<?php the_ID();?> -->

					<?php 
					endwhile; 
					?>
						
					<?php
					if(function_exists('atp_pagination')){ 
						atp_pagination(); 
					}?>

					<?php else : ?>
					<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'THEME_FRONT_SITE' ); ?></p>
					<?php get_search_form(); ?>
					<?php endif;?>
				</div>
				<!-- /entrycontent -->
				
			</div>
			<!-- /main -->
			
			<?php get_sidebar(); ?>
			<!-- /sidebar -->
			<div class="clear"></div>
		</div>
		<!-- /inner -->
	</div>
	<!-- /pagemid -->
<?php get_footer(); ?>