<?php get_header(); ?>
<script type="text/javascript">
jQuery(window).load(function() {
	// The slider being synced must be initialized first for the Thumbnails below the Large image
	jQuery('#carousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 210,
		itemMargin: 5,
		asNavFor: '#slider'
	});
	// The slider large image
	jQuery('#slider').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel"
	});
});
</script>
<div class="pagemid">
	<?php echo atp_generator( 'breadcrumb', $post->ID ); ?>	

	<div class="inner">

		<div id="main">
			<div class="entry-content">

				<div id="port-nav-below" class="navigation">
					<div class="nav-previous"><h3><?php previous_post_link('&lsaquo; %link') ?></h3></div>
					<div class="nav-next"><h3><?php next_post_link('%link &rsaquo;') ?></h3></div>
				</div>						
				<div class="divider_line"></div>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div <?php post_class();?> id="post-<?php the_ID(); ?>">
			
				<?php 

				$post_type = get_post_meta($post->ID, 'port_posttype_option', true);
				$content_width= get_post_meta($post->ID, 'content_width', true);
				$project_date = get_post_meta(get_the_id(),"project_date",true);
				
				echo $visite_link = get_post_meta(get_the_id(), 'visit_link',true); 
				if( $content_width != "fullwidth" ){ $width='600'; $layout_option="half"; }else{ $width='920'; $layout_option="full" ; }
				if(isset($project_date) && $project_date !='') {
					$post_date = $project_date;
				}else{
					$post_date = get_the_date('M / j / Y', get_the_id());
				}
				?>
				<div class="layout-<?php echo $layout_option;?>">
				<?php
				if(get_option('atp_portfoliofeaturedimg')  != "on" ) { 
					echo '<div class="postimg">';
						if( $post_type == 'gallery' ) {
							$images = get_children(array(
													'post_parent'    => $post->ID,
													'post_type'      => 'attachment',
													'order'          => 'DESC',
													'numberposts'    => 0,
													'post_mime_type' => 'image')
						);
						echo atp_generator('getPortfolioAttachments',$post->ID,'','',$width,'400','','slider');
					
						if(count($images) > 1){
						echo atp_generator('getPortfolioAttachments',$post->ID,'','','210','100','','carousel');
						}
					}elseif( $post_type == 'posttype_image' ){
							echo atp_resize($post->ID,'',$width,'','imageborder','');
					}elseif( $post_type == 'video' )	{
							$embed = get_post_meta($post->ID, 'video_embed', true);
							if( !empty($embed) ) {
								echo "<div class='video-frame'>";
								echo stripslashes(htmlspecialchars_decode($embed));
								echo "</div>";
							} 
					}
					
					echo '</div>';	
				}?>

				<div class="portfoliopage">

					<div class="two_third">
						<?php the_content();?>
					</div>

					<div class="one_third last">
						<ul class="details">
						<?php if(isset($project_date) && $project_date !='') { ?>
							<li>
								<h5><?php echo $projectdatetxt; ?></h5>
								<span><?php echo $post_date; ?></span>
							</li>
						<?php } ?>
						<li>
							<h5><?php echo $categoriestxt; ?></h5>
							<span><?php echo get_the_term_list( $post->ID, 'portfolio_cat', '', '<br />'); ?></span>
						</li>
						<?php if($visite_link !='') { ?>
						<li>
							<h5><?php echo $projecturltxt; ?></h5>
							<?php if($visite_link) { ?>
							<p><span><a class="visitsite" href="<?php echo $visite_link; ?>"><span><?php echo $visitsitetxt;?></span></a></span></p>
							<?php } ?>
							</li>
						<?php } ?>
						<?php if(has_tag('',$post->ID)) { ?>
						<li>
							<h5><?php echo $skillstxt; ?></h5>
							<span><?php the_tags('', '<br />', '<br />'); ?></span>
						</li>
						<?php } ?>
						</ul>
					</div>

				</div>
				<div class="clear"></div>
	
				</div>

				<?php endwhile; ?>


	
					<?php if( get_option('atp_portfolio_relatedpost') != "on" ) {
						echo atp_generator('relatedposts',get_the_id()); 
					} 
					?>
	
				</div>

				<!-- start:post pagination / metadata -->
				<?php edit_post_link(__('Edit', 'THEME_FRONT_SITE'), '<span class="edit-link">', '</span>'); ?>

						<div class="clear"></div>
						<?php
						if(get_option('atp_portfoliocomments')!='on') { 
							comments_template('', true); 
						}?>
						
						<?php else: ?>
						<?php '<p>'.__('Sorry, no projects matched your criteria.', 'THEME_FRONT_SITE').'</p>';?>
						<?php endif; ?>
					</div>
					<!-- /entry-content -->
				</div>
				<!-- /main -->

				<?php if(atp_generator('sidebaroption',$post->ID) != "fullwidth"){ get_sidebar(); } ?>

				<div class="clear"></div>
		</div>
		<!-- /inner -->
	</div>
	<!-- /pagemid -->
	<?php get_footer(); ?>