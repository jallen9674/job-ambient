<?php
/*
Template Name: Site Map
*/
?>
<?php get_header(); ?>
<div class="pagemid">
	<div class="inner">

			<div id="main">

				<div class="entry-content">
					
					<?php if (have_posts()): while (have_posts()): the_post(); ?>
						
						<?php the_content(); ?> 

					<?php endwhile; endif; ?>

					<div class="one_fourth">
						<h3><?php _e('Pages', 'THEME_FRONT_SITE'); ?></h3>
						<ul class="sitemap"><?php $args = array('title_li' => '', 'depth' => 0); wp_list_pages($args); ?></ul>
					</div>
		
					<div class="one_fourth">
						<h3><?php _e('Feeds', 'THEME_FRONT_SITE'); ?></h3>
						<ul class="sitemap">
							<li><a title="<?php _e('Main RSS', 'THEME_FRONT_SITE'); ?>" href="<?php bloginfo('rss2_url'); ?>"><?php _e('Main RSS', 'THEME_FRONT_SITE'); ?></a></li>
							<li><a title="<?php _e('Comment Feed', 'THEME_FRONT_SITE'); ?>" href="<?php bloginfo('comments_rss2_url'); ?>"><?php _e('Comment Feed', 'THEME_FRONT_SITE'); ?></a></li>
						</ul>
					</div>
		
					<div class="one_fourth">
						<h3><?php _e('Categories', 'THEME_FRONT_SITE'); ?></h3>
						<ul class="sitemap"><?php $args = array('title_li' => ''); wp_list_categories($args); ?></ul>
					</div>
		
					<div class="one_fourth last">
						<h3><?php _e('Archives', 'THEME_FRONT_SITE'); ?></h3>
						<ul class="sitemap">
							<?php wp_get_archives('type=monthly&show_post_count=true'); ?>
						</ul>
					</div>
				</div>
				<!-- .content -->
		
			</div>
			<!-- main -->

			<?php if( atp_generator( 'sidebaroption',$post->ID ) != "fullwidth" ) { get_sidebar(); } ?>
			<!-- #sidebar -->

			<div class="clear"></div>

		</div>
		<!-- .inner -->
	</div>
	<!-- .pagemid -->
	
	<?php get_footer(); ?>