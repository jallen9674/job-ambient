<?php get_header(); ?>

	<div class="pagemid">
	<?php echo atp_generator( 'breadcrumb', $post->ID ); ?>	
		<div class="inner">	
	
			<div id="main">
				<div class="entry-content">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div id="post-<?php the_ID(); ?>" <?php post_class('post');?>>		
					<?php echo atp_generator('postmetaStyle'); ?>						
					<!-- /postmetadata -->	
						
					<div class="post-right">
						<h2 class="entry-title"><?php the_title(); ?></h2>
						<?php if( has_post_thumbnail()){ 
							if( atp_generator( 'sidebaroption',$post->ID ) != "fullwidth" ){ $width='540'; }else{ $width='800';
						}
						?>
						<?php if(get_option('atp_blogfeaturedimg')  != "on" ) { ?>
						
						<!-- .postimg -->
						<div class="postimg">
							<figure>
								<a href="#"><?php
								$post_thumbnail_id = get_post_thumbnail_id();
								echo atp_resize($post->ID,'',$width,'300','imageborder','');?>
								</a>
							</figure>
						</div>
						<!-- .postimg -->
						<?php } ?>
						<?php } ?>
						<div class="post-entry">
						<?php 
							global $more; $more = 0;
							the_content();


							the_tags('<div class="tagcloud">',' ','</div>');
							?>

						</div>
						<!-- /post-entry -->
					</div>
					<!-- #post-<?php the_ID(); ?> -->

					<?php  
					if(get_option('atp_aboutauthor') != "on") {
					echo '<div class="divider_line"></div>';
						echo atp_generator('aboutauthor'); 
					} ?>	

					<?php if(get_option('atp_singlenavigation') != "on" ) { ?>
					<div id="nav-below" class="navigation">
						<div class="nav-previous"><?php previous_post_link('&larr; %link') ?></div>
						<div class="nav-next"><?php next_post_link('%link &rarr;') ?></div>
					</div>
					<!-- #nav-below-->
					<?php } ?>

					
				


					<?php endwhile; else: ?>
					<?php '<p>'.__('Sorry, no posts matched your criteria.', 'THEME_FRONT_SITE').'</p>';?>
					<?php endif; ?>
					<?php
					if(get_option('atp_relatedposts') != "on") {
						$args = '';
						echo atp_generator('relatedposts',$post->ID,$args);
					} ?>
						<?php edit_post_link( __( 'Edit', 'THEME_FRONT_SITE' ), '<span class="edit-link">', '</span>' ); ?>
					<?php
					if(get_option('atp_commentstemplate') == "posts" ||  get_option('atp_commentstemplate') == "both") {
						comments_template('', true); 
					}
					?>
					</div>
				</div>
				<!-- /entry-content -->
			</div>	
			<!-- /main -->

			<?php if(atp_generator('sidebaroption',$post->ID) != "fullwidth"){ get_sidebar(); } ?>
			
		</div>
		<!-- /inner -->
	</div>
	<!-- /pagemid -->

	<?php get_footer(); ?>