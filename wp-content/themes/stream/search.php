<?php get_header(); ?>
	<div class="pagemid">
		<div class="inner">	
		
			<div id="main">			
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div <?php post_class('searchresults');?> id="post-<?php the_ID(); ?>">
					<h2 class="entry-title">
						<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
					</h2>
					<?php global $more; $more = 0; 
						the_excerpt('');
					?>					

					<a href="<?php the_permalink() ?>" class="more-link"><?php echo _e('Read more','THEME_FRONT_SITE');?></a>
					
				</div>
				<!-- /post-<?php the_ID();?> -->
				<div class="divider_line"></div>
				<?php endwhile; ?>

				<?php
				if(function_exists('atp_pagination')) { 
					atp_pagination(); 
				}?>
				
				<?php else : ?>
				<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'THEME_FRONT_SITE' ); ?></p>
				<?php get_search_form(); ?>
				<?php endif; ?>
			
			</div>


			<?php get_sidebar();?>
			<!-- /pagemid -->
			<div class="clear"></div>
			
		</div>
		<!-- /inner -->
	</div>
	<!-- /pagemid -->
<?php get_footer(); ?>