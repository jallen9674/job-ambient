<?php

// S H O R T C O D E S   M E T A
//--------------------------------------------------------
global $atp_ribbons,$staff_social;
$atp_shortcodes = array(

	//LAYOUTS
	//--------------------------------------------------------
	"Layouts" => array(
		'name' => __('Layouts', 'ATP_ADMIN_SITE'),
		'value' => 'Layouts',
		'subtype' => true,
		'options' => array(
			// LAYOUT (1/2 - 1/2)
			//--------------------------------------------------------
			array(
				'name' => __('Two Column Layout', 'ATP_ADMIN_SITE'),
				'value' => 'one_half_layout',
				'options' => array(
					array(
						'name' => __('One Half', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'layout_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Half Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'layout_2',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (1/3 -1/3)
			//--------------------------------------------------------
			array(
				'name' => __('Three Column Layout', 'ATP_ADMIN_SITE'),
				'value' => 'one_third_layout',
				'options' => array(
					array(
						'name' => __('One Third', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'one_third_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Third', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'one_third_2',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Third Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your third Column', 'ATP_ADMIN_SITE'),
						'id' => 'one_third_3',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (1/4 -1/4 - 1/4 - 1/4)
			//--------------------------------------------------------
			array(
				'name' => __('Four Column Layout', 'ATP_ADMIN_SITE'),
				'value' => 'one_fourth_layout',
				'options' => array(
					array(
						'name' => __('One Fourth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'one_fourth_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fourth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'one_fourth_2',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fourth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your third Column', 'ATP_ADMIN_SITE'),
						'id' => 'one_fourth_3',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fourth Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your fourth Column', 'ATP_ADMIN_SITE'),
						'id' => 'one_fourth_4',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (1/5 - 1/5 - 1/5 - 1/5 - 1/5 - 1/5)
			//--------------------------------------------------------
			array(
				'name' => __('Five Column Layout', 'ATP_ADMIN_SITE'),
				'value' => 'one5thlayout',
				'options' => array(
					array(
						'name' => __('One Fifth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'one5thlayout1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fifth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'one5thlayout2',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fifth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your third Column', 'ATP_ADMIN_SITE'),
						'id' => 'one5thlayout3',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fifth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your fourth Column', 'ATP_ADMIN_SITE'),
						'id' => 'one5thlayout4',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fifth Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your fifth Column', 'ATP_ADMIN_SITE'),
						'id' => 'one5thlayout5',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (1/6 - 1/6 - 1/6 - 1/6 - 1/6 - 1/6)
			//--------------------------------------------------------
			array(
				'name' => __('Six Column Layout', 'ATP_ADMIN_SITE'),
				'value' => 'one6thlayout',
				'options' => array(
					array(
						'name' => __('One Sixth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'one6thlayout1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Sixth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'one6thlayout2',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Sixth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your third Column', 'ATP_ADMIN_SITE'),
						'id' => 'one6thlayout3',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Sixth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your fourth Column', 'ATP_ADMIN_SITE'),
						'id' => 'one6thlayout4',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Sixth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your fifth Column', 'ATP_ADMIN_SITE'),
						'id' => 'one6thlayout5',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Sixth Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your sixth Column', 'ATP_ADMIN_SITE'),
						'id' => 'one6thlayout6',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (1/3 -2/3)
			//--------------------------------------------------------
			array(
				'name' => __('One Third - Two Third', 'ATP_ADMIN_SITE'),
				'value' => 'one_3rd_2rd',
				'options' => array(
					array(
						'name' => __('One Third', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'one3rd2rd_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Two Third Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'one3rd2rd_2',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (2/3 -1/3)
			//--------------------------------------------------------
			array(
				'name' => __('Two Third - One Third', 'ATP_ADMIN_SITE'),
				'value' => 'two_3rd_1rd',
				'options' => array(
					array(
						'name' => __('Two Third', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'two3rd1rd_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Third Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'one3rd2rd_2',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (1/4 -3/4)
			//--------------------------------------------------------
			array(
				'name' => __('One Fourth - Three Fourth', 'ATP_ADMIN_SITE'),
				'value' => 'One_4th_Three_4th',
				'options' => array(
					array(
						'name' => __('One Fourth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'One4thThree4th_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Three Fourth Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'One4thThree4th_2',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (3/4 -1/4)
			//--------------------------------------------------------
			array(
				'name' => __('Three Fourth - One Fourth', 'ATP_ADMIN_SITE'),
				'value' => 'Three_4th_One_4th',
				'options' => array(
					array(
						'name' => __('Three Fourth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'Three4thOne4th_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fourth Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'Three4thOne4th_2',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (1/4 - 1/4 - 1/2)
			//--------------------------------------------------------
			array(
				'name' => __('One Fourth - One Fourth - One Half', 'ATP_ADMIN_SITE'),
				'value' => 'One_4th_One_4th_One_half',
				'options' => array(
					array(
						'name' => __('One Fourth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'One4thOne4thOnehalf_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fourth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'One4thOne4thOnehalf_2',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Half Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your third Column', 'ATP_ADMIN_SITE'),
						'id' => 'One4thOne4thOnehalf_3',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (1/2 - 1/4 - 1/4)
			//--------------------------------------------------------
			array(
				'name' => __('One Half - One Fourth - One Fourth', 'ATP_ADMIN_SITE'),
				'value' => 'One_half_One_4th_One_4th',
				'options' => array(
					array(
						'name' => __('One Half', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'OnehalfOne4thOne4th_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fourth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'OnehalfOne4thOne4th_2',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fourth Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your third Column', 'ATP_ADMIN_SITE'),
						'id' => 'OnehalfOne4thOne4th_3',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (1/4 - 1/2 - 1/4)
			//--------------------------------------------------------
			array(
				'name' => __('One Fourth - One Half - One Fourth', 'ATP_ADMIN_SITE'),
				'value' => 'One_4th_One_half_One_4th',
				'options' => array(
					array(
						'name' => __('One Fourth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'One4thOnehalfOne4th_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Half', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'One4thOnehalfOne4th_2',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fourth Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your third Column', 'ATP_ADMIN_SITE'),
						'id' => 'One4thOnehalfOne4th_3',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (1/5 - 4/5)
			//--------------------------------------------------------
			array(
				'name' => __('One Fifth - Four Fifth', 'ATP_ADMIN_SITE'),
				'value' => 'One_5th_Four_5th',
				'options' => array(
					array(
						'name' => __('One Fifth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'One5thFour5th_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Four Fifth Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'One5thFour5th_2',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (4/5 - 1/5)
			//--------------------------------------------------------
			array(
				'name' => __('Four Fifth - One Fifth', 'ATP_ADMIN_SITE'),
				'value' => 'Four_5th_One_5th',
				'options' => array(
					array(
						'name' => __('Four Fifth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'Four5thOne5th_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('One Fifth Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'Four5thOne5th_2',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (2/5 - 3/5)
			//--------------------------------------------------------
			array(
				'name' => __('Two Fifth - Three Fifth', 'ATP_ADMIN_SITE'),
				'value' => 'Two_5th_Three_5th',
				'options' => array(
					array(
						'name' => __('Two Fifth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'Two5thThree5th_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Three Fifth Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'Two5thThree5th_2',
						'std' => '',
						'type' => 'textarea'
					)
				)
			),
			// LAYOUT (3/5 -2/5)
			//--------------------------------------------------------
			array(
				'name' => __('Three Fifth - Two Fifth', 'ATP_ADMIN_SITE'),
				'value' => 'Three_5th_Two_5th',
				'options' => array(
					array(
						'name' => __('Three Fifth', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your first Column', 'ATP_ADMIN_SITE'),
						'id' => 'Three5thTwo5th_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Two Fifth Last', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter Content for your second Column', 'ATP_ADMIN_SITE'),
						'id' => 'Three5thTwo5th_2',
						'std' => '',
						'type' => 'textarea'
					)
				)
			)
		)
	),
	
		// Accordion 
	//--------------------------------------------------------
	"Accordion" =>  array(
		'name' => __('Accordion', 'ATP_ADMIN_SITE'),
		'value' => 'accordion',
		'subtype' => true,
		'options' => array(
			array(
				'name' => __('2 Accordion', 'ATP_ADMIN_SITE'),
				'value' => '2',
				'options' => array(
					array(
						'name' => __(' Accordion 1 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for Tab 1', 'ATP_ADMIN_SITE'),
						'id' => 'title_1',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __(' Accordion 1 Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for Tab 1', 'ATP_ADMIN_SITE'),
						'id' => 'text_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Accordion 2 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for  Accordion 2', 'ATP_ADMIN_SITE'),
						'id' => 'title_2',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __(' Accordion 2 Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for  Accordion 2', 'ATP_ADMIN_SITE'),
						'id' => 'text_2',
						'std' => '',
						'type' => 'textarea'
					),
					//------------------------- S E P A R A T O R
					array(
						'name' => __('Separator', 'ATP_ADMIN_SITE'),
						'type' => 'separator'
					)
				)
			),
			// 3 Accordion
			array(
				'name' => __('3 Accordion', 'ATP_ADMIN_SITE'),
				'value' => '3',
				'options' => array(
					array(
						'name' => __(' Accordion 1 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for  Accordion 1', 'ATP_ADMIN_SITE'),
						'id' => 'title_1',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Accordion 1 Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for  Accordion 1', 'ATP_ADMIN_SITE'),
						'id' => 'text_1',
						'std' => '',
						'type' => 'textarea'
					),
					//------------------------- S E P A R A T O R
					array(
						'name' => __('Separator', 'ATP_ADMIN_SITE'),
						'type' => 'separator'
					),
					array(
						'name' => __('Accordion 2 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for  Accordion 2', 'ATP_ADMIN_SITE'),
						'id' => 'title_2',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Accordion 2 Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for  Accordion 2', 'ATP_ADMIN_SITE'),
						'id' => 'text_2',
						'std' => '',
						'type' => 'textarea'
					),
					//------------------------- S E P A R A T O R
					array(
						'name' => __('Separator', 'ATP_ADMIN_SITE'),
						'type' => 'separator'
					),
					array(
						'name' => __('Accordion 3 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for  Accordion 3', 'ATP_ADMIN_SITE'),
						'id' => 'title_3',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Accordion 3  Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for  Accordion 3', 'ATP_ADMIN_SITE'),
						'id' => 'text_3',
						'std' => '',
						'type' => 'textarea'
					)
				)
			)
		)
	),
	// E N D  - Accordion
	// B L O C K Q U O T E 
	//--------------------------------------------------------
	"Blockquotes" => array(
		'name' => __('Block Quotes', 'ATP_ADMIN_SITE'),
		'value' => 'blockquote',
		'options' => array(
			array(
				'name' => __('Content', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the text you wish to display as a blockquote.', 'ATP_ADMIN_SITE'),
				'id' => 'content',
				'std' => '',
				'type' => 'textarea'
			),
			array(
				'name' => __('Align', 'ATP_ADMIN_SITE'),
				'desc' => __('Select the alignment for your blockquote.', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'align',
				'std' => '',
				'options' => array(
					'' => __('Choose one...', 'ATP_ADMIN_SITE'),
					'left' => __('Left', 'ATP_ADMIN_SITE'),
					'right' => __('Right', 'ATP_ADMIN_SITE'),
					'center' => __('Center', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			),
			array(
				'name' => __('Cite', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the name of the author which displays at the end of the blockquote.', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'cite',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Cite Link', 'ATP_ADMIN_SITE'),
				'desc' => __('The link displays after the Citation.', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'citelink',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Width', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the width in % or px if you wish to use the blockquote in a specific width.', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'width',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			)
		)
	),
	// D R O P C A P  1 
	//--------------------------------------------------------
	"Dropcap 1" => array(
		'name' => __('Drop Cap 1', 'ATP_ADMIN_SITE'),
		'value' => 'dropcap1',
		'options' => array(
			array(
				'name' => __('Dropcap Text', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the letter you want to display as Dropcap', 'ATP_ADMIN_SITE'),
				'id' => 'dropcap_text',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Dropcap Colors', 'ATP_ADMIN_SITE'),
				'desc' => __('Use Predefined Color for the Dropcap Background', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'color',
				'std' => '',
				'options' => array(
					'black' => 'Black',
					'blue' => 'Blue',
					'cyan' => 'Cyan',
					'green' => 'Green',
					'magenta' => 'Magenta',
					'navy' => 'Navy',
					'orange' => 'Orange',
					'pink' => 'Pink',
					'red' => 'Red',
					'yellow' => 'Yellow'
				),
				'type' => 'select'
			)
		)
	),
	// D R O P C A P   2 
	//--------------------------------------------------------
	"Dropcap 2" => array(
		'name' => __('Drop Cap 2', 'ATP_ADMIN_SITE'),
		'value' => 'dropcap2',
		'options' => array(
			array(
				'name' => __('Dropcap Text', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the letter you want to display as Dropcap', 'ATP_ADMIN_SITE'),
				'id' => 'dropcap_text',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Dropcap Background Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Use Colorpicker to choose your desired color for the Dropcap Background', 'ATP_ADMIN_SITE'),
				'id' => 'bgcolor',
				'std' => '',
				'type' => 'color'
			)
		)
	),
	// D R O P C A P   3 
	//--------------------------------------------------------
	"Dropcap 3" => array(
		'name' => __('Drop Cap 3', 'ATP_ADMIN_SITE'),
		'value' => 'dropcap3',
		'options' => array(
			array(
				'name' => __('Dropcap Text', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the letter you want to display as Dropcap', 'ATP_ADMIN_SITE'),
				'id' => 'dropcap_text',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Dropcap Text Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Use Colorpicker to choose your desired color for the Dropcap Text', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'color',
				'std' => '',
				'type' => 'color'
			)
		)
	),
	// H I G H L I G H T
	//--------------------------------------------------------
	"Highlight" => array(
		'name' => __('Highlight', 'ATP_ADMIN_SITE'),
		'value' => 'highlight',
		'options' => array(
			array(
				'name' => __('Highlight BG Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose the color you want to display for the highlight background.', 'ATP_ADMIN_SITE'),
				'id' => 'bgcolor',
				'std' => '',
				'type' => 'color'
			),
			array(
				'name' => __('Highlight Text Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose the color you want to display for the text.', 'ATP_ADMIN_SITE'),
				'id' => 'textcolor',
				'std' => '',
				'type' => 'color'
			),
			array(
				'name' => __('Highlight Text', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the text you wish to display as highlight.', 'ATP_ADMIN_SITE'),
				'id' => 'text',
				'std' => '',
				'type' => 'textarea'
			)
		)
	),
	// F A N C Y   H E A D I N G 
	//--------------------------------------------------------
	"Fancy Headings" => array(
		'name' => __('Fancy Heading', 'ATP_ADMIN_SITE'),
		'value' => 'fancyheading',
		'options' => array(
			array(
				'name' => __('Fancy Heading BG Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose the background color you want to display for fancy heading.', 'ATP_ADMIN_SITE'),
				'id' => 'bgcolor',
				'std' => '',
				'type' => 'color'
			),
			array(
				'name' => __('Fancy Heading Text Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose the text color you want to display for Fancy Heading.', 'ATP_ADMIN_SITE'),
				'id' => 'textcolor',
				'std' => '',
				'type' => 'color'
			),
			array(
				'name' => __('Fancy Heading Text', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the text you wish to use for Fancy Heading.', 'ATP_ADMIN_SITE'),
				'id' => 'text',
				'std' => '',
				'type' => 'textarea'
			)
		)
	),
	//	S T Y L E D   L I S T S 
	//--------------------------------------------------------
	"Lists" => array(
		'name' => __('List', 'ATP_ADMIN_SITE'),
		'value' => 'styledlist',
		'options' => array(
			array(
				'name' => __('List Style', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose the list style you want to have.', 'ATP_ADMIN_SITE'),
				'id' => 'style',
				'std' => '',
				'options' => array(
					'disc' => 'disc',
					'circle' => 'circle',
					'square' => 'square',
					'arrow1' => 'arrow1',
					'arrow2' => 'arrow2',
					'arrow3' => 'arrow3',
					'arrow4' => 'arrow4',
					'arrow5' => 'arrow5',
					'bullet1' => 'bullet1',
					'bullet2' => 'bullet2',
					'bullet3' => 'bullet3',
					'bullet4' => 'bullet4',
					'bullet5' => 'bullet5',
					'star1' => 'star1',
					'star2' => 'star2',
					'star3' => 'star3',
					'plus' => 'plus',
					'minus' => 'minus',
					'pointer' => 'pointer',
					'style1' => 'style1',
					'check' => 'check'
				),
				'type' => 'select'
			),
			array(
				'name' => __('Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Select the color variation', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'color',
				'std' => '',
				'options' => array(
					'black' => 'Black',
					'blue' => 'Blue',
					'cyan' => 'Cyan',
					'green' => 'Green',
					'magenta' => 'Magenta',
					'navy' => 'Navy',
					'orange' => 'Orange',
					'pink' => 'Pink',
					'red' => 'Red',
					'yellow' => 'Yellow'
				),
				'type' => 'select'
			),
			array(
				'name' => __('Content', 'ATP_ADMIN_SITE'),
				'desc' => __('For List Items use HTML Elements &lt;ul&gt;&lt;li&gt;List Item&lt;/li>&lt;/ul>', 'ATP_ADMIN_SITE'),
				'id' => 'content',
				'std' => '',
				'type' => 'textarea'
			)
		)
	),
	// I C O N S 
	//--------------------------------------------------------
	"Icon Text" => array(
		'name' => __('Icon Text', 'ATP_ADMIN_SITE'),
		'value' => 'icon',
		'options' => array(
			array(
				'name' => __('Icon Style', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose the list style you want to have.', 'ATP_ADMIN_SITE'),
				'id' => 'style',
				'std' => '',
				'options' => array(
					'male' => 'Male',
					'female' => 'Female',
					'zip' => 'Zip',
					'movie' => 'Movie',
					'addbook' => 'Address Book',
					'calc' => 'Calculator',
					'dollar' => 'Dollar',
					'pound' => 'Pound',
					'euro' => 'Euro',
					'yen' => 'Yen',
					'error' => 'Error',
					'exclamation' => 'Exclamation',
					'feed' => 'Feed',
					'help' => 'Help',
					'email' => 'Email',
					'medal' => 'Medal',
					'word' => 'Word',
					'pdf' => 'PDF',
					'phone' => 'Phone',
					'support' => 'Support',
					'vcard' => 'Vcard',
					'disk' => 'Disk',
					'monitor' => 'Monitor',
					'pin' => 'Pin',
					'location' => 'location',
					'find' => 'Find'
				),
				'type' => 'select'
			),
			array(
				'name' => __('Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Select the color variation', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'color',
				'std' => '',
				'options' => array(
					'black' => 'Black',
					'blue' => 'Blue',
					'cyan' => 'Cyan',
					'green' => 'Green',
					'magenta' => 'Magenta',
					'gray' => 'Gray',
					'orange' => 'Orange',
					'pink' => 'Pink',
					'red' => 'Red',
					'yellow' => 'Yellow'
				),
				'type' => 'select'
			),
			array(
				'name' => __('Icon Text', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the text you want to display beside the Icon', 'ATP_ADMIN_SITE'),
				'id' => 'text',
				'std' => '',
				'type' => 'textarea'
			)
		)
	),
	// I C O N S   W I TH   L I N K S
	//--------------------------------------------------------
	"Icons Links" => array(
		'name' => __('Icon Links', 'ATP_ADMIN_SITE'),
		'value' => 'iconlinks',
		'options' => array(
			array(
				'name' => __('Icon Style', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose the list style you want to have.', 'ATP_ADMIN_SITE'),
				'id' => 'style',
				'std' => '',
				'options' => array(
					'male' => 'Male',
					'female' => 'Female',
					'zip' => 'Zip',
					'movie' => 'Movie',
					'addbook' => 'Address Book',
					'calc' => 'Calc',
					'dollar' => 'Dollar',
					'pound' => 'Pound',
					'euro' => 'Euro',
					'yen' => 'Yen',
					'error' => 'Error',
					'exclamation' => 'Exclamation',
					'feed' => 'Feed',
					'help' => 'Help',
					'email' => 'Email',
					'medal' => 'Medal',
					'word' => 'Word',
					'pdf' => 'PDF',
					'phone' => 'Phone',
					'support' => 'Support',
					'vcard' => 'Vcard',
					'disk' => 'Disk',
					'monitor' => 'Monitor',
					'pin' => 'Pin',
					'location' => 'location',
					'find' => 'Find'
				),
				'type' => 'select'
			),
			array(
				'name' => __('Link URL', 'ATP_ADMIN_SITE'),
				'id' => 'href',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Link Target', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose option when reader clicks on the link.', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'target',
				'std' => '',
				'options' => array(
					'' => __('Choose one...', 'ATP_ADMIN_SITE'),
					'_blank' => __('Open the linked document in a new window or tab', 'ATP_ADMIN_SITE'),
					'_self' => __('Open the linked document in the same frame as it was clicked.', 'ATP_ADMIN_SITE'),
					'_parent' => __('Open the linked document in the parent frameset', 'ATP_ADMIN_SITE'),
					'_top' => __('Open the linked document in the full body of the window', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			),
			array(
				'name' => __('Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Select the color variation', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'color',
				'std' => '',
				'options' => array(
					'black' => 'Black',
					'blue' => 'Blue',
					'cyan' => 'Cyan',
					'green' => 'Green',
					'magenta' => 'Magenta',
					'navy' => 'Navy',
					'orange' => 'Orange',
					'pink' => 'Pink',
					'red' => 'Red',
					'yellow' => 'Yellow'
				),
				'type' => 'select'
			),
			array(
				'name' => __('Icon Text', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the text you want to display beside the Icon', 'ATP_ADMIN_SITE'),
				'id' => 'text',
				'std' => '',
				'type' => 'textarea'
			)
		)
	),
	// BUTTON
	//--------------------------------------------------------
	"Button" => array(
		'name' => __('Button', 'ATP_ADMIN_SITE'),
		'value' => 'Button',
		'options' => array(
			array(
				'name' => __('ID', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'id',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Class', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'subclass',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Link URL', 'ATP_ADMIN_SITE'),
				'id' => 'link',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Link Target ', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose option when reader clicks on the link.', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'linktarget',
				'std' => '',
				'options' => array(
					'' => __('Choose one...', 'ATP_ADMIN_SITE'),
					'_blank' => __('Open the linked document in a new window or tab', 'ATP_ADMIN_SITE'),
					'_self' => __('Open the linked document in the same frame as it was clicked.', 'ATP_ADMIN_SITE'),
					'_parent' => __('Open the linked document in the parent frameset', 'ATP_ADMIN_SITE'),
					'_top' => __('Open the linked document in the full body of the window', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			),
			array(
				'name' => __('Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Select the color variation', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'color',
				'std' => '',
				'options' => array(
					'' => __('Choose one...', 'ATP_ADMIN_SITE'),
					'gray' => 'Gray',
					'brown' => 'Brown',
					'cyan' => 'Cyan',
					'orange' => 'Orange',
					'red' => 'Red',
					'magenta' => 'Magenta',
					'yellow' => 'Yellow',
					'blue' => 'Blue',
					'pink' => 'Pink',
					'green' => 'Green',
					'black' => 'Black',
					'white' => 'White'
				),
				'type' => 'select'
			),
			array(
				'name' => __('Align', 'ATP_ADMIN_SITE'),
				'desc' => __('Select the alignment for a button', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'align',
				'std' => '',
				'options' => array(
					'' => __('Choose one...', 'ATP_ADMIN_SITE'),
					'left' => __('Left', 'ATP_ADMIN_SITE'),
					'right' => __('Right', 'ATP_ADMIN_SITE'),
					'center' => __('Center', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			),
			array(
				'name' => __('BG Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Button background color default state', 'ATP_ADMIN_SITE'),
				'id' => 'bgcolor',
				'std' => '',
				'type' => 'color'
			),
			array(
				'name' => __('Hover BG Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Button background color on hover state', 'ATP_ADMIN_SITE'),
				'id' => 'hoverbgcolor',
				'std' => '',
				'type' => 'color'
			),
			array(
				'name' => __('Hover Text Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Button Text color on hover state', 'ATP_ADMIN_SITE'),
				'id' => 'hovertextcolor',
				'std' => '',
				'type' => 'color'
			),
			array(
				'name' => __('Text Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Button Text color default state', 'ATP_ADMIN_SITE'),
				'id' => 'textcolor',
				'std' => '',
				'type' => 'color'
			),
			array(
				'name' => __('Button Size', 'ATP_ADMIN_SITE'),
				'desc' => __('Select the button size', 'ATP_ADMIN_SITE'),
				'id' => 'size',
				'std' => '',
				'type' => 'select',
				'options' => array(
					'' => __('Choose one...', 'ATP_ADMIN_SITE'),
					'small' => 'Small',
					'medium' => 'Medium',
					'large' => 'Large'
				)
			),
			array(
				'name' => __('Full Width', 'ATP_ADMIN_SITE'),
				'desc' => __('Check this if you wish to display button in full width and uncheck if you wish to use specific width below.', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'fullwidth',
				'std' => '',
				'type' => 'checkbox'
			),
			array(
				'name' => __('Button Width', 'ATP_ADMIN_SITE'),
				'desc' => __('Use px as units for width, do not leave only integers.', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'width',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Button Text', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the text you wish to display for button', 'ATP_ADMIN_SITE'),
				'id' => 'text',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			)
		)
	),
	// D I V I D E R S
	//--------------------------------------------------------
	"Divider" => array(
		'name' => __('Divider', 'ATP_ADMIN_SITE'),
		'value' => 'divider',
		'subtype' => true,
		'options' => array(
			array(
				'name' => __('Divider With Top Text', 'ATP_ADMIN_SITE'),
				'value' => 'divider_top',
				'options' => array(
					array(
						'name' => __('Text', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter The Divider Text.', 'ATP_ADMIN_SITE'),
						'id' => 'text',
						'std' => 'TOP',
						'type' => 'text',
						'inputsize' => '30'
					)
				)
			),
			array(
				'name' => __('Clear Both', 'ATP_ADMIN_SITE'),
				'value' => 'clear',
				'options' => array()
			),
			array(
				'name' => __('Divider', 'ATP_ADMIN_SITE'),
				'value' => 'divider',
				'options' => array()
			),
			array(
				'name' => __('Custom Divider', 'ATP_ADMIN_SITE'),
				'value' => 'custom_divider',
				'options' => array(
					array(
						'name' => __('Upload Image', 'ATP_ADMIN_SITE'),
						'desc' => __('Upload Image.', 'ATP_ADMIN_SITE'),
						'id' => 'dividerimg',
						'std' => '',
						'type' => 'upload',
						'inputsize' => '33'
					)
				)
			),
			array(
				'name' => __('Divider With Line', 'ATP_ADMIN_SITE'),
				'value' => 'divider_line',
				'options' => array()
			),
			array(
				'name' => __('Divider With Space', 'ATP_ADMIN_SITE'),
				'value' => 'divider_space',
				'options' => array()
			)
		)
	),
	// T A B L E 
	//--------------------------------------------------------
	"Table" => array(
		'name' => __('Table', 'ATP_ADMIN_SITE'),
		'value' => 'Table',
		'options' => array(
			array(
				'name' => __('Table Width', 'ATP_ADMIN_SITE'),
				'desc' => __('Use % or px as units for width, do not leave only numbers.', 'ATP_ADMIN_SITE'),
				'id' => 'width',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Align', 'ATP_ADMIN_SITE'),
				'desc' => __('Select the alignment for your Table.', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'align',
				'std' => '',
				'options' => array(
					'' => __('Choose one...', 'ATP_ADMIN_SITE'),
					'left' => __('Left', 'ATP_ADMIN_SITE'),
					'right' => __('Right', 'ATP_ADMIN_SITE'),
					'center' => __('Center', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			),
			array(
				'name' => __('Table Content', 'ATP_ADMIN_SITE'),
				'desc' => __('For Table use HTML Elements &lt;table&gt;&lt;tr&gt;&lt;td>content&lt;/td>&lt;/tr>&lt;/table>', 'ATP_ADMIN_SITE'),
				'id' => 'text',
				'std' => '',
				'type' => 'textarea'
			)
		)
	),
	// T O G G L E 
	//--------------------------------------------------------
	"Toogle" => array(
		'name' => __('Toggle', 'ATP_ADMIN_SITE'),
		'value' => 'Toggle',
		'options' => array(
			array(
				'name' => __('Toggle Heading', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the text you want to use as Toggle Heading', 'ATP_ADMIN_SITE'),
				'id' => 'heading',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Toggle Content', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the content you want to display as Toggle Content', 'ATP_ADMIN_SITE'),
				'id' => 'text',
				'std' => '',
				'type' => 'textarea'
			)
		)
	),
	// F A N C Y   T O G G L E 
	//--------------------------------------------------------
	"Fancy Toogle" => array(
		'name' => __('Fancy Toggle', 'ATP_ADMIN_SITE'),
		'value' => 'FancyToggle',
		'options' => array(
			array(
				'name' => __('Fancy Toggle. Heading', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the text you want to use as Fancy Toggle Heading', 'ATP_ADMIN_SITE'),
				'id' => 'heading',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Fancy Toggle Content', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the text you want to use as Fancy Toggle Content', 'ATP_ADMIN_SITE'),
				'id' => 'text',
				'std' => '',
				'type' => 'textarea'
			)
		)
	),
	// B O X E S 
	//--------------------------------------------------------
	"Boxes" => array(
		'name' => __('Boxes', 'ATP_ADMIN_SITE'),
		'value' => 'Boxes',
		'subtype' => true,
		'options' => array(
			// F A N C Y   B O X 
			//--------------------------------------------------------
			array(
				'name' => __('Fancy Box', 'ATP_ADMIN_SITE'),
				'value' => 'fancybox',
				'options' => array(
					array(
						'name' => __('Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type text you wish to display as Title for Fancy Box', 'ATP_ADMIN_SITE'),
						'id' => 'title',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Box Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type content you wish to display for Fancy Box', 'ATP_ADMIN_SITE'),
						'id' => 'text',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Title Color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose the color for title', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'titlecolor',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Title BG Color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose the color for title background', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'titlebgcolor',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Text Color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose the color for content', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'textcolor',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Border Color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose the color for border of the Box', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'bordercolor',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Box BG Color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose the color for Box Background', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'boxbgcolor',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Corner Ribbon', 'ATP_ADMIN_SITE'),
						'desc' => __('Corner Ribbon (Range from 01 - 64) for more details of the names you can check the ribbons bolder within the images folder', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'ribbon',
						'std' => '',
						'type' => 'select',
						'options' => $atp_ribbons
					)
				)
			),
			// M I N I M A L   B O X  
			//--------------------------------------------------------
			array(
				'name' => __('Minimal Box', 'ATP_ADMIN_SITE'),
				'value' => 'minimalbox',
				'options' => array(
					array(
						'name' => __('Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type text you wish to display as Title for Minimal Box', 'ATP_ADMIN_SITE'),
						'id' => 'title',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Title Color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose the color for title', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'titlecolor',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Title BG Color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose the color for title background', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'titlebgcolor',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Corner Ribbon', 'ATP_ADMIN_SITE'),
						'desc' => __('Corner Ribbon (Range from 01 - 64) for more details of the names you can check the ribbons bolder within the images folder', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'ribbon',
						'std' => '',
						'type' => 'select',
						'options' => $atp_ribbons
					),
					array(
						'name' => __('Box Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type content you wish to display for Fancy Box', 'ATP_ADMIN_SITE'),
						'id' => 'text',
						'std' => '',
						'type' => 'textarea'
					)
				)
			)
		)
	),
	// E N D  - BOXES
	
	// M E S S A G E   B O X  
	//--------------------------------------------------------
	"Message Box" => array(
		'name' => __('Message Box', 'ATP_ADMIN_SITE'),
		'value' => 'messagebox',
		'options' => array(
			array(
				'name' => __('Message Type', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose the Message Box Type Error, Notice, Success etc', 'ATP_ADMIN_SITE'),
				'id' => 'msgtype',
				'std' => '',
				'options' => array(
					'error' => __('Error', 'ATP_ADMIN_SITE'),
					'info' => __('Info', 'ATP_ADMIN_SITE'),
					'alert' => __('Alert', 'ATP_ADMIN_SITE'),
					'success' => __('Success', 'ATP_ADMIN_SITE'),
					'download' => __('Download', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			),
			array(
				'name' => __('Message Text', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the content you wish to display for the message box', 'ATP_ADMIN_SITE'),
				'id' => 'text',
				'std' => '',
				'type' => 'textarea'
			)
		)
	),
	// N O T E   B O X  
	//--------------------------------------------------------
	"Note Box" => array(
		'name' => __('Note Box', 'ATP_ADMIN_SITE'),
		'value' => 'notebox',
		'options' => array(
			array(
				'name' => __('Title', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the text you wish to display for the Note Box Heading', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'title',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Align', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'align',
				'std' => '',
				'options' => array(
					'' => __('Choose one...', 'ATP_ADMIN_SITE'),
					'left' => __('Left', 'ATP_ADMIN_SITE'),
					'right' => __('Right', 'ATP_ADMIN_SITE'),
					'center' => __('Center', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			),
			array(
				'name' => __('Width', 'ATP_ADMIN_SITE'),
				'desc' => __('Use % or px as units for width, do not leave only numbers.', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'width',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Message Text', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the content you wish to display for the Note Box', 'ATP_ADMIN_SITE'),
				'id' => 'text',
				'std' => '',
				'type' => 'textarea'
			)
		)
	),
	
	//  P R I C I N G T A B L E
	"Pricing Table" => array(
		'name' => __('Pricing Table', 'ATP_ADMIN_SITE'),
		'value' => 'pricing',
		'subtype' => true,
		'options' => array(
			// column 3
			array(
				'name' => __('3 Column', 'ATP_ADMIN_SITE'),
				'value' => '03',
				'options' => array(
					array(
						'name' => __('Column 1 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 1 Title', 'ATP_ADMIN_SITE'),
						'id' => 'title_1',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 1 Heading', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 1 Heading', 'ATP_ADMIN_SITE'),
						'id' => 'heading_1',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 1 Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for the Column 1', 'ATP_ADMIN_SITE'),
						'id' => 'desc_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Column 1 Heading Bg color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading bg', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'bgcolor_1',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Column 1 Heading color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading text color', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'color_1',
						'std' => '',
						'type' => 'color'
					),
					//------------------------- S E P A R A T O R
					array(
						'name' => __('Separator', 'ATP_ADMIN_SITE'),
						'type' => 'separator'
					),
					array(
						'name' => __('Column 2 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 2 Title', 'ATP_ADMIN_SITE'),
						'id' => 'title_2',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 2 Heading', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 2 Heading', 'ATP_ADMIN_SITE'),
						'id' => 'heading_2',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 2 Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for the Column 2', 'ATP_ADMIN_SITE'),
						'id' => 'desc_2',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Column 2 Heading Bg color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading bg', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'bgcolor_2',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Column 2 Heading color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading text color', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'color_2',
						'std' => '',
						'type' => 'color'
					),
					//------------------------- S E P A R A T O R
					array(
						'name' => __('Separator', 'ATP_ADMIN_SITE'),
						'type' => 'separator'
					),
					array(
						'name' => __('Column 3 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 3 Title', 'ATP_ADMIN_SITE'),
						'id' => 'title_3',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 3 Heading', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 3 Heading', 'ATP_ADMIN_SITE'),
						'id' => 'heading_3',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 3 Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for the Column 3', 'ATP_ADMIN_SITE'),
						'id' => 'desc_3',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Column 3 Heading Bg color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading bg', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'bgcolor_3',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Column 3 Heading color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading text color', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'color_3',
						'std' => '',
						'type' => 'color'
					)
				)
			),
			// column3 end
			// column 4
			array(
				'name' => __('4 Column', 'ATP_ADMIN_SITE'),
				'value' => '04',
				'options' => array(
					//--------------------------
					array(
						'name' => __('Column 1 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 1 Title', 'ATP_ADMIN_SITE'),
						'id' => 'title_1',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 1 Heading', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 1 Heading', 'ATP_ADMIN_SITE'),
						'id' => 'heading_1',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 1 Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for the Column 1', 'ATP_ADMIN_SITE'),
						'id' => 'desc_1',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Column 1 Heading Bg color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading bg', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'bgcolor_1',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Column 1 Heading color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading text color', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'color_1',
						'std' => '',
						'type' => 'color'
					),
					//------------------------- S E P A R A T O R
					array(
						'name' => __('Separator', 'ATP_ADMIN_SITE'),
						'type' => 'separator'
					),
					array(
						'name' => __('Column 2 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 2 Title', 'ATP_ADMIN_SITE'),
						'id' => 'title_2',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 2 Heading', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 2 Heading', 'ATP_ADMIN_SITE'),
						'id' => 'heading_2',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 2 Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for the Column 2', 'ATP_ADMIN_SITE'),
						'id' => 'desc_2',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Column 2 Heading Bg color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading bg', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'bgcolor_2',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Column 2 Heading color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading text color', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'color_2',
						'std' => '',
						'type' => 'color'
					),
					//------------------------- S E P A R A T O R
					array(
						'name' => __('Separator', 'ATP_ADMIN_SITE'),
						'type' => 'separator'
					),
					array(
						'name' => __('Column 3 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 3 Title', 'ATP_ADMIN_SITE'),
						'id' => 'title_3',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 3 Heading', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 3 Heading', 'ATP_ADMIN_SITE'),
						'id' => 'heading_3',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 3 Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for the Column 3', 'ATP_ADMIN_SITE'),
						'id' => 'desc_3',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Column 3 Heading Bg color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading bg', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'bgcolor_3',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Column 3 Heading color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading text color', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'color_3',
						'std' => '',
						'type' => 'color'
					),
					//------------------------- S E P A R A T O R
					array(
						'name' => __('Separator', 'ATP_ADMIN_SITE'),
						'type' => 'separator'
					),
					array(
						'name' => __('Column 4 Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 4 Title', 'ATP_ADMIN_SITE'),
						'id' => 'title_4',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 4 Heading', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the text for the Column 4 Heading', 'ATP_ADMIN_SITE'),
						'id' => 'heading_4',
						'std' => '',
						'type' => 'text',
						'inputsize' => '53'
					),
					array(
						'name' => __('Column 4 Content', 'ATP_ADMIN_SITE'),
						'desc' => __('Type the content for the Column 4', 'ATP_ADMIN_SITE'),
						'id' => 'desc_4',
						'std' => '',
						'type' => 'textarea'
					),
					array(
						'name' => __('Column 4 Heading Bg color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading bg', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'bgcolor_4',
						'std' => '',
						'type' => 'color'
					),
					array(
						'name' => __('Column 4 Heading color', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose color for the heading text color', 'ATP_ADMIN_SITE'),
						'info' => '(Optional)',
						'id' => 'color_4',
						'std' => '',
						'type' => 'color'
					)
				)
			)
		)
	),
	// E N D P R I C I N G T A B L E
	//--------------------------------------------------------
	// F L E X   S L I D E R 
	//--------------------------------------------------------
	"Flex Slider" => array(
		'name' => __('Flex Slider', 'ATP_ADMIN_SITE'),
		'value' => 'flexslider',
		'subtype' => true,
		'desc' => '',
		'inputsize' => '',
		'options' => array(
			array(
				'name' => __('Categories', 'ATP_ADMIN_SITE'),
				'value' => 'post',
				'desc' => '',
				'inputsize' => '',
				'options' => array(
					array(
						'name' => 'Categories',
						'desc' => __('Select the categories to hold the posts from for this slider', 'ATP_ADMIN_SITE'),
						'id' => 'flexcats',
						'std' => 'random',
						'type' => 'multiselect',
						'inputsize' => '53',
						'options' => $this->atp_variable('slider')
					),
					array(
						'name' => __('Slide Effects', 'ATP_ADMIN_SITE'),
						'desc' => __('Select your animation type, "fade" or "slide"', 'ATP_ADMIN_SITE'),
						'id' => 'flexeffect',
						'std' => '',
						'type' => 'select',
						'inputsize' => '',
						'options' => array(
							'fade' => 'Fade',
							'slide' => 'Slide'
						)
					),
					array(
						'name' => __('Slide Direction', 'ATP_ADMIN_SITE'),
						'desc' => __('Select the sliding direction, "horizontal" or "vertical"', 'ATP_ADMIN_SITE'),
						'id' => 'flexslidedir',
						'std' => '',
						'type' => 'select',
						'inputsize' => '',
						'options' => array(
							'horizontal' => 'Horizontal',
							'vertical' => 'Vertical'
						)
					),
					array(
						'name' => __('Navigation', 'ATP_ADMIN_SITE'),
						'desc' => __('Check this if you wish to display previous/next navigation (true/false).', 'ATP_ADMIN_SITE'),
						'id' => 'navigation',
						'std' => true,
						'type' => 'checkbox'
					),
					array(
						'name' => 'Animation Speed',
						'desc' => __('Set the speed of the slideshow cycling, in milliseconds', 'ATP_ADMIN_SITE'),
						'id' => 'flexanimspeed',
						'std' => '200',
						'type' => 'text',
						'options' => '',
						'inputsize' => '53'
					),
					array(
						"name" => "Image Width",
						"desc" => "Enter Image Width",
						"id" => "width",
						"std" => "600",
						"type" => "text",
						'options' => '',
						"inputsize" => "53"
					),
					array(
						"name" => "Image Height",
						"desc" => "Enter Image Height",
						"id" => "height",
						"std" => "300",
						"type" => "text",
						'options' => '',
						"inputsize" => "53"
					),
					array(
						'name' => 'Slides Limits',
						'desc' => __('Integer - Type the number of slides you wish to display', 'ATP_ADMIN_SITE'),
						'id' => 'flexslidelimit',
						'std' => '5',
						'type' => 'text',
						'options' => '',
						'inputsize' => '53'
					)
				)
			),
			// P O S T   A T T A C H M E N T ( Flex slider )
			//--------------------------------------------------------
			array(
				'name' => __('Post Attachment', 'ATP_ADMIN_SITE'),
				'value' => 'postattachment',
				'desc' => '',
				'inputsize' => '',
				'options' => array(
					array(
						'name' => __('Slide Effects', 'ATP_ADMIN_SITE'),
						'desc' => __('Select your animation type, "fade" or "slide"', 'ATP_ADMIN_SITE'),
						'id' => 'flexeffect',
						'std' => '',
						'type' => 'select',
						'inputsize' => '',
						'options' => array(
							'fade' => 'Fade',
							'slide' => 'Slide'
						)
					),
					array(
						'name' => __('Slide Direction', 'ATP_ADMIN_SITE'),
						'desc' => __('Select the sliding direction, "horizontal" or "vertical"', 'ATP_ADMIN_SITE'),
						'id' => 'flexslidedir',
						'std' => '',
						'type' => 'select',
						'inputsize' => '',
						'options' => array(
							'horizontal' => 'Horizontal',
							'vertical' => 'Vertical'
						)
					),
					array(
						'name' => __('Navigation', 'ATP_ADMIN_SITE'),
						'desc' => __('Check this if you wish to display previous/next navigation (true/false).', 'ATP_ADMIN_SITE'),
						'id' => 'navigation',
						'std' => true,
						'type' => 'checkbox'
					),
					array(
						'name' => 'Animation Speed',
						'desc' => __('Set the speed of the slideshow cycling, in milliseconds', 'ATP_ADMIN_SITE'),
						'id' => 'flexanimspeed',
						'std' => '200',
						'type' => 'text',
						'options' => '',
						'inputsize' => '53'
					),
					array(
						"name" => "Image Width",
						"desc" => "Enter Image Width",
						"id" => "width",
						"std" => "600",
						"type" => "text",
						'options' => '',
						"inputsize" => "53"
					),
					array(
						"name" => "Image Height",
						"desc" => "Enter Image Height",
						"id" => "height",
						"std" => "300",
						"type" => "text",
						'options' => '',
						"inputsize" => "53"
					),
					array(
						'name' => 'Slides Limits',
						'desc' => __('Integer - Type the number of slides you wish to display', 'ATP_ADMIN_SITE'),
						'id' => 'flexslidelimit',
						'std' => '5',
						'type' => 'text',
						'options' => '',
						'inputsize' => '53'
					)
				)
			),
			// CUSTOM POST TYPE ( Flex slider)
			//--------------------------------------------------------
			array(
				'name' => __('Custom Post Type', 'ATP_ADMIN_SITE'),
				'value' => 'slider',
				'desc' => '',
				'inputsize' => '',
				'options' => array(
					array(
						'name' => __('Slide Effects', 'ATP_ADMIN_SITE'),
						'desc' => __('Select your animation type, "fade" or "slide"', 'ATP_ADMIN_SITE'),
						'id' => 'flexeffect',
						'std' => '',
						'type' => 'select',
						'inputsize' => '',
						'options' => array(
							'fade' => 'Fade',
							'slide' => 'Slide'
						)
					),
					array(
						'name' => __('Slide Direction', 'ATP_ADMIN_SITE'),
						'desc' => __('Select the sliding direction, "horizontal" or "vertical"', 'ATP_ADMIN_SITE'),
						'id' => 'flexslidedir',
						'std' => '',
						'type' => 'select',
						'inputsize' => '',
						'options' => array(
							'horizontal' => 'Horizontal',
							'vertical' => 'Vertical'
						)
					),
					array(
						'name' => __('Navigation', 'ATP_ADMIN_SITE'),
						'desc' => __('Check this if you wish to display previous/next navigation (true/false).', 'ATP_ADMIN_SITE'),
						'id' => 'navigation',
						'std' => true,
						'type' => 'checkbox'
					),
					array(
						'name' => 'Animation Speed',
						'desc' => __('Set the speed of the slideshow cycling, in milliseconds', 'ATP_ADMIN_SITE'),
						'id' => 'flexanimspeed',
						'std' => '200',
						'type' => 'text',
						'options' => '',
						'inputsize' => '53'
					),
					array(
						"name" => "Image Width",
						"desc" => "Enter Image Width",
						"id" => "width",
						"std" => "600",
						"type" => "text",
						'options' => '',
						"inputsize" => "53"
					),
					array(
						"name" => "Image Height",
						"desc" => "Enter Image Height",
						"id" => "height",
						"std" => "300",
						"type" => "text",
						'options' => '',
						"inputsize" => "53"
					),
					array(
						'name' => 'Slides Limits',
						'desc' => __('Integer - Type the number of slides you wish to display', 'ATP_ADMIN_SITE'),
						'id' => 'flexslidelimit',
						'std' => '5',
						'type' => 'text',
						'options' => '',
						'inputsize' => '53'
					)
				)
			)
		)
	),
	// E N D  - FLEX SLIDER
	
	// I M AG E
	//--------------------------------------------------------
	"Image" =>  array(
		'name' => __('Image', 'ATP_ADMIN_SITE'),
		'value' => 'image',
		'options' => array(
			array(
				'name' => __('Image URL', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the URL of the image from the media library that you wish to use.', 'ATP_ADMIN_SITE'),
				'id' => 'imagesrc',
				'std' => '',
				'type' => 'upload',
				'inputsize' => '53'
			),
			array(
				'name' => __('Title', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter the title attribute for the image', 'ATP_ADMIN_SITE'),
				'id' => 'title',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Caption', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter the caption text for the image', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'caption',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Class', 'ATP_ADMIN_SITE'),
				'desc' => __('Add sub class for the image if you want to assign any new class for the image', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'class',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Link URL', 'ATP_ADMIN_SITE'),
				'desc' => __('Link url to the if you wish to link to any specific location when clicked on the image', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'alink',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Link Target', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose option when reader clicks on the image linked.', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'target',
				'std' => '',
				'options' => array(
					'' => __('Choose one...', 'ATP_ADMIN_SITE'),
					'_blank' => __('Open the linked document in a new window or tab', 'ATP_ADMIN_SITE'),
					'_self' => __('Open the linked document in the same frame as it was clicked.', 'ATP_ADMIN_SITE'),
					'_parent' => __('Open the linked document in the parent frameset', 'ATP_ADMIN_SITE'),
					'_top' => __('Open the linked document in the full body of the window', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			),
			array(
				'name' => __('Lightbox', 'ATP_ADMIN_SITE'),
				'desc' => __('Check this if you wish to use Lightbox for the image', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'lightbox',
				'std' => '',
				'type' => 'checkbox'
			),
			array(
				'name' => __('Align', 'ATP_ADMIN_SITE'),
				'desc' => __('Select the alignment for your image.', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'align',
				'std' => '',
				'options' => array(
					'' => __('Choose one...', 'ATP_ADMIN_SITE'),
					'left' => __('Left', 'ATP_ADMIN_SITE'),
					'right' => __('Right', 'ATP_ADMIN_SITE'),
					'center' => __('Center', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			),
			array(
				'name' => __('Width', 'ATP_ADMIN_SITE'),
				'desc' => __('Use px as units for width', 'ATP_ADMIN_SITE'),
				'id' => 'width',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Height', 'ATP_ADMIN_SITE'),
				'desc' => __('Use px as units for height', 'ATP_ADMIN_SITE'),
				'id' => 'height',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			)
		)
	),
	// P R O G R E S S B A R
	//--------------------------------------------------------
	"Progress Bar" => array(
		'name' => __('Progress Bar', 'ATP_ADMIN_SITE'),
		'value' => 'progressbar',
		'options' => array(
			array(
				'name' => __('Percent', 'ATP_ADMIN_SITE'),
				'desc' => __('Please Enter How much you percentage complete? ', 'ATP_ADMIN_SITE'),
				'id' => 'percentage',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Title', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the text you wish to display for Bar', 'ATP_ADMIN_SITE'),
				'id' => 'title',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Color', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose color fixer', 'ATP_ADMIN_SITE'),
				'id' => 'color',
				'std' => '',
				'type' => 'color',
				'inputsize' => ''
			)
		)
	),
	//S T A F F
	//--------------------------------------------------------
	"Staff Box" => array(
		'name' => __('Staff Box', 'ATP_ADMIN_SITE'),
		'value' => 'staff',
		'options' => array(
			array(
				'name' => __('Photo', 'ATP_ADMIN_SITE'),
				'desc' => __('Please Enter phto path? ', 'ATP_ADMIN_SITE'),
				'id' => 'photo',
				'std' => '',
				'type' => 'upload',
				'inputsize' => '53'
			),
			array(
				'name' => __('Title/Name', 'ATP_ADMIN_SITE'),
				'desc' => __('Please enter Title/Name', 'ATP_ADMIN_SITE'),
				'id' => 'title',
				'std' => '',
				'type' => 'text',
				'inputsize' => '70'
			),
			array(
				'name' => __('Sociables', 'ATP_ADMIN_SITE'),
				'desc' => __('Add Sociables to profile', 'ATP_ADMIN_SITE'),
				'id' => 'selectsociable',
				'std' => '',
				'type' => 'select',
				'options' => $staff_social,
				'inputsize' => '70'
			),
			array(
				'name' => __('Blogger', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Blogger URL ', 'ATP_ADMIN_SITE'),
				'id' => 'blogger',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Dribbble', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Dribble URL', 'ATP_ADMIN_SITE'),
				'id' => 'dribbble',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Delicious', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Delicous URL', 'ATP_ADMIN_SITE'),
				'id' => 'delicious',
				'std' => '',
				'class' => 'class_hide',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Digg', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Digg URL', 'ATP_ADMIN_SITE'),
				'id' => 'digg',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Facebook', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Facebook URL', 'ATP_ADMIN_SITE'),
				'id' => 'facebook',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Flickr', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Flickr URL', 'ATP_ADMIN_SITE'),
				'id' => 'flickr',
				'std' => '',
				'class' => 'class_hide',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Forrst', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Forrst URL', 'ATP_ADMIN_SITE'),
				'id' => 'forrst',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Google', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Google URL', 'ATP_ADMIN_SITE'),
				'id' => 'google',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Html5', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Html5 URL', 'ATP_ADMIN_SITE'),
				'id' => 'html5',
				'std' => '',
				'class' => 'class_hide',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Lastfm', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Lastfm URL', 'ATP_ADMIN_SITE'),
				'id' => 'lastfm',
				'std' => '',
				'class' => 'class_hide',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Linkedin', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Linkedin URL', 'ATP_ADMIN_SITE'),
				'id' => 'linkedin',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Paypal', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Paypal URL', 'ATP_ADMIN_SITE'),
				'id' => 'paypal',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Picasa', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Picasa URL', 'ATP_ADMIN_SITE'),
				'id' => 'picasa',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Pinterest', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Pinterest URL', 'ATP_ADMIN_SITE'),
				'id' => 'pinterest',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Rss', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Rss URL', 'ATP_ADMIN_SITE'),
				'id' => 'rss',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Skype', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Skype URL', 'ATP_ADMIN_SITE'),
				'id' => 'skype',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Stumbleupon', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Stumbleupon URL', 'ATP_ADMIN_SITE'),
				'id' => 'stumbleupon',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Technorati', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Technorati URL', 'ATP_ADMIN_SITE'),
				'id' => 'technorati',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Twitter', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Twitter URL', 'ATP_ADMIN_SITE'),
				'id' => 'twitter',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Twitter3', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Twitter URL', 'ATP_ADMIN_SITE'),
				'id' => 'twitter3',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Vimeo', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Vimeo URL', 'ATP_ADMIN_SITE'),
				'id' => 'vimeo',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Windows', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter windows URL', 'ATP_ADMIN_SITE'),
				'id' => 'windows',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Wordpress', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Wordpress URL', 'ATP_ADMIN_SITE'),
				'id' => 'wordpress',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Yahoo', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Yahoo URL', 'ATP_ADMIN_SITE'),
				'id' => 'yahoo',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Yelp', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Yelp URL', 'ATP_ADMIN_SITE'),
				'id' => 'yelp',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Youtube', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter Youtube URL', 'ATP_ADMIN_SITE'),
				'id' => 'youtube',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Rss', 'ATP_ADMIN_SITE'),
				'desc' => __('Please enter Content', 'ATP_ADMIN_SITE'),
				'id' => 'rss',
				'class' => 'class_hide',
				'std' => '',
				'type' => 'text_rm',
				'inputsize' => '70'
			),
			array(
				'name' => __('Content', 'ATP_ADMIN_SITE'),
				'desc' => __('Please enter Content', 'ATP_ADMIN_SITE'),
				'id' => 'content',
				'std' => '',
				'type' => 'textarea',
				'inputsize' => '70'
			)
		)
	),
	// M I N I   G A L L E R Y 
	//--------------------------------------------------------
	"Mini Gallery" => array(
		'name' => __('Mini Gallery', 'ATP_ADMIN_SITE'),
		'value' => 'minigallery',
		'options' => array(
			array(
				'name' => __('Image Width', 'ATP_ADMIN_SITE'),
				'desc' => __('Use % or px as units for width, do not leave only numbers.', 'ATP_ADMIN_SITE'),
				'id' => 'width',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Image Height', 'ATP_ADMIN_SITE'),
				'desc' => __('Use % or px as units for width, do not leave only numbers.', 'ATP_ADMIN_SITE'),
				'id' => 'height',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Class', 'ATP_ADMIN_SITE'),
				'desc' => __('Add sub class for the images if you want to assign any new class for the images but use spaces between multiple classes no commas', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'class',
				'std' => '',
				'type' => 'text',
				'inputsize' => '53'
			),
			array(
				'name' => __('Images URL', 'ATP_ADMIN_SITE'),
				'desc' => __('Please enter image url(s) in each separated lines.', 'ATP_ADMIN_SITE'),
				'id' => 'textarea_url',
				'std' => '',
				'type' => 'textarea',
				'options' => '',
				'inputsize' => '53'
			)
		)
	),
	// G A L L E R I A
	//--------------------------------------------------------
	"Galleria" => array(
		'name' => __('Galleria', 'ATP_ADMIN_SITE'),
		'value' => 'galleria',
		'subtype' => true,
		'desc' => '',
		'inputsize' => '',
		'options' => array(
			array(
				'name' => __('Attachment', 'ATP_ADMIN_SITE'),
				'value' => 'attachment',
				'inputsize' => '',
				'options' => array(
					array(
						'name' => __('Galleria Width', 'ATP_ADMIN_SITE'),
						'desc' => __('Use % or px as units for width, do not leave only numbers. <br> If you want to have this galleria as responsive then make sure you enter width as 100%', 'ATP_ADMIN_SITE'),
						'id' => 'width',
						'std' => '',
						'inputsize' => '53',
						'type' => 'text'
					),
					array(
						'name' => __('Galleria Height', 'ATP_ADMIN_SITE'),
						'desc' => __('Use % or px as units for width, do not leave only numbers.', 'ATP_ADMIN_SITE'),
						'id' => 'height',
						'std' => '',
						'inputsize' => '53',
						'type' => 'text'
					),
					array(
						'name' => __('Transition', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose the transition effect between images.', 'ATP_ADMIN_SITE'),
						'id' => 'transition',
						'std' => 'fade',
						'inputsize' => '',
						'options' => array(
							'fade' => 'Fade',
							'flash' => 'Flash',
							'slide' => 'slide',
							'fadeslide' => 'fade & slide'
						),
						'type' => 'select'
					),
					array(
						'name' => __('Autoplay', 'ATP_ADMIN_SITE'),
						'desc' => __('This is interval between the each transition of moving forward to next image', 'ATP_ADMIN_SITE'),
						'id' => 'autoplay',
						'std' => 'fade',
						'inputsize' => '',
						'options' => array(
							'false' => 'Stop',
							'1000' => '1000',
							'1500' => '1500',
							'2000' => '2000',
							'2500' => '2500',
							'3000' => '3000',
							'3500' => '3500',
							'4000' => '4000',
							'4500' => '4500',
							'5000' => '5000',
							'5500' => '5500',
							'6000' => '6000',
							'6500' => '6500',
							'7000' => '7000',
							'7500' => '7500',
							'8000' => '8000',
							'8500' => '8500',
							'9000' => '9000',
							'9500' => '9500'
						),
						'type' => 'select'
					)
				)
			),
			//--------------------------------------------------------
			array(
				'name' => __('External Images', 'ATP_ADMIN_SITE'),
				'value' => 'galleriaurl',
				'desc' => '',
				'inputsize' => '',
				'options' => array(
					array(
						'name' => __('Galleria Width', 'ATP_ADMIN_SITE'),
						'desc' => __('Use % or px as units for width, do not leave only numbers.', 'ATP_ADMIN_SITE'),
						'id' => 'width',
						'std' => '',
						'inputsize' => '53',
						'type' => 'text'
					),
					array(
						'name' => __('Galleria Height', 'ATP_ADMIN_SITE'),
						'desc' => __('Use % or px as units for width, do not leave only numbers.', 'ATP_ADMIN_SITE'),
						'id' => 'height',
						'std' => '',
						'inputsize' => '53',
						'type' => 'text'
					),
					array(
						'name' => __('Transition', 'ATP_ADMIN_SITE'),
						'desc' => __('Choose the transition effect between images.', 'ATP_ADMIN_SITE'),
						'id' => 'transition',
						'std' => 'fade',
						'inputsize' => '',
						'options' => array(
							'fade' => 'Fade',
							'flash' => 'Flash',
							'slide' => 'slide',
							'fadeslide' => 'fade & slide'
						),
						'type' => 'select'
					),
					array(
						'name' => __('Autoplay', 'ATP_ADMIN_SITE'),
						'desc' => __('This is interval between the each transition of moving forward to next image', 'ATP_ADMIN_SITE'),
						'id' => 'autoplay',
						'std' => 'fade',
						'options' => array(
							'false' => 'Stop',
							'1000' => '1000',
							'1500' => '1500',
							'2000' => '2000',
							'2500' => '2500',
							'3000' => '3000',
							'3500' => '3500',
							'4000' => '4000',
							'4500' => '4500',
							'5000' => '5000',
							'5500' => '5500',
							'6000' => '6000',
							'6500' => '6500',
							'7000' => '7000',
							'7500' => '7500',
							'8000' => '8000',
							'8500' => '8500',
							'9000' => '9000',
							'9500' => '9500'
						),
						'type' => 'select'
					),
					array(
						'name' => 'Images URL',
						'desc' => __('Please enter image url(s) in each separate lines.', 'ATP_ADMIN_SITE'),
						'id' => 'textarea_url',
						'std' => '',
						'type' => 'textarea',
						'options' => ''
					)
				)
			)
		)
	),
	// E N D  - GALLERIA
	// GOOGLE MAP
	//--------------------------------------------------------
	'Google Map' => array(
		'name'		=> __('Google Map','ATP_ADMIN_SITE'),
		'value'		=>'gmap',
		'options'	=> array(
			array(
				'name'	=> __('Width','ATP_ADMIN_SITE'),
				'desc'	=> 'Use % or px as units for Width, do not leave only numbers.',
				'id'	=> 'width',
				'std'	=> '',
				'type'	=> 'text',
				'inputsize'	=> '53',
			),
			array(
				'name'	=> __('Height','ATP_ADMIN_SITE'),
				'desc'	=> 'Use only Numbers Ex:300.',
				'id'	=> 'height',
				'std'	=> '300',
				'type'	=> 'text',
				'inputsize'	=> '53',
			),
			array(
				'name'	=> __('Address','ATP_ADMIN_SITE'),
				'desc'	=> 'Type the address you wish to display for the map u can use multiple address EX: Address1 | Address2 | Address3 .',
				'info'	=> '(optional)',
				'id'	=> 'address',
				'std'	=> '',
				'type'	=> 'text',
				'inputsize'	=> '53',
			),
			array(
				'name'	=> __('Latitude','ATP_ADMIN_SITE'),
				'id'	=> 'latitude',
				'std'	=> '',
				'type'	=> 'text',
				'inputsize'	=> '30',
			),
			array(
				'name'	=> __('longitude','ATP_ADMIN_SITE'),
				'id'	=> 'longitude',
				'std'	=> '',
				'type'	=> 'text',
				'inputsize'	=> '30',
			),
			array(
				'name'	=> __('Zoom','ATP_ADMIN_SITE'),
				'desc'	=> 'The initial Map zoom level. Required. (Zoom Range : 1-19)',
				'id'	=> 'zoom',
				'std'	=> '12',
				'type'	=> 'text',
				'inputsize'	=> '53',
			),
			array(
				'name'	=> __('Marker Description','ATP_ADMIN_SITE'),
				'desc'	=> 'You can use multiple Marker Description - Example: Description1 | Description2 | Description3.',
				'id'	=> 'marker_desc',
				'std'	=> '',
				'type'	=> 'text',
				'inputsize'	=> '53',
			),
			array(
				'name'	=> __('Info Window','ATP_ADMIN_SITE'),
				'desc'	=> 'Check this if you wish to open the marker window by default',
				'id'	=> 'infowindow',
				'std'	=> '',
				'type'	=> 'checkbox',
				'inputsize'	=> '53',
			),
			array(
				'name'	=> __('Controller','ATP_ADMIN_SITE'),
				'desc'	=> 'Check this if you wish to disable the Controller',
				'id'	=> 'controller',
				'std'	=> '',
				'type'	=> 'checkbox',
				'inputsize'	=> '53',
			),
			array(
						'name'	=> __('Google map stylers Color','ATP_ADMIN_SITE'),
						'desc'	=> 'Use Colorpicker to Googlemap stylers color',
						'id'	=> 'stylerscolor',
						'std'	=> '',
						'type'	=> 'color',
					),
			array(
				'name'	=> __('Gmap Types','ATP_ADMIN_SITE'),
				'desc'	=> 'HYBRID: <em>This map type displays a transparent layer of major streets on satellite images.</em> <br> ROADMAP: <em>This map type displays a normal street map.<br> SATELLITE: This map type displays satellite images.</em><br> TERRAIN: <em>This map type displays maps with physical features such as terrain and vegetation.</em>',
				'id'	=> 'types',
				'std'	=>'ROADMAP',
				'options'=> array(
							'ROADMAP'	=> 'Default road map',
							'SATELLITE'	=> 'Google Earth satellite',
							'HYBRID'	=> 'Hybrid',
							'TERRAIN'	=> 'Terain',
							),
				'type' => 'select',
			),
		)
	),
	// CONTACT FORM
	//--------------------------------------------------------
		"Contact Form" => array(
		'name' => __('Contact Form', 'ATP_ADMIN_SITE'),
		'value' => 'Contactform',
		'options' => array(
			array(
				'name' => __('Email Id', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the email-id you wish to recieve email when the form is submitted', 'ATP_ADMIN_SITE'),
				'id' => 'emailid',
				'std' => '',
				'type' => 'text',
				'inputsize' => '30'
			),
			array(
				'name' => __('Success Message', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the message that you want to show when the form is submitted successfully', 'ATP_ADMIN_SITE'),
				'id' => 'successmessage',
				'std' => '',
				'type' => 'text',
				'inputsize' => '50'
			)
		)
	),
	// END CONTACT FORM
	
	// TWITTER
	//--------------------------------------------------------
		"Twitter Tweets" => array(
		'name' => __('Twitter Tweets', 'ATP_ADMIN_SITE'),
		'value' => 'twitter',
		'options' => array(
				array(
					'name'	=> __('Twitter Id','ATP_ADMIN_SITE'),
					'desc'	=> 'Twitter ID: <small>Use your Id from twitter url <em>http://twitter.com/<span style="color:red">username</span></em></small>',
					'id'	=> 'username',
					'std'	=> '',
					'type'	=> 'text',
					'inputsize'	=> '30',
				),
				array(
					'name'	=> __('consumerkey','ATP_ADMIN_SITE'),
					'desc'	=> 'Enter Your Twitter consumer Key.',
					'id'	=> 'consumerkey',
					'std'	=> '',
					'type'	=> 'text',
					'inputsize'	=> '30',
				),
				array(
					'name'	=> __('Consumer Secret','ATP_ADMIN_SITE'),
					'desc'	=> 'Enter Your Twitter Consumer Secret.',
					'id'	=> 'consumersecret',
					'std'	=> '',
					'type'	=> 'text',
					'inputsize'	=> '30',
				),
				array(
					'name'	=> __('Access Token','ATP_ADMIN_SITE'),
					'desc'	=> 'Enter Your Twitter Access Token.',
					'id'	=> 'accesstoken',
					'std'	=> '',
					'type'	=> 'text',
					'inputsize'	=> '30',
				),
				array(
					'name'	=> __('Access Token Secret','ATP_ADMIN_SITE'),
					'desc'	=> 'Enter Your Twitter Access Token Secret.',
					'id'	=> 'accesstokensecret',
					'std'	=> '',
					'type'	=> 'text',
					'inputsize'	=> '30',
				),
				array(
					'name'	=> __('Limit','ATP_ADMIN_SITE'),
					'desc'	=> 'Type the number of tweets you wish to display.',
					'id'	=> 'limit',
					'std'	=> '4',
					'type'	=> 'text',
					'inputsize'	=> '30',
				),
		)
	),
	// END TWITTER
	
	// FLICKR
	//--------------------------------------------------------
		"Flickr Photos" => array(
		'name' => __('Flickr Photos', 'ATP_ADMIN_SITE'),
		'value' => 'flickr',
		'options' => array(
			array(
				'name' => __('Flickr Id', 'ATP_ADMIN_SITE'),
				'desc' => __('Flickr ID: <small>find your Id from <a href="http://idgettr.com" target="_blank">idGettr</a></small>', 'ATP_ADMIN_SITE'),
				'id' => 'id',
				'std' => '',
				'type' => 'text',
				'inputsize' => '30'
			),
			array(
				'name' => __('Limit', 'ATP_ADMIN_SITE'),
				'desc' => __('Flickr Photos Limit.', 'ATP_ADMIN_SITE'),
				'id' => 'limit',
				'std' => '3',
				'type' => 'text',
				'inputsize' => '30'
			),
			array(
				'name' => __('Type', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose Photos Type', 'ATP_ADMIN_SITE'),
				'id' => 'type',
				'std' => 'user',
				'options' => array(
					'user' => __('User', 'ATP_ADMIN_SITE'),
					'group' => __('Group', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			),
			array(
				'name' => __('Display', 'ATP_ADMIN_SITE'),
				'desc' => __('Choose Display Type', 'ATP_ADMIN_SITE'),
				'id' => 'display',
				'std' => 'random',
				'options' => array(
					'random' => __('Random', 'ATP_ADMIN_SITE'),
					'latest' => __('Latest', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			)
		)
	),
	// END FLICKR
	
	// POPULAR POSTS
	//--------------------------------------------------------
		"Popular Posts" => array(
		'name' => __('Popular Posts', 'ATP_ADMIN_SITE'),
		'value' => 'popularposts',
		'options' => array(
			array(
				'name' => __('Limit', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the number of posts you wish to display.', 'ATP_ADMIN_SITE'),
				'id' => 'limit',
				'std' => '3',
				'type' => 'text',
				'inputsize' => '30'
			),
			array(
				'name' => __('Thumbnail', 'ATP_ADMIN_SITE'),
				'desc' => __('Check this if you wish to display the post thumbnail.', 'ATP_ADMIN_SITE'),
				'id' => 'thumb',
				'std' => '',
				'type' => 'checkbox'
			)
		)
	),
	// END POPULAR POSTS
	
	// RECENT POSTS
	//--------------------------------------------------------
		"Recent Post" => array(
		'name' => __('Recent Posts', 'ATP_ADMIN_SITE'),
		'value' => 'recentposts',
		'options' => array(
			array(
				'name' => __('Limit', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the number of posts you wish to display.', 'ATP_ADMIN_SITE'),
				'id' => 'limit',
				'std' => '3',
				'type' => 'text',
				'inputsize' => '30'
			),
			array(
				'name' => __('Categories', 'ATP_ADMIN_SITE'),
				'desc' => __('Select the categories to hold the posts', 'ATP_ADMIN_SITE'),
				'id' => 'cat_id',
				'std' => 'random',
				'type' => 'multiselect',
				'inputsize' => '',
				'options' => $this->atp_variable('categories'),
			),
			array(
				'name' => __('Thumbnail', 'ATP_ADMIN_SITE'),
				'desc' => __('Check this if you wish to display the post thumbnail.', 'ATP_ADMIN_SITE'),
				'id' => 'thumb',
				'std' => '',
				'type' => 'checkbox'
			)
		)
	),
	// END RECENT POSTS
	
	// Tabs
		//------------------------------------------------------------------
		'Tabs'=> array(
			'name'		=> __('Tabs','ATP_ADMIN_SITE'),
			'value'		=>'Tabs',
			'subtype'	=> true,
			'options'	=> array(
				array(
					'name'		=> __('2 Tabs','ATP_ADMIN_SITE'),
					'value'		=>'t2',
					'options'	=> array(
						array(
							'name'	=> __('Tab 1 Title','ATP_ADMIN_SITE'),
							'desc'	=> 'Type the text for Tab 1',
							'id'	=> 'title_1',
							'std'	=> '',
							'type'	=> 'text',
							'inputsize'	=>'53',
						),
						array(
							'name'	=> __('Tab 1 Content','ATP_ADMIN_SITE'),
							'desc'	=> 'Type the content for Tab 1',
							'id'	=> 'text_1',
							'std'	=> '',
							'type'	=> 'textarea'
						),
						array(
							'name'	=> __('Tab 1 Bgcolor','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'titlebgcolor_1',
							'std'	=> '',
							'type'	=> 'color',
						),
						array(
							'name'	=> __('Tab 1 Title Color','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'titlecolor_1',
							'std'	=> '',
							'type'	=> 'color',
						),
						array(
							'name'	=> __('Tab 1 Icon','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'icon_1',
							'std'	=> '',
							'type'	=> 'text',
						),
						
						//------------------------- S E P A R A T O R
						array('name' => __('Separator','ATP_ADMIN_SITE'),'type' => 'separator', ),

						array(
							'name'	=> __('Tab 2 Title','ATP_ADMIN_SITE'),
							'desc'	=> 'Type the text for Tab 2',
							'id'	=> 'title_2',
							'std'	=> '',
							'type'	=> 'text',
							'inputsize'	=>'53',
						),
						array(
							'name'	=> __('Tab 2 Content','ATP_ADMIN_SITE'),
							'desc'	=> 'Type the content for Tab 2',
							'id'	=> 'text_2',
							'std'	=> '',
							'type'	=> 'textarea'
						),
						array(
							'name'	=> __('Tab 2 Bgcolor','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'titlebgcolor_2',
							'std'	=> '',
							'type'	=> 'color',
						),
						array(
							'name'	=> __('Tab 2 Title Color','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'titlecolor_2',
							'std'	=> '',
							'type'	=> 'color',
						),
						
						array(
							'name'	=> __('Tab 2 Icon','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'icon_2',
							'std'	=> '',
							'type'	=> 'text',
						),
						//------------------------- S E P A R A T O R
						array('name' => __('Separator','ATP_ADMIN_SITE'),'type' => 'separator', ),

						array(
							'name'	=> __('Tabs Type  ','ATP_ADMIN_SITE'),
							'desc'	=> 'Choose Tabs Type Horizontal/Vertical',
							'id'	=> 'ctabs',
							'std'	=> '',
							'options'=> array(
										'horitabs' => 'Horizontal',
										'vertabs' => 'Vertical',
							),
							'type'	=> 'select',
						),
					),
				),
				// 3 TABS
				array(
					'name'		=> __('3 Tabs','ATP_ADMIN_SITE'),
					'value'		=>'t3',
					'options'	=> array(
						array(
							'name'	=> __('Tab 1 Title','ATP_ADMIN_SITE'),
							'desc'	=> 'Type the text for Tab 1',
							'id'	=> 'title_1',
							'std'	=> '',
							'type'	=> 'text',
							'inputsize'	=>'53',
						),
						array(
							'name'	=> __('Tab 1 Content','ATP_ADMIN_SITE'),
							'desc'	=> 'Type the content for Tab 1',
							'id'	=> 'text_1',
							'std'	=> '',
							'type'	=> 'textarea'
						),
						array(
							'name'	=> __('Tab 1 Bgcolor','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'titlebgcolor_1',
							'std'	=> '',
							'type'	=> 'color',
						),
						array(
							'name'	=> __('Tab 1 Title Color','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'titlecolor_1',
							'std'	=> '',
							'type'	=> 'color',
						),
						//------------------------- S E P A R A T O R
						array('name' => __('Separator','ATP_ADMIN_SITE'),'type' => 'separator', ),

						array(
							'name'	=> __('Tab 2 Title','ATP_ADMIN_SITE'),
							'desc'	=> 'Type the text for Tab 2',
							'id'	=> 'title_2',
							'std'	=> '',
							'type'	=> 'text',
							'inputsize'	=>'53',
						),
						array(
							'name'	=> __('Tab 2 Content','ATP_ADMIN_SITE'),
							'desc'	=> 'Type the content for Tab 2',
							'id'	=> 'text_2',
							'std'	=> '',
							'type'	=> 'textarea'
						),
						array(
							'name'	=> __('Tab 2 Bgcolor','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'titlebgcolor_2',
							'std'	=> '',
							'type'	=> 'color',
						),
						array(
							'name'	=> __('Tab 2 Title Color','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'titlecolor_2',
							'std'	=> '',
							'type'	=> 'color',
						),
						//------------------------- S E P A R A T O R
						array('name' => __('Separator','ATP_ADMIN_SITE'),'type' => 'separator', ),

						array(
							'name'	=> __('Tab 3 Title','ATP_ADMIN_SITE'),
							'desc'	=> 'Type the text for Tab 3',
							'id'	=> 'title_3',
							'std'	=> '',
							'type'	=> 'text',
							'inputsize'	=>'53',
						),
						array(
							'name'	=> __('Tab 3 Content','ATP_ADMIN_SITE'),
							'desc'	=> 'Type the content for Tab 3',
							'id'	=> 'text_3',
							'std'	=> '',
							'type'	=> 'textarea'
						),
						array(
							'name'	=> __('Tab 3 Bgcolor','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'titlebgcolor_3',
							'std'	=> '',
							'type'	=> 'color',
						),
						array(
							'name'	=> __('Tab 3 Title Color','ATP_ADMIN_SITE'),
							'info'	=> '(Optional)',
							'id'	=> 'titlecolor_3',
							'std'	=> '',
							'type'	=> 'color',
						),
						//------------------------- S E P A R A T O R
						array('name' => __('Separator','ATP_ADMIN_SITE'),'type' => 'separator', ),

						array(
							'name'	=> __('Tabs Type  ','ATP_ADMIN_SITE'),
							'desc'	=> 'Choose Tabs Type Horizontal/Vertical',
							'id'	=> 'ctabs',
							'std'	=> '',
							'options'=> array(
										'horitabs' => 'Horizontal',
										'vertabs' => 'Vertical',
							),
							'type'	=> 'select',
						),
					),
				),	
			)
		),	
	
	// CONTACT INFO
		//--------------------------------------------------------
		'Contact Info'=> array(
			'name'		=> __('Contact Info','ATP_ADMIN_SITE'),
			'value'		=>'contactinfo',
			'options'	=> array(
				array(
					'name'	=> __('Name','ATP_ADMIN_SITE'),
					'desc'	=> 'Type the Name or Company Name you wish to display.',
					'id'	=> 'name',
					'std'	=> '',
					'type'	=> 'text',
					'inputsize'	=> '30',
				),
				array(
					'name'	=> __('Address','ATP_ADMIN_SITE'),
					'desc'	=> 'Type the Address you wish to display.',
					'id'	=> 'address',
					'std'	=> '',
					'type'	=> 'textarea',
				),
				array(
					'name'	=> __('Phone','ATP_ADMIN_SITE'),
					'desc'	=> 'Type the Phone Number you wish to display.',
					'id'	=> 'phone',
					'std'	=> '',
					'type'	=> 'text',
					'inputsize'	=> '30',
				),
				array(
					'name'	=> __('Email','ATP_ADMIN_SITE'),
					'desc'	=> 'Type the Email-ID you wish to display.',
					'id'	=> 'email',
					'std'	=> '',
					'type'	=> 'text',
					'inputsize'	=> '30',
				),
				array(
					'name'	=> __('Website URL','ATP_ADMIN_SITE'),
					'desc'	=> 'Type the Link URL you wish to display. excluding http',
					'id'	=> 'link',
					'std'	=> '',
					'type'	=> 'text',
					'inputsize'	=> '30',
				),
			)
		),
		// END CONTACTINFO
	
	// P O R T F O L I O 
	//--------------------------------------------------------
	"Portfolio" => array(
		'name' => __('Portfolio', 'ATP_ADMIN_SITE'),
		'value' => 'portfolio',
		'options' => array(
			array(
				'name' => __('Column', 'ATP_ADMIN_SITE'),
				'desc' => __('Selec the number of columns you wish to display the portfolio layout', 'ATP_ADMIN_SITE'),
				'id' => 'column',
				'std' => '4',
				'options' => array(
					'1' => __('Column 1', 'ATP_ADMIN_SITE'),
					'2' => __('Columns 2', 'ATP_ADMIN_SITE'),
					'3' => __('Columns 3', 'ATP_ADMIN_SITE'),
					'4' => __('Columns 4', 'ATP_ADMIN_SITE')
				),
				'type' => 'select'
			),
			array(
				'name' => __('Portfolio Limit', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the number of items you wish to display per page', 'ATP_ADMIN_SITE'),
				'id' => 'limit',
				'std' => '4',
				'type' => 'text'
			),
			array(
				'name' => __('Read More Text', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the read more text you wish to display for the portfolio items', 'ATP_ADMIN_SITE'),
				'id' => 'readmore',
				'std' => 'Read more',
				'type' => 'text'
			),
			array(
				'name' => __('Content Limits', 'ATP_ADMIN_SITE'),
				'desc' => __('Type the content character limits (default : 200)', 'ATP_ADMIN_SITE'),
				'id' => 'limitcontent',
				'std' => '200',
				'type' => 'text'
			),
			array(
				'name' => __('Image Height', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter the Image height', 'ATP_ADMIN_SITE'),
				'info' => '(Optional)',
				'id' => 'imgheight',
				'std' => '',
				'type' => 'text'
			),
			array(
				'name' => __('Category', 'ATP_ADMIN_SITE'),
				'desc' => __('Hold Control/Command key to select multiple categories', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'cat',
				'std' => '',
				'options' => $this->atp_variable('portfolio'),
				'type' => 'multiselect'
			),
			array(
				'name' => __('Title', 'ATP_ADMIN_SITE'),
				'desc' => __('Check this if you wish to disable the title.', 'ATP_ADMIN_SITE'),
				'id' => 'title',
				'std' => '',
				'type' => 'checkbox'
			),
			array(
				'name' => __('Description', 'ATP_ADMIN_SITE'),
				'desc' => __('Check this if you wish to disable the description.', 'ATP_ADMIN_SITE'),
				'id' => 'description',
				'std' => '',
				'type' => 'checkbox'
			),
			array(
				'name' => __('Pagination', 'ATP_ADMIN_SITE'),
				'desc' => __('Check this if you wish to disable the pagination.', 'ATP_ADMIN_SITE'),
				'id' => 'pagination',
				'std' => '',
				'type' => 'checkbox'
			),
			array(
				'name' => __('More Button', 'ATP_ADMIN_SITE'),
				'desc' => __('Check this if you wish to disable the readmore text', 'ATP_ADMIN_SITE'),
				'id' => 'morebutton',
				'std' => '',
				'type' => 'checkbox'
			),
			array(
				'name' => __('Sidebar', 'ATP_ADMIN_SITE'),
				'desc' => __('Un-Check this if you wish to use sidebar layout for the Portfolio', 'ATP_ADMIN_SITE'),
				'id' => 'sidebar',
				'std' => 'false',
				'type' => 'checkbox'
			),
			array(
				'name' => __('Sortable', 'ATP_ADMIN_SITE'),
				'desc' => __('Un-Check this if you wish to disable the Portfolio Display Style as Sortable', 'ATP_ADMIN_SITE'),
				'id' => 'sortable',
				'std' => 'false',
				'type' => 'checkbox'
			)
		)
	),
	/*--------------------------------------------------------
	Event list
	------------------------------------------------------- */
	"Event List" => array(
		'name' => __('Event list', 'ATP_ADMIN_SITE'),
		'value' => 'eventlist',
		'options' => array(
			array(
				'name' => __('Category', 'ATP_ADMIN_SITE'),
				'desc' => __('Hold Control/Command key to select multiple categories', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'cat',
				'std' => '',
				'options' => $this->atp_variable('events'),
				'type' => 'multiselect'
			),
			array(
				'name' => __('Title', 'ATP_ADMIN_SITE'),
				'desc' => __('Enter The Title.', 'ATP_ADMIN_SITE'),
				'id' => 'title',
				'std' => '',
				'type' => 'text'
			)
		)
	),
	
	// T E S T I M O N I A L 
	//--------------------------------------------------------
	"Testimonial" => array(
		'name' => __('Testimonial', 'ATP_ADMIN_SITE'),
		'value' => 'testimonial',
		'options' => array(
			array(
				'name' => __('Category', 'ATP_ADMIN_SITE'),
				'desc' => __('Hold Control/Command key to select multiple categories', 'ATP_ADMIN_SITE'),
				'info' => '(optional)',
				'id' => 'category',
				'std' => '',
				'options' => $this->atp_variable('testimonial'),
				'type' => 'multiselect'
			)
		)
	),
	// E N D   - T E S T I M O N I A L 
	
	// CAROUSEL SLIDER 
	//--------------------------------------------------------
	"Carousel Slider" => array(
		'name' => __('Carousel Slider', 'ATP_ADMIN_SITE'),
		'value' => 'carouselslider',
		'subtype' => true,
		'desc' => '',
		'inputsize' => '',
		'options' => array(
			array(
				'name' => __('Category', 'ATP_ADMIN_SITE'),
				'value' => 'category',
				'inputsize' => '',
				'options' => array(
					array(
						'name' => __('Category', 'ATP_ADMIN_SITE'),
						'id' => 'cat',
						'std' => '',
						'desc' => __('Hold Control/Command key to select multiple categories', 'ATP_ADMIN_SITE'),
						'options' => $this->atp_variable('categories'),
						'type' => 'multiselect'
					),
					array(
						'name' => __('Blog Posts Limit', 'ATP_ADMIN_SITE'),
						'desc' => __('Number of items to show per page', 'ATP_ADMIN_SITE'),
						'id' => 'limit',
						'std' => '4',
						'type' => 'text'
					),
					array(
						'name' => __('Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter The Title', 'ATP_ADMIN_SITE'),
						'id' => 'title',
						'std' => '',
						'type' => 'text'
					)
				)
			),
			//--------------------------------------------------------
			array(
				'name' => __('Portfolio', 'ATP_ADMIN_SITE'),
				'value' => 'Portfolio',
				'desc' => '',
				'inputsize' => '',
				'options' => array(
					array(
						'name' => __('Portfolio Category', 'ATP_ADMIN_SITE'),
						'id' => 'cat',
						'std' => '',
						'desc' => __('Hold Control/Command key to select multiple categories', 'ATP_ADMIN_SITE'),
						'options' => $this->atp_variable('portfolio'),
						'type' => 'multiselect'
					),
					array(
						'name' => __('Portfolio Posts Limit', 'ATP_ADMIN_SITE'),
						'desc' => __('Number of items to show per page', 'ATP_ADMIN_SITE'),
						'id' => 'limit',
						'std' => '4',
						'type' => 'text'
					),
					array(
						'name' => __('Title', 'ATP_ADMIN_SITE'),
						'desc' => __('Enter The Title', 'ATP_ADMIN_SITE'),
						'id' => 'title',
						'std' => '',
						'type' => 'text'
					)
				)
			)
		)
	),
	// E N D  - CAROUSEL SLIDER
// VIMEO 
		//--------------------------------------------------------
		'Vimeo' => array(
			'name'		=> __('Vimeo','ATP_ADMIN_SITE'),
			'value'		=>'vimeo',
			'options'	=> array(				
				array(
					'name'	=> __('Clip id','ATP_ADMIN_SITE'),
					'desc'	=> 'Enter the ID only from the clips URL (e.g. http://vimeo.com/<span style="color:red">123456<span style="color:red">)',
					'id'	=> 'clipid',
					'std'	=> '',

					'type'	=> 'textarea',
				),
				array(
					'name'	=> __('Autoplay','ATP_ADMIN_SITE'),
					'desc'	=> 'Check this if you wish to enable auto play option.',
					'id'	=> 'autoplay',
					'std'	=>'true',
					'type'	=> 'checkbox',
				),	
			)
		),
		//END - VIMEO
		// YOUTUBE 
		//--------------------------------------------------------
		'Youtube' => array(	
			'name'		=> __('Youtube','ATP_ADMIN_SITE'),
			'value'		=>'youtube',
			'options'	=> array(				
				array(
					'name'	=> __('Clip id','ATP_ADMIN_SITE'),
					'desc'	=> 'The id from the clip URL after v= (e.g. http://www.youtube.com/watch?v=<span style="color:red">GgR6dyzkKHI</span>)',
					'id'	=> 'clipid',
					'std'	=> '',
					'type'	=> 'textarea',
				),
				array(
					'name'	=> __('Autoplay','ATP_ADMIN_SITE'),
					'desc'	=> 'Check this if you wish to start playing the video after the player intialized.',
					'id'	=> 'autoplay',
					'type'	=> 'checkbox',
				),	
			)
		)
		// E N D  - YOUTUBE 
);
?>