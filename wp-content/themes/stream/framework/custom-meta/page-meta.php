<?php

	// P A G E   M E T A B O X 
	//--------------------------------------------------------
	$prefix = '';
	$shortname = '';
	$this->meta_box = array();
	global $staff_social, $atp_ribbons;
	// Defines custom sidebar widget based on custom option
	$sidebarwidget = get_option('atp_customsidebar');
	//--------------------------------------------------------

	$this->meta_box['page'] = array(
		'id'		=> 'page-meta-box',
		'title'		=> THEMENAME.'&nbsp;Page Options',
		'context'	=> 'normal',
		'priority'	=> 'core',
		'fields'	=> array(
	
							// C U S T O M   S U B H E A D E R   T E A S E R 
							//--------------------------------------------------------
							array(
								'name'	=> __('Page Slider','ATP_ADMIN_SITE'),
								'desc'	=> 'Select the Slider you wish to use for the this page. Make sure you add the slide posts',
								'id'	=> $prefix . 'page_slider',
								'std'	=> '',
								'type'	=> 'select',
								'options'=> $this->atp_variable('slider_type')
							),
							array(
								'name'	=> __('FlexSlider Category','ATP_ADMIN_SITE'),
									'desc'	=> 'Select Slider Category to hold the posts from.',
									'id'	=> $prefix.'flexslidercat',
									'class' => 'page_slider flexslider',
									'std'	=> '',
									'options' => $this->atp_variable('slider'),
									'type'	=> 'multiselect'
							),
							array(
								'name'	=> __('Elastic Category','ATP_ADMIN_SITE'),
									'desc'	=> 'Select Slider Category to hold the posts from.',
									'id'	=> $prefix.'eislidercat',
									'class' => 'page_slider eislider',
									'std'	=> '',
									'options' => $this->atp_variable('slider'),
									'type'	=> 'multiselect'
							),
							array(
								'name'	=> __('NivoSlider Category','ATP_ADMIN_SITE'),
									'desc'	=> 'Select Slider Category to hold the posts from.',
									'id'	=> $prefix.'nivoslidercat',
									'class' => 'page_slider nivoslider',
									'std'	=> '',
									'options' =>$this->atp_variable('slider'),
									'type'	=> 'multiselect'
							),
							array(
								'name'	=> __('Planbox Slider Content','ATP_ADMIN_SITE'),
									'desc'	=> 'Planbox Content.',
									'id'	=> $prefix.'planboxcontent',
									'class' => 'page_slider planbox',
									'std'	=> '',
									'type'	=> 'textarea'
							),
							array(
								'name'	=> __('Video Slider','ATP_ADMIN_SITE'),
									'desc'	=> 'Video Slider - Supports Custom Shortcodes too..',
									'id'	=> $prefix.'videoslider',
									'class' => 'page_slider videoslider',
									'std'	=> '',
									'type'	=> 'textarea'
							),
							array(
								'name'	=> __('Static Image','ATP_ADMIN_SITE'),
									'desc'	=> 'Upload Image.',
									'id'	=> $prefix.'staticimage',
									'class' => 'page_slider static_image',
									'std'	=> '',
									'type'	=> 'upload'
							),
							array(
								'name'	=> __('Image Link','ATP_ADMIN_SITE'),
									'desc'	=> 'Enter Image Link for the Static Slider Image (Optional)',
									'id'	=> $prefix.'cimage_link',
									'class' => 'page_slider static_image',
									'std'	=> '',
									'type'	=> 'text'
							),
							array(
								'name'	=> __('Custom Slider','ATP_ADMIN_SITE'),
									'desc'	=> 'Use in Your custom slider Plugin shortcodes Example:[revslider id="1"].',
									'id'	=> $prefix.'customslider',
									'class' => 'page_slider custom_slider',
									'std'	=> '',
									'type'	=> 'textarea'
							),
			
							// C U S T O M   S U B H E A D E R   T E A S E R 
							//--------------------------------------------------------
							array(
								'name'	=> __('Subheader Options','ATP_ADMIN_SITE'),
								'desc'	=> __('Select the Teaser type mode you want to display in subheader of the this Post','ATP_ADMIN_SITE'),
								'id'	=> $prefix . 'subheader_teaser_options',
								'std'	=> '',
								'type'	=> 'select',
								'options'=> array(
										'default'	=> 'Default ( Options Panel )',
										'twitter'	=> 'Twitter', 	
										'custom'	=> 'Custom', 
										'customhtml'	=> 'Custom Html',
										'disable'	=> 'Disable')
							),
		
							// T E A S E R   T E X T 
							//--------------------------------------------------------
							array(
								'name'	=> __('Teaser Text','ATP_ADMIN_SITE'),
								'desc'	=> __('Type the text you wish to display in the subheader of this page/post. If you wish to use bold text then use strong element example &lt;strong&gt;bold text &lt;/strong&gt;','ATP_ADMIN_SITE'),
								'id'	=> 'page_desc',
								'title'	=> 'Title &amp; Teaser Text',
								'class'	=> 'sub_teaser_option custom',
								'std'	=> '',
								'type'	=> 'textarea',
							),
		
							// C U S T O M   H T M L 
							//--------------------------------------------------------
							array(
								'name'	=> __('Custom HTML / Shortcode','ATP_ADMIN_SITE'),
								'desc'	=> __('Type any text or shortcode you wish to display in the subheader area. This field supports shortcode','ATP_ADMIN_SITE'),
								'id'	=> 'custom_html',
								'title'	=> 'Custom Text / Shortcode',
								'class'	=> 'sub_teaser_option customhtml',
								'std'	=> '',
								'type'	=> 'textarea',
							),
		
							// P A G E   B A C K G R O U N D
							//--------------------------------------------------------
							array(
								'name'	=> __('Page Background','ATP_ADMIN_SITE'),
								'desc'	=> __('Upload the image for the page background','ATP_ADMIN_SITE'),
								'id'	=> 'page_bg_image',
								'std'	=> '',
								'title'	=> 'Background Image',
								'type'	=> 'upload',
							),
		
							// S U B H E A D E R   C O L O R
							//--------------------------------------------------------
							array(
								'name'	=> __('Subheader Background','ATP_ADMIN_SITE'),
								'desc'	=> __('Choose the color for subheader background','ATP_ADMIN_SITE'),
								'id'	=> 'subheader_bg_color',
								'std'	=> '',
								'title'	=> 'Subheader Background',
								'type'	=> 'color',
							),
		
							// S U B H E A D E R   B A C K G R O U N D 
							//--------------------------------------------------------
							array(
								'name'	=> __('Subheader Image','ATP_ADMIN_SITE'),
								'desc'	=> __('Upload Subheader Image','ATP_ADMIN_SITE'),
								'id'	=> $prefix.'subheader_img',
								'type'	=> 'upload',
								'std' 	=> '',
								
							),
		
							// C U S T O M   S I D E B A R 
							//--------------------------------------------------------
							array(
								'name'	=> __('Custom Sidebar','ATP_ADMIN_SITE'),
								'desc'	=> __('Select the Sidebar you wish to assign for this page.','ATP_ADMIN_SITE'),
								'id'	=> $prefix . 'custom_widget',
								'type'	=> 'customselect',
								'std'	=> '',
								
								'options'=> $sidebarwidget
							),
		
							// L A Y O U T   O P T I O N S 
							//--------------------------------------------------------
							array(
								'name'	=> __('Layout Option','ATP_ADMIN_SITE'),
								'desc'	=> __('Select the Layout style you wish to use for this page, Left Sidebar or Right Sidebar or Full Width.','ATP_ADMIN_SITE'),
								'id'	=> $prefix . 'sidebar_options',
								'std'	=> 'rightsidebar',
								'type'	=> 'layout',
								'options'=> array(
										'rightsidebar'	=>  FRAMEWORK_URI . 'admin/images/right-sidebar.png', 
										'leftsidebar'	=>  FRAMEWORK_URI . 'admin/images/left-sidebar.png',
										'fullwidth'		=>  FRAMEWORK_URI . 'admin/images/fullwidth.png')	
							),
		
							// B R E A D C R U M B 
							//--------------------------------------------------------
							array(
								'name'	=> __('Breadcrumb','ATP_ADMIN_SITE'),
								'desc'	=> __('Check this if you wish to disable the breadcrumb for this page.','ATP_ADMIN_SITE'),
								'id'	=> $prefix.'breadcrumb',
								'std' 	=> 'off',
								'type'	=> 'checkbox',
							),
			)
	);
?>