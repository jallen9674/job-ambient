<?php
	/* Slider Meta box setup function. */
	$prefix = '';
	$this->meta_box['testimonialtype'] = array(
		'id'		=> 'Testimonial-meta-box',
		'title'		=> THEMENAME. ' Testimonial Options',
		'page'		=> 'testimonial',
		'context'	=> 'normal',
		'priority'	=> 'core',
		'fields'	=> array(
			// P O R T F O L I O   P O S T   T Y P E 
			//--------------------------------------------------------
			array(
				'name'	=> __('Image','ATP_ADMIN_SITE'),
				'desc'	=> __('Select the Image type mode you want to use either Gravatar or Upload Image.','ATP_ADMIN_SITE'),
				'id'	=> $prefix . 'testimonial_image_option',
				'std'	=> 'gravatar',
				'type'	=> 'select',
				'options'=> array(
								"gravatar"	=> "Gravatar",
								"customimage"	=>'Upload Picture')
			),
			// E M A I L 
			array(
				'name'	=> __('Email (Gravatar support)','ATP_ADMIN_SITE'),
				'desc'	=> __('Enter e-mail used for the gravatar.com or your client email.','ATP_ADMIN_SITE'),
				'id'	=> 'testimonial_email',
				'std'	=> '',
				'class'	=> 'testimonialoption gravatar',
				'title'	=> 'Name',
				'type'	=> 'text',
			),
			// U P L O A D I M A G E 
			array(
				'name'	=> __('Upload Photo','ATP_ADMIN_SITE'),
				'desc'	=> __('Upload Image.','ATP_ADMIN_SITE'),
				'id'	=> 'testimonial_photo',
				'class'	=> 'testimonialoption customimage',
				'std'	=> '',
				'type'	=> 'upload',
			),

			// W E B S I T E N A M E
			//--------------------------------------------------------
			array(
				'name'	=> __('Website Name','ATP_ADMIN_SITE'),
				'desc'	=> __('Enter Website Name.','ATP_ADMIN_SITE'),
				'id'	=> 'websitename',
				'std'	=> '',
				'title'	=> 'Website Name',
				'type'	=> 'text',
			),
			// W E B S I T E U R L
			//--------------------------------------------------------
			array(
				'name'	=> __('Website URL','ATP_ADMIN_SITE'),
				'desc'	=> __('Website URL.','ATP_ADMIN_SITE'),
				'id'	=> 'website_url',
				'std'	=> '',
				'title'	=> 'Website URL',
				'type'	=> 'text',
			),
		)
	);
?>