<?php
	// S L I D E R   M E T A B O X 
	//--------------------------------------------------------
	$prefix = '';

	$this->meta_box['slider'] = array(
		'id'		=> 'slider-meta-box',
		'title'		=> THEMENAME. ' Slider Options',
		'page'		=> 'slider',
		'context'	=> 'normal',
		'priority'	=> 'high',
		'fields'	=> array(

			array(
				'name'	=> 'Link for Slide Item',
				'id'	=> $prefix . 'postlinktype_options',
				'desc'	=> __('The url that the slide post is linked to','ATP_ADMIN_SITE'),
				'type'	=> 'radio',
				'std'	=>'default',
				'options' =>array(
						'linkpage'		=> 'Link to Page',
						'linktocategory'=> 'Link to Category', 
						'linktopost'	=> 'Link to Post',
						'linkmanually'	=> 'Link Manually',
						'nolink'		=> 'No Link'
						
				)	
			),
			
			array(
				'name'	=> __('Slider Caption','ATP_ADMIN_SITE'),
				'desc'	=> 'Slider Caption not more than 100 characters',
				'id'	=> $prefix.'slider_desc',
				'class' => '',
				'std'	=> '',
				'type'	=> 'textarea'
			),
			array(
				'name'	=> __('Caption Disable','ATP_ADMIN_SITE'),
				'desc'	=> 'Check this if you wish to disable caption for this slide',
				'id'	=> $prefix.'slider_cap_disable',
				'class' => '',
				'std'	=> '',
				'type'	=> 'checkbox'
			),
			
			
		),
	);
?>