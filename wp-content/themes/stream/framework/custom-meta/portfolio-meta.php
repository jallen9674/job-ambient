<?php
	/* Slider Meta box setup function. */
	$prefix = '';
	$this->meta_box['portfoliotype'] = array(
		'id'		=> 'portfolio-meta-box',
		'title'		=> THEMENAME. ' Portfolio Options',
		'page'		=> 'portfolio',
		'context'	=> 'normal',
		'priority'	=> 'core',
		'fields'	=> array(
		// P O R T F O L I O   P O S T   T Y P E 
			//--------------------------------------------------------
			array(
				'name'	=> __('Portfolio Type','ATP_ADMIN_SITE'),
				'desc'	=> __('Select the portfolio type mode you want to use either image or video.','ATP_ADMIN_SITE'),
				'id'	=> $prefix . 'port_posttype_option',
				'std'	=> 'posttype_image',
				'type'	=> 'select',
				'options'=> array(
								"posttype_image"	=> "Image",
								"gallery"	=>'Slide Show',
								"video"	=> "Video",
								)
			),
			array( 
				'name' => __('Video Embeded Code','ATP_ADMIN_SITE'),
				'desc' => __('If you\'re not using any of the video formats above or self hosted videos then you can add embeded code here. Max Width 580px for blog posts','ATP_ADMIN_SITE'),
				'id' => $prefix.'video_embed',
				'class'	=> 'ptoption video',
				'std' => '',
				'type' => 'textarea',
			),
			// C O N  T E N T W I D H T
			//--------------------------------------------------------
			array(
				'name'	=> __('Content Width','ATP_ADMIN_SITE'),
				'desc'	=> __('Select the Content Width you want to use for this page .','ATP_ADMIN_SITE'),
				'id'	=> $prefix . 'content_width',
				'std'	=> 'fullwidth',
				'type'	=> 'select',
				'options'=> array(
							'fullwidth'	=> 'Full Width', 
							'halfwidth'	=>  'Half Width'),	
			),
				// C U S T O M   D A T E 
			//--------------------------------------------------------
			array(
				'name'	=> __('Project Date','ATP_ADMIN_SITE'),
				'desc'	=> __('Select the Project Date.','ATP_ADMIN_SITE'),
				'id'	=> 'project_date',
				'title'	=> 'Project Date',
				'std'	=> date('d-M-y'),
				'type'	=> 'date',
			),
			// P O R T F O L I O   D E S C R I P T I O N
			//--------------------------------------------------------
			array(
				'name'	=> __('Portfolio Description','ATP_ADMIN_SITE'),
				'desc'	=> __('Enter Portfolio Short Summary here.','ATP_ADMIN_SITE'),
				'id'	=> 'project_desc',
				'std'	=> '',
				'title'	=> 'Portfolio Description',
				'type'	=> 'textarea',
			),
			// V I S I T   S I T E   L I N K 
			//--------------------------------------------------------
			array(
				'name'	=> __('Visit Site Link','ATP_ADMIN_SITE'),
				'desc'	=> __('External Link for the Portfolio Visit Site Link which adds a visit site link beside the read more text.','ATP_ADMIN_SITE'),
				'id'	=> 'visit_link',
				'std'	=> '',
				'title'	=> 'Visit Site Link',
				'type'	=> 'text',
			),
			// C U S T O M   S U B H E A D E R   T E A S E R 
			//--------------------------------------------------------
			array(
				'name'	=> __('Subheader Options','ATP_ADMIN_SITE'),
				'desc'	=> __('Select the Teaser type mode you want to display in subheader of the this Post','ATP_ADMIN_SITE'),
				'id'	=> $prefix . 'subheader_teaser_options',
				'std'	=> '',
				'type'	=> 'select',
				'options'=> array(
								'default'	=> 'Default ( Options Panel )',
								'twitter'	=> 'Twitter', 	
								'custom'	=> 'Custom', 
								'customhtml'	=> 'Custom Html',
								'disable'	=> 'Disable')
			),
			// T E A S E R   T E X T 
			//--------------------------------------------------------
			array(
				'name'	=> __('Teaser Text','ATP_ADMIN_SITE'),
				'desc'	=> __('Type the text you wish to display in the subheader of this page/post. If you wish to use bold text then use strong element example &lt;strong&gt;bold text &lt;/strong&gt;','ATP_ADMIN_SITE'),
				'id'	=> 'page_desc',
				'class'	=> 'sub_teaser_option custom',
				'std'	=> '',
				'title'	=> 'Title &amp; Teaser Text',
				'type'	=> 'textarea',
			),
			// C U S T O M   H T M L 
			//--------------------------------------------------------
			array(
				'name'	=> __('Custom HTML / Shortcode','ATP_ADMIN_SITE'),
				'desc'	=> __('Type any text or shortcode you wish to display in the subheader area. This field supports shortcode','ATP_ADMIN_SITE'),
				'id'	=> 'custom_html',
				'class'	=> 'sub_teaser_option customhtml',
				'std'	=> '',
				'title'	=> 'Custom Text / Shortcode',
				'type'	=> 'textarea',
			),
			// P A G E   B A C K G R O U N D 
			//--------------------------------------------------------
			array(
				'name'	=> __('Page Background Image','ATP_ADMIN_SITE'),
				'desc'	=> __('Upload the image for the page background','ATP_ADMIN_SITE'),
				'id'	=> 'page_bg_image',
				'std'	=> '',
				'title'	=> 'Background Image',
				'type'	=> 'upload',
			),
			// S U B H E A D E R   C O L O R 
			//--------------------------------------------------------
			array(
				'name'	=> __('Subheader Color','ATP_ADMIN_SITE'),
				'desc'	=> __('Choose the color for subheader background','ATP_ADMIN_SITE'),
				'id'	=> 'subheader_bg_color',
				'std'	=> '',
				'title'	=> 'Subheader Background',
				'type'	=> 'color',
			),
			// S U B H E A D E R   B A C K G R O U N D
			//--------------------------------------------------------
			array(
				'name'	=> __('Subheader Background Image','ATP_ADMIN_SITE'),
				'desc'	=> __('subheader background Image','ATP_ADMIN_SITE'),
				'id'	=> 'subheader_bg_img',
				'std'	=> '',
				'title'	=> 'Subheader Background',
				'type'	=> 'upload',
			),
			// C U S T O M   S I D E B A R 
			//--------------------------------------------------------
			array(
				'name'	=> __('Custom Sidebar','ATP_ADMIN_SITE'),
				'desc'	=> __('Select the Sidebar you want to assign for this page.','ATP_ADMIN_SITE'),
				'id'	=> $prefix . 'custom_widget',
				'std'	=> '',
				'type'	=> 'customselect',
				'options'=> $sidebarwidget
			),
		
			// B R E A D C R U M B 
			//--------------------------------------------------------
			array(
				'name'	=> __('Breadcrumb','ATP_ADMIN_SITE'),
				'desc'	=> __('Check this if you wish to disable the breadcrumb for this page.','ATP_ADMIN_SITE'),
				'id'	=> $prefix.'breadcrumb',
				'std' 	=> '',
				'type'	=> 'checkbox',
			),
		)
	);
?>