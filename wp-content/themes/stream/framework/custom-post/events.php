<?php
	//Events type
	//--------------------------------------------------------
	function events_register() {
		$labels = array(
			'name'				=> _x('Events','ATP_ADMIN_SITE'),
			'singular_name'		=> _x('Events','ATP_ADMIN_SITE'),
			'add_new'			=> _x('Add New', 'ATP_ADMIN_SITE'),
			'add_new_item'		=> __('Add New Event','ATP_ADMIN_SITE'),
			'edit_item'			=> __('Edit Event','ATP_ADMIN_SITE'),
			'new_item'			=> __('New Event','ATP_ADMIN_SITE'),
			'view_item'			=> __('View Event','ATP_ADMIN_SITE'),
			'search_items'		=> __('Search Events','ATP_ADMIN_SITE'),
			'not_found'			=> __('Nothing found','ATP_ADMIN_SITE'),
			'not_found_in_trash'=> __('Nothing found in Trash','ATP_ADMIN_SITE'),
			'parent_item_colon'	=> '',
			'all_items' =>  __( 'All Events' ,'ATP_ADMIN_SITE')
		);
	
		$args = array(
			'labels'			=> $labels,
			'public'			=> true,
			'exclude_from_search'=> false,
			'show_ui'			=> true,
			'capability_type'	=> 'post',
			'hierarchical'		=> false,
			'rewrite'			=> array( 'with_front' => false ),
			'query_var'			=> false,	
			'menu_icon'			=> THEME_URI . '/framework/admin/images/events-icon.png',  		
			'supports'			=> array('page-attributes','editor','title','thumbnail'),
			'taxonomies'		=> array('event_cat', 'post_tag')
		); 
		register_post_type( 'events' , $args );
	}
	
	register_taxonomy("events_cat", 'events', array(
		'hierarchical'		=> true,
		'labels' => array(
							'name' => _x( 'Event Categories', 'taxonomy general name' ),
							'singular_name' => _x( 'Events', 'taxonomy singular name' ),
							'search_items' =>  __( 'Search Events' ),
							'all_items' => __( 'All Events' ),
							'parent_item' => __( 'Parent Location' ),
							'parent_item_colon' => __( 'Parent Events:' ),
							'edit_item' => __( 'Edit Events' ),
							'update_item' => __( 'Update Events' ),
							'add_new_item' => __( 'Add Event Category' ),
							'new_item_name' => __( 'New Event' ),
							'menu_name' => __( 'Event Categories' ),
						),
		'show_ui'			=> true,
		'query_var'			=> true,
		'rewrite'			=> false,
	));
		
	add_action('init', 'events_register');
	
	function events_columns($columns) {
		$columns['eventscat'] = __('Event Categories','ATP_ADMIN_SITE');
		$columns['eventthumbnail'] =  __('Event Image','ATP_ADMIN_SITE');
		$columns['eventsdate'] =  __('Event Date','ATP_ADMIN_SITE');
		$columns['starttime'] =  __('Start Time','ATP_ADMIN_SITE');
		$columns['endtime'] =  __('End Time','ATP_ADMIN_SITE');
	
		return $columns;
	}
	
	function manage_events_columns($name) {
		global $post, $wp_query;
		switch ($name) {
			case 'eventscat':
							$terms = get_the_terms($post->ID, 'events_cat');
							//If the terms array contains items... (dupe of core)
							if ( !empty($terms) ) {
								//Loop through terms
								foreach ( $terms as $term ){
									//Add tax name & link to an array
									$post_terms[] = esc_html(sanitize_term_field('name', $term->name, $term->term_id, '', 'edit'));
								}
								//Spit out the array as CSV
								echo implode( ', ', $post_terms );
							} else {
								//Text to show if no terms attached for post & tax
								echo '<em>No terms</em>';
							}
							break;
			case 'eventthumbnail':
							echo the_post_thumbnail(array(100,100));
							break;
			case 'eventsdate':
							echo get_post_meta(get_the_ID(),'event_date',TRUE);
							break;						
			case 'starttime':
							$starttime=get_post_meta(get_the_ID(),'event_starttime',TRUE);
							echo date('g:i a', strtotime($starttime)); 
							break;
			case 'endtime':
							$endtime=get_post_meta(get_the_ID(),'event_endtime',TRUE);
							echo date('g:i a', strtotime($endtime)); 
							break;
		}
	}
	add_action('manage_posts_custom_column', 'manage_events_columns', 10, 2);
	add_filter('manage_edit-events_columns', 'events_columns');
?>