<?php
	//Testimonial type
	//--------------------------------------------------------
	function testimonial_register() {
		$labels = array(
			'name'				=> _x('Testimonials','ATP_ADMIN_SITE'),
			'singular_name'		=> _x('Testimonial','ATP_ADMIN_SITE'),
			'add_new'			=> _x('Add New', 'ATP_ADMIN_SITE'),
			'add_new_item'		=> __('Add Testimonial','ATP_ADMIN_SITE'),
			'edit_item'			=> __('Edit Testimonial','ATP_ADMIN_SITE'),
			'new_item'			=> __('New Item','ATP_ADMIN_SITE'),
			'view_item'			=> __('View Testimonial Item','ATP_ADMIN_SITE'),
			'search_items'		=> __('Search Testimonial Item','ATP_ADMIN_SITE'),
			'not_found'			=> __('Nothing found','ATP_ADMIN_SITE'),
			'not_found_in_trash'=> __('Nothing found in Trash','ATP_ADMIN_SITE'),
			'parent_item_colon'	=> '',
			'all_items' =>  __( 'All Testimonials' ,'ATP_ADMIN_SITE')
		);
	
		$args = array(
			'labels'			=> $labels,
			'public'			=> true,
			'exclude_from_search'=> false,
			'show_ui'			=> true,
			'capability_type'	=> 'post',
			'hierarchical'		=> false,
			'rewrite'			=> array( 'with_front' => false ),
			'query_var'			=> false,	
			'menu_icon'			=> THEME_URI . '/framework/admin/images/testimonial-icon.png',  		
			'supports'			=> array('page-attributes','editor','title','thumbnail'),
			'taxonomies'		=> array('testimonial_cat', 'post_tag')
		); 
		register_post_type( 'testimonialtype' , $args );
	}
	
	register_taxonomy("testimonial_cat", 'testimonialtype', array(
		'hierarchical'		=> true,
			'labels' => array(
							'name' => _x( 'Testimonial Categories', 'taxonomy general name' ),
							'singular_name' => _x( 'Testimonials', 'taxonomy singular name' ),
							'search_items' =>  __( 'Search Testimonials' ),
							'all_items' => __( 'All Testimonials' ),
							'parent_item' => __( 'Parent Testimonials' ),
							'parent_item_colon' => __( 'Parent Testimonials:' ),
							'edit_item' => __( 'Edit Testimonials' ),
							'update_item' => __( 'Update Testimonials' ),
							'add_new_item' => __( 'Add Testimonial Category' ),
							'new_item_name' => __( 'New Testimonials ' ),
							'menu_name' => __( 'Testimonial Categories' ),
						),
		'show_ui'			=> true,
		'query_var'			=> true,
		'rewrite'			=> false,
	));
		
	add_action('init', 'testimonial_register');
	
	function testimonial_columns($columns) {
		$columns['testimonial_cat'] = __('Category','ATP_ADMIN_SITE');
		$columns['thumbnail'] =  __('Post Image','ATP_ADMIN_SITE');
	
		return $columns;
	}
	
	function manage_testimonial_columns($name) {
		global $post, $wp_query;
		switch ($name) {
			case 'testimonial_cat':
							$terms = get_the_terms($post->ID, 'testimonial_cat');
							//If the terms array contains items... (dupe of core)
							if ( !empty($terms) ) {
								//Loop through terms
								foreach ( $terms as $term ){
									//Add tax name & link to an array
									$post_terms[] = esc_html(sanitize_term_field('name', $term->name, $term->term_id, '', 'edit'));
								}
								//Spit out the array as CSV
								echo implode( ', ', $post_terms );
							} else {
								//Text to show if no terms attached for post & tax
								echo '<em>No terms</em>';
							}
							break;
			case 'thumbnail':
							echo the_post_thumbnail(array(100,100));
							break;
		}
	}
	add_action('manage_posts_custom_column', 'manage_testimonial_columns', 10, 2);
	add_filter('manage_edit-testimonial_columns', 'testimonial_columns');
	
	/**
	* Excludes categories for custom post type tags archive
	*/
	add_filter('pre_get_posts', 'query_testimonialpost_type');

	function query_testimonialpost_type($query) {
		if ( is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
			$post_type = get_query_var('post_type');
			if($post_type)
			$post_type = $post_type;
		else
			$post_type = array('post','testimonial'); 
			$query->set('post_type',$post_type);
			return $query;
		}
	}
?>