<?php
	// P O R T F O L I O   T Y P E
	//--------------------------------------------------------
	function portfolio_register() {
		$labels = array(
			'name'				=> __('Portfolios','ATP_ADMIN_SITE'),
			'singular_name'		=> __('Portfolio','ATP_ADMIN_SITE'),
			'add_new'			=> __('Add New', 'ATP_ADMIN_SITE'),
			'add_new_item'		=> __('Add New Portfolio','ATP_ADMIN_SITE'),
			'edit_item'			=> __('Edit Portfolio','ATP_ADMIN_SITE'),
			'new_item'			=> __('New Item','ATP_ADMIN_SITE'),
			'view_item'			=> __('View Portfolio Item','ATP_ADMIN_SITE'),
			'search_items'		=> __('Search Portfolio Item','ATP_ADMIN_SITE'),
			'not_found'			=> __('Nothing found','ATP_ADMIN_SITE'),
			'not_found_in_trash'=> __('Nothing found in Trash','ATP_ADMIN_SITE'),
			'parent_item_colon'	=> '',
			'all_items' =>  __( 'All Portfolios' ,'ATP_ADMIN_SITE')
		);
	
		$args = array(
			'labels'			=> $labels,
			'public'			=> true,
			'exclude_from_search'=> false,
			'show_ui'			=> true,
			'capability_type'	=> 'post',
			'hierarchical'		=> false,
			'rewrite'			=> array( 'with_front' => false ),
			'query_var'			=> false,	
			'menu_icon'			=> THEME_URI . '/framework/admin/images/portfolio-icon.png',  		
			'supports'			=> array('title', 'editor', 'excerpt', 'thumbnail', 'comments', 'page-attributes'),
			'taxonomies' => array('portfolio_cat', 'post_tag')
		); 
		register_post_type( 'portfoliotype' , $args );
	}
	
	register_taxonomy("portfolio_cat", 'portfoliotype', array(
		'hierarchical'		=> true,
			'labels' => array(
							'name' => __( 'Portfolio Categories', 'taxonomy general name' ),
							'singular_name' => __( 'Portfolios', 'taxonomy singular name' ),
							'search_items' =>  __( 'Search Portfolio','ATP_ADMIN_SITE'),
							'all_items' => __( 'All Portfolios','ATP_ADMIN_SITE'),
							'parent_item' => __( 'Parent Portfolios' ,'ATP_ADMIN_SITE'),
							'parent_item_colon' => __( 'Parent Portfolios:' ,'ATP_ADMIN_SITE'),
							'edit_item' => __( 'Edit Portfolios','ATP_ADMIN_SITE'),
							'update_item' => __( 'Update Portfolios','ATP_ADMIN_SITE'),
							'add_new_item' => __( 'Add Portfolio Category','ATP_ADMIN_SITE'),
							'new_item_name' => __( 'New Portfolios ','ATP_ADMIN_SITE'),
							'menu_name' => __( 'Portfolio Categories' ,'ATP_ADMIN_SITE'),
						),
		'show_ui'			=> true,
		'query_var'			=> true,
		'rewrite'			=> false,
	));
		
	add_action('init', 'portfolio_register');
	
	function portfoliotype_columns($columns) {


$columns = array(
    'cb'       => '<input type="checkbox" />',
 	'title'       => __('Title','ATP_ADMIN_SITE'),
	'author'       => __('Author','ATP_ADMIN_SITE'),
	'thumbnails'       => __('Thumbnails','ATP_ADMIN_SITE'),
 	'portfolio_cat'       => __('Categories','ATP_ADMIN_SITE'),
	'tags'       => __('Tags','ATP_ADMIN_SITE'),
	'comments'       => '<div class="vers"><img alt="Comments" src="' . esc_url( admin_url( 'images/comment-grey-bubble.png' ) ) . '" /></div>',
    'date'       => __('Date','ATP_ADMIN_SITE'),
 	
  );
  return $columns;
	}
	
	function manage_portfoliotype_columns($name) {
		global $post, $wp_query;
		switch ($name) {
			case 'portfolio_cat':
						$terms = get_the_terms($post->ID, 'portfolio_cat');
						//If the terms array contains items... (dupe of core)
						if ( !empty($terms) ) {
							//Loop through terms
							foreach ( $terms as $term ){
								//Add tax name & link to an array
								$post_terms[] = esc_html(sanitize_term_field('name', $term->name, $term->term_id, '', 'edit'));
							}
							//Spit out the array as CSV
							echo implode( ', ', $post_terms );
						} else {
							//Text to show if no terms attached for post & tax
							echo '<em>No terms</em>';
						}
						break;
					case 'thumbnails':
							echo the_post_thumbnail(array(100,100));
							break;
		
		}
	}
	
	add_action('manage_posts_custom_column', 'manage_portfoliotype_columns', 10, 2);
	add_filter('manage_edit-portfoliotype_columns', 'portfoliotype_columns');
	
	/**
	* Excludes categories for custom post type tags archive
	*/
	add_filter('pre_get_posts', 'query_post_type');
		function query_post_type($query) {
			if ( $query->is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
				$post_type = get_query_var('post_type');
				if($post_type)
				$post_type = $post_type;
			else
				$post_type = array('post','portfoliotype'); 
				$query->set('post_type',$post_type);
				return $query;
			}
		}
?>