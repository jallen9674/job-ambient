<?php

	class atpgenerator {

		// P R I M A R Y   M E N U 
		//--------------------------------------------------------
		function atp_primary_menu() {
			if (has_nav_menu( 'primary-menu' ) ) {
				wp_nav_menu(array(
					'container'     => false, 
					'theme_location'=> 'primary-menu',
					'menu_class'    => 'sf-menu sf-mobilemenu',
					'menu_id'       => 'atp_menu', 
					'echo'          => true, 
					'before'        => '', 
					'after'         => '',
					'link_before'   => '', 
					'link_after'    => '', 
					'depth'         => 0,
					'walker'        => new description_walker()
					));
			}
		}

		// L O G O   G E N E R A T O R
		//--------------------------------------------------------
		function logo(){
			$atp_logo = get_option('atp_logo'); 
			if($atp_logo == 'logo'){ ?>
				<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>">
					<img src="<?php echo get_option('atp_header_logo'); ?>" alt="<?php bloginfo('name'); ?>" />
				</a>
			<?php 
			}else { ?>
				<h1 id="site-title"><span><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?>	</a></span></h1>
				<h2 id="site-description"><?php echo bloginfo( 'description' ); ?></h2>
			<?php 
			} 
		}
		
		// 	TESER FRONT PAGE
		//--------------------------------------------------------
		function teaser_option() {
				$out = '';
			if(get_option('atp_teaser_frontpage') != "on") {
				$out .= '<div class="frontpage_teaser">';
				$out .= '<div class="frontpage_teasercontent">';
				$out .= do_shortcode( stripslashes( get_option( 'atp_teaser_frontpage_text' ) ) );
				$out .= '</div>';
				$out .= '</div>';			
			}
			echo $out;
		}
		
		// S I D E B A R   P O S I T I O N S 
		//--------------------------------------------------------
		function sidebaroption($postid) {
			// Get sidebar class and adds sub class to pagemid block layout

			$sidebaroption=get_post_meta($postid, "sidebar_options", TRUE) ? get_post_meta($postid, "sidebar_options", TRUE):'rightsidebar';
			switch($sidebaroption){
				case  'rightsidebar':
						$sidebaroption="rightsidebar";
						break;
				case  'leftsidebar':
						$sidebaroption="leftsidebar";
						break;
				case  'fullwidth':
						$sidebaroption="fullwidth";
						break;
				default:
						$sidebaroption="rightsidebar";
			}

		
			if(is_archive() || is_search()) {
				$sidebaroption = 'rightsidebar';
			}
			if (is_404() || is_page_template('template_sortable.php') ||  is_tax() || is_singular('portfoliotype')) {
				$sidebaroption = 'fullwidth';
			}
		
			return $sidebaroption;
		}
		
		// C U S T O M   L O C A L I Z A T I O N 
		//--------------------------------------------------------
		function language_text($custom_txt) {
			$custom_txt = get_option($custom_txt);
			return $custom_txt;
		}

		/***
		 * P O S T   L I N K   T Y P E 
		 *--------------------------------------------------------
		 * atp_getPostLinkURL - generates URL based on link type
		 * @param - string link_type - Type of link 
		 * @return - string URL
		 * 
		 */
		function atp_getPostLinkURL($link_type) {
			global $post;
			
			//use switch to generate URL based on link type
			switch($link_type) {
				case 'linkpage':
						return get_page_link(get_post_meta($post->ID, 'link_page', true));
						break;
				case 'linktocategory':
						return get_category_link(get_post_meta($post->ID, 'link_cat', true));
						break;
				case 'linktopost':
						return get_permalink(get_post_meta($post->ID, 'link_post', true));
						break;
				case 'linkmanually':
						return get_post_meta($post->ID, 'link_manually', true);
						break;
				case 'default':
						return get_permalink($post->ID);
				case 'nolink':
						return;
						break;
				default :
						return;
						break;
			}
		}
		
		/**
		 * P O S T   A T T A C H M E N T S 
		 *--------------------------------------------------------
		 * getPostAttachments - displays post attachements 
		 * @param - int post_id - Post ID
		 * @param - string size - thumbnail, medium, large or full
		 * @param - string attributes - thumbnail, medium, large or full
		 * @param - int width - width to which image has be revised to
		 * @param - int height - height to which image has be revised to
		 * @return - string Post Attachments
		 */
		 
		function getPostAttachments($postid=0, $size='thumbnail', $attributes='',$width,$height,$postlinkurl) {
			global $post;
				//get the postid
				if ($postid<1) $postid = get_the_ID();
				
				//variables
				$rel = $out = '';
						
				//get the attachments (images)
				$images = get_children(array(
					'post_parent'    => $postid,
					'post_type'      => 'attachment',
					'order'          => 'DESC',
					'numberposts'    => 0,
					'post_mime_type' => 'image'));
					
				//if images exists	, define/determine the relation for lightbox
				if(count($images) >1) {
					$rel = '"group'.$postid.'"';
				}else{
					$rel='""'; 
				}
				$rel = ' rel='.$rel;
				//if images exists, loop through and prepare the output
				if($images) {
				$out .='<div class="flexslider">';
				$out .='<ul class="slides">';
					//loop through
					foreach($images as $image) {
						$full_attachment = wp_get_attachment_image_src($image->ID, 'full');
							if( !empty( $image->ID ) )
						$alt=get_the_title( $image->ID );
						$out .='<li>';
						$out.='<figure><a>';
						$out .= atp_resize('',$full_attachment['0'],$width,$height,'imageborder',$alt);
						$out.='</a></figure>';
						$out .='</li>';
					}//loop ends
					$out .='</ul>';
					$out .='</div><div class="clear"></div>';
				} else { //if images does not exists
					$post_thumbnail_id = get_post_thumbnail_id($postid);
					$full_attachment = wp_get_attachment_image_src($post_thumbnail_id,'full');
							if( !empty($post_thumbnail_id) )
					$alt=get_the_title($post_thumbnail_id);
						$out.='<figure><a href="'.$postlinkurl.'">';
						$out.=atp_resize('',$full_attachment['0'],$width,$height,'imageborder',$alt);
						$out.='</a></figure>';
				}// if images exists		
			return $out; 
		}
		
		/**
		 * P O S T A T T A C H M E N T S
		 *--------------------------------------------------------
		 * getPostAttachments - displays post attachements
		 * @param - int post_id - Post ID
		 * @param - string size - thumbnail, medium, large or full
		 * @param - string attributes - thumbnail, medium, large or full
		 * @param - int width - width to which image has be revised to
		 * @param - int height - height to which image has be revised to
		 * @param - int postclass - postclass
		 * @return - string Post Attachments
		 */

		function getPortfolioAttachments($postid=0, $size='thumbnail', $attributes='',$width,$height,$postlinkurl,$postclass) {
			global $post;
			//get the postid
			if ($postid<1) $postid = get_the_ID();
			//variables
			$rel = $out = '';

			//get the attachments (images)
			$images = get_children(array(
				'post_parent' => $postid,
				'post_type' => 'attachment',
				'order' => 'DESC',
				'numberposts' => 0,
				'post_mime_type' => 'image'));

			//if images exists , define/determine the relation for lightbox
			if(count($images) >1) {
				$rel = '"group'.$postid.'"';
			}else{
				$rel='""';
			}
			$rel = ' rel='.$rel;
			//if images exists, loop through and prepare the output
			if($images) {
				$out .='<div id="'.$postclass.'" class="portfolioflexslider">';
				$out .='<ul class="slides">';
				//loop through
				foreach($images as $image) {
					$full_attachment = wp_get_attachment_image_src($image->ID, 'full');
					if( !empty( $image->ID ) )
					$alt=get_the_title( $image->ID );
					$out .='<li>';
					$out.='<figure><a>';
					$out .= atp_resize('',$full_attachment['0'],$width,$height,'imageborder',$alt);
					$out.='</a></figure>';
					$out .='</li>';
				}//loop ends
				$out .='</ul>';
				$out .='</div><div class="clear"></div>';
			} else { //if images does not exists
				$post_thumbnail_id = get_post_thumbnail_id($postid);
				$full_attachment = wp_get_attachment_image_src($post_thumbnail_id,'full');
				if( !empty($post_thumbnail_id) )
				$alt=get_the_title($post_thumbnail_id);
				$out.='<figure><a href="'.$postlinkurl.'">';
				$out.=atp_resize('',$full_attachment['0'],$width,$height,'imageborder',$alt);
				$out.='</a></figure>';
			}// if images exists
			return $out;
		}
		// CUSTOM TWITTER TWEETS
		//--------------------------------------------------------
		function atp_customtwitter()
		{
			if ( function_exists( 'twitter_parse_cache_feed' ) ) {
				twitter_parse_cache_feed(array(
					'username'				=> get_option('atp_teaser_twitter'),
					'limit'					=> '1',
					'encode_utf8'			=> '',
					'twitter_cons_key'		=> get_option('atp_consumerkey'),
					'twitter_cons_secret'	=> get_option('atp_consumersecret'),
					'twitter_oauth_token'	=> get_option('atp_accesstoken'),
					'twitter_oauth_secret'	=> get_option('atp_accesstokensecret')
				));
			}
		}
		
		
		// S U B H E A D E R 
		//--------------------------------------------------------
		function subheader($postid){
			$sub_option			= get_post_meta($postid, "subheader_teaser_options", true);
			$username			= get_option('atp_teaser_twitter');
			$page				= get_post($postid);
			$subheadercolor		= get_post_meta($postid, "subheader_bg_color", true);
			$subheaderbgcolor	= $subheadercolor ? $subheadercolor : '' ;
			$subheader_image	= get_post_meta($postid,'subheader_img',true);
			$subheaderimage		= $subheader_image ? ' url('.$subheader_image.') center bottom' : '';
			$subheader_props	= 'style="background:'.$subheaderbgcolor.$subheaderimage.'"';

			if (is_page() || (is_single()) || (is_singular('portfoliotype')) || (is_front_page() && $postid != NULL) || (is_home() && $postid != NULL)){
				$subdesc='';
				$title='';
				switch($sub_option) {
					case 'custom':
							$title = $page->post_title;
							$subdesc = stripslashes(get_post_meta($postid, "page_desc", true));
							break;
					case 'twitter':
							$title = $page->post_title;
							ob_start();							
							$subdesc= atp_generator('atp_customtwitter');
							$subdesc .= ob_get_clean();	
							wp_reset_query();
							break;
					case 'default':
							if(get_option('atp_teaser') == 'twitter') : 
								$title = $page->post_title;
								ob_start();							
								$subdesc= atp_generator('atp_customtwitter');
								$subdesc .= ob_get_clean();
							elseif(get_option('atp_teaser') == 'default') :
								$title = $page->post_title;
							elseif(get_option('atp_teaser') == 'disable') :
							else :
								$title = $page->post_title;
							endif;
							break;
					case 'customhtml':
							$subdesc = do_shortcode(get_post_meta($postid, "custom_html", true));
							wp_reset_query();;
							break;
					default:
							$title = $page->post_title;
				}
			}
			
			
			// iF IS  is_single   
			//--------------------------------------------------------
			if (is_single()) {
				$title = get_option('atp_postsinglepage') ? get_option('atp_postsinglepage') : 'Blog';
			}
			
			if(is_singular( 'portfoliotype' ) ){
				$title = $page->post_title;
			}

			if(is_singular( 'events' ) ){
			
				$terms = get_the_terms( $postid,'events_cat');
				if(!empty($terms)){
					foreach ( $terms as $term ) {
						$term_links[]=$term->name;
					}
					$title = join( ', ', $term_links );
				}
			}

			// iF IS  PAGE   4 0 4 
			//--------------------------------------------------------
			if (is_404()) {
				$title = get_option('atp_error404txt') ? get_option('atp_error404txt') : '404 Error Page';
			}
			
			// iF IS  PAGE   4 0 4 
			//--------------------------------------------------------
			if (is_home()) {
				$title = __('Blog','THEME_FRONT_SITE');
			}
		
			// IF IS  A R C H I V E
			//--------------------------------------------------------
			if(is_archive()) {
			
			$title = __('Archives','THEME_FRONT_SITE');

			if ( is_category() || is_tag() || is_tax() ) {
					global $wp_query; 
				$taxonomy_archive_query_obj = $wp_query->get_queried_object();
				$title = $taxonomy_archive_query_obj->name; // Taxonomy term name
			}
				

				if ( is_day() ) :
							$subdesc=sprintf( __( 'Daily Archives: %s', 'THEME_FRONT_SITE' ), '<span>' . get_the_date() . '</span>' ); 
					elseif ( is_month() ) : 
							$subdesc= sprintf( __( 'Monthly Archives: %s', 'THEME_FRONT_SITE' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'THEME_FRONT_SITE' ) ) . '</span>' ); 
					elseif ( is_year() ) : 
							$subdesc=sprintf( __( 'Yearly Archives: %s', 'THEME_FRONT_SITE' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'THEME_FRONT_SITE' ) ) . '</span>' ); 
					else :
							
					endif;
			}
			// iF IS TAG PAGE  
			//--------------------------------------------------------
			if (is_tag()) {
				$title = sprintf( __( 'Tag Archives: %s', 'THEME_FRONT_SITE' ), '<span>' . single_tag_title( '', false ) . '</span>' );
			}
			// IF IS  S E A R C H
			//--------------------------------------------------------
			if(is_search()) {
				$title = __('Search Results','THEME_FRONT_SITE');
			}
			if(is_author()) {
				$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
				$subdesc = sprintf(__('Author Archives: %s', 'THEME_FRONT_SITE'),$curauth->nickname);
				}
			// C U S T O M   S U B H E A D E R   P R O P E R T I E S 			
			//--------------------------------------------------------
			if($subheaderbgcolor OR $subheaderimage) {
				$subheader_props = 'style="background:'.$subheaderbgcolor.$subheaderimage.'"';
			}else{
				$subheader_props = '';
			}
			$out = '<section id="subheader" '.$subheader_props.'>';
		
			if(get_option('atp_teaser') != 'disable' ) {
				
					if($sub_option!="disable"){
					$out .= '<div class="inner">';
					$out .= '<div class="subheader">';
						if($title !='') {
							$out .= '<div class="subtitle"><h1><span>'.$title.'</span></h1>';
							$out .='</div>';

						}
						
							$out .= '<div class="subdec">';
							if(isset($subdesc) && $subdesc !='') {
							$out .= $subdesc;
							};
							$out .= '</div>';
						
						$out .= '</div>';
						$out .= '</div>';
					}
				
			}
			
			$out .= '</section>';
			return $out;
		}

		// P O S T   F O R M A T   M E D I A   T Y P E
		//--------------------------------------------------------
		// Embed (Media) Player  to play audio/video or call slideshow or just embed image
		/*
		function embed_media($postid,$type,$width=1020) {
			switch($type) {
			
				case 'video' :
						$height = get_post_meta($postid, 'video_height', true);
						$m4v = get_post_meta($postid, 'video_m4v', true);
						$ogv = get_post_meta($postid, 'video_ogv', true);
						$poster = get_post_meta($postid, 'video_poster', true);
						include(THEME_DIR.'/jPlayer_mediatype.php');
						break;
				case 'audio' :
						$mp3 = get_post_meta($postid, 'audio_mp3', TRUE);
						$ogg = get_post_meta($postid, 'audio_ogg', TRUE);
						$poster = get_post_meta($postid, 'audio_poster', TRUE);
						$height = get_post_meta($postid, 'poster_height', TRUE);
						if($mp3 or $ogg or $poster or $height) {
							include(THEME_DIR.'/jPlayer_mediatype.php');
						}
						break;
			}
		}
		*/
		
		// P O S T   M E T A   D A T A 
		//--------------------------------------------------------
		function postmetaStyle() {
			ob_start();
			$out = '<div class="post-meta">'; 
			echo '<div class="postdate">'; 
			echo '<span class="month">';
			the_time("M"); 
			echo '</span>';
			echo '<span class="day">';
			the_time("j"); 
			echo '</span>';
			echo '</div>';
			echo '<div class="postedby">';
			echo the_author_posts_link(); 
			echo '</div>';
			echo '<div class="postedin">';
			echo the_category(', ');
			echo '</div>';
			echo '<div class="comments">';
			comments_popup_link( __( '0 Comment', 'THEME_FRONT_SITE' ), __( '1 Comment', 'THEME_FRONT_SITE' ), __( '% Comments', 'THEME_FRONT_SITE' ) );
			echo '</div>';
			$out .= ob_get_clean();
			$out .= '</div>';
			return $out;
		}
		
		// B R E A D C R U M B S 
		//--------------------------------------------------------
		
		function my_breadcrumb() {
			global $breadcrumb_separator;
			if ( function_exists( 'breadcrumbs_plus' ) ){
				breadcrumbs_plus(array(
					'prefix'	=> '',
					'suffix'	=> '',
					'title'		=> false,
					'home'		=> __( 'Home', 'THEME_FRONT_SITE' ),
					'separator'	=> $breadcrumb_separator,
					'front_page'=> false,
					'bold'		=> false,
					'blog'		=> __( 'Blog', 'THEME_FRONT_SITE' ),
					'show_blog' => false,
					'echo'		=> true,
					'singular_portfoliotype_taxonomy'	=> 'portfolio_cat',
					'singular_events_taxonomy'	=> 'events_cat',
					'singular_testimonialtype_taxonomy'	=> 'testimonial_cat'
				));
			}
		}
		
		function breadcrumb($postid){
			$breadcrumb = get_post_meta($postid,'breadcrumb','true');
			if( $breadcrumb != 'on') { (get_option('atp_breadcrumbs') != 'on') ? atp_generator('my_breadcrumb'):''; }
		}

		
		// A B O U T   A U T H O R 
		//--------------------------------------------------------
		function aboutauthor(){?>
			<div id="entry-author-info">
				<div class="author_entry">
					<div class="author-avatar">
						<?php echo get_avatar(get_the_author_meta('email'), $size = '80', $default=''); ?>
					</div>
					<div class="author-description">
						<h5><?php the_author_posts_link(); ?></h5>
						<p><?php the_author_meta('description'); ?></p>
					</div>
				</div>
			</div>
		<?php 
		} 
	
		// R E L A T E D   P O S T S 
		//--------------------------------------------------------
		function relatedposts($postid) {

		//Variables
		global $wpdb,$post;

		$tags = wp_get_post_tags($postid);
		if ($tags) {
			$tag_ids = array();
			foreach($tags as $individual_tag) 
			$tag_ids[] = $individual_tag->term_id;
			if(is_singular( 'portfoliotype' ) ){
			$args=array(
				'tag__in'			=> $tag_ids,
				'post__not_in'		=> array($post->ID),
				'post_type'			=>'portfoliotype',
				'showposts'			=>4, // Number of related posts that will be shown.
				'ignore_sticky_posts'	=>1
			);
			}else{
				$args=array(
				'tag__in'			=> $tag_ids,
				'post__not_in'		=> array($post->ID),
				'showposts'			=>4, // Number of related posts that will be shown.
				'ignore_sticky_posts'	=>1
			);
			}
			$related_post_found = "true";
			wp_reset_query();
			$my_query = new wp_query($args);
			
			if( $my_query->have_posts() ) { 
				
				echo '<div class="singlepostlists"><h5>'. __("Related Posts", "THEME_FRONT_SITE").'</h5><ul>';
				while ($my_query->have_posts()) {
					$my_query->the_post();
			
					echo "<li>";
					if (has_post_thumbnail($post->ID) ){
						$thumb = get_post_thumbnail_id($post->ID); 	

						$post_thumbnail_id = get_post_thumbnail_id($post->ID);
					$thumbnail=wp_get_attachment_image_src($post_thumbnail_id,'120x70');
					?>
					<a href="<?php echo get_permalink($post->ID); ?>" class="thumb" title="<?php the_title(); ?>">
					<?php echo atp_resize($post->ID,'','150','70','imageborder',''); ?>
					</a>
					<?php }	else { ?>
					<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" class="thumb">
						<img class="thinframe" src="<?php echo THEME_URI; ?>/images/no-image.jpg"  alt="" width="120" height="70" />
					</a>
					<?php } ?>
					<?php // echo '<span class="spdate">'.get_the_date().'</span>'; ?>
					<a href="<?php echo get_permalink($post->ID); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?></a>
				</li>
				<?php
				}
				echo '</ul>';
				echo '</div>';
			}
		}
		wp_reset_query();
	?>	
	<div class="clear"></div>
	<?php }
		
	
	}
	// end class
	
	/**
	 * Description Walker Class for Custom Menu
	 */
	class description_walker extends Walker_Nav_Menu {
	 function start_el(&$output, $object, $depth = 0, $args = array(), $current_object_id = 0){
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $object->classes ) ? array() : (array) $object->classes;
		$class=array();
		//$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $object ) );
		$class_names=apply_filters( 'nav_menu_css_class', array_filter( $classes ));
		foreach($class_names as $key=>$values){
				if($key!='0')
				{
				$class[].=$values;
				}

		}
		$custommneu_class = join( ' ',$class);
		$class_menu = ' class="'. esc_attr( $custommneu_class ) . '"';
		$output .= $indent . '<li id="menu-item-'. $object->ID . '"' . $value . $class_menu.'>';

		$attributes  = ! empty( $object->attr_title ) ? ' title="'  . esc_attr( $object->attr_title ) .'"' : '';
		$attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target     ) .'"' : '';
		$attributes .= ! empty( $object->xfn )        ? ' rel="'    . esc_attr( $object->xfn        ) .'"' : '';
		$attributes .= ! empty( $object->url )        ? ' href="'   . esc_attr( $object->url        ) .'"' : '';

		$prepend = '';
		$append = '';
		$description  = ! empty( $object->attr_title ) ? '<span class="msubtitle">'.esc_attr( $object->attr_title ).'</span>' : '';

		if($depth != 0){
			 $description = $append = $prepend = "";
		}

		$object_output = $args->before;
		$object_output .= '<a'. $attributes .'>';
		if($classes['0']!=''){
		$object_output .='<i class=' .$classes['0'].'></i>';
		}
		$object_output .= $args->link_before .$prepend.apply_filters( 'the_title', $object->title, $object->ID ).$append;
		$object_output .= $description.$args->link_after;
		$object_output .= '</a>';
		$object_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $object_output, $object, $depth, $args );
		}
	}

	// W R I T E   G E N E R A T O R
	//--------------------------------------------------------
	function atp_generator($function){
	//http:www//php.net/manual/en/function.call-user-func-array.php
	//http://php.net/manual/en/function.func-get-args.php	
		global $_atpgenerator;
		$_atpgenerator = new atpgenerator;
		$args = array_slice( func_get_args(), 1 );
		return call_user_func_array(array( &$_atpgenerator, $function ), $args );
	}
	// C U S T O M   E X C E R P T   L E N G T H
	//--------------------------------------------------------
	function excerpt($num) {
		$link = get_permalink();
		$ending = '...';
		$limit = $num+1;
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).$ending;
		echo $excerpt;
	}
	// C U S T O M   C O M M E N T   T E M P L A T E 
	//--------------------------------------------------------
	
	function atp_custom_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'THEME_FRONT_SITE' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( 'Edit', 'THEME_FRONT_SITE' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>" class="comment_wrap">
			<header class="comment-meta comment-author vcard">
				<?php
					echo get_avatar( $comment, 44 );
					printf( '<cite class="fn">%1$s %2$s</cite>',
						get_comment_author_link(),
						// If current post author is also comment author, make it known visually.
						( $comment->user_id === $post->post_author ) ? '<span> ' . __( 'Post author', 'THEME_FRONT_SITE' ) . '</span>' : ''
					);
					printf( '<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
						esc_url( get_comment_link( $comment->comment_ID ) ),
						get_comment_time( 'c' ),
						/* translators: 1: date, 2: time */
						sprintf( __( '%1$s at %2$s', 'THEME_FRONT_SITE' ), get_comment_date(), get_comment_time() )
					);
				?>
			</header><!-- .comment-meta -->
			<section class="comment-content comment single_comment">
				<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'THEME_FRONT_SITE' ); ?></p>
				<?php endif; ?>

				
					<?php comment_text(); ?>
					<?php edit_comment_link( __( 'Edit', 'THEME_FRONT_SITE' ), '<p class="edit-link">', '</p>' ); ?>
				

				<div class="reply">
					<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'THEME_FRONT_SITE' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
				</div><!-- .reply -->
			</section><!-- .comment-content -->
		</div><!-- #comment-## -->
	<?php
		break;
	endswitch; // end comment_type check
	} 
?>