<?php
	// REGISTER AND ENQUEUE SCRIPTS
	//--------------------------------------------------------
	function enqueue_scripts() {
		wp_register_script('atp-sf-hover', THEME_JS .'/hoverIntent.js','jquery','','in_footer');
		wp_register_script('atp-sf-menu', THEME_JS .'/superfish.js','jquery','','in_footer');
		wp_register_script('atp-gmap', 'http://maps.google.com/maps/api/js?sensor=false','jquery','2.0','in_footer');
		wp_register_script('atp-gmapmin', THEME_JS . '/jquery.gmap.js', 'jquery','2.0','in_footer');
		wp_register_script('atp-jgalleria', THEME_JS .'/src/galleria.js','jquery','','in_footer');
		wp_register_script('atp-jgclassic', THEME_JS .'/src/themes/classic/galleria.classic.js','jquery','','in_footer');
		wp_register_script('atp-contact', THEME_JS .'/atp_form.js','jquery','', 'in_footer');
		wp_register_script('atp-jcarousel', THEME_JS .'/jquery.jcarousel.min.js', 'jquery','','in_footer');
		wp_register_script('atp-flexslider', THEME_JS .'/jquery.flexslider.js', 'jquery','','in_footer');	
		wp_register_script('atp-isotope-core', THEME_JS .'/jquery.isotope.min.js','jquery','','in_footer');
		wp_register_script('atp-isotope', THEME_JS .'/isotope.js', 'jquery','','in_footer');
		wp_register_script('atp-ei-slider', THEME_JS .'/jquery.eislideshow.js', 'jquery','','in_footer');
		wp_register_script('atp-custom', THEME_JS . '/sys_custom.js', 'jquery','1.0','in_footer');
		// Enqueue Scripts
		//--------------------------------------------------------
		wp_enqueue_script('jquery');
		wp_enqueue_script('atp-easing', THEME_JS .'/jquery.easing.1.3.js','jquery','','in_footer');
		wp_enqueue_script('atp-sf-hover');
		wp_enqueue_script('atp-sf-menu');
		wp_enqueue_script('atp-preloader', THEME_JS .'/jquery.preloadify.min.js','jquery','','in_footer');
		wp_enqueue_script('atp-prettyPhoto', THEME_JS .'/jquery.prettyPhoto.js','jquery','','in_footer');
		wp_enqueue_script('atp-fitvids', THEME_JS .'/jquery.fitvids.js','jquery','','in_footer');
		wp_enqueue_script('atp-custom');
		wp_enqueue_script('atp-flexslider');
		wp_enqueue_script('atp-ei-slider');
		wp_localize_script( 'jquery', 'atp_panel', array(
			'SiteUrl' =>get_stylesheet_directory_uri()
		));		
		// enqueue scripts in homepage only
		//--------------------------------------------------------
		wp_register_style( 'responsive', THEME_CSS . '/responsive.css', array(), '1', 'all' ); 
		wp_register_style( 'flexslider', THEME_CSS . '/flexslider.css', array(), '1', 'all' ); 			
		
		// E N Q U E U E   S T Y L E S 
		//--------------------------------------------------------
		wp_enqueue_style( 'stream-style', get_stylesheet_uri());
		wp_enqueue_style( 'prettyphoto', THEME_CSS.'/prettyPhoto.css', false, false, 'all');
		wp_enqueue_style( 'font-awesome', THEME_CSS.'/fortawesome/font-awesome.css',false,false,'all');
		wp_enqueue_style( 'eislideshow', THEME_CSS.'/eislideshow.css', false, false, 'all');
		wp_enqueue_style( 'shortcodes', THEME_CSS.'/shortcodes.css', false, false, 'all');
		wp_enqueue_style( 'responsive' );
		wp_enqueue_style( 'datepicker', FRAMEWORK_URI.'admin/css/datepicker.css', false, false, 'all');
		if (is_singular( 'portfoliotype')){
			wp_enqueue_style( 'portfolio-flexslider', THEME_CSS.'/portfolio_flexslider.css', false, false, 'all');
		}else{
			wp_enqueue_style('flexslider');
		}

		if ( is_singular() ){
			wp_enqueue_script( 'comment-reply' );
		}
		if(get_option('atp_style') != '0'){
			$atp_style=get_option('atp_style');
			wp_enqueue_style('atp-style', THEME_URI.'/colors/'.$atp_style, false, false, 'all');
		}
		wp_enqueue_style('atp-nivoslider',THEME_URI.'/css/nivo-slider.css','','','all' );
	}  
	add_action( 'wp_enqueue_scripts','enqueue_scripts');
	
	// E N Q U E U E   S C R I P T S   I N   A D M I N 
	//--------------------------------------------------------
	function admin_enqueue_scripts(){
		wp_enqueue_script('theme-script',FRAMEWORK_URI . '/admin/js/script.js');
		wp_enqueue_script('theme-colorpicker',FRAMEWORK_URI . '/admin/js/colorpicker.js');
		wp_enqueue_script('theme-shortcode',FRAMEWORK_URI . '/admin/js/shortcode.js');
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_localize_script( 'theme-script', 'atp_panel', array(
			'SiteUrl' =>get_stylesheet_directory_uri()
		));
		wp_enqueue_script('atp-chosen', FRAMEWORK_URI . 'admin/js/chosen.jquery.min.js');
		wp_enqueue_style('thickbox');
		wp_enqueue_style('atpadmin', FRAMEWORK_URI . 'admin/css/atpadmin.css');
		wp_enqueue_style('appointment-style', FRAMEWORK_URI.'admin/css/datepicker.css', false, false, 'all');
		wp_enqueue_style( 'atp-chosen', FRAMEWORK_URI . 'admin/css/chosen.css', false, false, 'all' );
	}
	
	add_action( 'admin_enqueue_scripts', 'admin_enqueue_scripts' );
?>