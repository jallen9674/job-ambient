<?php
	// P R O G R E S S B A R
	//--------------------------------------------------------
	function sys_progressbar( $atts, $content = null ){
		extract(shortcode_atts(array(
			'progress'	=> '',
			'title'		=>'',
			'color'		=> ''        
		), $atts));
		
		$progressvalue  = $progress   ? ' width:'.$progress.'%':'';
		$barcolor 		= $color  ? 'background-color:'.$color.';':'';
		
		$extras =	( $barcolor!==''||$progressvalue!='' ) ? ' style="'.$barcolor.'"':'';

		$out = '<div class="progress_container">';
		$out .= '<div  data_width="'.$progress.'%" class="progress_bar" '.$extras.'>';
		$out .= '<span>'.$title.'</span>';
		$out .= '<span class="percentage">'.$progress.'%</span>';
		$out .= '</div>';
		$out .= '</div>';
	
		return $out;
	}
	add_shortcode('progressbar', 'sys_progressbar');
?>