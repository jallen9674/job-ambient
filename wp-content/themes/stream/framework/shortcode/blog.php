<?php

	// B L O G 
	//--------------------------------------------------------
	function sys_blog ($atts, $content = null) {
		extract(shortcode_atts(array(
			'cat'		=> '2',
			'limit'		=> '5',
			'image'		=> 'true',
			'pagination'=> 'true',
			'imgheight'	=> '',
			'charlimits'=> '200',
			'postmeta'	=> ''
		), $atts));

		global $sidebaroption, $readmoretxt, $post;

		$column=3;
		/*
		@parm $blogstyle: blogstyle1,blogstyle2,blogstyle3
		*/
		$post_id = $post->ID;
		ob_start();
		$out='';
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		query_posts("cat=$cat&posts_per_page=$limit&paged=$paged");
		if (have_posts()) : while (have_posts()) : the_post();
			// post title
			$blogtitle=get_the_title();
			$width = ($sidebaroption == 'fullwidth') ? '600' : '400';
			$format = get_post_format( $post_id ); 
			$out .= '<article id="post-'.get_the_ID().'" class="' . join( ' ', get_post_class( 'post2', $post_id ) ) . '">';
			$out .= '<a href="'.get_permalink($post_id).'"><span class="postformat-'.$format.'"></span></a>';
			$out .= '<h2 class="entry-title"><a  href="'.get_permalink($post_id).'" >'.$blogtitle.'</a></h2>';
			if($postmeta=="true") {
				$out .= atp_generator(postmetaStyle); 
			}
			if ($image =="true"){
				$out .= postmetaformatStyle($post_id);
			}

			$out .= '<p>'.wp_html_excerpt(get_the_excerpt(''),$charlimits).'</p>';
			$out .= '<a  href="'.get_permalink($post_id).'" >'.$readmoretxt.'</a>';
			$out .= '<div class="clear"></div></article>';

			endwhile;

			if( $pagination == "true") {
				if(function_exists('atp_pagination')) {
					$out .= atp_pagination(); 
				}
			}
		endif;

		$out .= ob_get_contents();
		ob_end_clean();
		wp_reset_query();
		return $out;
		
	} //end sys_blog function
	add_shortcode('blog','sys_blog');

	// P O S T   M E T A   S T Y L E 
	//--------------------------------------------------------

	if ( ! function_exists( 'postmetaStyle' ) ) :
		
	endif;
		if ( ! function_exists( 'postmetaformatStyle' ) ) :
		function postmetaformatStyle($post_id) {
			ob_start();
		 $format = get_post_format( $post_id ); 
			if( false === $format ) { $format = 'standard'; }
			$out = get_template_part( 'includes/' . $format ); 
			$out .= ob_get_clean();
			return $out;
		} 
	endif;
	
?>