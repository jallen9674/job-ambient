<?php
	//   
	//--------------------------------------------------------
	function sys_events_list ($atts, $content = null) {
		extract(shortcode_atts(array(
		'cat'			=> '',
		'title'			=>'',
		), $atts));

		global $post, $readmoretxt, $locationtxt, $starttimetxt, $endtimetxt;
		$eventcarousel_id=rand(10,99);
		wp_enqueue_script('atp-jcarousel');
		
		$out='<script type="text/javascript">
		/* <![CDATA[ */
		jQuery(document).ready(function() {
				jQuery("#jcarousel'.$eventcarousel_id.'").jcarousel({
				easing: "easeInOutExpo",
				scroll: (jQuery(window).width() > 767 ? 4 : 1),
				animation: 1000
			});
			
		});	

		jQuery(window).resize(function() {
		   var el = jQuery("#jcarousel'.$eventcarousel_id.'"),
		   	carousel = el.data("jcarousel"), win_width = jQuery(window).width();
						    	   var visibleItems = (win_width > 767 ? 4 : 1);
						    	   carousel.options.visible = visibleItems;
						    	   carousel.options.scroll = visibleItems;
						    	   carousel.reload();
		});
		/* ]]> */
		</script>';
		$out .= '<div class="list_jcarousel">';
		$out .= '<h3 class="carousel-title">'.$title.'&nbsp;</h3>';
		$out .= '<ul id="jcarousel'.$eventcarousel_id.'">';
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	
		$query = array(
			'post_type'			=> 'events', 
			'posts_per_page'	=> -1, 
			'taxonomy'			=> 'events_cat', 
			'term'				=> $cat, 
			'paged'				=> $paged,
			'order'				=> 'DESC'
		);

		query_posts($query); //get the results
		if (have_posts()) : while (have_posts()) : the_post();
			$blogtitle=get_the_title();
			$post_date = get_the_time('M  j   Y', get_the_id());
			$event_date=get_post_meta($post->ID, "event_date", true);
			$event_starttime=get_post_meta($post->ID, "event_starttime", true);
			$event_endtime=get_post_meta($post->ID, "event_endtime", true);
			$event_location=get_post_meta($post->ID,"event_location",true);
			$date_event=explode('-',$event_date);	
			$imagesrc = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );
			// post title
			
				$out .= '<li class="one_fourth">';
				$out .= '<div class="events">';
				$out .= '<div class="event_meta">';								
				$out .= '<span class="month">'.$date_event['1'].'</span>';
				$out .= '<span class="day">'.$date_event['0'].'</span>';										
				$out .= '</div>';
				$out .= '<div class="event_entry">';
				$out .= '<h4><a href="' .get_permalink($post->ID). '" >'.$blogtitle.'</a></h4>';
				$out .= '<p>';
				if(isset($event_starttime) && $event_starttime !='') {
					$out .= '<strong>'.$starttimetxt.'</strong>: '.date('g:i a', strtotime($event_starttime)).'<br>';
				}
				if(isset($event_endtime) && $event_endtime !='') {
					$out .= '<strong>'.$endtimetxt.'</strong>: '.date('g:i a', strtotime($event_endtime)).'<br>';
				}
				if(isset($event_location) && $event_location !='') {
					$out .= '<strong>'.$locationtxt.'</strong>: '.$event_location.'<br>';
				}
				$out .= '</p>';
				$out .= '</div>';
				$out .= '</div>'; 
				$out .= '</li>'; 
			
			endwhile;
		endif;
			
		$out .= '</ul>';
		//$out .= '<div class="clearfix"></div>';
		$out .= '</div>';
		wp_reset_query();
			
		return $out;
	} //end caroufredsel_slider function

	add_shortcode('events_list','sys_events_list');
?>