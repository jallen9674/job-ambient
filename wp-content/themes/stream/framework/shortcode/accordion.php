<?php
	// Accordion
 	//--------------------------------------------------------
	function atp_accordion_group( $atts, $content ){
		extract(shortcode_atts(array(
		), $atts));
	
		$GLOBALS['accordion_count'] = 0;
		do_shortcode( $content );
		
		$out= '<div id="accordion'.rand(1,9).'" class="ac_wrap">';
		if( is_array( $GLOBALS['accordions'] ) ){
			foreach( $GLOBALS['accordions'] as $accordion ){ 

				$key=uniqid();
				$out .= '<div class="ac_title" id="#tab'.$key.'" >'.$accordion['title'].'</div>';
				$out .= '<div class="ac_content" id="tab'.$key.'">'.do_shortcode($accordion['content']).'</div>';
			}
			
		}
		$out .= '</div>';
			unset( $GLOBALS['accordions'], $GLOBALS['accordion_count'] );
		return $out;
	}
	// Accordion
	//--------------------------------------------------------
	function atp_accordion( $atts, $content ){
		extract(shortcode_atts(array(
			'title'		=> 'accordion %s',
		), $atts));

		$x = $GLOBALS['accordion_count'];
		$GLOBALS['accordions'][$x] = array(
			'title'		=> sprintf( $title, $GLOBALS['accordion_count'] ),		
			'content'	=>  $content 
		);
		$GLOBALS['accordion_count']++;
	}
	add_shortcode( 'accordioncontent', 'atp_accordion_group' );
	add_shortcode( 'accordion', 'atp_accordion' );
?>