<?php

	// C O N T A C T   F O R M 
	//--------------------------------------------------------
	function atp_contact_form( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'emailid'      => '',
			'successmessage'      => '',
		), $atts));
	
		$name_str	= __('Name','THEME_FRONT_SITE');
		$email_str	= __('Email','THEME_FRONT_SITE');
		$id=rand(1,100);
		
		
		$my_string		= rand_string( 5 );
		$captcha_str	= __('Captcha *','ATP_ADMIN_SITE');
		$submit_str		= __('Submit','ATP_ADMIN_SITE');

		wp_print_scripts('atp-contact');
		global $wpdb;

		$out = '<form action="'.THEME_URI.'/framework/includes/submitform.php"  class="contactform"  method="post">';
		$out .= '<div id="response" class="response'.$id.'"></div>';
		$out .= '<div class="syswidget sysform sc" id="validate_form">';
		$out .= '<p><input type="text" name="contact_'.$id.'name" id="contact_'.$id.'name" class="txtfield input_medium" value="'.__('Name', 'THEME_FRONT').'"  onblur="if (this.value == \'\') {this.value = \'' . __('Name', 'THEME_FRONT') . '\';}" onfocus="if (this.value == \'' . __('Name', 'THEME_FRONT') . '\') {this.value = \'\';}"></p>';
		$out .= '<p><input type="text" name="contact_'.$id.'email" id="contact_'.$id.'email" class="txtfield input_medium" value="'.__('E-mail', 'THEME_FRONT').'"  onblur="if (this.value == \'\') {this.value = \'' . __('E-mail', 'THEME_FRONT') . '\';}" onfocus="if (this.value == \'' . __('E-mail', 'THEME_FRONT') . '\') {this.value = \'\';}"></p>';
		$out .= '<p><textarea name="contactcomment'.$id.'" id="contactcomment'.$id.'" class="required input_medium" value="'.__('Message', 'THEME_FRONT').'" onfocus="if(this.value == \''.__('Message', 'THEME_FRONT').'\') {this.value = \'\'}" onblur="if (this.value == \'\') {this.value = \''.__('Message', 'THEME_FRONT').'\'}">Message</textarea></p>';
		$out .= '<p><input type="text" name="contact_captcha'.$id.'" id="contact_captcha'.$id.'" class="txtfield input_medium" value="'.__('Captcha', 'THEME_FRONT').'"  onblur="if (this.value == \'\') {this.value = \'' . __('Captcha', 'THEME_FRONT') . '\';}" onfocus="if (this.value == \'' . __('Captcha', 'THEME_FRONT') . '\') {this.value = \'\';}"></p>';
		$out .= '<p><label><span class="atpcaptcha">'.$my_string.'</span> - case sensitive</label></p>';
		$out .= '<input type="hidden" name="contact_ccorrect'.$id.'" id="contact_ccorrect'.$id.'"  value="'.$my_string.'">';
		$out .= '<p><button type="button" value="submit" name="contactsubmit" id="contactsubmit" class="button small gray"><span>'.__('Submit','THEME_FRONT').'</span></button></p>';
		$out .= '<p><input type="hidden" name="contact_'.$id.'widgetemail" id="contact_'.$id.'widgetemail"  value="'.$emailid.'"></p>';
		$out .= '<p><input type="hidden" name="contact_id" id="contact_id"  value="'.$id.'"></p>';
		
		$out .= '<p><input type="hidden" name="contact_success'.$id.'"  id="contact_success'.$id.'" value="'.$successmessage.'"></p>';
		$out.='</div>';
		$out .= '</form>';
		return $out;
	}
	add_shortcode('contactform', 'atp_contact_form');
	function rand_string( $length ) {
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$size = strlen( $chars );
			$str='';
			for( $i = 0; $i < $length; $i++ ) {
				$str .= $chars[ rand( 0, $size - 1 ) ];
			}
			return $str;
		}
?>