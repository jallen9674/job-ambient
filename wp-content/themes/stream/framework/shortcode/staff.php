<?php
	// STAFF BOX
	//--------------------------------------------------------
	function sys_staff ($atts, $content= null){
		extract(shortcode_atts(array(
			'photo'		=> '',
			'title'		=> '',
			'blogger'	=> '',
			'delicious'	=> '',
			'digg'		=> '',
			'facebook'	=> '',
			'flickr'	=> '',
			'forrst'	=> '',
			'google'	=> '',
			'html5'		=> '',
			'lastfm'	=> '',
			'linkedin'	=> '',
			'paypal'	=> '',
			'picasa'	=> '',
			'pinterest'	=> '',
			'rss'		=> '',
			'skype'		=> '',
			'stumbleupon'=> '',
			'technorati'=> '',
			'twitter'	=> '',
			'twitter3'	=> '',
			'dribbble'	=> '',
			'vimeo'		=> '',
			'windows'	=> '',
			'wordpress'	=> '',
			'yahoo'		=> '',
			'yelp'		=> '',
			'youtube'	=> ''
		), $atts));
		
		global $staff_social;

		$before_staff = $after_staff = $out = '';
		
		$out .= '<div class="bio">';
			if( $photo != '' ){
				$image_photo = atp_resize('',$photo,'200','200','imageborder','');
				$out .= '<figure>'.$image_photo.'</figure>';	
			}
		$out .= '<div class="details bio_meta">';
			if( $title != '' ){
				$out .= '<hgroup class="head"><h4>'.$title.'</h4></hgroup>';	
			}
			if( $content != '' ){
				$out .= $content;
			}
		$out .= '</div>';	
		$count=0;
		foreach( $staff_social as $key => $value ){
			if($key !=''){
				if( $$key != '' ){
					if( $count<1 ){
						$before_staff = '<div class="sociables"><ul class="atpsocials">';
						$after_staff = '</ul></div>';
					}
					$count++;
				}
			}
		}
		
		$out .= $before_staff;
		foreach( $staff_social as $key => $value )	{ 
			if($key !=''){
				if( $$key != '' ){
					$out .= '<li><a class="'.$key.'" href="'.$$key.'"></a></li>';	
				}
			}
		}
		$out .= $after_staff;
		$out .= '</div><div class="clear"></div>';
		
		return $out;
	}
	add_shortcode('staff', 'sys_staff');
?>