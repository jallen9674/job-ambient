<?php

	// Testimonials
	//--------------------------------------------------------
	function sys_tesimonial ($atts, $content = null) {
		extract(shortcode_atts(array(
			'cat'		=> '',
		), $atts));
		global $post;
		
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$query = array(
			'post_type'			=> 'testimonialtype', 
			'posts_per_page'	=> -1, 
			'taxonomy'			=> 'testimonial_cat', 
			'term'				=> $cat, 
			'paged'				=> $paged,
			'order'				=> 'DESC'
		);
		query_posts($query); //get the results
		$rand = uniqid(1,100);
		echo'<script type="text/javascript">
		/* <![CDATA[ */
			jQuery(document).ready(function() {
				jQuery(window).load(function() {
					jQuery(".testimonial'.$rand.'").MySlider(4000);
				});
			});
		/* ]]> */
		</script>';
		$out = '<div id="testimonial'.$rand.'" class="testimonial_list"><ul class="testimonials">';
		if (have_posts()) : while (have_posts()) : the_post();

		$website_name				= get_post_meta($post->ID, 'websitename', true);
		$website_url				= get_post_meta($post->ID, 'website_url', true);
		$testimonial_image_option	= get_post_meta($post->ID, 'testimonial_image_option',true);
		$testimonial_content		= get_the_content($post->ID);
		switch( $testimonial_image_option ){
			case 'gravatar':
							$testimonial_email=get_post_meta($post->ID,'testimonial_email',true);
							$testimonial_gravatar_image=get_avatar($testimonial_email,50);
							break;
			case 'customimage':
							$testimonial_image=get_post_meta($post->ID,'testimonial_photo',true);
							$testimonial_gravatar_image=atp_resize('',$testimonial_image,'50','50','','');
							break;
		}
		$after = '';
		$before = '';
		// post title
		$out .= '<li>';
		if( $testimonial_content != '' ){
			$out .= '<div class="feedback">'.$testimonial_content.'</div>';
		}

		if( $testimonial_gravatar_image != '' ){
			$out .= '<div class="clientimg">'.$testimonial_gravatar_image.'</div>';
		}	
		if( $website_url != '' ){
			$after = '<a href="'.esc_url($website_url).'" target="_blank">';
			$before = '</a>';
		}
			$clientname = '<span>'.get_the_title().'</span>';
		if( $website_name != '' ){
			$out .= '<div class="clientmeta">'.$clientname.$after.$website_name.$before.'</div>';
		}
	
	
		$out .= '</li>'; 
		endwhile;
		endif;
		$out .= '</ul>';
		$out .= '</div><div class="clear"></div>';
		//$out .= '<button id="prev">Previous</button><button id="next">Next</button>';
		wp_reset_query();
		return $out;
		
	} //end caroufredsel_slider function
	add_shortcode('testimonial','sys_tesimonial');
	
?>