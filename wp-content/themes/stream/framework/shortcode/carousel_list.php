<?php
	// Carousel List
	//--------------------------------------------------------
	function sys_carousel_list ($atts, $content = null) {
		extract(shortcode_atts(array(
			'cat'		=> '',
			'port_cat'	=> '',
			'limit'		=> '-1',
			'title'		=> '',
			'charlimits'=> '200',
		), $atts));

		global $readmoretxt, $post;
		$jcarousel_id = rand(10,99);
		wp_enqueue_script('atp-jcarousel');
		$events_order = get_option( 'atp_eventsorder' ) ? get_option( 'atp_eventsorder' ) : 'id';
		$out = '<script type="text/javascript">
		/* <![CDATA[ */
		jQuery(document).ready(function() {
			jQuery("#jcarousel'.$jcarousel_id.'").jcarousel({
			easing: "easeInOutExpo",
				scroll: (jQuery(window).width() > 767 ? 4 : 1),
				animation: 1000
			});
	
			
		});	
		jQuery(window).resize(function() {
			var el = jQuery("#jcarousel'.$jcarousel_id.'"),
			carousel = el.data("jcarousel"), win_width = jQuery(window).width();
			var visibleItems = (win_width > 767 ? 4 : 1);
			carousel.options.visible = visibleItems;
			carousel.options.scroll = visibleItems;
			carousel.reload();
		});

		/* ]]> */
		</script>';
		$out .= '<div class="list_jcarousel">';
		$out .= '<h3 class="carousel-title">'.$title.'&nbsp;</h3>';
		$out .= '<ul id="jcarousel'.$jcarousel_id.'">';
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		if( $port_cat != '' ){
			$query = array(
				'post_type'			=> 'portfoliotype', 
				'posts_per_page'	=> $limit, 
				'taxonomy'			=> 'portfolio_cat', 
				'term'				=> $port_cat, 
				'paged'				=> $paged,
				'order'				=> 'DESC',
				'orderby'			=> $events_order,
			);
			query_posts($query); //get the results
		}
		if( $cat != '' ){
			query_posts("cat=$cat&posts_per_page=$limit&paged=$paged");
		}
	
		if (have_posts()) : while (have_posts()) : the_post();
			$blogtitle = get_the_title();
			$post_date = get_the_time('M  j   Y', get_the_id());
			$imagesrc = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );
			// post title
			if( has_post_thumbnail($post->ID)){
				$out .= '<li class="one_fourth">';
				$out .= '<div class="post_list">';
				$out .= '<div class="sort_img"><figure>';
				$out .= atp_resize($post->ID,'','450','290','','');
				$out .= '</figure>';
				$out .= '<div class="hover_type">';
				$out .= '<a class="hoverlink" href="' .get_permalink($post->ID). '" title="' . $blogtitle . '"></a>';
				$out .= '<a data-rel="prettyPhoto" class="hoverimage" href="' . $imagesrc['0'] . '" title="' . $blogtitle . '"></a>';
				$out .= '</div>';
				$out .= '</div>';
				$out .= '<div class="content">';
				$out .= '<h5><a href="' .get_permalink($post->ID). '" >'.$blogtitle.'</a></h5>';
				$out .= '</div>'; 
				$out .= '</div>'; 
				$out .= '</li>'; 
			}
			
			endwhile;
		endif;
			
		$out .= '</ul>';
		$out .= '</div>';
		wp_reset_query();
			
		return $out;
	} 

	add_shortcode('carousel_list','sys_carousel_list');
?>