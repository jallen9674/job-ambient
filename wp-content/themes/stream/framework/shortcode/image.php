<?php
	// I M A G E  
	//--------------------------------------------------------
	function sys_image($atts, $content = null) {
		extract(shortcode_atts(array(
			'link'		=> '#',
			'lightbox'	=> 'false',
			'title'		=> '',
			'class'		=> '',
			'align'		=> false,
			'width'		=> false,
			'height'	=> false,
			'caption'	=>'',
			'target'	=>'',
			'src'		=> '',
		), $atts));
		
		if(!$width||!$height){
			if(!$width){
				$width = '';
			}
			if(!$height){
				$height = '';
			}
		}
		
	
		$no_link = $rel = $out = '';
		$content= atp_resize('',$src,$width,$height,$class,$title);
		if($lightbox == 'true'){
			if(preg_match_all('!http://.+\.(?:jpe?g|png|gif)!Ui',$link,$matches)){
			$src=$link;
			}
			$rel = ' data-rel="prettyPhoto"';
			$rel .= ' class="lightbox"';
			$link = $src;
		}else{
			if($link == '#'){$no_link = 'image_no_link';}
			$target = ' target="'.$target.'"';
		}
		if($lightbox=="true") {
			$out.='<div><span class="'.($align?' align'.$align:'').'" ><a  '.$target.''.$rel.' '.($no_link? ' class="'.$no_link.'"':'').' title="'.$title.'" href="'.$link.'">' . $content;
			$out.='</a>';
			if($caption){
			$out.='<span class="image_caption">'.$caption.'</span>';
			}
			$out.='</span></div>';
		} else {
			$out.='<span class="'.($align?' align'.$align:'').'" >' ;
			if($link!=''){
			if(preg_match_all('!http://.+\.(?:jpe?g|png|gif)!Ui',$link,$matches)){
			$link="#";
			}
			$out.='<a  '.$target.''.$rel.' '.($no_link? ' class="'.$no_link.'"':'').' title="'.$title.'" href="'.esc_url($link).'">';
			}
			$out.=$content;
			if($link!=''){
			$out.='</a>';
			}
			
			if($caption){
			$out.='<span class="image_caption">'.$caption.'</span>';
			}
			$out.='</span>';
		}
		
		return $out;
	}
	add_shortcode('image', 'sys_image');
	?>