<?php
	// P O R T F O L I O 
	//--------------------------------------------------------
	function port_portfolio ($atts, $content = null,$code) {

		global $wp_filter, $post, $wpdb, $wp_query, $permalink ,$post_title , $post_date;

		//variables
		$nopage_str = __('Apologies, but no results were found. Perhaps searching will help find a related Portfolio.', 'THEME_FRONT_SITE');
		$hover_class = ''; //stores class name of the postlink based on postlinktype(image/video/link)
		$visitsitetxt = get_option( 'atp_visitsitetxt' ) ? get_option( 'atp_visitsitetxt' ) : 'Visit Site';
		$portfolio_order = get_option( 'atp_portfolio_order' ) ? get_option( 'atp_portfolio_order' ) : 'id';

		$the_content_filter = $wp_filter['the_content'];
			extract(shortcode_atts(array(
				'cat'			=> '',
				'limit'			=> '-1',
				'columns'		=> '4',
				'readmoretext'	=> '',
				'title'			=> '',
				'pagination'	=> '',
				'charlimits'	=> '60',
				'readmore' 		=> '',
				'imgheight'		=> '',
				'desc'			=> '',	
				'sidebar'		=> '',
				'sortable'		=> ''
			), $atts)); 
			
		if( $columns == '4' ) { $class = "one_fourth";	$columns_id = "fourth"; }
		if( $columns == '3' ) { $class = "one_third";	$columns_id = "third";	}
		if( $columns == '2' ) { $class = "one_half";	$columns_id = "second";	}
		if( $columns == '1' ) { $class = "fullwidth";	$columns_id = "one";	}

		if( $sidebar == "true" ) {
			// Portfolio Image Sizes With Sidebar Layout
			if( $columns == '4' ) { $width="140"; $height = $imgheight ? $imgheight : 200 ; }
			if( $columns == '3' ) { $width="450"; $height = $imgheight ? $imgheight : 150 ; }
			if( $columns == '2' ) { $width="315"; $height = $imgheight ? $imgheight : 250 ; }
			if( $columns == '1' ) { $width="440"; $height = $imgheight ? $imgheight : 200 ; }
		} else {
			//Full Width Portfolio Image Sizes
			if( $columns == '4' ) { $width="450"; $height = $imgheight ? $imgheight : 200 ; }
			if( $columns == '3' ) { $width="450"; $height = $imgheight ? $imgheight : 250 ; }
			if( $columns == '2' ) { $width="450"; $height = $imgheight ? $imgheight : 250 ; }
			if( $columns == '1' ) { $width="650"; $height = $imgheight ? $imgheight : 300 ; } 
		}
		if($readmoretext == '') {
		$readmoretext = get_option('atp_readmoretxt') ? get_option('atp_readmoretxt') : 'Readmore';
		}
		$out = '';
		$column_index = 0; //used to identify the column number and move the futher results to next row

		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$query = array(
			'post_type'			=> 'portfoliotype', 
			'posts_per_page'	=> $limit, 
			'taxonomy'			=> 'portfolio_cat', 
			'term'				=> $cat, 
			'paged'				=> $paged,
			'order'				=> 'DESC',
			'orderby'			=> $portfolio_order,
		);

		query_posts($query); //get the results

		if( $sortable != 'true' ){
		echo '<script type="text/javascript">
				/* <![CDATA[ */
				jQuery(document).ready(function($) {
				atp_hoverimage();
				});	
				/* ]]> */
				</script>';
		}
		if( $sortable == 'true' ){
			wp_print_scripts('atp-isotope-core');
			wp_print_scripts('atp-isotope');
			$out .= '<ul id="options" class="clearfix splitter"><li>';
			$out .= '<ul id="filters" class="option-set clearfix" data-option-key="filter">';
			$out .= '<li><a class="all selected" href="#show-all" data-option-value="*">All</a></li>';
			if( $cat ){
				$portfoliocategory = @explode(",", $cat);
				for( $i = 0; $i < count($portfoliocategory); $i++ ){
					$terms[] = get_term_by('slug', $portfoliocategory[$i], 'portfolio_cat');
				}
				foreach( $terms as $term ) {
					$out .= '<li><a class="'.$term->slug.'" data-option-value=".' . $term->slug . '" href="#filter">' . $term->name . '</a></li>';
				}
			}else{
				ob_start();
				wp_list_categories(array(
					'title_li' => '',
					'taxonomy' => 'portfolio_cat',
					'walker'   => new Portfolio_Walker()
				));
				$out .= ob_get_clean();
		    }
			$out .= '</ul></li></ul>';
			$out .= '<div class="clear"></div>';
			$out .= '<div class="sortable sort_column'.$columns.'">';
		}else{
			$out .= '<div class="portfolio_items">';
		}


		// Columns Switch Case
		switch($columns){
			
			// 1 Column 
			case '1':
					$count = 0;
					if( $sortable == "true" ) {
						$out.='<div id="list" class="image-grid">';
					}
					if(have_posts()) : while(have_posts()) : the_post();
						// get post type link url
						$postlinkurl = "";
						$visit_link = get_post_meta(get_the_id(), 'visit_link',true);
						$project_date = get_post_meta(get_the_id(),"project_date",true);
						$project_desc = get_post_meta(get_the_id(),"project_desc",true);

						//Get Term Slug for defining class
						$terms = get_the_terms(get_the_ID(), 'portfolio_cat');
						$terms_slug = array();
						if (is_array($terms)) {
							foreach($terms as $term) {
								$terms_slug[] = $term->slug;
							}
						}

						//get the post details
						$post_title = get_the_title(get_the_id());
						$permalink = get_permalink(get_the_id());
						$post_title = get_the_title(get_the_id());
						if( $project_date != '' ) {
							$post_date = $project_date;
						}else{
							$post_date = get_the_date();
						}
						$column_index++;
						$last = ($column_index == $columns && $columns != 1) ? 'last ' : '';
						$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), 'full', true);

						if( $sortable == "true" ){
							$out .= '<div class="'.$class.'  '. $last.' item  element images  '.implode('  ', $terms_slug).'">';
						}else{
							$out .= '<div class="item">'; 
						}

						$out .= '<div class="two_third">';
						$out .= '<div class="port_img sort_img"><figure>';
						$out .= atp_resize($post->ID,'',$width,$height,'imageborder','');
						$out .= '<div class="hover_type">';
						$out .= '<a class="hoverlink"  href="' .$permalink. '" title="' . get_the_title() . '"></a>';
						$out .= '<a data-rel="prettyPhoto"  class="hoverimage" href="' . $image['0'] . '" title="' . get_the_title() . '"></a>';
						$out .= '</div>';
						$out .= '</figure></div>';
						$out .= '</div>';

						// two third end -->
						$out .= '<div class="one_third last"><div class="port_desc col1">';
						if($title=="true"){	
							$out .= '<h1><a href="' .$permalink. '">'. $post_title. '</a></h1>';
						}
						$out .= '<p class="date">'.$post_date.'</p>';
						if ($project_desc) {
							$out .= '<p>'.do_shortcode($project_desc).'</p>';
						} else {
							$out .= '<p>'.wp_html_excerpt(get_the_excerpt(''),$charlimits).'</p>';
						}
						if($readmore == "true" && $readmoretext !=''){
							$out .= '<a class="button small black" href="' .$permalink. '"><span>'.$readmoretext.'</span></a>';
						}
						$out .= '</div></div></div>';
						$out .= '<div class="clear"></div>';
						// divider after each item
						$count++;
					endwhile;
					else :
						$out .= '<h2>'.$nopage_str.'</h2>';
						endif; 
						if($sortable=="true") {
							$out .= '</div>';
						}
						if($pagination == "true"){ 
							$out .= atpPortfolioPagination();
						} 
						$out .= '</div><div class="clear"></div>';// end of portfolio item block
					
						wp_reset_query();
						$wp_filter['the_content'] = $the_content_filter;
						return $out;
						break;
			// Column 2,3,4
			case '2':
			case '3':
			case '4':
			
					//if have posts, loop through posts
					$count='0';
					// Sortable Portfolio
					//================================================
					if($sortable=="true") {
						$out.='<div id="list" class="image-grid">';
					}
					if(have_posts()) : while(have_posts()) : the_post(); 
			
					// get post type link url
					$postlinkurl = "";
					$visit_link = get_post_meta(get_the_id(), 'visit_link',true);
					$postlinktype_options = get_post_meta(get_the_id(), 'postlinktype_options',true);
					$portfoliotype_options = get_post_meta(get_the_id(),"port_posttype_option",true);
					$project_date = get_post_meta(get_the_id(),"project_date",true);
					$project_desc = get_post_meta(get_the_id(),"project_desc",true);
					
					$terms = get_the_terms(get_the_ID(), 'portfolio_cat');
					$terms_slug = array();
					if (is_array($terms)) {
						foreach($terms as $term) {
							$terms_slug[] = $term->slug;
						}
					}
					
					//get the post details
					$post_title = get_the_title(get_the_id());
					$permalink = get_permalink(get_the_id());
					$post_title = get_the_title(get_the_id());
					if( $project_date !='' ) {
						$post_date = $project_date;
					}else{
						$post_date = get_the_time('M / j / Y', get_the_id());
					}
					$column_index++;
					$last = ( $column_index == $columns && $columns != 1 ) ? 'last ' : '';
					$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), 'full', true);
					// Normal Portfolio
					//================================================
					if($sortable != 'true')
					{
						$out .= '<div class="'.$class.' '. $last.'">'; 
						$out .= '<div class="portfoliogrid">';
						$out .= '<div class="port_img"><figure>';
						$out .= atp_resize( $post->ID,'',$width,$height,'imageborder','' );
						$out .= '</figure>';
						$out .= '<div class="hover_type">';
						$out .= '<a class="hoverlink"  href="' .$permalink. '" title="' . get_the_title() . '"></a>';
						$out .= '<a data-rel="prettyPhoto" class="hoverimage"   href="' . $image['0'] . '" title="' . get_the_title() . '"></a>';
						$out .= '</div>';
						$out .= '</div>';
						
						if( $desc == "true" ){
						$out .= '<div class="port_desc">';
							if( $title == "true" ) {
							$out .= '<h4><a href="' .$permalink. '">'. $post_title. '</a></h4>'; 
							}
							if ( $project_desc ) {
								$out .= '<p class="sort_desc">'.wp_html_excerpt( do_shortcode( $project_desc ), $charlimits ) .'</p>';
							} else {
								$out .= '<p class="sort_desc">'.wp_html_excerpt( get_the_excerpt(''),$charlimits ).'</p>';
							}
							$out .= '<p>';
							if( $readmore == "true" ){
								$out .= '<a class="left" href="' .$permalink. '">'.$readmoretext.'</a>&nbsp;';
								}
								$out .= '</p>';	
							$out.='</div>';	
						}
					
					}
					if($sortable =='true')
					{
					$out .= '<div class="'.$class.'  '. $last.' item  element images  '.implode('  ', $terms_slug).'">';
							$out .= '<div class="port_img sort_img"><figure>';
							$out .= atp_resize( $post->ID,'',$width,$height,'','' );
							$out .= '</figure>';
							$out .= '<div class="hover_type">';
							$out .= '<a class="hoverlink"  href="' .$permalink. '" title="' . get_the_title() . '"></a>';
							$out .= '<a data-rel="prettyPhoto" class="hoverimage"   href="' . $image['0'] . '" title="' . get_the_title() . '"></a>';
							$out .= '</div>';
							$out .='</div>';

							if( $desc == "true" ) {
								$out .= '<div class="port_desc">';
								if( $title == "true" ) {
									$out .= '<h5><a href="' .$permalink. '">' .$post_title. '</a></h5>'; 
								}
								$out .= '</div>';
							}
							// portfolio desc end
					}
					
					
					if($sortable!="true") {
					$out .= '</div>';
					}					
					$out .= '</div>';	
					if( $column_index == $columns ){
						$column_index = 0;
						$out .= '';
					}
					$count++;
					endwhile;
					else :
					$out .= '<h2>'.$nopage_str.'</h2>';
					endif; 
					if($sortable=="true") {
					$out.='</div>';
					}
					$out .= '</div><div class="clear"></div>';
					if($pagination == "true"){ 
						$out .= atpPortfolioPagination(); 
					} 
					wp_reset_query();
					$wp_filter['the_content'] = $the_content_filter;
					return $out;
					break;
		}
	}//end of function

	add_shortcode('portfolio','port_portfolio'); //add shortcode

	/***
	 * atpPortfolioPagination - displays pagination 
	 * @param - int range determines a current page range and displays.
	 * @param - int pages 
	 * @return - string pagination
	 * 
	 */
	function atpPortfolioPagination($range =2,$pages = '') { 
		 
		 global $paged,$wp_query;
		 $showitems = ($range * 2)+1;       
		 if(empty($paged)) $paged = 1;
		 if($pages == ''){
			 $pages = $wp_query->max_num_pages;
			 if(!$pages){
				 $pages = 1; 
			}
		 }   
		$out = '';
		 //if pages more than one
		 if(1 != $pages){
			 $out ='<div class="pagination wp-pagenavi">
					<span class="pages extend">
						Page '.$paged.'  of  '.$pages.
					'</span>';
			 if($paged > 2 && $paged > $range+1 && $showitems < $pages) $out.='<a href="'.get_pagenum_link(1).'">&laquo;</a>';
			 if($paged > 1 && $showitems < $pages) $out.='<a href="'.get_pagenum_link($paged - 1).'">&lsaquo;</a>';

			 for ($i=1; $i <= $pages; $i++)
			 {
				 if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
				 {
					 $out.=($paged == $i)? '<span class="current">'.$i.'</span>':'<a href="'.get_pagenum_link($i).'" class="inactive" >'.$i.'</a>';
				 }
			 }

			 if ($paged < $pages && $showitems < $pages) $out.='<a href="'.get_pagenum_link($paged + 1).'">&rsaquo;</a>';  
			 if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) $out.='<a href="'.get_pagenum_link($pages).'">&raquo;</a>';
			 $out.= '</div>';
		 }
		return $out;
	}//end of function atpPortfolioPagination
	
	// Portfolio Walker
	class Portfolio_Walker extends Walker_Category {
		
			function start_el(&$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {
			extract($args);

			$cat_name = esc_attr( $object->name);
			$cat_name = apply_filters( 'list_cats', $cat_name, $object );
			$link = '<a class="'.strtolower(preg_replace('/\s+/', '-', $cat_name)).'" data-value="'.strtolower(preg_replace('/\s+/', '-', $cat_name)).'"  href="#" ';
			if ( $use_desc_for_title == 0 || empty($object->description) )
				$link .= 'title="' . sprintf(__( 'View all posts filed under %s','ATP_ADMIN_SITE' ), $cat_name) . '"';
			else
				$link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $object->description, $object ) ) ) . '"';
				$link .= '>';

			if ( isset($show_count) && $show_count )
				$link .= $cat_name . ' (' . intval($object->count) . ')</a>';
			else
				$link .= $cat_name . '</a>';
			if ( (! empty($feed_image)) || (! empty($feed)) ) {
				$link .= ' ';
			if ( empty($feed_image) )
				$link .= '(';
				$link .= '<a href="' . get_category_feed_link($object->term_id, $feed_type) . '"';

			if ( empty($feed) )
				$alt = ' alt="' . sprintf(__( 'Feed for all posts filed under %s','ATP_ADMIN_SITE'), $cat_name ) . '"';
			else {
				$title = ' title="' . $feed . '"';
				$alt = ' alt="' . $feed . '"';
				$name = $feed;
				$link .= $title;
			}

			$link .= '>';
			if ( empty($feed_image) )
				$link .= $name;
			else
				$link .= "<img src='$feed_image'$alt$title" . ' />';
				$link .= '</a>';
			if ( empty($feed_image) )
				$link .= ')';
			}

			if ( isset($show_date) && $show_date ) {
				$link .= ' ' . gmdate('Y-m-d', $object->last_update_timestamp);
			}

			if ( isset($current_category) && $current_category )
				$_current_category = get_category( $current_category );
		
			if ( 'list' == $args['style'] ) {
				$output .= '<li class="segment-'.rand(1,9).'"';
				//$class = 'cat-item cat-item-'.$object->term_id;
				if ( isset($current_category) && $current_category && ($object->term_id == $current_category) )
					$class .=  ' current-cat';
				elseif ( isset($_current_category) && $_current_category && ($object->term_id == $_current_category->parent) )
					$class .=  ' current-cat-parent';
					$output .=  ' class="'.$class.'"';
					$output .= ">$link\n";
			} else {
				$output .= "\t$link<br />\n";
			}
		}
	}
?>