<?php

	// F L A S H   V I D E O 
	//--------------------------------------------------------
	function sys_flash($atts, $content = null) {
		extract(shortcode_atts(array (
			'width'		=> '480',
			'height'	=> '385',
			'id'		=> '',
			'src'		=> '',
		), $atts));

		$out = '<div class="video-stage" style="width:'.$width.'px; height:'.$height.'px">
				<object id="'.$id.'" width="'.$width.'" height="'.$height.'">
				<param name="movie" value="'.$src.'"></param>
				<param name="allowFullScreen" value="true"></param>
				<param name="expressInstaller" value="'.THEME_DIR.'/swf/expressInstall.swf"></param>
				<param name="allowscriptaccess" value="always"></param>
				<embed src="'.$src.'" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="'.$width.'" height="'.$height.'" wmode="opaque">
				</embed>
				</object>';
		$out .= '</div>';
		return $out;
	}
	add_shortcode('flash','sys_flash');

	// Y O U T U B E   V I D E O 
	//--------------------------------------------------------
	function sys_youtube($atts, $content = null) {
		extract(shortcode_atts(array (
			'width'		=> '480',
			'height'	=> '385',
			'clipid'	=> '',
			'loop'		=> '',
			'autohide'	=> '',
			'autoplay'	=> '',
			'controls'	=> '',
			'disablekb'	=> '',
			'fs'		=> '',
			'hd'		=> '',
			'rel'		=> '',
			'showinfo'	=> '',
			'showsearch'=> '',
		), $atts));
		
		return '<div class="video-stage" style="width:'.$width.'px; height:'.$height.'px">
			<iframe style="height:'.$height.'px; width:'.$width.'px" src="http://www.youtube.com/embed/'.$clipid.'?autohide='.$autohide.'&amp;autoplay='.$autoplay.'&amp;controls='.$controls.'&amp;disablekb='.$disablekb.'&amp;fs='.$fs.'&amp;hd='.$hd.'&amp;loop='.$loop.'&amp;rel='.$rel.'&amp;showinfo='.$showinfo.'&amp;showsearch='.$showsearch.'&amp;wmode=transparent" width='.$width.' height='.$height.' frameborder="0"></iframe>
			</div>';
	}
	add_shortcode('youtube','sys_youtube');

	// V I M E O   V I D E O 
	//--------------------------------------------------------
	function sys_vimeo($atts, $content = null) {
		extract(shortcode_atts(array (
			'width'		=> '480',
			'height'	=> '385',
			'clip_id'	=> '',
			'byline'	=> '',
			'title'		=> '',
			'portrait'	=> '',
			'autoplay'	=> '',
			'loop'		=> '',
			'html5'		=> '1',
		), $atts));

		$out = '';
		if ($height && !$width) $width = intval($height * 16 / 9);
		if (!$height && $width) $height = intval($width * 9 / 16);
		if (!empty($clip_id) && is_numeric($clip_id)){
			$out .= '<div class="video-stage" style="width:'.$width.'px; height:'.$height.'px">';
			if (empty($clip_id) || !is_numeric($clip_id)) $out.='envalid clipid';
			if ($height && !$width) $width = intval($height * 16 / 9);
			if (!$height && $width) $height = intval($width * 9 / 16);
			if($html5){
				$out .= "<iframe src='http://player.vimeo.com/video/$clip_id?title=$title&amp;byline=$byline&amp;portrait=$portrait' width='$width' height='$height' frameborder='0'></iframe>"; 
			}else{
				$out .= "<object width='$width' height='$height'><param name='allowfullscreen' value='true' />".
					"<param name='allowscriptaccess' value='always' /><param name='wmode' value='transparent' />".
					"<param name='movie' value='http://vimeo.com/moogaloop.swf?clip_id=$clip_id&amp;server=vimeo.com&amp;show_title=$title&amp;show_byline=$byline&amp;show_portrait=$portrait&amp;&amp;fullscreen=1' />".
					"<embed src='http://vimeo.com/moogaloop.swf?clip_id=$clip_id&amp;server=vimeo.com&amp;show_title=$title&amp;show_byline=$byline&amp;show_portrait=$portrait&amp;&amp;fullscreen=1' type='application/x-shockwave-flash' allowfullscreen='true' allowscriptaccess='always' width='$width' height='$height'></embed></object>";
			}
			$out .= '</div>';
		}
		return $out;
	}
	add_shortcode('vimeo','sys_vimeo');
?>