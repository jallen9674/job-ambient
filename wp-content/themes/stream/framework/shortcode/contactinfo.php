<?php
// CONTACT INFO 
//--------------------------------------------------------
function sys_contact_info( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'name'		=> '',
		'address'	=> '',
		'phone'		=> '',
		'link'		=> '',
		'email'		=> '',
	), $atts));

	$out = '<div class="contactinfo-wrap">';
	if( $name ) {
		$out .= '<p><span class="icon"><i class="icon-home"></i></span><span class="details">'.$name.'</span></p>';
	}
	if( $address ) {
		$out .= '<p><span class="icon"><i class="icon-map-marker"></i></span>';
		$out .= '<span class="details">'. stripslashes(nl2br($address)) .'</span>';
		$out .= '</p>';
	}
	if( $phone ) {
		$out .= '<p class="phone"><span class="icon"><i class="icon-phone"></i></span><span class="details">'.$phone.'</span></p>';
	}
	if( $email ) {
		$out .= '<p><span class="icon"><i class="icon-envelope"></i></span><span class="details"><a href="mailto:'.$email.'">'.$email.'</a></span></p>';
	}
	if( $link ) {
		$out .= '<p><span class="icon"><i class="icon-link"></i></span><span class="details"><a href="'.esc_url($link).'">'.esc_url($link).'</a></span></p>';
	}
	$out .= '</div>';
	
	return $out;
}
	add_shortcode('contactinfo', 'sys_contact_info');
?>