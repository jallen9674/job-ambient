<?php

	// T O G G L E S
	//--------------------------------------------------------
	function sys_toggle_content( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'heading'	=> '',
		), $atts));
	
		$out = '<div class="simpletoggle">';
		$out .= '<span class="toggle"><a href="#">' .$heading. '</a></span>';
		$out .= '<div class="toggle_content" style="display: none;">';
		$out .= '<div class="toggleinside">';
		$out .= do_shortcode($content);
		$out .= '</div></div></div>';
		
		return $out;
	}
	add_shortcode('toggle', 'sys_toggle_content');

	// F A N C Y   T O G G L E
	//--------------------------------------------------------
	function sys_fancy_toggle_content( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'heading'	=> '',
		), $atts));
		$out= '<div class="fancytoggle">';
		$out .= '<div class="fancytogglebg">';
		$out .= '<span class="toggle"><a href="#">' .$heading. '</a></span>';
		$out .= '<div class="toggle_content" style="display: none;">';
		$out .= '<div class="toggleinside">';
		$out .= do_shortcode($content);
		$out .= '</div></div>';
		$out .= '</div>';
		$out .= '</div>';
		
		return $out;
	}
	add_shortcode('fancytoggle', 'sys_fancy_toggle_content');

	// T A B S G R O U P
 	//--------------------------------------------------------
	function atp_tab_group($atts, $content = null, $code) {
		extract(shortcode_atts(array(
		       'tabtype'	=> '',
			'position'	=> '',
			
		), $atts));
		if($tabtype == "vertabs") {
				$tabtype = 'vertabs';
			}else{ 
				$tabtype = "";
			}
			switch($position){
				case'centertabs':
							$positiontype = "centertabs";
							break;
				case'righttabs':
							$positiontype = "righttabs";
							break;
				default:
							$positiontype = "";
			}
        if (!preg_match_all("/(.?)\[(tab)\b(.*?)(?:(\/))?\](?:(.+?)\[\/tab\])?(.?)/s", $content, $matches)) {
                return do_shortcode($content);
        } else {
			for($i = 0; $i < count($matches[0]); $i++) {
				$matches[3][$i] = shortcode_parse_atts($matches[3][$i]);
			}
			
			$out = '<div id="tab'.rand(1,9).'" class="systabspane '.$tabtype.'"><ul  class="tabs">';
			for($i = 0; $i < count($matches[0]); $i++) {
				$customtabcolor = $customtabtextcolor='';
				if( isset( $matches[3][$i]['tabcolor'] ) ){
					if (strpos($matches[3][$i]['tabcolor'], '#') !== false) {
						$customtabcolor = ' style="background-color:'.$matches[3][$i]['tabcolor'].'"';
					}
				}
				if( isset( $matches[3][$i]['textcolor'] ) ){
					if (strpos($matches[3][$i]['textcolor'], '#') !== false) {
						$customtabtextcolor = ' style="color:'.$matches[3][$i]['textcolor'].'"';
					}
				}
				if(isset($matches[3][$i]['title'])){
					$out .= '<li   id="#'.strtolower(preg_replace('/\s+/', '-', $matches[3][$i]['title'])).'" ' .$customtabcolor. '  ><a '.$customtabtextcolor.'>' . $matches[3][$i]['title'] . '</a></li>';
				}
			}
			$out .= '</ul>';
			$out .= '<div class="panes">';
			for($i = 0; $i < count($matches[0]); $i++) {
				if(isset($matches[3][$i]['title'])){
					$out .= '<div  class="tab_content" id="'.strtolower(preg_replace('/\s+/', '-',$matches[3][$i]['title'] )).'" >' . do_shortcode(trim($matches[5][$i])) . '</div>';
				}
			}
			$out .= '</div></div>';
		       
			return $out;
		}
	}
	add_shortcode('tab', 'atp_tab_group');
	add_shortcode('minitabs', 'atp_tab_group');
	
	// P R I C E G R O U P
 	//--------------------------------------------------------
	function atp_price_group( $atts, $content ){
		extract(shortcode_atts(array(
		), $atts));
		wp_reset_query();
		$GLOBALS['price_count'] = 0;
		$count = 0;
		$columnid = $class = $out = '';
		do_shortcode( $content );
		if( is_array( $GLOBALS['price'] ) ){
			foreach( $GLOBALS['price'] as $price ){
				$out .='<div class="block '.$columnid.'">
							<div class="pricebox">';
				$bgcolor=$price['headingbgcolor'];
				$color=$price['headingcolor'];
				$bgcolor = $bgcolor?'background-color:'.$bgcolor.';':'';
				$color = $color?'color:'.$color.';':'';
				$extras =	($color!==''||$bgcolor!='')?' style="'.$color.$bgcolor.'"':'';

				$out .= "<div class='pricetag' $extras>";
				$out .= '<h4>'.$price['title'].'</h4>';
				$out .= $price['heading'];
				$out .= '</div>';
				$out .= '<div class="pt_desc">'.do_shortcode($price['content']).'</div>';
				$out .= '</div>';
				$out .= '</div>';
				$count++;
			}
			if($count =="3") { $class="col3"; }
			$return= '<div class="pricetable '.$class.' clearfix"><div class="pricinginner">';
			$return .= $out;
			$return .= '</div>';
		}

		$return.='<div class="clear"></div></div>';
		unset( $GLOBALS['price'], $GLOBALS['price_count'] );

		return $return;
	}
	
	// P R I C E 
	//--------------------------------------------------------
	function atp_price( $atts, $content ){
		extract(shortcode_atts(array(
			'title'				=> '',
			'heading'			=> '',
			'headingbgcolor'	=> '',
			'headingcolor'		=> '',
			'desc'				=> ''
		), $atts));

		$x = $GLOBALS['price_count'];
		$GLOBALS['price'][$x] = array(
			'title'				=> $title,
			'heading'			=> do_shortcode($heading),
			'headingbgcolor'	=> $headingbgcolor,
			'headingcolor'		=> $headingcolor,
			'content'			=>  $content
		);
		$GLOBALS['price_count']++;
	}
	add_shortcode( 'pricingcolumns', 'atp_price_group' );
	add_shortcode( 'col', 'atp_price' );
?>