<?php
	add_action('init','atp_options');
	
	if (!function_exists('atp_options')) {
		$options = array();
		function atp_options(){

			global $options, $atp_options, $url, $shortname, $atp_theme;

			$easeing_options = array(
				'linear'	 => 'linear',
				'swing'		 => 'swing',
				'jswing'	 => 'jswing',
				'easeInQuad' => 'easeInQuad',
				'easeInCubic'=> 'easeInCubic',
				'easeInQuart'=> 'easeInQuart',
				'easeInQuint'=> 'easeInQuint',
				'easeInSine' => 'easeInSine',
				'easeInExpo' => 'easeInExpo',
				'easeInCirc' => 'easeInCirc',
				'easeInElastic' => 'easeInElastic',
				'easeInBack' => 'easeInBack',
				'easeInBounce' => 'easeInBounce',
				'easeOutQuad' => 'easeOutQuad',
				'easeOutCubic' => 'easeOutCubic',
				'easeOutQuart' => 'easeOutQuart',
				'easeOutQuint' => 'easeOutQuint',
				'easeOutSine' => 'easeOutSine',
				'easeOutExpo' => 'easeOutExpo',
				'easeOutCirc' => 'easeOutCirc',
				'easeOutElastic' => 'easeOutElastic',
				'easeOutBounce' => 'easeOutBounce',
				'easeInOutQuad' => 'easeInOutQuad',
				'easeInOutCubic' => 'easeInOutCubic',
				'easeInOutQuart' => 'easeInOutQuart',
				'easeInOutQuint' => 'easeInOutQuint',
				'easeInOutSine' => 'easeInOutSine',
				'easeInOutExpo' => 'easeInOutExpo',
				'easeInOutCirc' => 'easeInOutCirc',
				'easeInOutElastic' => 'easeInOutElastic',
				'easeInOutBack' => 'easeInOutBack',
				'easeInOutBounce' => 'easeInOutBounce'
			);
			$fontface = array(
				' ' 										=> 'Select a font',
				'Arial'										=> 'Arial',
				'Verdana'									=> 'Verdana',
				'Tahoma'									=> 'Tahoma',
				'Sans-serif'								=> 'Sans-serif',
				'Lucida Grande'								=> 'Lucida Grande',
				'Georgia, serif'							=> 'Georgia',
				'Trebuchet MS, Tahoma, sans-serif'			=> 'Trebuchet',
				'Times New Roman, Geneva, sans-serif'		=> 'Times New Roman',
				'Palatino,Palatino Linotyp,serif'			=> 'Palatino',
				'Helvetica Neue, Helvetica, sans-serif'		=> 'Helvetica'
			);
			// Slider speed
			$slider_speed=array();
				for($i=100;$i<=3000;$i+=100) {
				$slider_speed[$i]=$i;
			}

			// Slider Interval
			$slider_interval=array();
				for($i=1000;$i<=9000;$i+=1000) {
				$slider_interval[$i]=$i;
			}
			
			//
			$colors = array();
			if(is_dir(THEME_DIR . "/colors/")) {
				if($style_dirs = opendir(THEME_DIR . "/colors/")) {
					while(($color = readdir($style_dirs)) !== false) {
						if(stristr($color, ".css") !== false) {
							$colors[$color] = $color;
						}
					}
				}
			}
			$colors_select = $colors;
			array_unshift($colors_select,'Default Color');

			// ***** Image Alignment radio box
			$options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center");
			
			// ***** Image Links to Options
			$options_image_link_to = array(
					'image'	=> 'The Image',
					'post'	=> 'The Post'); 
			$body_repeat = array(
					'repeat'	=> 'Repeat',
					'no-repeat'	=> 'No Repeat',
					'repeat-x'	=> 'Repeat X',
					'repeat-y'	=> 'Repeat Y');
			$body_pos = array(
					'left top'		=> 'Left Top',
					'left_center'	=> 'Left Center',
					'left_bottom'	=> 'Left Bottom',
					'right_top'		=> 'Right Top',
					'right_center'	=> 'Right Center',
					'right_bottom'	=> 'Right Bottom',
					'center top'	=> 'Center Top',
					'center_center'	=> 'Center Center',
					'center_bottom'	=> 'Center Bottom');
			$body_attachment_style=array(
					'fixed'		=> 'Fixed',
					'scroll'	=> 'Scroll');

		

			//More Options
			$uploads_arr = wp_upload_dir();
			$all_uploads_path = $uploads_arr['path'];
			$all_uploads = get_option('atp_uploads');
			
			// G E N E R A L ################################################################################
			//-----------------------------------------------------------------------------------------------
			$options[] = array( 'name' => 'General','icon' => $url.'cog-icon.png','type' => 'heading');
			//-----------------------------------------------------------------------------------------------

					$options[] = array( 'name'	=> 'Custom Logo',
										'desc'	=> 'Select the Logo style you want to use title or image.',
										'id'	=> $shortname.'_logo',
										'std'	=> 'title',
										'options' => array(	
														'title'	=> 'Title',
														'logo'	=> 'Logo',
														),
										'type'	=> 'select');

					$options[] = array( 'name'	=> 'Logo Image',
										'desc'	=> 'Upload a logo for your theme, or specify the image url of your online logo. (http://yoursite.com/logo.png)',
										'id'	=> $shortname.'_header_logo',
										'std'	=> '',
										'class' => 'atplogo logo',
										'type'	=> 'upload');

					$options[] = array(	'name'		=> 'Site Title',
										'desc'		=> 'Site Title Color Properties',
										'id'		=> $shortname.'_logotitle',
										'std'		=> array(	
														'size'		=> '',
														'lineheight'=> '',
														'style'		=> '',
														'fontvariant' => ''),
										'class' 	=> 'atplogo title',
										'type'		=> 'typography');

					$options[] = array(	'name'		=> 'Site Description',
										'desc'		=> 'Site Description Color and properties',
										'id'		=> $shortname.'_tagline',
										'std'		=> array(
														'size'		=> '',
														'lineheight'=> '',
														'style'		=> '',
														'fontvariant' => ''),
										'class' 	=> 'atplogo title',
										'type'		=> 'typography');
									
					$options[] = array( 'name'	=> 'Custom Favicon',
										'desc'	=> "Upload a 16px x 16px ICO icon format that will represent your website's favicon.",
										'id'	=> $shortname.'_custom_favicon',
										'std'	=> '',
										'type'	=> 'upload'); 

					$options[] = array( 'name'	=> 'Subheader Teaser',
										'desc'	=> 'Teaser call out for the subheader section of the theme.',
										'id'	=> $shortname.'_teaser',
										'std'	=> 'default',
										'options' => array( 
														'default'	=> 'Default (post title)',
														'twitter'   => 'Twitter Tweets',
														'disable'	=> 'Disable Subheader'),
										'type'	=> 'select');

					$options[] = array( 'name'	=> 'Menu Position',
										'desc'	=> 'Check this if you wish to change the menu position to bottom of header area.',
										'id'  	=> $shortname.'_menu_position',
										'std' 	=> 'true',
										'type' 	=> 'checkbox');
									
					$options[] = array( 'name'	=> 'Breadcrumbs',
										'desc'	=> 'Check this if you wish to disable the breadcrumbs option for the theme which appears in the subheader. If you wish to disable the breadcrumb for a specific page then go to edit page',
										'id'  	=> $shortname.'_breadcrumbs',
										'std' 	=> 'true',
										'type' 	=> 'checkbox');

					$options[] = array( 'name'	=> 'Layout Option',
										'desc'	=> 'Select the layout option BOXED or STRETCHED LAYOUT',
										'id'	=> $shortname.'_layoutoption',
										'std'	=> 'boxed',
										'options'=> array(
														'stretched'	=> 'Stretched',
														'boxed'		=> 'Boxed'),
										'type'	=> 'select');

					$options[] = array(	'name'	=> 'Google Analytics',
										'desc'	=> 'Paste your Google Analytics code here which starts from &lt;script> here. This will be added into the footer of your theme.',
										'id'	=> $shortname.'_googleanalytics',
										'std'	=> '',
										'type'	=> 'textarea');

			// TWITER #########################################################################################
			//---------------------------------------------------------------------------------------------------
			$options[] = array( 'name' => 'Twitter API','icon' => $url.'twitter-icon.png','type' => 'heading');
			//---------------------------------------------------------------------------------------------------
						$options[] = array( 'name'	=> 'Twitter Username',
											'desc'	=> 'Enter Twitter username to display tweets in subheader area of the theme. <br>You will need to visit <a href="https://dev.twitter.com/apps/" target="_blank">https://dev.twitter.com/apps/</a>, sign in with your account and create your own keys.',
											'id'	=> $shortname.'_teaser_twitter',
											'std'	=> '',
											'inputsize' => '300',
											'type'	=> 'text');
										
						$options[] = array( "name"	=> "Twitter Consumer key",
											"desc"	=> "Twitter Consumer key",
											"id"	=> $shortname."_consumerkey",
											'inputsize' => '300',
											"std"	=> "",
											"type"	=> "text");
						$options[] = array( "name"	=> "Twitter Consumer secret",
											"desc"	=> "Twitter Consumer secret",
											"id"	=> $shortname."_consumersecret",
											'inputsize' => '300',
											"std"	=> "",
											"type"	=> "text");

						$options[] = array( "name"	=> "Twitter Access token",
											"desc"	=> "Twitter Access token",
											"id"	=> $shortname."_accesstoken",
											'inputsize' => '300',
											"std"	=> "",
											"type"	=> "text");
						$options[] = array( "name"	=> "Twitter Token secret",
											"desc"	=> "Twitter Access secret",
											"id"	=> $shortname."_accesstokensecret",
											'inputsize' => '300',
											"std"	=> "",
											"type"	=> "text");


			// HOMEPAGE #########################################################################################
			//---------------------------------------------------------------------------------------------------
			$options[] = array( 'name' => 'Homepage','icon' => $url.'home-icon.png','type' => 'heading');
			//---------------------------------------------------------------------------------------------------
										
				$options[] = array( "name"	=> "Teaser",
									"desc"	=> 'Check this if you wish to disable the Teaser on Homepage below the slider.',
									"id"	=> $shortname."_teaser_frontpage",
									"std"	=> "",
									"type"	=> "checkbox");
	

				$options[] = array( "name"	=> "Teaser Text",
									"desc"	=> "Custom HTML and Text that will appear in the teaser area of your Homepage below Slider.",
									"id"	=> $shortname."_teaser_frontpage_text",
									"std"	=> "",
									"type"	=> "textarea");


			// COLORS ###########################################################################################
			//---------------------------------------------------------------------------------------------------
			$options[] = array( 'name' => 'Colors', 'icon' => $url.'colors-icon.png','type' => 'heading');
			//---------------------------------------------------------------------------------------------------
			
					//---------------------------------------------------------------------------------------------------
					$options[] = array( 'name'	=> 'General Elements', 'type' => 'subnav');
					//---------------------------------------------------------------------------------------------------

					$options[] =array(	'name'	=> 'Colors',
										'desc'	=> 'Select your themes alternative color scheme for this Theme Current theme has no extra custom made skins',
										'id'	=> $shortname.'_style',
										'std'	=> '', 
										'options'=> $colors_select,
										'type'	=> 'select');


					$options[] = array(	'name'	=> 'Theme Color',
										'desc'	=> 'Theme Color set to default with theme if you choose color from here it will change all the links and backgrounds used in the theme.',									
										'id'	=> $shortname.'_themecolor',
										'std'	=> '', 
										'type'	=> 'color');


					$options[] = array(	'name'	=> 'Body Background Properties',
										'desc'	=> 'Select the Background Image for Body and assign its Properties according to your requirements.',
										'id'	=> $shortname.'_bodyproperties',
										'std'	=> array(
														'image'		=> '',
														'color'		=> '',
														'style' 	=> '',
														'position'	=> '',
														'attachment'=> ''),
										'type'	=> 'background');
		
					$options[] = array( 'name' => 'Background Pattern Images',
										'desc' => 'Patter overlay on the body background color or image, displays on if the layout is selected as boxed in General Options Panel',
										'id'   => $shortname.'_overlayimages',
										'std'  => '',
										'type' => 'images',
										'options' => array(
														''			   => $url . 'patterns/pat0.png',
														'pattern1.png' => $url . 'patterns/pat1.png',
														'pattern2.png' => $url . 'patterns/pat2.png',
														'pattern3.png' => $url . 'patterns/pat3.png',
														'pattern4.png' => $url . 'patterns/pat4.png',
														'pattern5.png' => $url . 'patterns/pat5.png',
														'pattern6.png' => $url . 'patterns/pat6.png',
														'pattern7.png' => $url . 'patterns/pat7.png',
														'pattern8.png' => $url . 'patterns/pat8.png'),
												);
				

					$options[] = array(	'name'	=> 'Header Background Color',
										'desc'	=> 'This will apply background color to the header area of theme.',
										'id'	=> $shortname.'_headerbg',
										'std'	=> '', 
										'type'	=> 'color');
	
					$options[] = array(	'name'	=> 'Header Text Color',
										'desc'	=> 'This will apply text color to the header area of theme.',
										'id'	=> $shortname.'_headertxt',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Teaser Background Properties',
										'desc'	=> 'Select the Background Image or Color for Homepage Teaser and assign its Properties according to your requirements.',
										'id'	=> $shortname.'_teaser_properties',
										'std'	=> array(
														'image'		=> '',
														'color'		=> '',
														'style' 	=> 'repeat',
														'position'	=> 'center top',
														'attachment'=> 'scroll'),
										'type'	=> 'background');
	
					$options[] = array(	'name'	=> 'Teaser Text Color',
										'desc'	=> 'This will apply color to the teaser text.',
										'id'	=> $shortname.'_teaser_text',
										'std'	=> '', 
										'type'	=> 'color');
	
					$options[] = array(	'name'	=> 'Subheader Background Properties',
										'desc'	=> 'Select the Background Image for Subheader and assign its Properties according to your requirements.',
										'id'	=> $shortname.'_subheaderproperties',
										'std'	=> array(
														'image'		=> '',
														'color'		=> '',
														'style' 	=> 'repeat',
														'position'	=> 'center top',
														'attachment'=> 'scroll'),
										'type'	=> 'background');

					$options[] = array(	'name'	=> 'Subheader Text Color',
										'desc'	=> 'Subheader Text Color',
										'id'	=> $shortname.'_subheader_textcolor',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Content Area Background',
										'desc'	=> 'This will apply color to the primary content area of theme.',
										'id'	=> $shortname.'_wrapbg',
										'std'	=> '', 
										'type'	=> 'color');
		
					$options[] = array(	'name'	=> 'Footer Background Properties',
										'desc'	=> 'Footer background properties includes the sidebar footer widgets area as well. If you wish to disable footer area you can go to Footer Tab and do that..',
										'id'	=> $shortname.'_footerbg',
										'std'	=> array(
														'image'		=> '',
														'color'		=> '',
														'style' 	=> 'repeat',
														'position'	=> 'center top',
														'attachment'=> 'scroll'),
										'type'	=> 'background');

					$options[] = array(	'name'		=> 'Footer Copyright',
										'desc'		=> 'Select the Color &amp; Font Properties for <strong>Footer Copyright Section</strong>',
										'id'		=> $shortname.'_copyrights',
										'std'		=> array(
														'color'		=> '',
														'size'		=> '',
														'lineheight'=> '',
														'style'		=> '',
														'fontvariant' => ''),
										'type'		=> 'typography');
		
					$options[] = array(	'name'	=> 'Footer Text',
										'desc'	=> 'Footer text color including paragraph element, (without links).',
										'id'	=> $shortname.'_footertextcolor',
										'std'	=> '', 
										'type'	=> 'color');
		
					$options[] = array(	'name'	=> 'Footer Heading',
										'desc'	=> 'Headings Color which appears as a Footer Widget Titles',
										'id'	=> $shortname.'_footerheadingcolor',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Copyright Background',
										'desc'	=> 'Copyright area which appears below the footer sidebar footer.',
										'id'	=> $shortname.'_copybgcolor',
										'std'	=> '', 
										'type'	=> 'color');
					//---------------------------------------------------------------------------------------------------
					$options[] = array( 'name' => 'Breadcrumb', 'type' => 'subnav');
					//---------------------------------------------------------------------------------------------------

					$options[] = array(	'name'		=> 'Breadcrumb Separator',
										'desc'		=> 'Breadcrumb Text Separator. Only HTML Special Characters Supports',
										'id'		=> $shortname.'_breadcrumbsep',
										'std'		=> '', 
										'type'		=> 'text',
										'inputsize'	=>'');

					$options[] = array(	'name'	=> 'Background',
										'desc'	=> 'Breadcrumb Background Color.',
										'id'	=> $shortname.'_breadcrumbbg',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Text Color',
										'desc'	=> 'Breadcrumb Text Color.',
										'id'	=> $shortname.'_breadcrumbtext',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Link Color',
										'desc'	=> 'Breadcrumb Link Color.',
										'id'	=> $shortname.'_breadcrumblink',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Link Hover Color',
										'desc'	=> 'Breadcrumb Link Hover Color.',
										'id'	=> $shortname.'_breadcrumblinkhover',
										'std'	=> '', 
										'type'	=> 'color');

					//---------------------------------------------------------------------------------------------------
					$options[] = array( 'name' => 'Menu', 'type' => 'subnav');
					//---------------------------------------------------------------------------------------------------

					$options[] = array(	'name'	=> 'Menu Background',
										'desc'	=> 'Menu Background Color',
										'id'	=> $shortname.'_topmenu_bg',
										'std'	=> '', 
										'type'	=> 'color');


					$options[] = array(	'name'		=> 'Menu Font and Link Color',
										'desc'		=> 'Select the Color and Font Properties for Menu Parent Lists.',
										'id'		=> $shortname.'_topmenu',
										'std'		=> array(
														'size' 		=> '',
														'lineheight'=> '',
														'style' 	=> '',
														'color' 	=> ''),
													
										'type'		=> 'typography');
									
					$options[] = array(	'name'	=> 'Menu Hover Text',
										'desc'	=> 'Select the Color for Menu Hover Text.',
										'id'	=> $shortname.'_topmenu_linkhover',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Menu Hover Background Color',
										'desc'	=> 'Select the Color for Menu Hover Background.',
										'id'	=> $shortname.'_topmenu_linkhoverbg',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Menu Dropdown Background Color',
										'desc'	=> 'Select the Color for Dropdown Menu Background Color',
										'id'	=> $shortname.'_topmenu_sub_bg',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Menu Dropdown Text Color',
										'desc'	=> 'Select the Color for Dropdown Menu Text Color',
										'id'	=> $shortname.'_topmenu_sub_link',
										'std'	=> '', 
										'type'	=> 'color');
		
					$options[] = array(	'name'	=> 'Menu Dropdown Text Hover Color',
										'desc'	=> 'Select the Color for Dropdown Menu Text Hover Color',
										'id'	=> $shortname.'_topmenu_sub_linkhover',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Menu Dropdown Hover Background Color',
										'desc'	=> 'Select the Color for Dropdown Menu Hover Background Color',
										'id'	=> $shortname.'_topmenu_sub_hoverbg',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Menu Active Link Background Color',
										'desc'	=> 'Select the Color for Active Link Background Color',
										'id'	=> $shortname.'_topmenu_active_linkbg',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Menu Active Link Color',
										'desc'	=> 'Select the Color for Active Link Color',
										'id'	=> $shortname.'_topmenu_active_link',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Menu Dropdown Border Color',
										'desc'	=> 'Select the Color for Dropdown Menu Border Color ',
										'id'	=> $shortname.'_topmenu_bordercolor',
										'std'	=> '', 
										'type'	=> 'color');

					//---------------------------------------------------------------------------------------------------
					$options[] = array( 'name' => 'Link Colors', 'type' => 'subnav');
					//---------------------------------------------------------------------------------------------------

					$options[] = array(	'name'	=> 'Link Color',
										'desc'	=> 'Select the Color for Theme links',
										'id'	=> $shortname.'_link',
										'std'	=> '', 
										'type'	=> 'color');
			
					$options[] = array(	'name'	=> 'Link Hover Color',
										'desc'	=> 'Select the Color for Theme links hover',
										'id'	=> $shortname.'_linkhover',
										'std'	=> '', 
										'type'	=> 'color');
			
					$options[] = array(	'name'	=> 'Subheader Link Color',
										'desc'	=> 'Links under subheader section',
										'id'	=> $shortname.'_subheaderlink',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Subheader Link Hover Color',
										'desc'	=> 'Links Hover under subheader section',
										'id'	=> $shortname.'_subheaderlinkhover',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Footer Link Color',
										'desc'	=> 'Footer containing links under widget or text widget, (link color).',
										'id'	=> $shortname.'_footerlinkcolor',
										'std'	=> '', 
										'type'	=> 'color');
			
					$options[] = array(	'name'	=> 'Footer Link Hover Color',
										'desc'	=> 'Footer content containing any links under widget or text widget, (link hover color).',
										'id'	=> $shortname.'_footerlinkhovercolor',
										'std'	=> '', 
										'type'	=> 'color');

					$options[] = array(	'name'	=> 'Copyright Link Color',
										'desc'	=> 'Copyright content containing any links color. (link color).',
										'id'	=> $shortname.'_copylinkcolor',
										'std'	=> '', 
										'type'	=> 'color');

						//---------------------------------------------------------------------------------------------------
					$options[] = array( 'name'	=> 'Typography', 'type' => 'subnav');
					//---------------------------------------------------------------------------------------------------
					//---------------------------------------------------------------------------------------------------
					$options[] = array( 'name'	=> 'Select Font Family', 'desc' => '<h3>Font Family</h3> Select the Fonts as standards fonts or google webfonts. If you select the headings font it will replace all the heading font family for the whole theme including footer and sidebar widget titles.', 'type' => 'subsection');
					//---------------------------------------------------------------------------------------------------
					
					$options[] = array( 
										"name" 		=> "Body Font Face",
										"desc" 		=> "Select a font family for body text",
										"id" 		=> $shortname.'bodyfont',
										"options" 	=>$fontface,
										"preview" 	=> array(
															"text" 		=> "This is my preview text!", //this is the text from preview box
															"size" 		=> "25px"
										),
										"type" 		=> "atpfontfamily");

					$options[] = array( 
										"name" 		=> "Headings Font Face",
										"desc" 		=> "Select a font family for Headings ( h1, h2, h3, h4, h5, h6 )",
										"id" 		=> $shortname.'_headings',
										"options" 	=>$fontface,
										"preview" 	=> array(
															"text" 		=> "This is my preview text!", //this is the text from preview box
															"size" 		=> "25px"
										),
										"type" 		=> "atpfontfamily");
										
					$options[] = array( 
										"name" 		=> "Menu Font Face",
										"desc" 		=> "Select a font family for Top Navigation Menu",
										"id" 		=> $shortname.'_mainmenufont',
										"options" 	=> $fontface,
										"preview" 	=> array(
															"text" 		=> "This is my preview text!", //this is the text from preview box
															"size" 		=> "25px"
										),
										"type" 		=> "atpfontfamily");

				

					//---------------------------------------------------------------------------------------------------
					$options[] = array( 'name'	=> 'Select Font Properties', 'desc' => '<h3>Font Properties</h3> Select the front properties like font size, line-height, font-style and font-weight.', 'type' => 'subsection');
					//---------------------------------------------------------------------------------------------------
					
					$options[] = array(	'name'	=> 'Body',
										'desc'	=> 'Select the Color and Font Properties for Body Font.',
										'id'	=> $shortname.'_bodyp',
										'std'	=> array(
														'color'		=> '',
														'size'		=> '',	
														'lineheight'=> '',
														'style'		=> '',
														'fontvariant' => ''),
										'type'	=> 'typography');


					$options[] = array(	'name'	=> 'H1',
										'desc'	=> 'Select the Color and Font Properties for H1 Heading.',
										'id'	=> $shortname.'_h1',
										'std'	=> array(
														'color'			=> '',
														'size' 			=> '',
														'lineheight'	=> '',
														'style' 		=> '',
														'fontvariant' 	=> ''),
										'type'	=> 'typography');

					$options[] = array(	'name'	=> 'H2',
										'desc'	=> 'Select the Color and Font Properties for H2 Heading.',
										'id'	=> $shortname.'_h2',
										'std'	=> array(
														'color'			=> '',
														'size' 			=> '',
														'lineheight'	=> '',
														'style'			=> '',
														'fontvariant' 	=> ''),
										'type'	=> 'typography');

					$options[] = array(	'name'	=> 'H3',
										'desc'	=> 'Select the Color and Font Properties for H3 Heading.',
										'id'	=> $shortname.'_h3',
										'std'	=> array(
														'color'			=> '',
														'size' 			=> '',
														'lineheight'	=> '',
														'style' 		=> '',
														'fontvariant' 	=> ''),
										'type'	=> 'typography');

					$options[] = array(	'name'	=> 'H4',
										'desc'	=> 'Select the Color and Font Properties for H4 Heading.',
										'id'	=> $shortname.'_h4',
										'std'	=> array(
														'color'		=> '',
														'size' 		=> '',
														'lineheight'=> '',
														'style' 	=> '',
														'fontvariant' => ''),
										'type'	=> 'typography');

					$options[] = array(	'name'	=> 'H5',
										'desc'	=> 'Select the Color and Font Properties for H5 Heading.',
										'id'	=> $shortname.'_h5',
										'std'	=> array(
														'color'		=> '',
														'size'		=> '',
														'lineheight'=> '',
														'style'		=> '',
														'fontvariant' => ''),
										'type'	=> 'typography');

					$options[] = array(	'name'	=> 'H6',
										'desc'	=> 'Select the Color and Font Properties for H6 Heading.',
										'id'	=> $shortname.'_h6',
										'std'	=> array(
														'color'		=> '',
														'size'		=> '',
														'lineheight'=> '',
														'style'		=> '',
														'fontvariant' => ''),
										'type'	=> 'typography');

					$options[] = array(	'name'	=> 'Sidebar Widget Titles',
										'desc'	=> 'Select the Color and Font Properties for sidebar Widget Titles.',
										'id'	=> $shortname.'_sidebartitle',
										'std'	=> array(
														'color'		=> '',
														'size'		=> '',
														'lineheight'=> '',
														'style'		=> '',
														'fontvariant' => ''),
										'type'	=> 'typography');

					$options[] = array(	'name'	=> 'Footer Widget Titles',
										'desc'	=> 'Select the Color and Font Properties for Footer Widget Titles.',
										'id'	=> $shortname.'_footertitle',
										'std'	=> array(
														'color'		=> '',
														'size'		=> '',
														'lineheight'=> '',
														'style'		=> '',
														'fontvariant' => ''),
										'type'	=> 'typography');

					
	
					$options[] = array(	'name'	=> 'Footer Text',
										'desc'	=> 'Select the Color &amp; Font Properties for Footer Section',
										'id'	=> $shortname.'_footertext',
										'std'	=> array(
														'color'		=> '',
														'size'		=> '',
														'lineheight'=> '',
														'style'		=> '',
														'fontvariant' => ''),
										'type'	=> 'typography');
						$options[] = array(	'name'	=> 'Copyright Text',
										'desc'	=> 'Select the Color &amp; Font Properties for Copyright Section.',
										'id'	=> $shortname.'_copyright',
										'std'	=> array(
														'color'		=> '',
														'size'		=> '',
														'lineheight'=> '',
														'style'		=> '',
														'fontvariant' => ''),
										'type'	=> 'typography');

						$options[] = array(	'name'	=> 'Border Color',
										'desc'	=> 'Select the Color for Theme Border',
										'id'	=> $shortname.'_border',
										'std'	=> '', 
										'type'	=> 'color');				
				
					
					//---------------------------------------------------------------------------------------------------
					$options[] = array( 'name' => 'Custom Font', 'type' => 'subnav');
					//---------------------------------------------------------------------------------------------------

					$options[] = array(	'name'	=> 'Custom Font <strong>.woff</strong>',
										'desc'	=> 'Upload Custom Font .woff.',
										'id'	=> $shortname.'_fontwoff',
										'std'	=> '', 
										'type'	=> 'upload');

					$options[] = array(	'name'	=> 'Custom Font <strong>.ttf</strong>',
										'desc'	=> 'Upload Custom Font .ttf',
										'id'	=> $shortname.'_fontttf',
										'std'	=> '', 
										'type'	=> 'upload');

					$options[] = array(	'name'	=> 'Custom Font <strong>.svg</strong>',
										'desc'	=> 'Upload Custom Font .svg',
										'id'	=> $shortname.'_fontsvg',
										'std'	=> '', 
										'type'	=> 'upload');

					$options[] = array(	'name'	=> 'Custom Font <strong>.eot</strong>',
										'desc'	=> 'Upload Custom Font .eot',
										'id'	=> $shortname.'_fonteot',
										'std'	=> '', 
										'type'	=> 'upload');

					$options[] = array(	'name'		=> 'Font Name',
										'desc'		=> 'Enter Font Name Select the name as mentioned in fontface css in the downloaded readme file of your custom font',
										'id'		=> $shortname.'_fontname',
										'std'		=> '', 
										'inputsize' => '',
										'type'		=> 'text');

					$options[] = array(	'name'		=> 'Custom Fonts Headings and class Names',
										'desc'		=> 'Enter your own custom elements to which you want to assign this custom font. Ex: h1,h2,h3, .class1,.class2',
										'id'		=> $shortname.'_fontclass',
										'std'		=> '', 
										'inputsize' => '',
										'type'		=> 'textarea');
										
					//---------------------------------------------------------------------------------------------------
					$options[] = array( 'name' => 'Custom CSS', 'type' => 'subnav');
					//---------------------------------------------------------------------------------------------------

					$options[] = array( 'name'	=> 'Custom CSS',
										'desc'	=> 'Quickly add some CSS of your own choice to your theme by adding it to this block.',
										'id'	=> $shortname.'_extracss',
										'std'	=> '',
										'type'	=> 'textarea');
		
			// S L I D E R S
			//------------------------------------------------------------------------
			$options[] = array( 'name'	=> 'Sliders',
								'icon'	=> $url.'slider-icon.png',
								'type'	=> 'heading');

					$options[]=	array(	'name'	=> 'Slider',
										'desc'	=> 'Check this if you wish to disable the slider the frontpage Slider',
										'id'	=> $shortname.'_slidervisble',
										'std'	=> '',
										'type'	=> 'checkbox');

					$options[] = array( 'name'	 	=> 'Select Slider',
										'desc'		=> 'Select which slider you want to use for the Frontpage of the theme.',
										'id'		=> $shortname.'_slider',
										'std'		=> '',
										'options' 	=> $atp_theme->atp_variable('slider_type'),
										'type'		=> 'select');
				
					//--------------------------------------------------------------------
					$options[] = array( 'name'	=> 'NivoSlider Category',
										'desc'	=> 'Select NivoSlider Category.',
										'id'	=> $shortname.'_nivolidercat',
										'class' => 'atpsliders nivoslider',
										'std'	=> '',
										'options' => $atp_theme->atp_variable('slider'),
										'type'	=> 'multiselect');

					$options[] = array( 'name'	=> 'Slides Limits',
										'desc'	=> 'Number of slides you wish to display in NivoSlider',
										'id'	=> $shortname.'_nivolimit',
										'std'	=> '2',
										'class' => 'atpsliders nivoslider',
										'type'	=> 'text',
										'inputsize' => '70');

					$options[] = array( 'name'	=> 'Slides Effect',
										'desc'	=> 'Select the slides transition effect or set as random to aload all styles randomly.',
										'id'	=> $shortname.'_nivoeffect',
										'std'	=> 'random',
										'class' => 'atpsliders nivoslider',
										'type'	=> 'select',
										'options' => array(
												'random'			=> 'random',
												'fold'				=> 'fold',
												'fade'				=> 'fade',
												'sliceDown'			=> 'sliceDown',
												'sliceDownLeft'		=> 'sliceDownLeft',
												'sliceUp'			=> 'sliceUp',
												'sliceUpLeft'		=> 'sliceUpLeft',
												'sliceUpDown'		=> 'sliceUpDown',
												'sliceUpDownLeft'	=> 'sliceUpDownLeft',
												'slideInRight'		=> 'slideInRight',
												'slideInLeft'		=> 'slideInLeft',
												'boxRandom'			=> 'boxRandom',
												'boxRain'			=>  'boxRain',
												'boxRainReverse' 	=>'boxRainReverse',
												'boxRainGrow'		=> 'boxRainGrow',
												'boxRainGrowReverse' => 'boxRainGrowReverse'
											),
										);

					$options[] = array( 'name'		=> 'Slide Transition speed',
										'desc'		=> 'Chooser Slide transition speed',
										'id'		=> $shortname.'_nivotransitionspeed',
										'std'		=> '500',
										'class' 	=> 'atpsliders nivoslider',
										'type'		=> 'select',
										'options' 	=>$slider_speed,
										);
					
					$options[] = array( 'name'		=> 'Slide Pause Time',
										'desc'		=> 'Choose How long each slide will show',
										'id'		=> $shortname.'_nivopausetime',
										'std'		=> '3000',
										'class' 	=> 'atpsliders nivoslider',
										'type'		=> 'select',
										'options' 	=>$slider_interval,
										);
					$options[] = array( 'name'		=> 'Direction Nav',
										'desc'		=> 'Next/Previous Navigation',
										'id'		=> $shortname.'_nivoslidednav',
										'class' 	=> 'atpsliders nivoslider',
										'std'		=> '',
										'options' 	=> array( 
														'true'	=> 'True',
														'false'	=> 'False'
														),
										'type'		=> 'select');
					//--------------------------------------------------------------------
					$options[] = array( 'name'		=> 'FlexSlider Category',
										'desc'		=> 'Select FlexSlider Category.',
										'id'		=> $shortname.'_flexslidercat',
										'class' 	=> 'atpsliders flexslider',
										'std'		=> 'flexslider',
										'options' 	=> $atp_theme->atp_variable('slider'),
										'type'		=> 'multiselect');
		
					$options[] = array( 'name'		=> 'Slides Limits',
										'desc'		=> 'Enter the limit for Slides you want to hold on the Flex Slider',
										'id'		=> $shortname.'_flexslidelimit',
										'std'		=> '2',
										'class' 	=> 'atpsliders flexslider',
										'type'		=> 'text',
										'inputsize' => '70');

					$options[] = array( 'name'		=> 'Slides Speed',
										'desc'		=> 'Enter the limit for Slide speed you want to set',
										'id'		=> $shortname.'_flexslidespeed',
										'std'		=> '800',
										'class' 	=> 'atpsliders flexslider',
										'options'	=> $slider_speed,
										'type'		=> 'select',
										);

					$options[] = array( 'name'		=> 'Slider Animation Type',
										'desc'		=> 'Select your animation type, "fade" or "slide"',
										'id'		=> $shortname.'_flexslideffect',
										'std'		=> 'slide',
										'class' 	=> 'atpsliders flexslider',
										'options' 	=> array( 
														'fade'	=> 'Fade',
														'slide'	=> 'Slide'
													),
										'type'	=> 'select');

					$options[] = array( 'name'		=> 'Direction Nav',
										'desc'		=> 'Enable navigation for previous/next navigation arrows',
										'id'		=> $shortname.'_flexslidednav',
										'class' 	=> 'atpsliders flexslider',
										'std'		=> '',
										'options' 	=> array( 
														'true'	=> 'True',
														'false'	=> 'False'
													),
										'type'	=> 'select');

					$options[] = array( 'name'		=> 'Control Nav',
										'desc'		=> 'Enable navigation for paging control of each slide',
										'id'		=> $shortname.'_flexslidecnav',
										'class' 	=> 'atpsliders flexslider',
										'std'		=> '',
										'options' 	=> array( 
														'true'	=> 'True',
														'false'	=> 'False'
														),
										'type'		=> 'select');

					//--------------------------------------------------------------------
					$options[] = array( 'name'		=> 'Ei-Slider Category',
										'desc'		=> 'Select Ei-Slider Category.',
										'id'		=> $shortname.'_eislidercat',
										'class' 	=> 'atpsliders eislider',
										'std'		=> 'flexslider',
										'options' 	=> $atp_theme->atp_variable('slider'),
										'type'		=> 'multiselect');
										
					$options[] = array( 'name'		=> 'Slides Limits',
										'desc'		=> 'Enter the slides you want to hold on the Ei Slider',
										'id'		=> $shortname.'_eislidelimit',
										'std'		=> '2',
										'class' 	=> 'atpsliders eislider',
										'type'		=> 'text',
										'inputsize' => '70');	

					$options[] = array( 'name'	=> 'Slider Caption',
										'desc'	=> 'Check this if you wish to disable the Ei-Slider Caption',
										'id'	=> $shortname.'_ei_caption',
										'std'	=> '',
										'class' => 'atpsliders eislider',
										'type'	=> 'checkbox');	
										
					$options[] = array( 'name'		=> 'Animation Types',
										'desc'		=> '"sides" : new slides will slide in from left / right, "center": new slides will appear in the center',
										'id'		=> $shortname.'_eislider_animtype',
										'std'		=> 'center',
										'class' 	=> 'atpsliders eislider',
										'options'	=> array( 'sides'	=> 'Sides','center'=> 'Center'),
										'type'		=> 'select');

					$options[] = array( 'name'		=> 'Sliding Animation',
										'desc'		=> 'Easing for the sliding animation',
										'id'		=> $shortname.'_slideanimation',
										'class' 	=> 'atpsliders eislider',
										'std'		=> 'easeOutExpo',
										'options' 	=> $easeing_options,
										'type'		=> 'select');

					$options[] = array( 'name'		=> 'Titles Animation easing',
										'desc'		=> 'Titles animation easing',
										'id'		=> $shortname.'_titleanimation',
										'class' 	=> 'atpsliders eislider',
										'std'		=> 'easeOutExpo',
										'options' 	=> $easeing_options,
										'type'		=> 'select');
										
					$options[] = array( 'name'		=> 'Titles Animation Speed',
										'desc'		=> 'Titles Animation Speed',
										'id'		=> $shortname.'_titlespeed',
										'class' 	=> 'atpsliders eislider',
										'std'		=> '800',
										'options' 	=>$slider_speed,
										'type'		=> 'select');

					$options[] = array( 'name'		=> 'Sliding Speed',
										'desc'		=> 'speed for the sliding animation',
										'id'		=> $shortname.'_speed',
										'class' 	=> 'atpsliders eislider',
										'std'		=> '800',
										'options' 	=> $slider_speed,
										'type'		=> 'select');
										
					$options[] = array( 'name'		=> 'Slideshow Interval',
										'desc'		=> 'Interval for the slideshow',
										'id'		=> $shortname.'_interval',
										'class' 	=> 'atpsliders eislider',
										'std'		=> '3000',
										'options' 	=>$slider_interval,
										'type'		=> 'select');

					$options[] = array( 'name'	=> 'Slider Autoplay',
										'desc'	=> 'if true the slider will automatically slide, and it will only stop if the user clicks on a thumb',
										'id'	=> $shortname.'_autoplay',
										'std'	=> 'false',
										'class' => 'atpsliders eislider',
										'options' => array( 
														'true'	=> 'True',
														'false'	=> 'False'
													),
										'type'	=> 'select');
					//--------------------------------------------------------------------
					$options[] = array( 'name'		=> 'Planbox Content',
										'desc'		=> 'Enter the video embed code which will display on your frontpage slider area.',
										'id'		=> $shortname.'_planboxcontent',
										'std'		=> '',
										'class' 	=> 'atpsliders planbox',
										'type'		=> 'textarea',
										'inputsize' => '');
					//--------------------------------------------------------------------										
					$options[] = array( 'name'		=> 'Video Embed Code',
										'desc'		=> 'Enter the video embed code which will display on your frontpage slider area.',
										'id'		=> $shortname.'_video_id',
										'std'		=> '',
										'class' 	=> 'atpsliders videoslider',
										'type'		=> 'textarea',
										'inputsize' => '');
					//--------------------------------------------------------------------
					$options[] = array( 'name'	=> 'Static Image',
										'desc'	=> 'Upload the image size 980 x 400 pixels to display on the homepage instead of slider.',
										'id'	=> $shortname.'_static_image_upload',
										'std'	=> '',
										'class' => 'atpsliders static_image',
										'type'	=> 'upload');

					$options[] = array( 'name'		=> 'Static Image Slider URL',
										'desc'		=> 'Enter the external link to site or any URL to link to this static image which clicked.',
										'id'		=> $shortname.'_static_link',
										'std'		=> '',
										'class' 	=> 'atpsliders static_image',
										'type'		=> 'text',
										'inputsize' => '');
				//--------------------------------------------------------------------
					$options[] = array( 'name'		=> 'Custom Slider',
										'desc'		=> 'Use in Your custom slider Plugin shortcodes Example : [revslider id="1"]',
										'id'		=> $shortname.'_customslider',
										'std'		=> '',
										'class' 	=> 'atpsliders custom_slider',
										'type'		=> 'textarea',
										'inputsize' => '');

			// P O R T F O L I O
			//------------------------------------------------------------------------
			$options[] = array( 'name'	=> 'Portfolio',
								'icon'	=> $url.'portfolio-icon.png',
								'type'	=> 'heading');
			
			
					$options[] = array(	'name'	=> 'Portfolio Comments',
										'desc'	=> 'Check this if you wish to disable comments in Portfolio Single Page',
										'id'	=> $shortname.'_portfoliocomments',
										'std'	=> '',
										'type'	=> 'checkbox');

					$options[] = array(	"name"	=> "Portfolio Single Page Featured Image",
										"desc"	=> 'Check this if you wish to disable Featured Image on Portfolio Single Page',
										"id"	=> $shortname."_portfoliofeaturedimg",
										"std"	=> "",
										"type"	=> "checkbox");

					$options[] = array(	"name"	=> "Portfolio Related Posts",
										"desc"	=> 'Check this if you wish to disable Related Posts in Portfolio Single Page',
										"id"	=> $shortname."_portfolio_relatedpost",
										"std"	=> "",
										"type"	=> "checkbox");


									
			// P O S T   O P T I O N S 
			//------------------------------------------------------------------------
			$options[] = array( 'name'	=> 'Post Options',
								'icon'	=> $url.'post-icon.png',
								'type'	=> 'heading');

					$options[] = array( 'name'	=> 'Blog Page Categories',
										'desc'	=> 'Selected categories will hold the posts to display in blog page template. ( template : template_blog.php)',
										'id'	=> $shortname.'_blogacats',
										'std'	=> '',
										'type'	=> 'multicheck',
										'options'	=> $atp_theme->atp_variable('categories'));

					$options[] = array(	'name'	=> 'About Author',
										'desc'	=> 'Check this if you wish to disable the Author Info Box in post single page at the end of the post',
										'id'	=> $shortname.'_aboutauthor',
										'std'	=> '',
										'type'	=> 'checkbox');	

					$options[] = array(	'name'	=> 'Related Posts',
										'desc'	=> 'Check this if you wish to disable the related posts in post single page (based on tags).',
										'id'	=> $shortname.'_relatedposts',
										'std'	=> '',
										'type'	=> 'checkbox');	

					$options[] = array(	'name'	=> 'Post Comments',
										'desc'	=> 'Select where you wish to display comments on posts or pages.',
										'id'	=> $shortname.'_commentstemplate',
										'std'	=> 'fullpage',
										'options'	=> array(
														'posts'	=> 'Posts Only',
														'pages'	=> 'Pages Only', 
														'both'	=> 'Pages/posts',
														'none'	=> 'None'),
										'type'	=> 'select');

					$options[] = array(	'name'	=> 'Single Page Pagination(Next/Previous)',
										'desc'	=> 'Check this if you wish to disable next/previous Post Single Page',
										'id'	=> $shortname.'_singlenavigation',
										'std'	=> '',
										'type'	=> 'checkbox');
					$options[] = array(	"name"	=> "Single Page Featured Image",
										"desc"	=> 'Check this if you wish to disable Featured Image on  Single Page',
										"id"	=> $shortname."_blogfeaturedimg",
										"std"	=> "",
										"type"	=> "checkbox");

															
			// C U S T O M   S I D E B A R 
			//------------------------------------------------------------------------
			$options[] = array( 'name'	=> 'Sidebars',
								'icon'	=> $url.'sidebar-icon.png',
								'type'	=> 'heading');

			$options[] = array( 'name'	=> 'Custom Sidebars',
								'desc'	=> 'Create the desired name for your site sidebars and go to widgets which will appear in rightsidebar below the footer column widgets. After assigning the widgets in the prefered sidebar you can assign the sidebar to individual pages/posts. Find the custom sidebar in rightside of the wordpress admin panels.',
								'id'	=> $shortname.'_customsidebar',
								'std'	=> '',
								'type'	=> 'customsidebar');

			// F O O T E R 
			//------------------------------------------------------------------------
			$options[] = array( 'name'	=> 'Footer',
								'icon'	=> $url.'footer-icon.png',
								'type'	=> 'heading');										

			$options[] = array(	'name'	=> 'Footer Sidebar',	
								'desc'	=> 'Check this if you wish to disable the Footer Sidebar containing the widgets with columnized',
								'id'	=> $shortname.'_footer_sidebar',
								'std'	=> '',
								'type'	=> 'checkbox');
		
			$options[] = array( 'name' 		=> 'Footer Columns',
								'desc' 		=> 'Select the Columns Layout Style from below images to display footer sidebar area. After selecting save the options and go to your widgets panel to assign the widgets in each column',
								'id'   		=> $shortname.'_footerwidgetcount',
								'std'  		=> '2',
								'type' 		=> 'images',
								'options' 	=> array(
												'1' => $url . 'columns/2col.png',
												'2' => $url . 'columns/2col.png',
												'3' => $url . 'columns/3col.png',
												'4' => $url . 'columns/4col.png',
												'5' => $url . 'columns/5col.png',
												'6' => $url . 'columns/6col.png',
												'half_one_half'		=> $url . 'columns/half_one_half.png',
												'half_one_third'	=> $url . 'columns/half_one_third.png',
												'one_half_half'		=> $url . 'columns/one_half_half.png',
												'one_third_half'	=> $url . 'columns/one_third_half.png')
									);

			$options[] = array(	'name'	=> 'Copyright Left',	
								'desc'	=> 'Enter the content that you wish the display on the left side',
								'id'	=> $shortname.'_copy_left',
								'std'	=> '',
								'type'	=> 'textarea');
								
			$options[] = array(	'name'	=> 'Copyright Right',	
								'desc'	=> 'Enter the content that you wish the display on the right side',
								'id'	=> $shortname.'_copy_right',
								'std'	=> '',
								'type'	=> 'textarea');
			// S O C I A B L E S
			//------------------------------------------------------------------------
			$options[] = array( 'name'	=> 'Sociables',
								'icon'	=> $url.'link-icon.png',
								'type'	=> 'heading');

			$options[] = array(	'name'	=> 'Sociables',	
								'desc'	=> 'Click Add New to add sociables where you can edit/add/delete.<br><br> If you want to have a different icon please you icon png or gif file in sociables directory located in theme images directory',
								'id'	=> $shortname.'_social_bookmark',
								'std'	=> '',
								'type'	=> 'custom_socialbook_mark');
			//Sticky Bar
			// -----------------------------------------------------------------------
			
			$options[] = array( 'name'	=> 'Sticky Bar',
								'icon'	=> $url.'sticky-icon.png',
								'type'	=> 'heading');
			
			$options[] = array( 'name'	=> 'Sticky Notice Bar',
								'desc'	=> 'Check this if you wish to hide the sticky bar on top.',
								'id'	=> $shortname.'_stickybar',
								'std'	=> '',
								'type'	=> 'checkbox');
	
			$options[] = array( 'name'	=> 'Sticky Content',
								'desc'	=> 'Enter the content which will be displayed in sticky bar',
								'id'	=> $shortname.'_stickycontent',
								'std'	=> '',
								'type'	=> 'textarea');
									
			$options[] = array( 'name'	=> 'Sticky Bar Background Color',
								'desc'	=> 'Select the color you want to assign for the Sticky Bar',
								'id'	=> $shortname.'_stickybarcolor',
								'std'	=> '',
								'type'	=> 'color');
			
			// L A N G U A G E S
			//------------------------------------------------------------------------
			$options[] = array( 'name'	=> 'Language',
								'icon'	=> $url.'lang-icon.png',
								'type'	=> 'heading');

			$options[] = array(	'name'		=> 'Read More',	
								'desc'		=> 'Read more text on sliders and other different areas of the theme',
								'id'		=> $shortname.'_readmoretxt',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');

			$options[] = array(	'name'		=> 'Visit Site',	
								'desc'		=> 'Visit Site text on Portfolio page',
								'id'		=> $shortname.'_visitsitetxt',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');

			$options[] = array( 'name'		=> 'Post Single Page',
								'desc'		=> 'Custom text which appears on Post Single Page',
								'id'		=> $shortname.'_postsinglepage',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');
	
			$options[] = array( 'name'		=> '404 Page',
								'desc'		=> 'Custom text which appears on 404 Error page',
								'id'		=> $shortname.'_error404txt',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');
										
			$options[] = array( 'name'		=> 'Start Time',
								'desc'		=> 'Insert your event start time',
								'id'		=> $shortname.'_starttime',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');	

			$options[] = array( 'name'		=> 'End Time',
								'desc'		=> 'Insert your event end time',
								'id'		=> $shortname.'_endtime',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');	

			$options[] = array( 'name'		=> 'Date',
								'desc'		=> 'Insert your event date',
								'id'		=> $shortname.'_date',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');

			$options[] = array( 'name'		=> 'Venue',
								'desc'		=> 'Insert your event venue',
								'id'		=> $shortname.'_venue',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');	
											
			$options[] = array( 'name'		=> 'Location',
								'desc'		=> 'Insert your event Location/Address',
								'id'		=> $shortname.'_location',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');

			$options[] = array( 'name'		=> 'Project Date',
								'desc'		=> 'Insert your Portfolio Progect Date',
								'id'		=> $shortname.'_project_date',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');

			$options[] = array( 'name'		=> 'category',
								'desc'		=> 'Insert your Portfolio category',
								'id'		=> $shortname.'_category',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');

			$options[] = array( 'name'		=> 'ProjectURL',
								'desc'		=> 'Insert your Portfolio ProjectURL',
								'id'		=> $shortname.'_project_url',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');

			$options[] = array( 'name'		=> 'Skills',
								'desc'		=> 'Insert your Portfolio Skills',
								'id'		=> $shortname.'_skills',
								'std'		=> '',
								'type'		=> 'text',
								'inputsize'	=> '');												
			}
	}
?>