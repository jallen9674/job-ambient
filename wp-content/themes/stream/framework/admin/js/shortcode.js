
	var shortcode = {
		init:function(){
			jQuery('.primary_select select').val('');
			jQuery('.primary_select select').change(function(){
			jQuery(".secondary_select").hide();
				if(this.value !=''){
					if(jQuery("#secondary_"+this.value).show().children('.tertiary_select').size() == 0){
						jQuery("#secondary_"+this.value).show();
					}
				}
			});
			
			jQuery('#sendtoeditor').click(function(){
				shortcode.sendToEditor();
			});
			
			jQuery('.secondaryselect select').val('');
			jQuery('.secondaryselect select').change(function(){
				jQuery(this).closest('.secondary_select').children('.tertiary_select').hide();
				if(this.value !=''){
					jQuery("#atp-"+this.value).show();
				}
			});
		},
		generate:function(){
			var type = jQuery('.primary_select select').val();
			switch(type){
				// C O L U M N   L A Y O U T S 
				//--------------------------------------------------------
				case 'Columns':
						var types =jQuery('[name="Columns_type"]').val();
						if(types != ''){
							var content =jQuery('[name="Columns_content"]').val();
							return '\n['+types+']\n'+content+'\n[/'+types+']\n';
						}else{
							return '';
						}
						break;	
				
				// L A Y O U T S 
				//--------------------------------------------------------
				case 'Layouts':
						var secondary_type =jQuery('#secondary_Layouts select').val();
						switch(secondary_type) {
							//--------------------------------------------------------
							case 'one_half_layout':	
									var one_half_layout =jQuery('[name="Layouts_one_half_layout_layout_1"]').val();
									var one_half_layout_last =jQuery('[name="Layouts_one_half_layout_layout_2"]').val();
									return '[one_half]'+one_half_layout+'[/one_half]\n[one_half_last]'+one_half_layout_last+'[/one_half_last]\n';	
									break;
							//--------------------------------------------------------
							case 'one_third_layout':
									var one_third_layout1 = jQuery('[name="Layouts_one_third_layout_one_third_1"]').val();
									var one_third_layout2 = jQuery('[name="Layouts_one_third_layout_one_third_2"]').val();
									var one_third_layout3 = jQuery('[name="Layouts_one_third_layout_one_third_3"]').val();
									return '[one_third]'+one_third_layout1+'[/one_third]\n[one_third]'+one_third_layout2+'[/one_third]\n[one_third_last]'+one_third_layout3+'[/one_third_last]\n';	
									break;
							//--------------------------------------------------------
							case 'one_fourth_layout':
									var one_fourth_layout1 = jQuery('[name="Layouts_one_fourth_layout_one_fourth_1"]').val();
									var one_fourth_layout2 = jQuery('[name="Layouts_one_fourth_layout_one_fourth_2"]').val();
									var one_fourth_layout3 = jQuery('[name="Layouts_one_fourth_layout_one_fourth_3"]').val();
									var one_fourth_layout4 = jQuery('[name="Layouts_one_fourth_layout_one_fourth_4"]').val();
									return '[one_fourth]'+one_fourth_layout1+'[/one_fourth]\n[one_fourth]'+one_fourth_layout2+'[/one_fourth]\n[one_fourth]'+one_fourth_layout3+'[/one_fourth]\n[one_fourth_last]'+one_fourth_layout4+'[/one_fourth_last]\n';	
									break;
							//--------------------------------------------------------
							case 'one5thlayout':
									var one5thlayout1 = jQuery('[name="Layouts_one5thlayout_one5thlayout1"]').val();
									var one5thlayout2 = jQuery('[name="Layouts_one5thlayout_one5thlayout2"]').val();
									var one5thlayout3 = jQuery('[name="Layouts_one5thlayout_one5thlayout3"]').val();
									var one5thlayout4 = jQuery('[name="Layouts_one5thlayout_one5thlayout4"]').val();
									var one5thlayout5 = jQuery('[name="Layouts_one5thlayout_one5thlayout5"]').val();
									return '[one_fifth]'+one5thlayout1+'[/one_fifth]\n[one_fifth]'+one5thlayout2+'[/one_fifth]\n[one_fifth]'+one5thlayout3+'[/one_fifth]\n[one_fifth]'+one5thlayout4+'[/one_fifth]\n[one_fifth_last]'+one5thlayout5+'[/one_fifth_last]\n';
									break;
							//--------------------------------------------------------
							case 'one6thlayout':
									var one6thlayout1 = jQuery('[name="Layouts_one6thlayout_one6thlayout1"]').val();
									var one6thlayout2 = jQuery('[name="Layouts_one6thlayout_one6thlayout2"]').val();
									var one6thlayout3 = jQuery('[name="Layouts_one6thlayout_one6thlayout3"]').val();
									var one6thlayout4 = jQuery('[name="Layouts_one6thlayout_one6thlayout4"]').val();
									var one6thlayout5 = jQuery('[name="Layouts_one6thlayout_one6thlayout5"]').val();
									var one6thlayout6 = jQuery('[name="Layouts_one6thlayout_one6thlayout6"]').val();
									return '[one_sixth]'+one6thlayout1+'[/one_sixth]\n[one_sixth]'+one6thlayout2+'[/one_sixth]\n[one_sixth]'+one6thlayout3+'[/one_sixth]\n[one_sixth]'+one6thlayout4+'[/one_sixth]\n[one_sixth]'+one6thlayout5+'[/one_sixth]\n[one_sixth_last]'+one6thlayout6+'[/one_sixth_last]\n';
									break;
							//--------------------------------------------------------
							case 'one_3rd_2rd':
									var one3rd2rd_1 = jQuery('[name="Layouts_one_3rd_2rd_one3rd2rd_1"]').val();
									var one3rd2rd_2 = jQuery('[name="Layouts_one_3rd_2rd_one3rd2rd_2"]').val();
									return '[one_third]'+one3rd2rd_1+'[/one_third]\n[two_third_last]'+one3rd2rd_2+'[/two_third_last]\n';	
									break;
							//--------------------------------------------------------
							case 'two_3rd_1rd':
									var two3rd1rd_1 = jQuery('[name="Layouts_two_3rd_1rd_two3rd1rd_1"]').val();
									var two3rd1rd_2 = jQuery('[name="Layouts_two_3rd_1rd_one3rd2rd_2"]').val();
									return '[two_third]'+two3rd1rd_1+'[/two_third]\n[one_third_last]'+two3rd1rd_2+'[/one_third_last]\n';	
									break;
							//--------------------------------------------------------
							case 'One_4th_Three_4th':
									var One4thThree4th_1 = jQuery('[name="Layouts_One_4th_Three_4th_One4thThree4th_1"]').val();
									var One4thThree4th_2 = jQuery('[name="Layouts_One_4th_Three_4th_One4thThree4th_2"]').val();
									return '[one_fourth]'+One4thThree4th_1+'[/one_fourth]\n[three_fourth_last]'+One4thThree4th_2+'[/three_fourth_last]\n';
									break;
							//--------------------------------------------------------
							case 'Three_4th_One_4th':
									var Three4thOne4th_1 = jQuery('[name="Layouts_Three_4th_One_4th_Three4thOne4th_1"]').val();
									var Three4thOne4th_2 = jQuery('[name="Layouts_Three_4th_One_4th_Three4thOne4th_2"]').val();
									return '[three_fourth]'+Three4thOne4th_1+'[/three_fourth]\n[one_fourth_last]'+Three4thOne4th_2+'[/one_fourth_last]\n';	
									break;
							//--------------------------------------------------------
							case 'One_4th_One_4th_One_half':
									var One_4th_One_4th_One_half_1 = jQuery('[name="Layouts_One_4th_One_4th_One_half_One4thOne4thOnehalf_1"]').val();
									var One_4th_One_4th_One_half_2 = jQuery('[name="Layouts_One_4th_One_4th_One_half_One4thOne4thOnehalf_2"]').val();
									var One_4th_One_4th_One_half_3 = jQuery('[name="Layouts_One_4th_One_4th_One_half_One4thOne4thOnehalf_3"]').val();
									return '[one_fourth]'+One_4th_One_4th_One_half_1+'[/one_fourth]\n[one_fourth]'+One_4th_One_4th_One_half_2+'[/one_fourth]\n[one_half_last]'+One_4th_One_4th_One_half_3+'[/one_half_last]\n';
									break;
							//--------------------------------------------------------
							case 'One_half_One_4th_One_4th':
									var OnehalfOne4thOne4th_1 = jQuery('[name="Layouts_One_half_One_4th_One_4th_OnehalfOne4thOne4th_1"]').val();
									var OnehalfOne4thOne4th_2 = jQuery('[name="Layouts_One_half_One_4th_One_4th_OnehalfOne4thOne4th_2"]').val();
									var OnehalfOne4thOne4th_3 = jQuery('[name="Layouts_One_half_One_4th_One_4th_OnehalfOne4thOne4th_3"]').val();
									return '[one_half]'+OnehalfOne4thOne4th_1+'[/one_half]\n[one_fourth]'+OnehalfOne4thOne4th_2+'[/one_fourth]\n[one_fourth_last]'+OnehalfOne4thOne4th_3+'[/one_fourth_last]\n';
									break;
							//--------------------------------------------------------		
							case 'One_4th_One_half_One_4th':
									var One4thOnehalfOne4th_1 = jQuery('[name="Layouts_One_4th_One_half_One_4th_One4thOnehalfOne4th_1"]').val();
									var One4thOnehalfOne4th_2 = jQuery('[name="Layouts_One_4th_One_half_One_4th_One4thOnehalfOne4th_2"]').val();
									var One4thOnehalfOne4th_3 = jQuery('[name="Layouts_One_4th_One_half_One_4th_One4thOnehalfOne4th_3"]').val();
									return '[one_fourth]'+One4thOnehalfOne4th_1+'[/one_fourth]\n[one_half]'+One4thOnehalfOne4th_2+'[/one_half]\n[one_fourth_last]'+One4thOnehalfOne4th_3+'[/one_fourth_last]\n';
									break;
							//--------------------------------------------------------
							case 'One_5th_Four_5th':
									var One5thFour5th_1 = jQuery('[name="Layouts_One_5th_Four_5th_One5thFour5th_1"]').val();
									var One5thFour5th_2 = jQuery('[name="Layouts_One_5th_Four_5th_One5thFour5th_2"]').val();
									return '[one_fifth]'+One5thFour5th_1+'[/one_fifth]\n[four_fifth_last]'+One5thFour5th_2+'[/four_fifth_last]\n';
									break;
							//--------------------------------------------------------
							case 'Four_5th_One_5th':
									var Four5thOne5th_1 = jQuery('[name="Layouts_Four_5th_One_5th_Four5thOne5th_1"]').val();
									var Four5thOne5th_2 = jQuery('[name="Layouts_Four_5th_One_5th_Four5thOne5th_2"]').val();
									return '[four_fifth]'+Four5thOne5th_1+'[/four_fifth]\n[one_fifth_last]'+Four5thOne5th_2+'[/one_fifth_last]\n';
									break;
							//--------------------------------------------------------
							case 'Two_5th_Three_5th':
									var Two5thThree5th_1 = jQuery('[name="Layouts_Two_5th_Three_5th_Two5thThree5th_1"]').val();
									var Two5thThree5th_2 = jQuery('[name="Layouts_Two_5th_Three_5th_Two5thThree5th_2"]').val();
									return '[two_fifth]'+Two5thThree5th_1+'[/two_fifth]\n[three_fifth_last]'+Two5thThree5th_2+'[/three_fifth_last]\n';
									break;
							//--------------------------------------------------------
							case 'Three_5th_Two_5th':
									var Three5thTwo5th_1 = jQuery('[name="Layouts_Three_5th_Two_5th_Three5thTwo5th_1"]').val();
									var Three5thTwo5th_2 = jQuery('[name="LayoutsLayouts_Three_5th_Two_5th_Three5thTwo5th_2"]').val();
									return '[three_fifth]'+Three5thTwo5th_1+'[/three_fifth]\n[two_fifth_last]'+Three5thTwo5th_2+'[/two_fifth_last]\n';	
									break;
						}
						break;


							// D R O P C A P 
							//--------------------------------------------------------
							case 'dropcap1':
									var text = jQuery('[name="dropcap1_dropcap_text"]').val();
									var color = jQuery('[name="dropcap1_color"]').val();
									if(color !== '')	{color = ' color="'+color+'"';}
									return '[dropcap1'+color+']'+text+'[/dropcap1]';
									break;
							// D R O P C A P   2
							//--------------------------------------------------------
							case 'dropcap2':
									var text = jQuery('[name="dropcap2_dropcap_text"]').val();
									var bgcolor = jQuery('[name="dropcap2_bgcolor"]').val();
									if(bgcolor !== '')	{bgcolor = ' bgcolor="'+bgcolor+'"';}
									return '[dropcap2'+bgcolor+']'+text+'[/dropcap2]';
									break;
							// D R O P C A P   3
							//--------------------------------------------------------
							case 'dropcap3':
									var text = jQuery('[name="dropcap3_dropcap_text"]').val();
									var color = jQuery('[name="dropcap3_color"]').val();
									if(color !== '')	{color = ' color="'+color+'"';}
									return '[dropcap3'+color+']'+text+'[/dropcap3]';
									break;
							
							// H I G H L I G H T 
							//--------------------------------------------------------
							case 'highlight':
									var textcolor = jQuery('[name="highlight_textcolor"]').val();
									var bgcolor = jQuery('[name="highlight_bgcolor"]').val();
									var text = jQuery('[name="highlight_text"]').val();
									if(text !== '')	{ text = ''+text+'';}
									if(bgcolor !== '')	{ bgcolor= ' bgcolor="'+bgcolor+'"';}
									if(textcolor !== ''){ textcolor = ' textcolor="'+textcolor+'"';}
									return '\n[highlight'+bgcolor+textcolor+']'+ text +'[/highlight]\n';
									break;
							// F A N C Y   H E A D I N G 
							//--------------------------------------------------------
							case 'fancyheading':
									var textcolor = jQuery('[name="fancyheading_textcolor"]').val();
									var bgcolor = jQuery('[name="fancyheading_bgcolor"]').val();
									var text = jQuery('[name="fancyheading_text"]').val();
									if(text !== '')	{ text = ''+text+'';}
									if(bgcolor !== '')	{ bgcolor= ' bgcolor="'+bgcolor+'"';}
									if(textcolor !== ''){ textcolor = ' textcolor="'+textcolor+'"';}
									return '\n[fancyheading'+bgcolor+textcolor+']'+ text +'[/fancyheading]\n';
									break;

				// B L O C K Q U O T E 
				//--------------------------------------------------------
				case 'blockquote':
						var align = jQuery('[name="blockquote_align"]').val();
						var cite = jQuery('[name="blockquote_cite"]').val();
						var citelink = jQuery('[name="blockquote_citelink"]').val();
						var content = jQuery('[name="blockquote_content"]').val();				
						var width = jQuery('[name="blockquote_width"]').val();
						if(content !== '')	{ content = ''+content+'';}
						if(align !== '')	{ align = ' align="'+align+'"';}
						if(cite !== '')		{ cite = ' cite="'+cite+'"';}
						if(citelink !== '')		{ citelink = ' citelink="'+citelink+'"';}
						if(width !== '')		{ width = ' width="'+width+'"';}
						return '[blockquote'+align+width+cite+citelink+']'+content+'[/blockquote]\n';
						break;
				// S T Y L E D   L I S T S 
				//--------------------------------------------------------
				case 'styledlist':
						var style = jQuery('[name="styledlist_style"]').val();
						var color = jQuery('[name="styledlist_color"]').val();
						var content = jQuery('[name="styledlist_content"]').val();
						if(content !== '')	{ content = ''+content+'';}
						if(style !== '')	{ style= ' style="'+style+'"';}
						if(color !== '')	{ color = ' color="'+color+'"';}
						return '\n[list'+style+color+']\n'+ content +'\n[/list]\n';
						break;
				// I C O N S 
				//--------------------------------------------------------
				case 'icon':
						var text = jQuery('[name="icon_text"]').val();
						var style = jQuery('[name="icon_style"]').val();
						var color = jQuery('[name="icon_color"]').val();
						if(text !== '')	{ text = ''+text+'';}
						if(style !== '')	{ style= ' style="'+style+'"';}
						if(color !== '')	{ color = ' color="'+color+'"';	}
						return '\n[icon'+style+color+']'+ text +'[/icon]\n';
						break;
				// I C O N   L I N K S
				//--------------------------------------------------------
				case 'iconlinks':
						var style = jQuery('[name="iconlinks_style"]').val();
						var color = jQuery('[name="iconlinks_color"]').val();
						var href = jQuery('[name="iconlinks_href"]').val();
						var target = jQuery('[name="iconlinks_target"]').val();
						var text = jQuery('[name="iconlinks_text"]').val();
						if(text !== '')	{ text = ''+text+'';}
						if(style !== '')	{ style= ' style="'+style+'"'; }
						if(color !== '')	{ color = ' color="'+color+'"'; }
						if(href !== '')		{ href = ' href="'+href+'"'; }
						if(target !== '')	{ target = ' target="'+target+'"';}
						return '\n[icon'+style+color+href+target+']'+ text +'[/icon]\n';
						break;
				// B U T T O N  
				//--------------------------------------------------------
				case 'Button':
						var id = jQuery('[name="Button_id"]').val();
						var subclass =  jQuery('[name="Button_subclass"]').val();
						var link = jQuery('[name="Button_link"]').val();
						var linktarget =  jQuery('[name="Button_linktarget"]').val();
						var color =  jQuery('[name="Button_color"]').val();
						var align =  jQuery('[name="Button_align"]').val();
						var bgcolor =  jQuery('[name="Button_bgcolor"]').val();
						var hoverbgcolor =  jQuery('[name="Button_hoverbgcolor"]').val();
						var hovertextcolor =  jQuery('[name="Button_hovertextcolor"]').val();
						var textcolor =  jQuery('[name="Button_textcolor"]').val();
						var size =  jQuery('[name="Button_size"]').val();
						var width =  jQuery('[name="Button_width"]').val();
						var fullwidth =  jQuery('[name="Button_fullwidth"]');
							if(fullwidth.is('.atp-button')){
							if(fullwidth.is(':checked')){
							fullwidth= ' fullwidth="true"';	
							}else{
							fullwidth= ' fullwidth="false"';		
							}
						}
						var text = jQuery('[name="Button_text"]').val();
						if(text !== '')	{ text = ''+text+'';}
						if(id !== '')			{ id= ' id="'+id+'"'; }
						if(subclass !== '')			{ subclass= ' class="'+subclass+'"'; }
						if(link !== '')			{ link= ' link="'+link+'"'; }
						if(linktarget !== '')	{ linktarget= ' linktarget="'+linktarget+'"'; }
						if(color !== '')		{ color= ' color="'+color+'"';}
						if(align !== '')		{ align= ' align="'+align+'"';}
						if(bgcolor !== '')		{ bgcolor= ' bgcolor="'+bgcolor+'"';}
						if(hoverbgcolor !== '')	{ hoverbgcolor= ' hoverbgcolor="'+hoverbgcolor+'"';	}
						if(hovertextcolor !== ''){ hovertextcolor= ' hovertextcolor="'+hovertextcolor+'"';}
						if(textcolor !== '')	{ textcolor= ' textcolor="'+textcolor+'"';}
						if(size !== '')			{ size= ' size="'+size+'"';}
						if(width !== '')		{ width= ' width="'+width+'"';}	
						return '\n[button'+id+subclass+link+linktarget+color+align+bgcolor+hoverbgcolor+hovertextcolor+textcolor+size+width+fullwidth+']'+ text +'[/button]\n';
						break;
				// D I V I D E R S 
				//--------------------------------------------------------
				case 'divider':
						var shortcodesub_type =jQuery('#secondary_divider select').val();
									if(shortcodesub_type== 'divider_top')	{
								var title = jQuery('[name="divider_divider_top_text"]').val();					
									return '\n[divider_top]'+title+'[/divider_top]\n';
								}else if(shortcodesub_type== 'custom_divider')	{
									var img= jQuery("[name=divider_custom_divider_dividerimg]").val();
									if(img !='') { img = ' img="'+img+'"';}else{img = ''; }
									return '\n[custom_divider'+img+']\n';
								}else{
								return '\n['+jQuery('#secondary_divider select').val()+']\n';
								}
						break;
				// T A B L E 
				//--------------------------------------------------------
				case 'Table':
						var width =  jQuery('[name="Table_width"]').val();
						var align =  jQuery('[name="Table_align"]').val();
						var text = jQuery('[name="Table_text"]').val();
						if(text !== '')	{ text = ''+text+'';}
						if(width !== '')	{ width= ' width="'+width+'"';}
						if(align !== '')	{ align= ' align="'+align+'"';}
						return '\n[fancytable'+align+width+']'+ text +'[/fancytable]\n';
						break;
				// T O G G L E 
				//--------------------------------------------------------
				case 'Toggle':
						var heading =  jQuery('[name="Toggle_heading"]').val();
						var text = jQuery('[name="Toggle_text"]').val();
						if(text !== '')	{ text = ''+text+'';}
						if(heading !== '')	{ heading= ' heading="'+heading+'"';}
						return '\n[toggle'+heading+']\n'+ text+'\n[/toggle]\n';
						break;
				// F A N C Y   T O G G L E 
				//--------------------------------------------------------
				case 'FancyToggle':
						var heading = jQuery('[name="FancyToggle_heading"]').val();
						var text = jQuery('[name="FancyToggle_text"]').val();
						if(text !== '')	{ text = ''+text+'';}
						if(heading !== '')	{ heading= ' heading="'+heading+'"';}
						return '\n[fancytoggle'+heading+']\n'+ text+'\n[/fancytoggle]\n';
						break;
				// B O X E S  
				//--------------------------------------------------------
				case 'Boxes':
						var shortcodesub_type =jQuery('#secondary_Boxes select').val();
						switch(shortcodesub_type) {
							// F A N C Y B O X 
							//--------------------------------------------------------
							case 'fancybox':
									var title = jQuery('[name="Boxes_fancybox_title"]').val();
									var titlebgcolor = jQuery('[name="Boxes_fancybox_titlebgcolor"]').val();
									var titlecolor = jQuery('[name="Boxes_fancybox_titlecolor"]').val();
									var textcolor = jQuery('[name="Boxes_fancybox_textcolor"]').val();
									var bordercolor = jQuery('[name="Boxes_fancybox_bordercolor"]').val();
									var boxbgcolor = jQuery('[name="Boxes_fancybox_boxbgcolor"]').val();
									var ribbon = jQuery('[name="Boxes_fancybox_ribbon"]').val();
									var text = jQuery('[name="Boxes_fancybox_text"]').val();
									if(text !== '')	{ text = ''+text+'';}
									if(title !== '')		{ title= ' title="'+title+'"';}
									if(titlebgcolor !== '')		{ titlebgcolor= ' titlebgcolor="'+titlebgcolor+'"'; }
									if(titlecolor !== '')	{ titlecolor= ' titlecolor="'+titlecolor+'"';}
									if(textcolor !== '')	{ textcolor= ' textcolor="'+textcolor+'"';}
									if(boxbgcolor !== '')	{ boxbgcolor= ' boxbgcolor="'+boxbgcolor+'"';}
									if(bordercolor !== '')	{ bordercolor= ' bordercolor="'+bordercolor+'"';}
									if(ribbon !== '')		{ ribbon= ' ribbon="'+ribbon+'"';}
									return '\n[fancy_box'+title+titlebgcolor+titlecolor+textcolor+bordercolor+boxbgcolor+ribbon+']'+ text +'[/fancy_box]\n';
									break;
							// S P E C I A L   B O X 
							//--------------------------------------------------------
							case 'specialbox':
									var boxbgcolor = jQuery('[name="Boxes_specialbox_boxbgcolor"]').val();
									var bordercolor = jQuery('[name="Boxes_specialbox_bordercolor"]').val();
									var ribbon = jQuery('[name="Boxes_specialbox_ribbon"]').val();
									var text = jQuery('[name="Boxes_specialbox_text"]').val();
									if(text !== '')	{ text = ''+text+'';}
									if(boxbgcolor !== '')	{ boxbgcolor= ' boxbgcolor="'+boxbgcolor+'"';}
									if(bordercolor !== '')	{ bordercolor= ' bordercolor="'+bordercolor+'"';}
									if(ribbon !== '')		{ ribbon= ' ribbon="'+ribbon+'"';}
									return '\n[special_box'+boxbgcolor+bordercolor+ribbon+']'+ text +'[/special_box]\n';
									break;
							// M I N I M A L   B O X 
							//--------------------------------------------------------
							case 'minimalbox':
									var title = jQuery('[name="Boxes_minimalbox_title"]').val();
									var titlebgcolor = jQuery('[name="Boxes_minimalbox_titlebgcolor"]').val();
									var titlecolor = jQuery('[name="Boxes_minimalbox_titlecolor"]').val();
									var ribbon = jQuery('[name="Boxes_minimalbox_ribbon"]').val();
									var text = jQuery('[name="Boxes_minimalbox_text"]').val();
									if(text !== '')	{ text = ''+text+'';}
									if(title !== '')		{ title= ' title="'+title+'"';}
									if(titlecolor !== '')	{ titlecolor= ' titlecolor="'+titlecolor+'"';}
									if(titlebgcolor !== '')	{ titlebgcolor= ' titlebgcolor="'+titlebgcolor+'"';}
									if(ribbon !== '')		{ ribbon= ' ribbon="'+ribbon+'"';}
									return '\n[minimal_box'+title+titlebgcolor+titlecolor+ribbon+']'+ text +'[/minimal_box]\n';
									break;
							
						}
						break;
						// M E S S A G E   B O X 
						//--------------------------------------------------------
						case 'messagebox':
								var msgtype =  jQuery('[name="messagebox_msgtype"]').val();
								var text =  jQuery('[name="messagebox_text"]').val();
								if(text !== '')	{ text = ''+text+'';}
								if(msgtype == '')		{ msgtype='info'; }
								return '\n['+msgtype+']\n'+ text +'\n[/'+msgtype+']\n';
								break;
						// N O T E   B O X 
						//--------------------------------------------------------
						case 'notebox':
								var align = jQuery('[name="notebox_align"]').val();
								var width = jQuery('[name="notebox_width"]').val();
								var title = jQuery('[name="notebox_title"]').val();
								var text = jQuery('[name="notebox_text"]').val();
								if(text !== '')	{ text = ''+text+'';}
								if(align !== '')	{align= ' align="'+align+'"';}
								if(width !== '')	{width= ' width="'+width+'"';}
								if(title !== '')	{title= ' title="'+title+'"';}
								return '\n[note'+align+width+title+']'+ text +'[/note]\n';
								break;

				// T A B S 
				//--------------------------------------------------------
				case 'Tabs':
						var shortcodesub_tabs=jQuery('#secondary_Tabs select').val();
							count = shortcodesub_tabs.replace('t', '');
							var stabstype =jQuery('[name="Tabs_'+shortcodesub_tabs+'_ctabs'+'"]').val();
							//alert(shortcodesub_tabs);

						var outputs = '[minitabs tabtype="'+stabstype+'"]';
						for(var i=1;i<=count;i++){
							var title =jQuery('[name="Tabs_'+shortcodesub_tabs+'_title_'+i+'"]').val();
							var bgcolor =jQuery('[name="Tabs_'+shortcodesub_tabs+'_titlebgcolor_'+i+'"]').val();
							var color =jQuery('[name="Tabs_'+shortcodesub_tabs+'_titlecolor_'+i+'"]').val();
							var content =jQuery('[name="Tabs_'+shortcodesub_tabs+'_text_'+i+'"]').val();
							var stabstype =jQuery('[name="Tabs_'+shortcodesub_tabs+'_ctabs'+'"]').val();
							//var icon =jQuery('[name="Tabs_'+shortcodesub_tabs+'_icon'+i+'"]').val();
							if(title !== '')	{title= ' title="'+title+'"';}
							if(bgcolor !== '')	{bgcolor= ' tabcolor="'+bgcolor+'"';}
							if(color !== '')	{color= ' textcolor="'+color+'"';}
							if(content !== '')	{ content = ''+content+'';}
							//if(icon !== '')	{ icon= ' icon="'+icon+'"';}
							outputs +='[tab'+title+color+bgcolor+']\n'+content+'\n[/tab]\n';
						}
						outputs +='[/minitabs]';
						return outputs;
						break;
				// Accordion
				//--------------------------------------------------------
				case 'accordion':
						var shortcodesub_accordion=jQuery('#secondary_accordion select').val();
						 
						var outputs = '[accordioncontent]';
						for(var i=1;i<=shortcodesub_accordion;i++){
							var title =jQuery('[name="accordion_'+shortcodesub_accordion+'_title_'+i+'"]').val();
							var content =jQuery('[name="accordion_'+shortcodesub_accordion+'_text_'+i+'"]').val();
							
							if(title !== '')	{title= ' title="'+title+'"';}
							if(content !== '')	{ content = ''+content+'';}
							outputs +='[accordion'+title+']\n'+content+'\n[/accordion]\n';
						}
						outputs +='[/accordioncontent]';
						return outputs;
						break;
				// 	P R I C I N G T A B L E
				//--------------------------------------------------------
				case 'pricing':
						var shortcodesub_pricing=jQuery('#secondary_pricing select').val();
						var shortcodesub_pricing=jQuery('#secondary_pricing select').val();
						var outputs ='[pricingcolumns]';
						for(var i=1;i<=shortcodesub_pricing;i++){	
							var shortcodesub_title =jQuery('[name="pricing_'+shortcodesub_pricing+'_title_'+i+'"]').val();
							var shortcodesubheading =jQuery('[name="pricing_'+shortcodesub_pricing+'_heading_'+i+'"]').val();
							var shortcodesub_content =jQuery('[name="pricing_'+shortcodesub_pricing+'_desc_'+i+'"]').val();
							var shortcodesub_bgcolor =jQuery('[name="pricing_'+shortcodesub_pricing+'_bgcolor_'+i+'"]').val();
							var shortcodesub_color =jQuery('[name="pricing_'+shortcodesub_pricing+'_color_'+i+'"]').val();
							
							if(shortcodesub_title!="")			{ title = ' title="'+shortcodesub_title+'"';}else{title	 = '';}
							if(shortcodesub_bgcolor!="")			{ headingbgcolor = ' headingbgcolor="'+shortcodesub_bgcolor+'"';}else{headingbgcolor	 = '';}
							if(shortcodesub_color!="")			{ headingcolor = ' headingcolor="'+shortcodesub_color+'"';}else{headingcolor	 = '';}
							if(shortcodesubheading!="")			{ heading = ' heading="'+shortcodesubheading+'"';}else{heading	 = '';}
			
							outputs +='[col'+title+headingbgcolor+headingcolor+heading+']\n'+shortcodesub_content+'\n[/col]\n';
						}
						outputs +='[/pricingcolumns]';
						return  outputs;
						break;
				// F L E X   S L I D E R 
				//--------------------------------------------------------
				case 'flexslider':
						var shortcodesub_slider=jQuery('#secondary_flexslider select').val();
				
						switch(shortcodesub_slider){
							// P O S T  
							//--------------------------------------------------------
							case 'post':
									var effect = jQuery('[name="flexslider_post_flexeffect"]').val();
									var cat = jQuery('[name="flexslider_post_flexcats[]"]').val();
									var speed = jQuery('[name="flexslider_post_flexanimspeed"]').val();
									var limits = jQuery('[name="flexslider_post_flexslidelimit"]').val();
									var width = jQuery('[name="flexslider_post_width"]').val();
									var height = jQuery('[name="flexslider_post_height"]').val();
									var navigation =  jQuery('[name="flexslider_post_navigation"]');
									if(navigation.is('.atp-button')){
										if(navigation.is(':checked')){
											navigation= ' navigation="true"';	
										}else{
											navigation= ' navigation="false"';		
										}
									}
									if(effect!="")			{ effect = ' effect="'+effect+'"';}else{effect	 = '';}
									if(cat!="")				{ cat = ' cat="'+cat+'"';}else{cat	 = '';}
									if(speed!="")			{ speed = ' speed="'+speed+'"';}else{speed	 = '';}
									if(limits!="")			{ limits = ' limits="'+limits+'"';}else{limits	 = '';}
									if(width!="")			{ width = ' width="'+width+'"';}else{width	 = '';}
									if(height!="")			{ height = ' height="'+height+'"';}else{height	 = '';}

									return '\n[slider'+effect+cat+speed+limits+width+height+navigation+']\n';	
									break;
							// S L I D E R 
							//--------------------------------------------------------
							case 'slider':
									var effect = jQuery('[name="flexslider_slider_flexeffect"]').val();					
									var speed = jQuery('[name="flexslider_slider_flexanimspeed"]').val();
									var limits = jQuery('[name="flexslider_slider_flexslidelimit"]').val();
									var width = jQuery('[name="flexslider_slider_width"]').val();
									var height = jQuery('[name="flexslider_slider_height"]').val();
									var navigation =  jQuery('[name="flexslider_slider_navigation"]');
									if(navigation.is('.atp-button')){
										if(navigation.is(':checked')){
											navigation= ' navigation="true"';	
										}else{
											navigation= ' navigation="false"';		
										}
									}
									if(effect!="")			{ effect = ' effect="'+effect+'"';}else{effect	 = '';}
									if(cat!="")				{ cat = ' cat="'+cat+'"';}else{cat	 = '';}
									if(speed!="")			{ speed = ' speed="'+speed+'"';}else{speed	 = '';}
									if(limits!="")			{ limits = ' limits="'+limits+'"';}else{limits	 = '';}
									if(width!="")			{ width = ' width="'+width+'"';}else{width	 = '';}
									if(height!="")			{ height = ' height="'+height+'"';}else{height	 = '';}
									return '\n[slider'+effect+speed+limits+width+height+navigation+']\n';		
									break;
							// A T T A C H M E N T 
							//--------------------------------------------------------
							case 'postattachment':
									var effect = jQuery('[name="flexslider_postattachment_flexeffect"]').val();					
									var speed = jQuery('[name="flexslider_postattachment_flexanimspeed"]').val();
									var limits = jQuery('[name="flexslider_postattachment_flexslidelimit"]').val();
									var width = jQuery('[name="flexslider_postattachment_width"]').val();
									var height = jQuery('[name="flexslider_postattachment_height"]').val();
									var navigation =  jQuery('[name="flexslider_postattachment_navigation"]');
									if(navigation.is('.atp-button')){
										if(navigation.is(':checked')){
											navigation= ' navigation="true"';	
										}else{
											navigation= ' navigation="false"';		
										}
									}
									if(effect!="")			{ effect = ' effect="'+effect+'"';}else{effect	 = '';}
									if(speed!="")			{ speed = ' speed="'+speed+'"';}else{speed	 = '';}
									if(limits!="")			{ limits = ' limits="'+limits+'"';}else{limits	 = '';}
									if(width!="")			{ width = ' width="'+width+'"';}else{width	 = '';}
									if(height!="")			{ height = ' height="'+height+'"';}else{height	 = '';}
									return '\n[postslider'+effect+speed+limits+width+height+navigation+']\n';		
									break;
						}
						break;	
				// M I N I   G A L L E R Y 
				//--------------------------------------------------------
				case 'minigallery':
						var width = jQuery('[name="minigallery_width"]').val();
						var height = jQuery('[name="minigallery_height"]').val();
						var imageclass = jQuery('[name="minigallery_class"]').val();
						var minigallery_textareaurl = jQuery('[name="minigallery_textarea_url"]').val();
						if(minigallery_textareaurl !="") {content = ''+minigallery_textareaurl+'';}
						if(width!='')	{ width	=' width="'+width+'"';}else{ width=' width="200"';}
						if(height!='')	{ height =' height="'+height+'"';}else{ height=' height="200"';}
						if(imageclass!='')	{ imageclass =' class="'+imageclass+'"';	}		
						return '\n[minigallery'+imageclass+width+height+']'+ content+'[/minigallery]\n';;	
						break;
				// I M A G E 
 				case 'image':
						var title = jQuery('[name="image_title"]').val();
						var caption = jQuery('[name="image_caption"]').val();
						var lightbox = jQuery('[name="image_lightbox"]');
						var width = jQuery('[name="image_width"]').val();
						var height = jQuery('[name="image_height"]').val();
						var align = jQuery('[name="image_align"]').val();
						var alink = jQuery('[name="image_alink"]').val();
						var imgclass =	jQuery('[name="image_class"]').val();
						var target = jQuery('[name="image_target"]').val();
						var imagesrc = jQuery('[name="image_imagesrc"]').val();
						if(imagesrc!='')	{ imagesrc	=' src="'+imagesrc+'"';}
						if(width!='')	{ width	=' width="'+width+'"';}else{ width=' width="200"';}
						if(height!='')	{ height =' height="'+height+'"';}else{ height=' height="200"';}
						if(title!='')	{ title =' title="'+title+'"'; }
						if(caption!='')	{ caption =' caption="'+caption+'"'; }
						if(alink!='')	{ alink	=' link="'+alink+'"';}
						if(target!='')	{ target	=' target="'+target+'"';}
						if(imgclass!='')	{ imgclass	=' class="'+imgclass+'"';}
						if(align!='')	{ align =' align="'+align+'"';	}
						if(lightbox.is('.atp-button')){
							if(lightbox.is(':checked')){
							lightbox= ' lightbox="true"';	
							}else{
							lightbox= ' lightbox="false"';		
							}
						}		
						return '\n[image'+width+height+title+caption+lightbox+align+alink+target+imagesrc+imgclass+']\n';		
						break;
				// P H O T O F R A M E 
				case 'photoframe':
						var imagesrc = jQuery('[name="photoframe_imagesrc"]').val();
						var alt = jQuery('[name="photoframe_alt"]').val();
						var width = jQuery('[name="photoframe_width"]').val();
						var height = jQuery('[name="photoframe_height"]').val();
						if(imagesrc!='')	{imagesrc =' src="'+imagesrc+'"';}
						if(width!='')		{width =' width="'+width+'"';}
						if(height!='')		{height =' height="'+height+'"';}
						if(alt!='')			{alt =' alt="'+alt+'"';	}
						if(alink!='')		{link =' link="'+alink+'"';}
						return '\n[photoframe'+imagesrc+width+height+alt+']\n';
						break;
				// G A L L E R I A 
				case 'galleria':
						var shortcodesub_galleria=jQuery('#secondary_galleria select').val();

						switch(shortcodesub_galleria){
							// A T T A C H M E N T 
							//--------------------------------------------------------
							case'attachment':
									var galleria_width = jQuery('[name="galleria_attachment_width"]').val();
									var galleria_height = jQuery('[name="galleria_attachment_height"]').val();
									var galleria_transition = jQuery('[name="galleria_attachment_transition"]').val();
									var galleria_autoplay = jQuery('[name="galleria_attachment_autoplay"]').val();
									if(galleria_width !="") { width = ' width="'+galleria_width+'"';}else{width = ' width="500"';}
									if(galleria_height !="") { height = ' height="'+galleria_height+'"';}else{height = ' height="500"';}
									if(galleria_transition !="") { transition = ' transition="'+galleria_transition+'"';}else{transition = ' transition="500"';}
									if(galleria_autoplay !="") { autoplay = ' autoplay="'+galleria_autoplay+'"';}else{autoplay = ' autoplay="500"';}
									return '\n[galleria'+width+height+transition+autoplay+']\n';
									break;
							// G A L L E R I A   U R L 
							//--------------------------------------------------------
							case'galleriaurl':
									var galleria_width = jQuery('[name="galleria_galleriaurl_width"]').val();
									var galleria_height = jQuery('[name="galleria_galleriaurl_height"]').val();
									var galleria_transition = jQuery('[name="galleria_galleriaurl_transition"]').val();
									var galleria_textareaurl = jQuery('[name="galleria_galleriaurl_textarea_url"]').val();
									var galleria_autoplay = jQuery('[name="galleria_galleriaurl_autoplay"]').val();
									if(galleria_width !="") { width = ' width="'+galleria_width+'"';}else{width = ' width="500"';}
									if(galleria_textareaurl !="") {content = ''+galleria_textareaurl+'';}
									if(galleria_height !="") { height = ' height="'+galleria_height+'"';}else{height = ' height="500"';}
									if(galleria_transition !="") { transition = ' transition="'+galleria_transition+'"';}else{transition = ' transition="500"';}
									if(galleria_autoplay !="") { autoplay = ' autoplay="'+galleria_autoplay+'"';}else{autoplay = ' autoplay="500"';}
									return '\n[galleriaurl'+width+height+transition+autoplay+']'+ content+'[/galleriaurl]\n';;
									break;
						}
						break;
				// G O O G L E   M A P 
				case 'gmap':
						var width = jQuery('[name="gmap_width"]').val();
						var height = jQuery('[name="gmap_height"]').val();
						var address = jQuery('[name="gmap_address"]').val();
						var latitude = jQuery('[name="gmap_latitude"]').val();
						var longitude = jQuery('[name="gmap_longitude"]').val();
						var stylerscolor = jQuery('[name="gmap_stylerscolor"]').val();
						var zoom = jQuery('[name="gmap_zoom"]').val();
						var marker_desc = jQuery('[name="gmap_marker_desc"]').val();
						var popup = jQuery('[name="gmap_infowindow"]');
						var maptype = jQuery('[name="gmap_types"]').val();
						var controller = jQuery('[name="gmap_controller"]');
						if(popup.is('.atp-button')){
							if(popup.is(':checked')){
							popup= ' infowindow="true"';	
							}else{
							popup= ' infowindow="false"';		
							}
						}
						if(controller.is('.atp-button')){
							if(controller.is(':checked')){
							controller= ' controller="true"';	
							}else{
							controller= ' controller="false"';		
							}
						}	
						if(width!='')		{ width =' width="'+width+'"'; }
						if(height!='')		{ height =' height="'+height+'"'; }
						if(address!='')		{ address =' address="'+address+'"';	}
						if(latitude!='')	{ latitude =' latitude="'+latitude+'"';}
						if(longitude!='')	{ longitude =' longitude="'+longitude+'"';}
						if(stylerscolor!='')	{ stylerscolor =' color="'+stylerscolor+'"';}
						
						if(content!='')		{ html =' html="'+marker_desc+'"';}
						if(zoom!='')		{ zoom =' zoom="'+zoom+'"';}
						if(maptype!='')		{ maptype =' maptype="'+maptype+'"';}
						return '[gmap'+width+height+latitude+longitude+address+zoom+html+popup+controller+maptype+stylerscolor+']';				
						break;
				

							// C O N T A C T   F O R M 
							//--------------------------------------------------------
							case 'Contactform':
									var emailid =  jQuery('[name="Contactform_emailid"]').val();
									var successmessage =  jQuery('[name="Contactform_successmessage"]').val();
									if(emailid!=''){ emailid =' emailid="'+emailid+'"'; }
									if(successmessage!=''){ successmessage =' successmessage="'+successmessage+'"'; }
									return '\n[contactform'+emailid+successmessage+']\n';	
									break;
							// T W I T T E R
							//--------------------------------------------------------
							case 'twitter':
									var username =  jQuery('[name="twitter_username"]').val();
									var limit =  jQuery('[name="twitter_limit"]').val();
									var consumerkey =  jQuery('[name="twitter_consumerkey"]').val();
									var consumersecret =  jQuery('[name="twitter_consumersecret"]').val();
									var accesstoken =  jQuery('[name="twitter_accesstoken"]').val();
									var accesstokensecret =  jQuery('[name="twitter_accesstokensecret"]').val();
									if(username !='')	{ username=' username="'+username+'"';	}
									if(consumerkey!='')		{ consumerkey =' consumerkey="'+consumerkey+'"'; }
									if(consumersecret!='')		{ consumersecret =' consumersecret="'+consumersecret+'"'; }
									if(accesstoken!='')		{ accesstoken =' accesstoken="'+accesstoken+'"'; }
									if(accesstokensecret!='')		{ accesstokensecret =' accesstokensecret="'+accesstokensecret+'"'; }
									if(limit!='')		{ limit =' limit="'+limit+'"'; }
									return '\n[twitter'+username+limit+consumerkey+consumersecret+accesstoken+accesstokensecret+']\n';
									break;
							// F L I C K R 
							//--------------------------------------------------------
							case 'flickr':
									var id = jQuery('[name="flickr_id"]').val();
									var limit = jQuery('[name="flickr_limit"]').val();
									var type = jQuery('[name="flickr_type"]').val();
									var display = jQuery('[name="flickr_display"]').val();
									if(id!='')		{ id =' id="'+id+'"'; }
									if(limit!='')	{ limit =' limit="'+limit+'"';	}
									if(type!='')	{ type =' type="'+type+'"';	}
									if(display!='')	{ display =' display="'+display+'"';	}
									return '\n[flickr'+id+limit+display+type+']\n';
									break;
							// P O P U L A R   P O S T S 
							//--------------------------------------------------------
							case 'popularposts':
									var thumb = jQuery('[name="popularposts_thumb"]');
									var limit = jQuery('[name="popularposts_limit"]').val();
									if(thumb.is('.atp-button')){
										if(thumb.is(':checked')){
											thumb= ' thumb="true"';	
										}else{
											thumb= ' thumb="false"';		
										}
									}	
									if(limit!='')		{ limit =' limit="'+limit+'"';	}
									return '\n[popularpost '+thumb+limit+']\n';
									break;
							// R E C E N T   P O S T S 
							//--------------------------------------------------------
							case 'recentposts':
									var thumb = jQuery('[name="recentposts_thumb"]');
									var limit = jQuery('[name="recentposts_limit"]').val();
									var cat_id = jQuery('[name="recentposts_cat_id[]"]').val();
									if(thumb.is('.atp-button')){
										if(thumb.is(':checked')){
											thumb= ' thumb="true"';	
										}else{
											thumb= ' thumb="false"';		
										}
									}	
									if(limit!='')		{ limit =' limit="'+limit+'"';}
									if(cat_id!='')		{ cat_id =' cat_id="'+cat_id+'"';}
									return '\n[recentpost '+thumb+limit+cat_id+']\n';
									break;
							// C O N T A C T   I N F O 
							//--------------------------------------------------------
							case 'contactinfo':
									var name = jQuery('[name="contactinfo_name"]').val();
									var address = jQuery('[name="contactinfo_address"]').val();
									var email = jQuery('[name="contactinfo_email"]').val();
									var phone = jQuery('[name="contactinfo_phone"]').val();
									var link = jQuery('[name="contactinfo_link"]').val();
									if(name!='')	{ name =' name="'+name+'"';}
									if(address!='')	{ address =' address="'+address+'"';}
									if(email!='')	{ email =' email="'+email+'"';}
									if(phone!='')	{ phone =' phone="'+phone+'"';}
									if(link!='')	{ link =' link="'+link+'"';}
									return '\n[contactinfo '+name+address+email+phone+link+']\n';
									break;	
							// F L A S H 
							//--------------------------------------------------------
							case 'flash':
									var width = jQuery('[name="flash_width"]').val();
									var height = jQuery('[name="flash_height"]').val();
									var src = jQuery('[name="flash_src"]').val();
									var id = jQuery('[name="flash_id"]').val();
									if(width!='')	{ width =' width="'+width+'"';}
									if(height!='')	{ height =' height="'+height+'"';}
									if(src!='')		{ src =' src="'+src+'"'; }
									if(id!='')		{ id =' id="'+id+'"'; }
								
									return '\n[flash'+width+height+src+id+']\n';	
									break;
							// V I M E O 
							//--------------------------------------------------------
							case 'vimeo':
									
									var clip_id = jQuery('[name="vimeo_clipid"]').val();
									var autoplay = jQuery('[name="vimeo_autoplay"]');
									if(clip_id!='')		{ clip_id	 =' clip_id="'+clip_id+'"';	}
									if(autoplay.is('.atp-button')){
										if(autoplay.is(':checked')){
										autoplay= ' autoplay="1"';	
										}else{
										autoplay= ' autoplay="0"';		
										}
									}	
									return '\n[vimeo'+clip_id+autoplay+']\n';	
									break;
							// Y O U T U B E
							//--------------------------------------------------------
							case 'youtube':
									var clipid = jQuery('[name="youtube_clipid"]').val();
									var autoplay = jQuery('[name="youtube_autoplay"]');
									if(clipid!='')			{ clip_id =' clipid="'+clipid+'"';}
									if(autoplay.is('.atp-button')){
										if(autoplay.is(':checked')){
											autoplay= ' autoplay="1"';	
										}else{
											autoplay= ' autoplay="0"';		
										}
									}
										
									return '\n[youtube'+clip_id+autoplay+']\n';	
									break;
				// B L O G 
				//--------------------------------------------------------
				case 'blog':
						var blog_cat = jQuery('[name="blog_cat[]"]').val();
						var blogimage = jQuery('[name="blog_image"]');
						var blogmeta = jQuery('[name="blog_meta"]');
						var blog_max = jQuery('[name="blog_limit"]').val();
						var blog_limitcontent = jQuery('[name="blog_limitcontent"]').val();

						var blogpagination = jQuery('[name="blog_pagination"]');
						var blog_imgheight = jQuery('[name="blog_imgheight"]').val();
						if(blog_imgheight !="")	{ imgheight = ' imgheight="'+blog_imgheight+'"';}else{imgheight = '';}
						if(blogimage.is('.atp-button')){
							if(blogimage.is(':checked')){
							image= ' image="true"';	
							}else{
							image= ' image="false"';		
							}
						}		
						if(blogpagination.is('.atp-button')){
							if(blogpagination.is(':checked')){
							pagination= ' pagination="true"';	
							}else{
							pagination= ' pagination="false"';		
							}
						}		
						if(blogmeta.is('.atp-button')){
							if(blogmeta.is(':checked')){
							meta= ' postmeta="true"';	
							}else{
							meta= ' meta="false"';		
							}
						}		
						if(blog_cat!="")			{ cat = ' cat="'+blog_cat+'"';	}else{	cat = '';}
						if(blog_max!="")			{ max = ' limit="'+blog_max+'"';}else{max	 = '';}
						if(blog_limitcontent!="")			{ charlimits = ' charlimits="'+blog_limitcontent+'"';}else{charlimits = '';}	
						return '[blog'+cat+meta+max+pagination+imgheight+image+charlimits+']';
						break;
	
				// P O R T F O L I O
				//--------------------------------------------------------
				case 'portfolio':
						var columns = jQuery('[name="portfolio_column"]').val();
						var portfolio_cat = jQuery('[name="portfolio_cat[]"]').val();
						var portfoliotitle = jQuery('[name="portfolio_title"]');
						var portfoliodesc = jQuery('[name="portfolio_description"]');
						var portfolio_sidebar = jQuery('[name="portfolio_sidebar"]');
						var portfolio_sortable = jQuery('[name="portfolio_sortable"]');
						var portfolio_limit = jQuery('[name="portfolio_limit"]').val();
						var portfolio_readmoretxt = jQuery('[name="portfolio_readmore"]').val();
						var portfoliomorebutton = jQuery('[name="portfolio_morebutton"]');
						var portfolio_limitcontent = jQuery('[name="portfolio_limitcontent"]').val();
						var portfoliopagination = jQuery('[name="portfolio_pagination"]');
						var portfolio_imgheight = jQuery('[name="portfolio_imgheight"]').val();
						if(columns !="") { columns = ' columns="'+columns+'"';}else{columns = ' columns="4"';}
						if(portfolio_imgheight !="")	{ imgheight = ' imgheight="'+portfolio_imgheight+'"';}else{imgheight = ' imgheight="250"';}
							if(portfoliotitle.is('.atp-button')){
							if(portfoliotitle.is(':checked')){
							title= ' title="false"';	
							}else{
							title= ' title="true"';		
							}
						}
						if(portfoliodesc.is('.atp-button')){
							if(portfoliodesc.is(':checked')){
							desc= ' desc="false"';	
							}else{
							desc= ' desc="true"';		
							}
						}	

						if(portfolio_cat == null)			{ portfolio_cat = "";}
						if(portfolio_cat!="")			{ cat = ' cat="'+portfolio_cat+'"';	}else{	cat = ' cat=""';}
						if(portfolio_limit!="")			{ limit = ' limit="'+portfolio_limit+'"';}else{limit	 = '';}
						if(portfolio_readmoretxt!="")		{ readmoretext = ' readmoretext="'+portfolio_readmoretxt+'"'; }else{ readmoretext = '';}
						if(portfoliomorebutton.is('.atp-button')){
							if(portfoliomorebutton.is(':checked')){
							readmore= ' readmore="false"';	
							}else{
							readmore= ' readmore="true"';		
							}
						}
						if(portfolio_sidebar.is('.atp-button')){
									if(portfolio_sidebar.is(':checked')){
									sidebar= ' sidebar="false"';	
									}else{
									sidebar= ' sidebar="true"';		
									}
								}
						if(portfolio_sortable.is('.atp-button')){
									if(portfolio_sortable.is(':checked')){
									sortable= ' sortable="false"';	
									}else{
									sortable= ' sortable="true"';		
									}
								}
							
						if(portfoliopagination.is('.atp-button')){
							if(portfoliopagination.is(':checked')){
							pagination= ' pagination="false"';	
							}else{
							pagination= ' pagination="true"';		
							}
						}	
						if(portfolio_limitcontent!="")	{ charlimits = ' charlimits="'+portfolio_limitcontent+'"';}else{charlimits = ' charlimits="150"';}	
						return '[portfolio'+columns+cat+title+desc+limit+readmoretext+readmore+charlimits+pagination+imgheight+sidebar+sortable+']';
						break;
				case 'eventlist':
						
						var events_cat = jQuery('[name="eventlist_cat[]"]').val();
						var eventstitle = jQuery('[name="eventlist_title"]').val();
						if(events_cat == null)			{ events_cat = "";}
						if(events_cat!="")			{ cat = ' cat="'+events_cat+'"';	}else{	cat = ' cat=""';}
						if(eventstitle!="")	{ title = ' title="'+eventstitle+'"';}else{title = "";}
						
						return '[events_list'+cat+title+']';
				break;
				// P R O G R E S S B A R
				case 'progressbar':
						var progress_width= jQuery('[name="progressbar_percentage"]').val();
						var progresstitle= jQuery('[name="progressbar_title"]').val();
						var progress_color=jQuery('[name="progressbar_color"]').val();
						if(progress_width !='') { progress = ' progress="'+progress_width+'"';}else{progress = ''; }
						if(progresstitle !='') { title = ' title="'+progresstitle+'"';}else{title = ''; }
						if(progress_color !=''){ color = ' color="'+progress_color+'"';}else{color = ''; }
						return '\n[progressbar'+progress+color+title+']\n';
						break;
				case 'staff':
						var staff_photo= jQuery("[name=staff_photo]").val();
						var staff_title=jQuery('[name="staff_title"]').val();
						var staff_content=jQuery('[name="staff_content"]').val();
						var arr = ['blogger','delicious','digg','facebook','flickr','forrst','google','html5','lastfm','linkedin','paypal','picasa','pinterest','rss','skype','stumbleupon','technorati','twitter','twitter3','dribbble','vimeo','windows','wordpress','yahoo','yelp','youtube'];
						jQuery.each(arr, function(key,value) {
							if ( jQuery('[name="staff_'+value+'"]').val() !== 'undefined' && jQuery('[name="staff_'+value+'"]').val() !== '') { 
								jQuery('#atp-sociables-result').val(jQuery('#atp-sociables-result').val() + ' ' + value + '="' + jQuery('[name="staff_'+value+'"]').val() + '"' );
							}
						});
						jQuery('#atp-sociables-result').val(jQuery('#atp-sociables-result').val());
						var staff_sociables = jQuery('#atp-sociables-result').val();
						if(staff_photo !='') { photo = ' photo="'+staff_photo+'"';}else{photo = ''; }
						if(staff_title !=''){ title = ' title="'+staff_title+'"';}else{title = ''; }
						if(staff_content !== '') { content = ''+staff_content+'';}else{ content='';}
						jQuery('#atp-sociables-result').val('');
						return '[staff'+photo+title+staff_sociables+']'+content+'[/staff]\n';
						break;
				// TESTIMONIAL
				//--------------------------------------------------------
				case 'testimonial':
						var testimonial_cat = jQuery('[name="testimonial_category[]"]').val();
						if(testimonial_cat == null)	{ testimonial_cat = "";}
						if(testimonial_cat!=""){ cat = ' cat="'+testimonial_cat+'"';	}else{	cat = ' cat=""';}
						return '[testimonial'+cat+']';
						break;
				
				// C A R O U F R E D S E L S L I D E R  
				//--------------------------------------------------------
				case 'carouselslider':
						var shortcodesub_carouselslider=jQuery('#secondary_carouselslider select').val();
						switch(shortcodesub_carouselslider){
							// A T T A C H M E N T 
							//--------------------------------------------------------
							case'category':
			
								var carouselslider_cat = jQuery('[name="carouselslider_category_cat[]"]').val();
								var carouselslider_max = jQuery('[name="carouselslider_category_limit"]').val();
								var carouselslider_title = jQuery('[name="carouselslider_category_title"]').val();
								if(carouselslider_cat!="")			{ cat = ' cat="'+carouselslider_cat+'"';	}else{	cat = '';}
								if(carouselslider_max!="")			{ max = ' limit="'+carouselslider_max+'"';}else{max	 = '';}
								if(carouselslider_title!="")			{ title = ' title="'+carouselslider_title+'"';}else{title	 = '';}
								return '[carousel_list'+cat+max+title+']';
									break;
							// G A L L E R I A   U R L 
							//--------------------------------------------------------
							case'Portfolio':
								var carouselslider_cat = jQuery('[name="carouselslider_Portfolio_cat[]"]').val();
								var carouselslider_max = jQuery('[name="carouselslider_Portfolio_limit"]').val();
								var carouselslider_title = jQuery('[name="carouselslider_Portfolio_title"]').val();
								if(carouselslider_cat!="")			{ cat = ' port_cat="'+carouselslider_cat+'"';	}else{	port_cat = '';}
								if(carouselslider_max!="")			{ max = ' limit="'+carouselslider_max+'"';}else{max	 = '';}
								if(carouselslider_title!="")			{ title = ' title="'+carouselslider_title+'"';}else{title	 = '';}
								return '[carousel_list'+cat+max+title+']';
									break;
						}
						break;
				
			}
		},
		sendToEditor :function(){
			send_to_editor(shortcode.generate());
		}
	}
	jQuery(document).ready( function() {
	jQuery('.staff_delete').live("click", function() {
					jQuery(this).closest('tr').hide();
					e.preventDefault();
				});
		shortcode.init();
		
				jQuery("select[name=staff_selectsociable]").live('change', function(e) {
					jQuery("#secondary_staff table").find("."+this.value).show();
					e.preventDefault();
	});
	
	});