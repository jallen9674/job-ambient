<div id="featured_slider">
	<div class="planbox_slider">
	<?php
		$pageslider = get_post_meta($post->ID,'page_slider', true);
		if( $pageslider != "" ) {
			$planboxcontent = get_post_meta($post->ID,'planboxcontent', true);
		}else{ 
			$planboxcontent = get_option( 'atp_planboxcontent' ); 
		}
	
		echo do_shortcode($planboxcontent);
	?>
	</div>
</div>