<?php
$slidelimit = get_option('atp_nivolimit') ? get_option('atp_nivolimit'):'3';
$slideeffect = get_option('atp_nivoeffect') ? get_option('atp_nivoeffect'):'random';
$slidespeed = get_option('atp_nivotransitionspeed') ? get_option('atp_nivotransitionspeed'):'500';
$slidepausespeed = get_option('atp_nivopausetime') ? get_option('atp_nivopausetime'):'3000';
$slidecnav = get_option('atp_nivoslidecnav') ? get_option('atp_nivoslidecnav'):'false';
$slidednav = get_option('atp_nivoslidednav') ? get_option('atp_nivoslidednav'):'true';

wp_enqueue_script('atp-nivoslider',THEME_URI.'/js/jquery.nivo.slider.js','','','in_footer' );
$pageslider = get_post_meta($post->ID,'page_slider', true);
if( $pageslider != "" ) {
	$slider_cat = get_post_meta($post->ID,'nivoslidercat', true);
  }else{ $slider_cat=get_option('atp_nivolidercat'); }
?>
<script type="text/javascript">
jQuery(document).ready(function($) {
	jQuery(window).load(function() {
		jQuery('#atp-nivoslider').nivoSlider({
			effect: '<?php echo $slideeffect; ?>',
			animSpeed: <?php echo $slidespeed; ?>,
			pauseTime: <?php echo $slidepausespeed ?>,
			startSlide: 0,
			directionNav: <?php echo $slidednav ?>,
			controlNav: true,
			controlNavThumbs: false,
			beforeChange: function(){
				jQuery('.nivo-caption').animate({bottom:"50%", opacity: "0"}, {easing:"jswing", duration: "300"});
			},
			afterChange: function(){
				jQuery('.nivo-caption').animate({bottom:"15%", opacity: "1"}, {easing:"easeOutBack",  duration: "300"});
			}
		});
	});
});
</script>
<div id="featured_slider" class="box-shadow">
	<div class="slider_stretched">
		<div class="theme-default">
			<div id="atp-nivoslider" class="nivoSlider">	
				<?php if( is_array( $slider_cat ) && count($slider_cat)>0) { 
					$slider_cat=implode( ",",$slider_cat );
				}
					$query = array(
				'post_type'			=> 'slider', 
				'posts_per_page'	=> $slidelimit, 
				'taxonomy'			=> 'slider_cat', 
				'term'				=> $slider_cat, 
				'orderby'			=> 'menu_order',
				'order'				=> 'ASC'
			);
				query_posts($query); //get the results
				if (have_posts()) :while (have_posts()) :the_post();
						$terms = get_the_terms(get_the_ID(), 'slider_cat');
						$terms_slug = array();
						if (is_array($terms)) {
							foreach($terms as $term) {
								$terms_slug[] = $term->slug;
							}
						}
					if( get_option('atp_layoutoption') == "stretched"){ $width='1920'; }else{ $width='1080';  }

					$postlinktype_options = get_post_meta($post->ID, "postlinktype_options", true);
					$postlinkurl = atp_generator('atp_getPostLinkURL',$postlinktype_options);
	
					if ($postlinkurl !='nolink') {
					echo '<a href="'.$postlinkurl.'"  >';
					}
					echo atp_resize($post->ID,'',$width,'450','','');
					if ($postlinkurl !='nolink') {
					echo '</a>';
					}
					endwhile; 
				endif; wp_reset_query(); ?> 
			</div>
		</div><!-- /nivoSlider -->
	</div><!-- /slider_wrapper -->
</div><!-- /featured_slider -->