<?php 
$eis_animtype = get_option('atp_eislider_animtype') ? get_option('atp_eislider_animtype') : 'sides';
$eis_slidelimit = get_option('atp_eislidelimit') ? get_option('atp_eislidelimit') : '3';
$eis_titleanimation = get_option('atp_titleanimation') ? get_option('atp_titleanimation') : 'easeOutExpo';
$eis_titlespeed = get_option('atp_titlespeed') ? get_option('atp_titlespeed') : '800';
$eis_slideanimation = get_option('atp_slideanimation') ? get_option('atp_slideanimation') : 'easeOutExpo';
$eis_slidedspeed = get_option('atp_speed') ? get_option('atp_speed') : '800';
$eis_autoplay = get_option('atp_autoplay') ? get_option('atp_autoplay') : 'false';
$eis_slideinterval = get_option('atp_interval') ? get_option('atp_interval') :'3000';

$pageslider = get_post_meta($post->ID,'page_slider', true);
if( $pageslider != "" ) {
	$slider_cat = get_post_meta($post->ID,'eislidercat', true);
} else {
	$slider_cat=get_option('atp_eislidercat');
}
?>
<?php
echo '<script type="text/javascript">
jQuery(document).ready(function($) {
	jQuery("#ei-slider").eislideshow({
		animation : "'.$eis_animtype.'", 
		easing : 		"'.$eis_slideanimation.'", 
		titleeasing	:	"'.$eis_titleanimation.'", 
		titlespeed :	'.$eis_titlespeed.',
		speed :			'.$eis_slidedspeed.' ,
		slideshow_interval : '.$eis_slideinterval.',
		autoplay : '.$eis_autoplay.'
	});
});	
</script>';
?>
<div id="featured_slider">
	<div class="slider_wrapper">
		<div id="ei-slider" class="ei-slider">
			<ul class="ei-slider-large">
			<?php 
	
			if( is_array( $slider_cat ) && count($slider_cat)>0) { 
				$slider_cat = implode( ",",$slider_cat );
			}
	
			$query = array(
				'post_type'			=> 'slider', 
				'posts_per_page'	=> $eis_slidelimit, 
				'taxonomy'			=> 'slider_cat', 
				'term'				=> $slider_cat, 
				'orderby'			=> 'menu_order',
				'order'				=> 'ASC'
			);
	
			query_posts($query);
			$count = 0;
		
			while (have_posts()) : the_post();
			
			if( get_option('atp_layoutoption') == "stretched"){ $width='1920'; }else{ $width='1020';  }
	
			$postlinktype_options = get_post_meta($post->ID, "postlinktype_options", true);
			$ei_sliderdescription= get_post_meta($post->ID, "slider_desc", true);
			$ei_slider_cap_disable = get_post_meta($post->ID, "slider_cap_disable", true);
			$ei_slider_disable = get_post_meta($post->ID, "slider_caption", true);
			$postlinkurl = atp_generator('atp_getPostLinkURL',$postlinktype_options);
	
			$terms = get_the_terms(get_the_ID(), 'slider_cat');
			$terms_slug = array();
			if (is_array($terms)) {
				foreach($terms as $term) {
					$terms_slug[] = $term->slug;
				}
			}
			echo '<li>';
			if ($postlinkurl !='nolink') {
				echo '<a href="'.$postlinkurl.'"  >';
			}
			echo atp_resize($post->ID,'',$width,'450','','');
			if ($postlinkurl !='nolink') {
				echo '</a>';
			}
			if ( $ei_slider_cap_disable != "on") { ?>
			<div class="ei-title">
				<?php the_title( '<h2><span>', '</span></h2>' ); ?>
					<?php if($ei_sliderdescription != '') { ?>
						<h3><span><?php echo do_shortcode($ei_sliderdescription);?></span></h3>
					<?php } ?>
			</div>
			<?php } ?>
			<?php 
			echo '</li>';
	
			$count++;
	
			endwhile;
	
			wp_reset_query(); ?>
			</ul>
			<!-- ei-slider-large -->
			<ul class="ei-slider-thumbs">
				<li class="ei-slider-element">Current</li>
				<?php
				for($i = 1; $i <= $count; $i++){
					echo '<li><a href="#">Slide '.$i.'</a></li>';
				}?>
			</ul><!-- ei-slider-thumbs -->
		</div><!-- .ei_slider -->
	</div><!-- .slider_wrapper -->
</div><!-- #featured_slider -->