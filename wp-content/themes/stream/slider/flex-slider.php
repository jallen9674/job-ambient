<?php
$fs_slidelimit = get_option('atp_flexslidelimit') ? get_option('atp_flexslidelimit') : '3';
$fs_slidespeed = get_option('atp_flexslidespeed') ? get_option('atp_flexslidespeed') : '3000';
$fs_slideffect = get_option('atp_flexslideffect') ? get_option('atp_flexslideffect') : 'fade';
$fs_slidednav = get_option('atp_flexslidednav') ? get_option('atp_flexslidednav') : 'true';
$fs_slidecnav = get_option('atp_flexslidecnav') ? get_option('atp_flexslidecnav') : 'true';

$pageslider = get_post_meta($post->ID,'page_slider', true);

if($pageslider != "" ) {
	$slider_cat = get_post_meta($post->ID,'flexslidercat', true);
}else{
	$slider_cat = get_option('atp_flexslidercat');
}
?>
<?php
echo '<script type="text/javascript">
jQuery(document).ready(function($) {
	jQuery(".flexslider").flexslider({
		animation: "'.$fs_slideffect.'",
		controlsContainer: ".flex-container",
		slideshow: true,
		slideshowSpeed: '.$fs_slidespeed.',
		animationDuration: 400,
		directionNav: '.$fs_slidednav.',
		controlNav: '.$fs_slidecnav.',
		mousewheel: false,
		smoothHeight :true,
		start: function(slider) {
			jQuery(".total-slides").text(slider.count);
		},
		after: function(slider) {
			jQuery(".current-slide").text(slider.currentSlide);
		}
	});
});	
</script>';
?>
<div id="featured_slider">
	<div class="slider_wrapper">
		<div class="flexslider">
			<ul class="slides">
			<?php
				if( is_array( $slider_cat ) && count($slider_cat)>0) { 
					$slider_cat = implode( ",",$slider_cat );
				}
				$query = array(
					'post_type'			=> 'slider', 
					'posts_per_page'	=> $fs_slidelimit, 
					'taxonomy'			=> 'slider_cat', 
					'term'				=> $slider_cat, 
					'orderby'			=> 'menu_order',
					'order'				=> 'ASC'
				);
				query_posts($query);
				while (have_posts()) :the_post();
				$terms = get_the_terms(get_the_ID(), 'slider_cat');
				$terms_slug = array();
				if (is_array($terms)) {
					foreach($terms as $term) {
						$terms_slug[] = $term->slug;
					}
				}

				if( get_option('atp_layoutoption') == "stretched"){ $width='1920'; }else{ $width='1080';  }

				$postlinktype_options = get_post_meta($post->ID, "postlinktype_options", true);
				$flex_sliderdescription= get_post_meta($post->ID, "slider_desc", true);
				$flex_slider_cap_disable = get_post_meta($post->ID, "slider_cap_disable", true);
				//$flex_cap_disable =  get_post_meta($post->ID, "slider_desc", true);
				$postlinkurl = atp_generator('atp_getPostLinkURL',$postlinktype_options);
				echo '<li>';
				echo '<figure>';
				if ($postlinkurl !='nolink') {
					echo '<a href="'.$postlinkurl.'"  >';
				}
				echo atp_resize($post->ID,'',$width,'450','','');
				if ($postlinkurl !='nolink') {
					echo '</a>';
				}
			
				echo '</figure>'; 
				if($flex_slider_cap_disable !='on')	{
					?> 
					<div class="flex-caption">
						<div class="flex-title"><h2><span><?php the_title();?></span></h2>
						<?php if($flex_sliderdescription !='') { ?>
						<h3><span><?php echo do_shortcode($flex_sliderdescription); ?></span></h3>
						<?php } ?>
						</div>
					</div>
					<?php
				}
				echo '</li>';
				endwhile;
				wp_reset_query(); ?> 
		</ul>
		</div><!-- .flex_slider -->
	</div><!-- .slider_stretched -->
</div><!-- #featured_slider -->