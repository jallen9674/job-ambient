<div id="featured_slider">
	<div class="slider_wrapper">
		<div class="staticslider">
			<?php
			$pageslider = get_post_meta($post->ID,'page_slider', true);
			if( get_option('atp_layoutoption') == "stretched"){ $width='1920'; } else{ $width='1020';  }
			if( $pageslider != "" ) {
				$src = get_post_meta($post->ID,'staticimage', true);
				$link =esc_url( get_post_meta($post->ID,'cimage_link', true));
			}else{ 
				$src = get_option( 'atp_static_image_upload' ); 
				$link = esc_url( get_option( 'atp_static_link' ) );
			}
			if( $link != "" ) { 
				echo '<a href="'.$link.'">'; 
			}
			echo '<figure>'.atp_resize( '', $src, $width, '450', '', '' ).'</figure>';
			if( $link != "" ) { 
				echo '</a>';
			}?>
		</div>
	</div>
</div>