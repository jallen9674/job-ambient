<div id="featured_slider">
	<div class="slider_wrapper">
		<?php 
		$pageslider = get_post_meta($post->ID,'page_slider', true);
		if( $pageslider != "" ) {
			$customslider = get_post_meta($post->ID,'customslider', true);
		  }else{
				$customslider = get_option( 'atp_customslider' ); 
		}
		echo do_shortcode($customslider); 
		?>
	</div>
</div>