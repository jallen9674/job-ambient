jQuery(function(){
	jQuery('.contactform').click(function(e) {
	 if (!e.isDefaultPrevented()) {
			var form=jQuery(this);
			var $id = form.find('input[name="contact_id"]').val();
				var name = jQuery('#contact_'+$id+'name').val();
		var email = jQuery('#contact_'+$id+'email').val();
		var contactcomment = jQuery('#contactcomment'+$id+'').val();
		var contact_success = jQuery('#contact_'+$id+'success').val();
		var contact_widgetemail = jQuery('#contact_'+$id+'widgetemail').val();
		var contact_ccorrect = jQuery('#contact_ccorrect'+$id+'').val();
	    var contact_captcha = jQuery('#contact_captcha'+$id+'').val();		//Validates
			//Validates
		if(contact_ccorrect != contact_captcha){
			jQuery('#contact_captcha'+$id+'').addClass('error');
		}else{ 
			jQuery('#contact_captcha'+$id+'').removeClass('error'); 
		}
		if(name =='') {
			jQuery('#contact_'+$id+'name').addClass('error');
		}else{ 
			jQuery('#contact_'+$id+'name').removeClass('error'); 
		}
		if(contactcomment ==''){
			jQuery('#contactcomment'+$id+'').addClass('error');
		}else{ 
			jQuery('#contactcomment'+$id+'').removeClass('error'); 
		}
		
		var filter = /^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*?)\s*;?\s*)+/;
		if(filter.test(email)){
			jQuery('#contact_'+$id+'email').removeClass('error'); 
		}else{
			jQuery('#contact_'+$id+'email').addClass('error');
		}			//Process Action
		jQuery.ajax({
			type: 'post',
			url: atp_panel.SiteUrl+'/framework/includes/submitform.php',
			data: 'contact_name=' + name + '&contact_email=' + email + '&contactcomment=' + contactcomment + '&contact_success=' + contact_success + '&contact_widgetemail=' + contact_widgetemail+ '&contact_ccorrect=' + contact_ccorrect+ '&contact_captcha=' + contact_captcha,
			success: function(results) {
				jQuery('.response'+$id+'').html(results);
			}
		}); // end ajax
		// end ajax
		e.preventDefault();
 }
	});
});