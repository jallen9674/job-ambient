/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
	T E S T I M O N I A L
 -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
(function($){
    $.fn.MySlider = function(interval) {
	
		var slides;
        var cnt;
        var amount;
        var i;

        function run() {
            // hiding previous image and showing next
            $(slides[i]).fadeOut('slow', function() {
		    // Animation complete.
		    i++;
		    if (i >= amount) i = 0;
		    $(slides[i]).fadeIn('slow');

		    // updating counter
		    cnt.text(i+1+' / '+amount);
		  });
            

            // loop
            setTimeout(run, interval);
        }

        slides = $('.testimonials > li');
        cnt = $('#counter');
        amount = slides.length;
        i=0;

        // updating counter
        cnt.text(i+1+' / '+amount);

		if(amount > 1)
			setTimeout(run, interval);
	
	};	
})(jQuery);


/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
	P R E T T Y P H O T O
-----------------------------
	F L E X   S L I D E R 
 -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

jQuery(window).load(function(){
	jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({theme:'pp_default'}); /* light_square / dark_rounded /  dark_square / facebook */
	jQuery('.flexslider').flexslider({
		animation: "slide",				//String: Select your animation type, "fade" or "slide"
		controlsContainer: ".flex-container",
		slideshow: true,					//Boolean: Animate slider automatically
		slideshowSpeed: 3000,				//Integer: Set the speed of the slideshow cycling, in milliseconds
		animationDuration: 1000,			//Integer: Set the speed of animations, in milliseconds
		directionNav: false,				//Boolean: Create navigation for previous/next navigation? (true/false)
		controlNav: true,					//Boolean: Create navigation for paging control of each clide? Note: Leave true for	
		mousewheel: false,					//Boolean: Allow slider navigating via mousewheel				  
		
		start: function(slider) {
			jQuery('.total-slides').text(slider.count);
		},
		after: function(slider) {
			jQuery('.current-slide').text(slider.currentSlide);
		}
	});
});

// Toggle =====================================
function sys_toggle() {
	jQuery(".toggle_content").append("<div class='arrow'></div>").hide();

	jQuery("span.toggle").toggle(function(){
		jQuery(this).addClass("active");
	}, function () {
		jQuery(this).removeClass("active");
	});
		
	jQuery("#trigger").toggle(function(){
		jQuery(this).animate({top:5},50).animate({top:-30},50).animate({top:5},800).addClass("active");
	}, function () {
		jQuery(this).animate({top:5},50).animate({top:-30},50).animate({top:5},800).removeClass("active");
	});
		
	// Sticy Bar ==============================		
	jQuery("#trigger").click(function(){
		jQuery(this).prev("#sticky").slideToggle({ duration: 300, easing: 'easeOutQuart'});
	});
	jQuery("span.toggle").click(function(){
		jQuery(this).next(".toggle_content").slideToggle();
	});
}

	
// Pricing Table ==============================
function atp_pricingtable() {

	jQuery('.pricetable .block').hover(	function(){
		jQuery(this).addClass('active');
		jQuery(this).animate({'top': '-10px'},{	duration: 300,easing: 'easeOutBack'});
		jQuery(this).siblings().removeClass('active');
	}, function() {
		jQuery(this).animate({'top': '0px'},{duration: 300,	easing: 'easeInBack'});
	});
			
	jQuery('.pricetable .block').eq(0).addClass('first');
	jQuery('.pricetable .block').eq(1).addClass('second active');
	jQuery('.pricetable .block').eq(2).addClass('third');
	jQuery('.pricetable .block').eq(3).addClass('fourth');

}
	
// Sociables ==================================
function atp_sociables() {
	jQuery('ul.atpsocials li').find("img").css({opacity:'0.8'});
	jQuery("ul.atpsocials li").hover(function(){
		jQuery(this).find("img").animate({top:"-4px", opacity:'1'}, "fast")
	},function(){
		jQuery(this).find("img").animate({top:"0px",opacity:'0.8'}, "fast")
	});
}

// Hover Image ================================
function atp_hoverimage() {
	jQuery('.hover_type').animate({opacity: 0});
	jQuery(".port_img, .sort_img").hover(function() {
		jQuery(this).find('.hover_type').css({display:'block'}).animate({
			opacity: 1, 
			bottom: (jQuery('.port_img, .sort_img').height())/2 - 20+'px'
		}, 300, 'easeInSine');
		jQuery(this).find('img').animate({"opacity": "0.5"}, 200);
	
	},function() {
		jQuery(this).find('.hover_type').animate({
			opacity: 0,
			bottom: (jQuery('.port_img, .sort_img').height()) 
			}, 300, 'easeInSine', function() {
				jQuery(this).css({'bottom':'0'});
			});
		jQuery(this).find('img').animate({"opacity": "1"}, 200);
	});
}

// Accordion ===================================
function atp_accordion(){
	jQuery('.ac_wrap ').each(function() {
		tabid=jQuery(this).attr('id');
		jQuery("#"+tabid+" .ac_title:first").addClass("active");
		jQuery("#"+tabid+" .ac_content:not(:first)").hide();
	});

	jQuery(".ac_wrap .ac_title").click(function(){
		jQuery(this).next(".ac_content").slideToggle(400, 'swing').siblings(".ac_content:visible").slideUp(400, 'swing');
		jQuery(this).toggleClass("active");
		jQuery(this).siblings(".ac_title").removeClass("active");
	});
}

// Tabs ========================================
function atp_tabs()	{

	jQuery('.systabspane ').each(function () {
		tabid = jQuery(this).attr('id');

		jQuery("#" + tabid + " .tab_content").hide(); // Hide all tab conten divs by default
		jQuery("#" + tabid + " .tab_content:first").show(); // Show the first div of tab content by default
		jQuery("#" + tabid + " ul.tabs li:first").addClass("current"); // Show the current by default
	});
	jQuery("ul.tabs li").click(function () { //Fire the click event
		tab_id = jQuery(this).parents('.systabspane').attr("id");
		var activeTab = jQuery(this).attr("id"); // Catch the click link
		jQuery("#" + tab_id + " ul li").removeClass("current"); // Remove pre-highlighted link
		jQuery(this).addClass("current"); // set clicked link to highlight state
		jQuery("#" + tab_id + " .tab_content").hide(); // hide currently visible tab content div
		jQuery(activeTab).fadeIn(600); // show the target tab content div by matching clicked link.

	});
        
}

// Button Data Attributes ======================
function atp_buttondata(){

	jQuery(".button").hover(function(){
		
		var $hoverBg = jQuery(this).attr('btn-hoverBg');
		var $hoverColor = jQuery(this).attr('btn-hoverColor');
		if($hoverBg!=undefined){
			jQuery(this).css('background-color',$hoverBg);
		}else{}

		if($hoverColor!=undefined){
			jQuery('span',this).css('color',$hoverColor);
		}else{}

	},function(){

		var $btnbg = jQuery(this).attr('btn-bg');
		var $btncolor = jQuery(this).attr('btn-color');
		if($btnbg!=undefined){
			jQuery(this).css('background-color',$btnbg);
		}
		if($btncolor!=undefined){
			jQuery('span',this).css('color',$btncolor);
		}
	});
}

//Mobile Menu
function atp_mobilemenu() { 
	
	//var timer = null;
	var  win_width= jQuery(window).width();

	if( win_width <= 767 ) {
		jQuery("#atp_menu").removeClass("sf-menu");
		 jQuery('#atp_menu li').unbind();
		jQuery('#atp_menu li ul').removeAttr('style');
	}else{
		jQuery("#atp_menu").addClass("sf-menu").addClass('sf-js-enabled');
		jQuery('#atp_menu').removeAttr('style');
		jQuery("ul.sf-menu").superfish(); 
	}
}

	// Load functions
	jQuery(function($){

		atp_tabs();
		sys_toggle();
		atp_sociables();
		atp_pricingtable();
		atp_accordion();
		atp_hoverimage();
		atp_buttondata();
		atp_mobilemenu();

		jQuery('nav').prepend('<div id="menu-trigger">Menu</div>');
		jQuery("#menu-trigger").on("click", function(){
			jQuery(".sf-mobilemenu").slideToggle();
		});

		// Preloadify
		jQuery(".minithumb, .gallery, .gallery-item, .port_img, .sort_img, .postimg, .imgborder, .fancyimage, .lightbox").preloadify({force_icon:"true", mode:"sequence" });
		
		// Back to Scroll ======================
		jQuery('.back-top a').click(function () {
			jQuery('body,html').animate({
				scrollTop: 0
			}, 800);
			
			return false;
		});

		// Progress Bar ========================
		jQuery('.progress_bar').each(function() {
			var percent = jQuery(this).attr('data_width');
			jQuery(this).animate({width: percent},1000);
		});	
		
		// Planbox Slider ======================
		$('.plan_box').hover(function(){
			$(".plan_info", this).stop().animate({top:'-430'},{
				queue:false,
				duration:300
			});
		}, function() {
			$(".plan_info", this).stop().animate({top:'0'},{
				queue:false,
				duration:300
			});
		});
		
		// FitVids Video Responsive ============
		$('.video-frame,.boxcontent,.video-stage,.video').fitVids();
		
	});		

	// Load Mobile Menu on Window Resize
	jQuery(window).resize(function() {
	   atp_mobilemenu();
	});