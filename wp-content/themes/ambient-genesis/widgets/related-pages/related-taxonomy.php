<?php

/***************************************************
** ADDING LOCATION TAXONOMY
***************************************************/

    register_taxonomy( 'hc_related',
        array('page'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
        array('hierarchical' => true,     /* if this is true, it acts like categories */
            'labels' => array(
                'name' => __( 'Related Page Group', 'bonestheme' ), /* name of the custom taxonomy */
                'singular_name' => __( 'Related Group', 'bonestheme' ), /* single taxonomy name */
                'search_items' =>  __( 'Search Related Groups', 'bonestheme' ), /* search title for taxomony */
                'all_items' => __( 'All Related Groups', 'bonestheme' ), /* all title for taxonomies */
                'parent_item' => __( 'Parent Related Group', 'bonestheme' ), /* parent title for taxonomy */
                'parent_item_colon' => __( 'Parent Related Group:', 'bonestheme' ), /* parent taxonomy title */
                'edit_item' => __( 'Edit Related Group', 'bonestheme' ), /* edit custom taxonomy title */
                'update_item' => __( 'Update Related Group', 'bonestheme' ), /* update title for taxonomy */
                'add_new_item' => __( 'Add New Related Group', 'bonestheme' ), /* add new title for taxonomy */
                'new_item_name' => __( 'New Related Group Name', 'bonestheme' ) /* name title for taxonomy */
            ),
            'show_admin_column' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'hc-related-group' ),
        )
    );

    
/*-------------
Add NAP to Taxonomy Entries
--------------*/


//Add New Screen
function hc_related_add_links( $term ) {
	
	?>
	<div class="form-field">
		<label for="acLink">Air Conditioning Emergency Link</label>
		<input type="text" name="acLink" id="acLink" value="">
	</div>
    <div class="form-field">
		<label for="heatLink">Heating Emergency Link</label>
		<textarea type="text" name="heatLink" id="heatLink" value=""></textarea>
	</div>

<?php
}
add_action( 'hc_related_add_form_fields', 'hc_related_add_links', 10, 2 );

//Edit Screen
function hc_related_edit_links( $term ) {
	
	// put the term ID into a variable
	$t_id = $term->term_id;
 
    $term_acLink = get_term_meta( $t_id, 'ac_link', true ); 
    $term_heatLink = get_term_meta( $t_id, 'heat_link', true ); 

    
	?>
	<tr class="form-field">
		<th><label for="acLink">Air Conditioning Emergency Link</label></th>
		 
		<td>	 
			<input type="text" name="acLink" id="acLink" value="<?php echo esc_attr( $term_acLink ) ? esc_attr( $term_acLink ) : ''; ?>">
		</td>
	</tr>
    <tr class="form-field">
		<th><label for="heatLink">Heating Emergency Link</label></th>
		 
		<td>	 
			<input type="text" name="heatLink" id="heatLink" value="<?php echo esc_attr( $term_heatLink ) ? esc_attr( $term_heatLink ) : ''; ?>">
		</td>
	</tr>

<?php
}
add_action( 'hc_related_edit_form_fields', 'hc_related_edit_links', 10 );

//Save Data
function hc_related_save_links( $term_id ) {
	
	if ( isset( $_POST['acLink'] ) ) {
		$term_ac_link = $_POST['acLink'];
		if( $term_ac_link ) {
			 update_term_meta( $term_id, 'ac_link', $term_ac_link );
		} else {
            update_term_meta( $term_id, 'ac_link', '' );
        }
    } 
    
    if ( isset( $_POST['heatLink'] ) ) {
		$term_heat_link = $_POST['heatLink'];
		if( $term_heat_link ) {
			 update_term_meta( $term_id, 'heat_link', $term_heat_link );
		} else {
            update_term_meta( $term_id, 'heat_link', '' );
        }
    }  
   
				
}  
add_action( 'edited_hc_related', 'hc_related_save_links' );  
add_action( 'create_hc_related', 'hc_related_save_links' );

/*-------------
Add NAP to Taxonomy Summary
--------------*/

function create_nap_column($columns) {

    
    $columns['ac_link'] = 'AC Emergency Link';
    $columns['heat_link'] = 'Heat Emergency Link';

    return $columns;
  }
  add_filter('manage_edit-hc_related_columns', 'create_nap_column');

  function populate_nap_column($value, $column_name, $term_id) {

    $term_acLink = get_term_meta( $term_id, 'ac_link', true ); 
    $term_heatLink = get_term_meta( $term_id, 'heat_link', true ); 

   
    switch($column_name) {
        case 'ac_link': 
            $value = $term_acLink;
            break;
        case 'heat_link': 
            $value = $term_heatLink;
            break;
        default:
            break;
    }
    return $value;    
  }
  add_filter('manage_hc_related_custom_column', 'populate_nap_column', 10, 3);
