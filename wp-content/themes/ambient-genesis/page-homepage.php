<?php
/**
 * Template Name: Homepage Template
 */


//Adjusting Body Class
add_filter( 'body_class', 'hennessey_body_class' );
function hennessey_body_class( $classes ) {
	$classes[] = 'homepage-template';
	return $classes;
}

//Enqueue Styles for Page
$fileVersion =  filemtime($_SERVER["DOCUMENT_ROOT"] . '/wp-content/themes/ambient-genesis/scss/pages/homepage.scss');
wp_enqueue_style( 'page-homepage-styles', get_stylesheet_directory_uri() . '/css/pages/homepage.css', array(), $fileVersion );

//Full Width Layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//Remove Default the_content()
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//Adding Homepage Layout
add_action( 'genesis_before_content',  'hennessey_homepage_layout');

// Runs the Genesis loop.
genesis();


/*------------------------------
Homepage Layout
--------------------------------*/

function hennessey_homepage_layout(){
	?>

    <?php 
    /**************************************
    * Homepage Hero Section
    *****/ ?>

    <div class="homepage-hero-section">
		<div class="homepage-hero-section__inner wrapper">

			<div class="homepage-hero-section__content">
			
				<span class="homepage-hero-section__heading">
					$25 Off Any Service
				</span>

				<span class="homepage-hero-section__subheading">
					To qualify for the $25 off service deal, you must schedule an appointment.
				</span>

				<div class="homepage-hero-section__cta">
					<span>Call <a href="tel:<?php echo hennessey_phone_display(); ?>"><?php echo hennessey_phone_display(); ?></a> or <a href="<?php echo site_url(); ?>/contact/">click here</a> for service!</span>
				</div>

			</div>

			<!-- <div class="homepage-hero-section__girl"></div> -->

		</div>
    </div>

	<?php 
    /**************************************
    * Trust Logo Section
    *****/ ?>

	<div class="homepage-trust-logos">
		<div class="homepage-trust-logos__inner wrapper">
		
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/bbb_a_home.png" alt="BBB Logo" />
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/dave_lennox_premier-png2.png" alt="Trust Logo" />
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/nate.png" alt="NATE Logo" />
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/boha-2017.png" alt="Home Advisor, Best of 2017 Logo" />
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/kingman-chamber-logo.jpg" alt="Kingman COC Logo" />
			<?php /* <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/DC_Logo_Lockup_DC_RGB.png" alt="DC Logo" /> */ ?>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/elite.png" alt="Home Advisor Elite SErvice" />
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/hcc-logo.png" alt="Henderson COC Logo" />
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/Proud-Member-Logo-jpg-.png" alt="Las Vegas COC Logo" />

		</div>
	</div>

	<?php 
    /**************************************
    * Primary Content Section
	*****/ ?>

	
	<div class="homepage-primary-content">
		<div class="homepage-primary-content__inner wrapper">

			<span class="homepage-primary-content__subtitle">
				Why Ambient Edge?
			</span>

			<h1 class="homepage-primary-content__title">
				Arizona & Nevada's Trusted <br>Air Conditioning & Heating Repair Company
			</h1>

			<a href="<?php echo get_the_permalink(4201); ?>" class="homepage-primary-content__coupon">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25-off-coupon.jpg" alt="25 dollars off coupon" />
			</a>

			<p>Staying comfortable at home requires an efficient climate control system and the right service company to keep it running smoothly all year long. No matter where you live, whether you enjoy living in Kingman, Arizona in the heart of Historic Route 66 or if you love the hustle and bustle and the city lights in Las Vegas, Nevada, we are here to serve you.</p>

			<p>Our service area includes a full range of services no matter if you’re looking for Las Vegas HVAC system installation, repair, or maintenance; or if you need quality air conditioning repair in Kingman, Arizona; we have the quality and service guarantee you can count on. Heating, cooling, and refrigeration systems are complex machines that need a broad range of services to ensure optimal performance and efficiency. Here at Ambient Edge, we have the products and services you need. So sit back, relax, and let the industry trained experts at Ambient Edge be your first choice for all your heating and cooling needs.</p>
			
			<div style="clear: both;"></div>

			<div class="homepage-icon-section">

				<div class="homepage-icon-section__single">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-heating-cooling.png" alt="Heating and Cooling Services">
					<strong>Heating & Cooling Services</strong>
					<ul>
						<li>Air Conditioning Repair, Maintenance, and Installation</li>
						<li>Heating Repair, Maintenance, and Installation</li>
						<li>Energy Efficient Upgrades</li>
						<li>Duct Cleaning and Sealing</li>
						<li>Indoor Air Quality Product Sales and Installation</li>
						<li>24-hour Emergency Service and Repair</li>
						<li>Annual Maintenance Service Agreements</li>
						<li>Home energy audit</li>
					</ul>
				</div>	

				<div class="homepage-icon-section__single">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-24hour-service.png" alt="24 Hour Emergency Repair">
					<strong>24 Hour Emergency Repair</strong>
					<p>
						You never know when something unexpected can happen. When it comes to your families comfort, losing air conditioning in the summer, heat in the winter, or an unexpected leak can be a frustrating and in some cases a dangerous issue.
					</p>
					<p><span>
						That’s why we are here 24/7 to help with your emergency heating, cooling, and plumbing needs.
					</span></p>
				</div>	

				<div class="homepage-icon-section__single">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-plumbing.png" alt="Heating and Cooling Services">
					<strong>Plumbing Services</strong>
					<p>
						Your plumbing system is a vital part of your daily life, from showers to washing dishes, no matter the task, a plumbing issue can become a real nightmare for any homeowner. We aim to be your total comfort specialist, that means we also provide you with top-notch plumbing services from Plumbing by Jake.
					</p>
				</div>	

				<div class="homepage-icon-section__single">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-products.png" alt="Heating and Cooling Services">
					<strong>Products</strong>
					<p>
						We have the products and solutions to reach your home comfort needs. We work with a variety of manufactures and pride ourselves on providing quality products at affordable prices.
					</p>
				</div>	

			</div>

		</div>
	</div>

	<?php 
    /**************************************
    * Testimonials Row
	*****/ ?>

	<div class="homepage-testimonials-row">
		<div class="homepage-testimonials-row__inner wrapper">

			<div class="homepage-testimonials-row__quote">
				<span>“</span>
			</div>
			
			<?php //Slider Uses Slick Slider, inii in 1-scripts.js ?>
			<div class="homepage-testimonial-slider">
					
				<div class="homepage-testimonial-slider__slide">
					<span class="homepage-testimonial-slider__slide-content">
						I have had Ambient Edge Heating & Air in for service calls on my old unit and was thrilled that they would be the company to install my upgrade. I saw a big improvement right away and was sure that I had made the right decision.
					</span>
					<span class="homepage-testimonial-slider__slide-author">
						Carl Stough
					</span>
				</div>

						
				<div class="homepage-testimonial-slider__slide">
					<span class="homepage-testimonial-slider__slide-content">
						Brian, the service technician, was most helpful diagnosing the problem quickly, isolating the defective part and replacing it. He was very friendly and polite. He was also dressed neatly. Thank you.
					</span>
					<span class="homepage-testimonial-slider__slide-author">
						Greg and Ada Guindon
					</span>
				</div>

				<div class="homepage-testimonial-slider__slide">
					<span class="homepage-testimonial-slider__slide-content">
						Plan to have the same contractor replace the unit with a Trane, when it wears out. Very satisfied with contractor, Ambient Edge.
					</span>
					<span class="homepage-testimonial-slider__slide-author">
						Murray Gibson
					</span>
				</div>

			</div>

		</div>
	</div>

	<?php 
    /**************************************
    * Service Experts Section
	*****/ ?>

	<div class="homepage-service-section">
		<div class="homepage-service-section__inner wrapper">
		
			<strong class="homepage-service-section__subtitle">
				The Ambient Edge Difference
			</strong>
			
			<h2 class="homepage-service-section__title">
				Your Trusted Service Experts
			</h2>

			<div class="homepage-service-section__row">
			
				<div class="homepage-single-service-section all-hours">
					<span class="homepage-single-service-section__title">
						24/7
					</span>
					<p class="homepage-single-service-section__content">
						We at Ambient Edge care about your family’s health and safety. That’s why our AC repair team is available <a href="<?php echo site_url(); ?>/about-us/emergency-services/">24 hours a day</a>, seven days a week for emergency repairs. We’ll come to your home as quickly as possible to assess the issue and get your heating and cooling system back up and running, so that your entire family can stay safe and comfortable all year long.
					</p>
				</div>

				<div class="homepage-single-service-section satisfaction-guarantee">
					<span class="homepage-single-service-section__title">
						100% Satisfaction Guarantee
					</span>
					<p class="homepage-single-service-section__content">
						We’re supremely confident that our service and product quality will surpass what you expect, and that’s reflected in how our service contracts are written. We put our <a href="<?php echo site_url(); ?>/about-us/satisfaction-guarantee/">100% satisfaction guarantee</a> right there for everyone to see.
					</p>
				</div>

				<div class="homepage-single-service-section flat-rate">
					<span class="homepage-single-service-section__title">
						Flat Rate Pricing
					</span>
					<p class="homepage-single-service-section__content">
					Our Flat-Rate pricing system means you will never be caught off-guard by hidden costs. Our accurate estimates and quality service mean no surprises and no unexpected fees.
					</p>
				</div>

				<div class="homepage-single-service-section free-estimate">
					<span class="homepage-single-service-section__title">
						Free Estimates
					</span>
					<p class="homepage-single-service-section__content">
						Replacing your heating or cooling system is easy with our free in-home consultation. Our comfort specialist will arrive on time and ready to go over all your options. We provide <a href="<?php echo site_url(); ?>/services/air-conditioning/installation/">free estimates for new units and upgrades</a>. Let our team of experts find the right products for your home comfort needs.
					</p>
				</div>

			</div>


		</div>
	</div>

	<?php 
    /**************************************
    * Blog Feed
	*****/ ?>

	<div class="homepage-blog-section">
		<div class="homepage-blog-section__inner wrapper">
		
			<h2 class="homepage-blog-section__title">
				Featured Articles
			</h2>

			<div class="homepage-blog-section__loop">
			<?php //BEGIN BLOG LOOP
				$args = array(
					'posts_per_page' => 4,
					'post_type' => 'post',					
					'order' => 'DSC',
					'orderby' => 'date',
					'post__not_in' => [6063]
				);

				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		

					<a href="<?php the_permalink(); ?>" class="homepage-single-blog-post">
						<?
							if ( has_post_thumbnail() ) {
								the_post_thumbnail('faq-thumb', [ 'alt' => get_the_title() ]);
							} else {
								echo '<img src="' . get_stylesheet_directory_uri() . '"/images/default-faq-thumb.jpg" alt="' . get_the_title() . '"/>';
							}
						?>
						<span class="homepage-single-blog-post__title">
							<?php the_title(); ?>
						</span>
					</a>

				<?php endwhile; endif; ?>
				<?php wp_reset_query(); ?>

			</div>

		</div>
	</div>


	<?php 
    /**************************************
    * Frequently Asked Questions
	*****/ ?>

	<div class="homepage-faq-section">
		<div class="homepage-faq-section__inner wrapper">
		
			<h2 class="homepage-faq-section__title">
				More Frequently Asked Questions
			</h2>

			<div class="homepage-faq-section__loop">
				<ul>
				<?php //BEGIN FAQ LIST LOOP

				$faqTerms = get_terms( 'ambient_faqs' );
				$faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

				$args = array(
					'posts_per_page' => 10,
					'post_type' => 'page',
					'tax_query' => array(
							array(
								'taxonomy' => 'ambient_faqs',
								'field' => 'term_id',
								'terms' => $faqTermIDs
							),
						),
					'order' => 'DSC',
					'orderby' => 'rand',
				);

				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		

					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

				<?php endwhile; endif; ?>
				<?php wp_reset_query(); ?>

					
				</ul>
			</div>

		</div>
	</div>


   
	<?php 
}