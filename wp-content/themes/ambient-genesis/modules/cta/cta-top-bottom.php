<?php
//Adding top CTA Accepts "top" and "bottom"
function content_cta($placement)
{
	global $post;
	$is_top_level = '';
  $this_page_keyword = '';
	$html = '';
  $city_name = '';
	$hit_it = false;
	$post_id = $post->ID;
  $site_url = get_site_url();
  if ($post_id) {
    $this_page_keyword = get_post_meta( $post_id, '_hc_related_widget_title', true );
  }
	if ($post->post_parent) {
		$is_top_level = get_post_meta( $post->post_parent, '_hc_related_widget_title', true );
	}
	switch ($is_top_level) {
		case 'Personal Injury':
			$hit_it = false;
			break;
		case 'Birth Injuries':
		case 'Birth Injury':
		case 'Car Accident':
		case 'Car Accidents':
		case 'Defective Drug':
		case 'Defective Tires':
		case 'Erb’s Palsy':
		case 'Florida Hospitals':
		case 'Medical Malpractice':
		case 'Slip and Fall':
		case 'Slip and Fall Accidents':
		case 'Slip & Fall Injury':
		case 'Tire Accident':
		case 'Truck Accident':
		case 'Truck Accidents':
		case 'Workers Comp.':
		case 'Construction Accidents':
		case 'Motorcycle Accidents':
		case 'Work Injury':
			$hit_it = true;
			break;

		default:
			$hit_it = false;
			break;
	}

  $city = wp_get_post_terms( $post->post_parent, 'hc_location_phone');
  $city_name = $city[0]->name;
  if (!$city_name) {
    $city = wp_get_post_terms( $post->post_parent, 'hc_location_phone');
    $city_name = $city[0]->name;
  }
  if (!$city_name) {
    $city = wp_get_post_terms( $post_id, 'hc_location_phone');
    $city_name = $city[0]->name;
  }

  $phone = wp_get_post_terms( $post->post_parent, 'hc_location_phone');
  if ($phone[0]->description) {
    $phone_clean_up =  preg_replace('/[^0-9.]/', '', $phone[0]->description);
    $phone_num_html =  '+1'.$phone_clean_up;
    $phone_num_anchor =  localize_us_number($phone_clean_up);
  } else {
    $phone_num_html =  '+1-855-529-0269';
    $phone_num_anchor =  '(855) 529-0269';
  }

	if ($is_top_level == 'Personal Injury') { //Check if pahe is Personal Injury
    if ($placement == 'top') {
      $html = '<p class="topcta">If you have been injured in an accident, call <a href="tel:'.$phone_num_html.'>'.$phone_num_anchor.'</a> to get a free consultation from a <a href="'.get_permalink($post->post_parent).'" title="'.$city_name.' Personal Injury Lawyer">'.$city_name.' Personal Injury Lawyer</a>.</p>';
    }
    if ($placement == 'bottom' && $city_name != '' && $is_top_level != '') {
      $html = '<p class="bottomcta">Our team of dedicated <a href="'.get_permalink($post->post_parent).'" title="'.$city_name.' '.$is_top_level.' Lawyers">'.$city_name.' '.$is_top_level.' Lawyers</a> can help with your '.$is_top_level.' case today. Get Free Case Review at <a href="tel:'.$phone_num_html.'>'.$phone_num_anchor.'</a>.</p>';
    }
	}

	if ($hit_it == true) {

		if ($is_top_level == 'Workers Comp.') {
			$is_top_level = 'Workers Compensation';
		}

		if ($is_top_level == 'Car Accidents') {
			$is_top_level = 'Car Accident';
		}

		if ($is_top_level == 'Slip and Fall Accidents') {
			$is_top_level = 'Slip and Fall';
		}
		if ($is_top_level == 'Slip & Fall Injury') {
			$is_top_level = 'Slip and Fall';
		}

    if ($is_top_level == 'Truck Accidents') {
			$is_top_level = 'Truck Accident';
		}
    if ($is_top_level == 'Construction Accidents') {
			$is_top_level = 'Construction Accident';
		}
    if ($placement == 'top') {
      $html = '<p class="topcta">If you have been injured in an accident, call <a href="tel:'.$phone_num_html.'>'.$phone_num_anchor.'</a> to get a free consultation from a <a href="'.get_permalink($post->post_parent).'" title="'.$city_name.' '.$is_top_level.' Lawyer">'.$city_name.' '.$is_top_level.' Lawyer</a>.</p>';
    }
    if ($placement == 'bottom' && $city_name != '' && $this_page_keyword != '') {
      $html = '<p class="bottomcta">Our team of dedicated <a href="'.get_permalink($post->post_parent).'" title="'.$city_name.' '.$is_top_level.' Lawyers">'.$city_name.' '.$is_top_level.' Lawyers</a> can help with your '.$is_top_level.' case today. Get Free Case Review at <a href="tel:'.$phone_num_html.'>'.$phone_num_anchor.'</a>.</p>';
    }
	}
	return $html;
}

function faq_cta($placement)
{
  global $post;
	$post_id = $post->ID;
  $hit_it = false;
  $category_title = '';
  $cta_url = '';
  $phone_num_html = '+1-954-476-1000';
  $phone_num_anchor = '(954) 476-1000';
  $site_url = get_site_url();

  if (!has_term('', 'hc_faqs')) {
    return;
  }

  $terms = get_the_terms($post_id, 'hc_faqs');
  $master_term_name = $terms[0]->name;
  switch ($master_term_name) {
    case 'Birth Injury':
      $hit_it = true;
      $category_title = 'Birth Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/';
      break;
    case 'Car accidents':
      $hit_it = true;
      $category_title = 'Car Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/';
      break;
    case 'Erb’s Palsy':
      $hit_it = true;
      $category_title = 'Erb’s Palsy';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/erbs-palsy/';
      break;
    case 'Medical Malpractice':
      $hit_it = true;
      $category_title = 'Medical Malpractice';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/medical-malpractice-lawyer/';
      break;
    case 'Motorcycle Accidents':
      $hit_it = true;
      $category_title = 'Motorcycle Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/motorcycle-accident-lawyer/';
      break;
    case 'Pedestrian Accidents':
      $hit_it = true;
      $category_title = 'Pedestrian Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/pedestrian-accident-lawyer/';
      break;
    case 'Tire Defects':
      $hit_it = true;
      $category_title = 'Tire Defects';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/tire-accident/';
      break;
    case 'Truck Accidents':
      $hit_it = true;
      $category_title = 'Truck Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/truck-accident-lawyer/';
      break;

    default:
      $hit_it = true;
      $category_title = 'Personal Injury';
      $cta_url = $site_url.'/fort-lauderdale-personal-injury-lawyer/';
      break;
  }

    if ($placement == 'top') {
      $html = '<p class="topcta">If you have been injured in an accident, call <a href="tel:'.$phone_num_html.'>'.$phone_num_anchor.'</a> to get a free consultation from a <a href="'.$cta_url.'" title="Fort Lauderdale '.$category_title.' Lawyer">Fort Lauderdale '.$category_title.' Lawyer</a>.</p>';
    }
    if ($placement == 'bottom') {
      $html = '<p class="bottomcta">Our team of dedicated <a href="'.$site_url.'/fort-lauderdale-personal-injury-lawyer/" title="Fort Lauderdale Personal Injury Lawyers">Fort Lauderdale Personal Injury Lawyers</a> can help with your '.$category_title.' case today. Get Free Case Review at <a href="tel:'.$phone_num_html.'>'.$phone_num_anchor.'</a> with a '.$category_title.' Lawyer in Fort Lauderdale.</p>';
    }
    return $html;
}

function news_cta($placement)
{
  global $post;
	$post_id = $post->ID;
  $hit_it = false;
  $category_title = '';
  $cta_url = '';
  $phone_num_html = '+1-954-476-1000';
  $phone_num_anchor = '(954) 476-1000';
  $site_url = get_site_url();

  if (!has_term('', 'news_category')) {
    return;
  }

  $terms = get_the_terms($post_id, 'news_category');
  $master_term_name = $terms[0]->name;
  switch ($master_term_name) {
    case 'Community Involvement':
    case 'General':
    case 'Scholarships':
    case 'Legal News':
    case 'Injuries':
    case 'Personal Injury':
      $hit_it = true;
      $category_title = 'Personal Injury';
      $cta_url = $site_url.'/fort-lauderdale-personal-injury-lawyer/';
    break;
    case 'Accidents':
    case 'Car Accidents':
    case 'Motor Vehicle Accident':
    case 'Intersection Accident':
      $hit_it = true;
      $category_title = 'Car Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/';
    break;
    case 'Bicycle Accident':
      $hit_it = true;
      $category_title = 'Bicycle Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/bicycle-accident-lawyer/';
    break;
    case 'Bus Accidents':
      $hit_it = true;
      $category_title = 'Bus Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/bus-accident-lawyer/';
    break;
    case 'Fatal Crash':
      $hit_it = true;
      $category_title = 'Fatal Car Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/fatal/';
    break;
    case 'Hit And Run Accident':
      $hit_it = true;
      $category_title = 'Hit And Run Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/hit-and-run/';
    break;
    case 'Motorcycle Accidents':
      $hit_it = true;
      $category_title = 'Motorcycle Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/motorcycle-accident-lawyer/';
    break;
    case 'Rear End Collision':
      $hit_it = true;
      $category_title = 'Rear End Collision';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/rear-end-collision/';
    break;
    case 'Truck Accidents':
      $hit_it = true;
      $category_title = 'Truck Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/truck-accident-lawyer/';
    break;
    case 'Parasailing Accident':
      $hit_it = true;
      $category_title = 'Parasailing Accident';
      $cta_url = $site_url.'locations/fort-lauderdale-fl/parasailing-accident-lawyer/';
    break;
    case 'Defective':
    case 'Defective Airbags':
    case 'Recall':
      $hit_it = true;
      $category_title = 'Defective Auto Parts';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/defective-auto-part-lawyer/';
    break;
    case 'Tire Defects':
      $hit_it = true;
      $category_title = 'Tire Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/tire-accident/';
    break;
    case 'Birth Injury':
      $hit_it = true;
      $category_title = 'Birth Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/';
    break;
    case 'Birth Injury: Erb’s Palsy':
      $hit_it = true;
      $category_title = 'Erb’s Palsy';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/erbs-palsy/';
    break;
    case 'Brachial Plexus Palsy':
      $hit_it = true;
      $category_title = 'Erbs Palsy Brachial Plexus';
      $cta_url = $site_url.'locations/fort-lauderdale-fl/birth-injury-lawyer/erbs-palsy-brachial-plexus/';
    break;
    case 'Brain Injury':
      $hit_it = true;
      $category_title = 'Brain Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/brain-injury-lawyer/';
    break;
    case 'C-Section':
      $hit_it = true;
      $category_title = 'Birth Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/';
    break;
    case 'Cerebral palsy':
      $hit_it = true;
      $category_title = 'Cerebral palsy';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/cerebral-palsy/';
    break;
    case 'Property Owner Liability':
      $hit_it = true;
      $category_title = 'Property Owner Liability';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/property-owner-liability-lawyer/';
    break;
    case 'Slip And Fall Accident':
      $hit_it = true;
      $category_title = 'Slip And Fall Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/slip-and-fall-accident-lawyer/';
    break;
    case 'Medical Malpractice':
      $hit_it = true;
      $category_title = 'Medical Malpractice';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/medical-malpractice-lawyer/';
    break;

    default:
      $hit_it = true;
      $category_title = 'Personal Injury';
      $cta_url = $site_url.'/fort-lauderdale-personal-injury-lawyer/';
    break;
  }
    if ($placement == 'top') {
      $html = '<p class="topcta">If you have been injured in an accident, call <a href="tel:'.$phone_num_html.'>'.$phone_num_anchor.'</a> to get a free consultation from a <a href="'.$cta_url.'" title="Fort Lauderdale '.$category_title.' Lawyer">Fort Lauderdale '.$category_title.' Lawyer</a>.</p>';
    }
    if ($placement == 'bottom') {
      $html = '<p class="bottomcta">Our team of dedicated <a href="'.$site_url.'/fort-lauderdale-personal-injury-lawyer/" title="Fort Lauderdale Personal Injury Lawyers">Fort Lauderdale Personal Injury Lawyers</a> can help with your '.$category_title.' case today. Get Free Case Review at <a href="tel:'.$phone_num_html.'>'.$phone_num_anchor.'</a> with a '.$category_title.' Lawyer in Fort Lauderdale.</p>';
    }
    return $html;
}

// Also used for Videos
function blog_cta($placement)
{
  global $post;
	$post_id = $post->ID;
  $hit_it = false;
  $category_title = '';
  $cta_url = '';
  $phone_num_html = '+1-954-476-1000';
  $phone_num_anchor = '(954) 476-1000';
  $site_url = get_site_url();

  if (!has_term('', 'category')) {
    return;
  }

  $terms = get_the_terms($post_id, 'category');
  $master_term_name = $terms[0]->name;
  switch ($master_term_name) {
    case 'Bike Accidents':
      $hit_it = true;
      $category_title = 'Bicycle Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/bicycle-accident-lawyer/';
    break;
    case 'Birth Asphyxia':
      $hit_it = true;
      $category_title = 'Birth Asphyxia';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/birth-asphyxia/';
    break;
    case 'Birth Injuries':
    case 'Birth Injury':
    case 'Brachial Plexus Palsy':
    case 'C-Section':
    case 'Delayed Emergency C-Section':
    case 'Fetal Distress':
    case 'Fetal Stroke':
    case 'Preeclampsia: Failure to Diagnose':
    case 'Prolonged Labor':
      $hit_it = true;
      $category_title = 'Birth Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/';
    break;
    case 'Boating Accidents':
      $hit_it = true;
      $category_title = 'Boating Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/boating-accident-lawyer/';
    break;
    case 'Brain Cooling Error':
      $hit_it = true;
      $category_title = 'Brain Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/brain-injury-lawyer/';
    break;
    case 'Brain Injury':
      $hit_it = true;
      $category_title = 'Brain Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/brain-injury-lawyer/';
    break;
    case 'Breech Birth':
      $hit_it = true;
      $category_title = 'Breech Birth Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/breech/';
    break;
    case 'Broken Bones During Delivery':
      $hit_it = true;
      $category_title = 'Broken Bone';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/broken-bone-lawyer/';
    break;
    case 'Burn Injury':
      $hit_it = true;
      $category_title = 'Burn Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/burn-injury-lawyer/';
    break;
    case 'Bus Accidents':
      $hit_it = true;
      $category_title = 'Bus Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/bus-accident-lawyer/';
    break;
    case 'Accidents':
    case 'Car Accidents':
    case 'Child Car Seat':
    case 'Motor Vehicle Accidents':
      $hit_it = true;
      $category_title = 'Car Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/';
    break;
    case 'Cerebral palsy':
      $hit_it = true;
      $category_title = 'Cerebral palsy';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/cerebral-palsy/';
    break;
    case 'Construction Accidents':
      $hit_it = true;
      $category_title = 'Construction Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/construction-accident-lawyer/';
    break;
    case 'Crane Accidents':
      $hit_it = true;
      $category_title = 'Crane Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/construction-accident-lawyer/crane-accidents/';
    break;
    case 'Crib Injuries':
      $hit_it = true;
      $category_title = 'Crib Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/crib-injury/';
    break;
    case 'Cystic Fibrosis':
      $hit_it = true;
      $category_title = 'Cystic Fibrosis';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/cystic-fibrosis/';
    break;
    case 'Defective Auto Parts':
    case 'Lipitor':
    case 'Low T':
    case 'Product Recalls':
    case 'Recall':
    case 'Defective':
    case 'Defective Products':
      $hit_it = true;
      $category_title = 'Defective Products';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/defective-product-lawyer/';
    break;
    case 'Elderly Driver Accident':
      $hit_it = true;
      $category_title = 'Elderly Driver Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/elderly-drivers/';
    break;
    case 'Birth Injury: Erbs Palsy':
      $hit_it = true;
      $category_title = 'Erb’s Palsy';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/erbs-palsy/';
    break;
    case 'Facial Paralysis':
      $hit_it = true;
      $category_title = 'Facial Paralysis';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/facial-paralysis-injuries/';
    break;
    case 'Fatal Crash':
      $hit_it = true;
      $category_title = 'Fatal Car Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/fatal/';
    break;
    case 'Hematomas':
    case 'Hematomas in Newborns':
      $hit_it = true;
      $category_title = 'Hematomas';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/hematomas/';
    break;
    case 'Herniated Disc Accident':
      $hit_it = true;
      $category_title = 'Herniated Disc';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/symptoms-of-a-herniated-disc/';
    break;
    case 'Hit And Run Accident':
      $hit_it = true;
      $category_title = 'Hit and Run';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/hit-and-run/';
    break;
    case 'Florida Hospitals and Therapy Centers':
      $hit_it = true;
      $category_title = 'Hospital Error';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/medical-malpractice-lawyer/hospital-error/';
    break;
    case 'Intersection Accident':
      $hit_it = true;
      $category_title = 'Intersection Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/dangerous-intersections/';
    break;
    case 'Medical Malpractice':
      $hit_it = true;
      $category_title = 'Medical Malpractice';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/medical-malpractice-lawyer/';
    break;
    case 'Motorcycle Accidents':
      $hit_it = true;
      $category_title = 'Motorcycle Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/motorcycle-accident-lawyer/';
    break;
    case 'Neonatal Hypoglycemia':
      $hit_it = true;
      $category_title = 'Neonatal Hypoglycemia';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/neonatal-hypoglycemia/';
    break;
    case 'Parasailing Accidents':
      $hit_it = true;
      $category_title = 'Parasailing Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/parasailing-accident-lawyer/';
    break;
    case 'Pedestrian Accidents':
      $hit_it = true;
      $category_title = 'Pedestrian Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/pedestrian-accident-lawyer/';
    break;
    case 'Periventricular Leukomalacia':
      $hit_it = true;
      $category_title = 'Periventricular Leukomalacia';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/periventricular-leukomalacia/';
    break;
    case 'Pool Accidents':
      $hit_it = true;
      $category_title = 'Pool Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/pool-accident-lawyer/';
    break;
    case 'Prescription Drugs':
    case 'Prescription Drugs and Labor-Inducing Drugs that May Harm Mother and Baby':
      $hit_it = true;
      $category_title = 'Prescription Drugs ';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/prescription-drugs/';
    break;
    case 'Property Owner Liability':
      $hit_it = true;
      $category_title = 'Property Owner Liability';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/property-owner-liability-lawyer/';
    break;
    case 'Slip and Fall':
      $hit_it = true;
      $category_title = 'Slip and Fall';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/slip-and-fall-accident-lawyer/';
    break;
    case 'Spina Bifida':
      $hit_it = true;
      $category_title = 'Spina Bifida';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/birth-injury-lawyer/spina-bifida/';
    break;
    case 'Spinal Cord Injury':
      $hit_it = true;
      $category_title = 'Spinal Cord Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/medical-malpractice-lawyer/spinal-cord-injury/';
    break;
    case 'Texting and Driving Accident':
      $hit_it = true;
      $category_title = 'Texting and Driving Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/texting-while-driving/';
    break;
    case 'ATV Tire Failures':
    case 'Defective Tires':
    case 'Motorcycle Tire Failure':
    case 'Tire Defects':
      $hit_it = true;
      $category_title = 'Tire Defects';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/tire-accident/';
    break;
    case 'Garbage Truck Accidents':
    case 'Truck Accidents':
      $hit_it = true;
      $category_title = 'Truck Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/truck-accident-lawyer/';
    break;
    case 'Roofing Accidents':
    case 'Scaffold accidents':
    case 'Work Injuries':
      $hit_it = true;
      $category_title = 'Work Injury';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/work-injury-lawyer/';
    break;
    case 'Wrong Way Accidents':
      $hit_it = true;
      $category_title = 'Wrong Way Accident';
      $cta_url = $site_url.'/locations/fort-lauderdale-fl/car-accident-lawyer/wrong-way-driving/';
    break;

    default:
      $hit_it = true;
      $category_title = 'Personal Injury';
      $cta_url = $site_url.'/fort-lauderdale-personal-injury-lawyer/';
    break;
  }
    if ($placement == 'top') {
      $html = '<p class="topcta">If you have been injured in an accident, call <a href="tel:'.$phone_num_html.'>'.$phone_num_anchor.'</a> to get a free consultation from a <a href="'.$cta_url.'" title="Fort Lauderdale '.$category_title.' Lawyer">Fort Lauderdale '.$category_title.' Lawyer</a>.</p>';
    }
    if ($placement == 'bottom') {
      $html = '<p class="bottomcta">Our team of dedicated <a href="'.$site_url.'/fort-lauderdale-personal-injury-lawyer/" title="Fort Lauderdale Personal Injury Lawyers">Fort Lauderdale Personal Injury Lawyers</a> can help with your '.$category_title.' case today. Get Free Case Review at <a href="tel:'.$phone_num_html.'>'.$phone_num_anchor.'</a> with a '.$category_title.' Lawyer in Fort Lauderdale.</p>';
    }
    return $html;
}

function localize_us_number($phone) {
  $numbers_only = preg_replace("/[^\d]/", "", $phone);
  return preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $numbers_only);
}
