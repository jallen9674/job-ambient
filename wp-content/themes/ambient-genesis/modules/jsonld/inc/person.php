<?php
function jsonldPerson()
{ 
  	global $post;
	$thePostID = $post->ID;

	if (get_field('schema_type_json', $thePostID)) {
		$schema_type = get_field('schema_type_json', $thePostID);
	}
    if ($schema_type == 'person') {
	$image = get_the_post_thumbnail_url($thePostID, 'large');	
	// Name
	
	$firstname = get_field('person_json', $thePostID);
	$firstname = json_encode($firstname['name_person_json']);
	
	// Alternate Name - Surname
	
	$alternateName = get_field('person_json', $thePostID);
	$alternateName = json_encode($alternateName['alternate_name_json']);
    
	// Person jobTitle
	$jobTitle = get_field('person_json', $thePostID);
	$jobTitle = json_encode($jobTitle['jobTitle_json']);
	
	// Image
	
	// Image 
		
		if(empty($image)) {
			ob_start();
			ob_end_clean();
			$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
			$image = $matches[1][0];
		}
		
		if(empty($image)) {
			$image = get_field('logo_json', 'options');
		}
		
		$i = 1;
	
	
	// Social Networks
	
	
    $sameAs_social = get_field('person_json', $thePostID);
    $social_section = $sameAs_social['social_profiles_json'];
	$sameAs_social_new = '';
    $s = 1;
	foreach($social_section as $row) {	
	$sameAs_social_new .=  ''. json_encode($row['url_to_social_profile_json']) .'';
	if ($s < count($social_section)) {
			  $sameAs_social_new .= ',';
			}
	$s++;
	}

/*	
$socials = json_encode(get_field('social_profiles_json', $thePostID));	
$socials = $socials['url_to_social_profile_json'];

$socials = '';
$socials_field = json_encode(get_field('url_to_social_profile_json', $thePostID));	
$socials_count = count($socials_field);
$i = 1;

foreach($socials_field as $row) {

	$socials = '"'. $rsoc['url_to_social_profile_json'] .'"';
	
	if ($i < $socials_count) {
	  $socials .= ',';
	}
	
	$i++;
}*/

	
// JSON-lD Schema for Person	
$html = '<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Person",
  "name": '. $firstname .',
  "alternateName": '. $alternateName .',
  "url": "https://www.yoursite.com/user-profile",
  "image": "'. $image .'",
  "sameAs": ['. $sameAs_social_new .'] ,
  "jobTitle": '. $jobTitle .',
  "worksFor": {
    "@type": "Organization",
    "name": '. json_encode(get_field('legalname_json', 'options')) .'
  }  
}
</script>';
	
	echo $html;
}

}