<?php
function jsonldImages()
{
  add_filter('post_thumbnail_html', 'featuredImagesFilter', 30, 5 );
  add_filter('the_content', 'contentImagesFilter', 15);
}

/* Used for the_post_thumbnail() images */
function featuredImagesFilter($html, $post_id, $post_thumbnail_id, $size, $attr)
{
  $size = get_image_size_name($size[0], $size[1]);
  $jsonld = imageSchemaHTML($post_thumbnail_id, $size);
  $html = $html.$jsonld;
  return $html;
}

/* function for manual deployment */
function addImageSchema($img_url)
{
  $attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $img_url );
  $id = attachment_url_to_postid($attachment_url);
  $jsonld = imageSchemaHTML($id, 'full', $img_url);
  echo $jsonld;
}

/* create the html for the schema */
function imageSchemaHTML($post_thumbnail_id, $size = 'full', $attachment_url = ''){
  $image = get_post($post_thumbnail_id);
  if($attachment_url !== '') {
    $image_url = $attachment_url;
  } else {
    $image_url = wp_get_attachment_image_src($post_thumbnail_id, $size);
    $image_url = $image_url[0];
  }
  $image_title = json_encode($image->post_title);
  $image_description = json_encode($image->post_content);
  $image_date = strtotime($image->post_date);
  $converted_image_date = date('Y-m-d', $image_date);

  if(empty($image_url)) {
    return;
  }

  if (isLocal($image_url) === false) {
    return;
  }

  $image_url = WPtoCDNurl($image_url);

  return '<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "ImageObject",
    "contentUrl": "'. $image_url .'",
    "datePublished": "'. $converted_image_date .'",
    "description": '. $image_description .',
    "name": '. $image_title .'
  }
  </script>';
}

/* Used for images inside the_content() */
function contentImagesFilter($the_content)
{
  $reg_exUrl = "/<img[^>]+\>/i";

  $var = preg_replace_callback($reg_exUrl, function ($m) {

      preg_match('/src="([^"]*)"/i', $m[0], $src);
      $m = $m[0] . imageSchemaHTMLContent($src[1]);
      return $m;

  }, $the_content);
  return $var;
}

function imageSchemaHTMLContent($img_url)
{
  $attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $img_url );
  $id = attachment_url_to_postid($attachment_url);
  $jsonld = imageSchemaHTML($id, 'full', $img_url);
  return $jsonld;
}

function get_image_size_name( $w = 150, $h = 150 ) {
  global $_wp_additional_image_sizes;

  $sizes = array();
  foreach ( get_intermediate_image_sizes() as $_size ) {
    if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
      if ( $w == get_option( "{$_size}_size_w" ) && $h == get_option( "{$_size}_size_h" ))
      return $_size;
    } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
      if ( $w == $_wp_additional_image_sizes[ $_size ]['width'] && $h == $_wp_additional_image_sizes[ $_size ]['height'] )
      return $_size;
    }
  }

  return false;
}

function isLocal($url)
{
  global $wpdb;
  $CDN = get_field('cdn_json', 'options');
  $home_url = get_home_url('/') . '/';
  $check_CDN = strpos($url, $CDN);
  $check_Home = strpos($url, $home_url);

  if (($check_CDN !== false) || ($check_Home !== false)) {
     return true;
  } else {
    return false;
  }
}

function CDNtoWPurl($url)
{
  $CDN = get_field('cdn_json', 'options');
  $home_url = get_home_url('/') . '/';

  if (!empty($CDN)) {
  $url = str_replace($CDN, $home_url, $url);
  }

  return $url;
}

function WPtoCDNurl($url)
{
  $CDN = get_field('cdn_json', 'options');
  $home_url = get_home_url('/') . '/';

  if (!empty($CDN)) {
    $url = str_replace($home_url, $CDN, $url);
  }

  return $url;
}
