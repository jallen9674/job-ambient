<?php
function jsonldDefinedTerm()
{
  if (is_archive() || is_post_type_archive() || is_tag()) {
    $term = get_queried_object();
    $queried_object = get_queried_object();
    $taxonomy = $queried_object->taxonomy;
    $term_id = $queried_object->term_id;
    $schema_type = get_field('schema_type_json', $taxonomy . '_' . $term_id);
    if ($schema_type != 'definedterm') {
      return;
    }

    $description = hc_strip_shortcodes(wpautop( $queried_object->description ));
    $short_description = substr( $description, 0, strpos( $description, '</p>' ) + 4 );
    $short_description = wp_strip_all_tags($short_description);
    $short_description = json_encode($short_description);
    $description = wp_strip_all_tags($description);
    $description = json_encode($description);
    $the_title = json_encode($queried_object->name);
    $page_url = get_field('main_glossary_url', 'options');
  } else {
    global $post;
    $thePostID = $post->ID;
    if (get_field('schema_type_json', $thePostID)) {
      $schema_type = get_field('schema_type_json', $thePostID); 
    }
    if ($schema_type != 'definedterm') {
      return;
    }
    $description = hc_strip_shortcodes(wpautop( get_the_content( $thePostID )));
    $short_description = substr( $description, 0, strpos( $description, '</p>' ) + 4 );
    $short_description = wp_strip_all_tags($short_description);
    $short_description = json_encode($short_description);
    $description = wp_strip_all_tags($description);
    $description = json_encode($description);
    $the_title = json_encode(get_the_title($thePostID));
    $page_url = get_field('main_glossary_url', 'options');
  }

  if ($schema_type == 'definedterm') {
    $html = '<script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "DefinedTerm",
      "name": '. $the_title .',
      "termCode": "'. $thePostID .'",
      "description": '. $description .',
      "inDefinedTermSet": '. json_encode($page_url) .'
    }
    </script>';

    echo $html;
  }

}
