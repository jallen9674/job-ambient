<?php
/**
 * Single Template
 */


//Remove Post Meta & Info
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

//* Remove the post info function
remove_action( 'genesis_before_post_content', 'genesis_post_info' );


add_filter( 'genesis_post_meta', 'hennessey_remove_post_meta' );
function hennessey_remove_post_meta( $post_meta ) {
	$post_meta = '';
	return $post_meta;
}

// Runs the Genesis loop.
genesis();

