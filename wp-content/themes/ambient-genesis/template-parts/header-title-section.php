<?php 
/*---------------------------------------
Main H1 Section on General Interior Pages
----------------------------------------*/

function hennessey_single_interior_header(){
    ?>

    <?php if (  is_singular() && !is_front_page() ) { ?>

        <div class="interior-header">
            <div class="interior-header__top"
                <?php if ( has_post_thumbnail() && is_page() ){ 
                    echo 'style="background-image: url(' .get_the_post_thumbnail_url(null, 'page-hero') . ')"'; 
                    }
                ?>        
            >

                <div class="wrapper">
                    <h1 class="page-title interior-header__title"><?php the_title(); ?></h1>
                    <?php 
                        if ( is_singular('post') ){
                            echo '<span class="interior-header__meta">Posted On: ' . get_the_date('F j, Y') . '</span>';
                        }
                    ?>
                    <div  class="interior-header__cta">
                        <p>We're On Call 24/7 to react promptly to your ac, heating, and plumbing emergencies.</p>
                        <a href="<?php echo site_url(); ?>/schedule-service/" class="interior-header__cta-button">Book Online Now!</a>
                    </div>
                </div>
            </div>

            <div class="interior-header__breadcrumbs">
                <div class="wrapper">
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                ?>
                </div>
           </div>
        </div>

    <?php } else if ( is_post_type_archive('hc_glossary') ){ 
    
    //Glossary archive title
    ?>

        <div class="interior-header">
            <div class="interior-header__top">
                <div class="wrapper">
                    <h1 class="page-title interior-header__title">Legal Glossary</h1>            
                </div>
            </div>
        </div>

    <?php } else { ?>

        <?php if (!is_front_page()) { genesis_do_post_title(); } ?>

    <?php } //End Else?>

    <?php 
}


function hennessey_archive_header(){
    ?>

    <div class="interior-header">
        <div class="interior-header__top">
            <div class="wrapper">
                <h1 class="page-title interior-header__title">
                
                <?php 
                
                if (is_category()) { 
                    echo 'Posts Categorized: '; single_cat_title(); 
                } elseif (is_tag()) { 
                    echo 'Posts Tagged: ';   single_tag_title(); 
                } else {
                    the_archive_title();
                } 
                
                if ( is_paged() ){
                    $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
                    echo '- Page ' . $currentPageNum;
                }
                
                ?>
                </h1>  
            </div> 
        </div>
        <div class="interior-header__breadcrumbs">
            <div class="wrapper">
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                ?>
                </div>
           </div>     
    </div>
    
    <?php
}

function hennessey_blog_header() {
    ?>
  
      <div class="interior-header">
          <div class="interior-header__top">
            <div class="wrapper">
                <h1 class="page-title interior-header__title">
                    Blog
                    <?php
                    if( is_paged() ){
                        $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
                        echo '- Page ' . $currentPageNum;
                    }
                    ?>
                </h1>    
                </div>        
          </div>
          <div class="interior-header__breadcrumbs">
                <div class="wrapper">
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                ?>
                </div>
           </div>
      </div>
  
    <?php 
  }