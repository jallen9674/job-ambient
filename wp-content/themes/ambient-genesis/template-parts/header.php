<?php 

/*----------------------------------
Global Header Content
-----------------------------------*/

function hennessey_global_header() {

    global $post;

    $cityTermID = get_the_terms($post->ID, 'hc_related');
    $acEmergencyLink    = get_term_meta( $cityTermID[0]->term_id, 'ac_link', true ); 
    $heatEmergencyLink  = get_term_meta( $cityTermID[0]->term_id, 'heat_link', true ); 

    //Swap Out Heat for AC When season switches
    if ( $heatEmergencyLink ) {
        $emergencyButtonLink = $heatEmergencyLink;
      } else {
        $emergencyButtonLink =  site_url() . '/about-us/emergency-services/';
      }

    ?>

    <div class="site-header__notification-bar notification-bar">
        <div class="notification-bar__inner">
            <a href="<?php echo $emergencyButtonLink; ?>">
                24/7 Emergency Service Available! Click Here!
            </a>
        </div>
    </div>

   <div class="site-header__inner desktop-header">

        <div class="desktop-header__logo">

            <a href="<?php echo site_url(); ?>">
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ambient-logo-color.png" alt="Ambient Edge Heating and Air Conditioning Logo" class="desktop-header__logo--default"/>              
            </a>

        </div>


        <div class="desktop-header__cta header-cta">
            <span class="header-cta__subheading">
                    Call Today!
            </span>
            <a class="header-cta__phone" href="tel:<?php echo hennessey_phone_display(); ?>">
                <?php echo hennessey_phone_display(); ?>
            </a>
        </div>

        <?php // Tablet Navigation TODO -- Better Implementation if Time?>


   </div>

   <div class="desktop-navigation-wrapper">
      <div class="desktop-navigation-wrapper__inner wrapper">
        <nav role="navigation">
            <?php 
                wp_nav_menu(
                    array(
                        'container' => false,                           
                        'container_class' => 'menu cf',                 
                        'menu' => 'New Main Menu',  
                        'menu_class' => 'desktop-nav',               
                        'theme_location' => 'main-nav',                 
                        'before' => '',                                 
                        'after' => '',                                  
                        'link_before' => '',                            
                        'link_after' => '',                             
                        'depth' => 0,                                   
                        'fallback_cb' => ''                             
                    )
                ); 
            ?>               
        </nav>
      </div>
    </div>



    <?php 
}