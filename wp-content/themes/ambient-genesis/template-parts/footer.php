<?php 
/*----------------------------------
Global Footer Content
-----------------------------------*/

function hennessey_global_footer() {
    ?>

    <div class="footer-large-cta">
        <div class="footer-large-cta__inner wrapper">
        
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-full-vertical.png" alt="Ambient Edge Logo" / class="footer-large-cta__image" />

            <span class="footer-large-cta__content">
                Call <a href="tel:<?php echo hennessey_phone_display(); ?>"><?php echo hennessey_phone_display(); ?></a> To Shedule Service, Or...
            </span>

            <a href="<?php echo site_url(); ?>/contact/" class="footer-large-cta__button">Click Here!</a>

        </div>
    </div>

    <footer class="site-footer">
        
        <div class="footer-top">
            <div class="footer-top__inner wrapper">


                <div class="footer-top__logo">

                    <img class="footer-top__logo-image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer-logo.png" alt="Ambient Edge" />

                    <span class="footer-top__logo-cta">
                        Call <a href="tel:<?php echo hennessey_phone_display(); ?>"><?php echo hennessey_phone_display(); ?></a> or <br><a href="<?php echo site_url(); ?>/contact/">click here</a> for service!
                    </span>


                </div>

                <div class="footer-top__links">
                    <strong class="footer-top__heading">Navigation</strong>
                    <ul>
                        <li><a href="<? echo site_url(); ?>">Home</a></li>
                        <li><a href="<? echo get_the_permalink(3979); ?>">Locations</a></li>
                        <li><a href="<? echo get_the_permalink(35); ?>">Residential</a></li>
                        <li><a href="<? echo get_the_permalink(42); ?>">Commercial</a></li>
                        <li><a href="<? echo get_the_permalink(1125); ?>">Products</a></li>
                        <li><a href="<? echo get_the_permalink(4083); ?>">About Us</a></li>
                        <li><a href="<? echo site_url(); ?>/blog/">Blog</a></li>
                    </ul>
                </div>

                

                <div class="footer-top__address">

                    <strong class="footer-top__heading">Locations</strong>
                        <div class="footer-top__address-inner">
                            <address>
                                <strong><a href="/locations/henderson-air-conditioning-repair-company/">HENDERSON, NV</a></strong>
                                <p>
                                    110 Corporate Park Dr. #111<br>
                                    Henderson, NV 89074
                                </p>
                                <a class="footer-top__directions-link" href="https://www.google.com/maps/dir//ambient+edge+henderson+nv/@36.0362633,-115.0691161,13z/data=!4m8!4m7!1m0!1m5!1m1!1s0x80c8d11204e55f2f:0x7a682545a2c76e99!2m2!1d-115.034011!2d36.036199" target="_blank">Get Directions &raquo;</a>
                                <a href="tel:702-979-7855">702-979-7855</a>
                            </address>

                            <address>
                                <strong><a href="/locations/las-vegas-air-conditioning-repair-company/">LAS VEGAS, NV</a></strong>
                                <p>
                                    5940 S Rainbow Blvd. #213<br>
                                    Las Vegas, NV 89118
                                </p>
                                <a class="footer-top__directions-link" href="https://www.google.com/maps/dir//ambient+edge+las+vegas/@36.0811347,-115.2762319,13z/data=!4m8!4m7!1m0!1m5!1m1!1s0x80c8c7ac161d02eb:0x52be064c278fa7b9!2m2!1d-115.2412126!2d36.0810704" target="_blank">Get Directions &raquo;</a>
                                <a href="tel:702-928-7201">702-928-7201</a>
                            </address>

                            <address>
                                <strong><a href="/locations/kingman-air-conditioning-repair-company/">KINGMAN, AZ</a></strong>
                                <p>
                                    3270 Kino Avenue<br>
                                    Kingman, AZ 86409
                                </p>
                                <a class="footer-top__directions-link" href="https://www.google.com/maps/dir//ambient+edge+kingman/@35.2319087,-114.0358975,13z/data=!4m8!4m7!1m0!1m5!1m1!1s0x80cdc635ba69e57d:0x40be889d8736361b!2m2!1d-114.0008782!2d35.2318436" target="_blank">Get Directions &raquo;</a>
                                <a href="tel:928-433-2979">928-433-2979</a>
                            </address>
                        </div>
                    </div>
            
            </div>
        </div>

        <div class="footer-newsletter">
        
            <strong class="footer-top__heading footer-newsletter__heading">Newsletter Signup</strong>
            <p>
                Stay updated with our newsletter. We promise not to spam.
            </p>
            <?php echo do_shortcode('[contact-form-7 id="6944" title="Newsletter Form" html_class="footer-newsletter-form"]');?>

        </div>

        <ul class="footer-social-icons">
            <li><a href="https://www.twitter.com/ambientedge" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://facebook.com/AmbientEdgeAC" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://www.linkedin.com/company/ambient-edge" target="_blank"><i class="fa fa-linkedin"></i></a></li>
        </ul>
        
        <div class="footer-bottom">
            <div class="footer-bottom__inner wrapper">
            
                <div class="footer-bottom__navigation">
                    <ul>
                        <li><a href="<?php echo get_the_permalink(1210); ?>">Sitemap</a></li>&nbsp;&nbsp;|&nbsp;&nbsp;<li><a href="<?php echo get_the_permalink(1145); ?>">Privacy Policy</a></li>
                    </ul>
                </div>

                <div class="footer-bottom__copyright">
                    AZ ROC# 198597, 296317 | NV LIC# 0071575<br>
                    &copy <?php echo date('Y'); ?> Ambient Edge Air Conditioning and Refrigeration. All Rights Reserved.
                </div>

            </div>
        </div> <?php //End .footer-bottom ?>

    </footer>


    <?php 
}