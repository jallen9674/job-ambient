/*------------------------------------------------
General JS (Most of this is from original theme)
-------------------------------------------------*/

jQuery(document).ready(function($) {

    
  /*****************************************
  Get Initial Referrer and Set in Cookie
  *****************************************/

  //Cookie Helper Functions

  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }


  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
  }

  function getQueryVariable(variable){
      var query = window.location.search.substring(1);
      var vars = query.split("&");
      for (var i=0;i<vars.length;i++) {
              var pair = vars[i].split("=");
              if(pair[0] == variable){return pair[1];}
      }
      return(false);
  }

  //Getting Referrer Info and Setting Cookie

  if ( getCookie('hennessey-referrer') !== '' ) {
  console.log('Existing Cookie: ' + getCookie('hennessey-referrer'));
  console.log('Current Referrer: ' + document.referrer);

  //Update TextArea in Form
  $('.your-info textarea').html(getCookie('hennessey-referrer'));
  } else {

  //Putting Together Cookie Info
  var utmSource = getQueryVariable('utm_source');
  var utmCampaign = getQueryVariable('utm_campaign');
  var utmMedium = getQueryVariable('utm_medium');
  var initialReferrer = document.referrer;

  //Pretty Values for No Info
  if ( utmSource == false ){
    utmSource = 'Not Set';
  } 

  if ( utmCampaign == false ){
    utmCampaign = 'Not Set';
  }

  if ( utmMedium == false ){
    utmMedium = 'Not Set';
  }

  if ( initialReferrer == false ){
    initialReferrer = 'Direct/Not Set';
  }

  //Setting up Cookie Value
  var cookieSourceValue =   '  Tracking Information - Source: ' + utmSource +
                            ', Campaign: ' + utmCampaign +
                            ', Medium: ' + utmMedium +
                            ', Entry Referrer: ' + initialReferrer;

  //Set Cookie
  setCookie('hennessey-referrer',cookieSourceValue,1);

  //Debugging
  console.log('New Cookie: ' + getCookie('hennessey-referrer'));
  console.log('Current Referrer: ' + document.referrer);

  //Update TextArea in Form
  $('.your-info textarea').html(getCookie('hennessey-referrer'));
  }

  /*-----------------------------------------------
  Displaying Mobile Contact Button on Scroll
  -----------------------------------------------*/
   $(window).on( 'scroll', function(){
      if ($(window).scrollTop() >= 300) {
          $('.footer-phone-circle').addClass('visible');
      } else {
          $('.footer-phone-circle').removeClass('visible');
      }
    });



  /*-----------------------------------------------
  Display Ambient Footer Popup
  -----------------------------------------------*/
  $('.ambient-footer-popup').addClass('visible');
  $('.ambient-footer-popup__close-button').on('click', function(e){
    e.preventDefault();
    $('.ambient-footer-popup').addClass('closed');
  });

    //Smooth Scroll to Anchor
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top - 200
            }, 300, function() {});
          }
        }
      });


    //Sorting Alphabetically
    $.fn.sortList = function() {
        var mylist = $(this);
        var listitems = $('li', mylist).get();
        listitems.sort(function(a, b) {
            var compA = $(a).text().toUpperCase();
            var compB = $(b).text().toUpperCase();
            return (compA < compB) ? -1 : 1;
        });
        $.each(listitems, function(i, itm) {
            mylist.append(itm);
        });
    }


    //Sort Related Areas List
    $("ul.related-listing").sortList();
    //$("ul.location-listing").sortList();


    //Practice Area Filter Functionality

    var options = {
      valueNames: [ 'parent', 'child' ]
    };

    var practiceAreaList = new List('pa-list', options);

    practiceAreaList.on('updated', function(){
      console.log('filter-fire!' + practiceAreaList.visibleItems.length);

      if ( !$('#pa-list .list').hasClass('filtered') ) {
        $('#pa-list .list').addClass('filtered');
      }

      if ( (practiceAreaList.searched === false) ) {
        $('#pa-list .list').removeClass('filtered');
        console.log('default');
      }

      if ( (practiceAreaList.visibleItems.length <= 7) ) {
        $('#pa-list .list').addClass('filtered');
      } else {
        $('#pa-list .list').removeClass('filtered');
      }


    });

    /*Location Specific Menu Functiona lity
    var locationMenu = $('.taxonomy-menu-wrapper .menu').html();
    console.log(locationMenu);
    if (locationMenu){
      $('#childrenof100 .links-list').html(locationMenu);
    }*/
    


    $('.btn-load-more-videos').click(function(e){
      e.preventDefault();

      $('.video-box-hidden').each(function(i){
        if(i < 6) {
          $(this).removeClass('video-box-hidden')
          $(this).find('iframe').attr('src', $(this).find('iframe').data('src'));
        }
      });
    })


    /*-------------------------------
    Homepage Testimonial Slider Activate
    ---------------------------------*/
    $('.homepage-testimonial-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 7000
    });

    $('.interior-testimonial-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 7000
    });



    /*-------------------------------
    Header Menu Updates Depending on Location   
    ---------------------------------*/

    setPracticeAreas();

    function setPracticeAreas(){
      if($('.widget_hc_related_widget .location-widget-links.location-listing a').length) {
        
        var ddHTML = ''
        
        $('.widget_hc_related_widget .location-widget-links.location-listing a').each(function(){
          ddHTML+='<li><a href="'+$(this).attr('href')+'">'+$(this).text()+'</a></li>'
        });

        //Get & Set Location Widget Title
        var locationTitle = $('.widget_hc_related_widget .location-widget-title').text().trim();       
        var locationTitleArray = locationTitle.split(" ").filter(Boolean);
        locationTitle = locationTitleArray.toString().replace(/,/g,' ').replace(/(\r\n|\n|\r)/gm,"").replace('Practice', ' Practice');
        //console.log(locationTitle);

        //Location Link
        var locationLink = $('.menu-title-link').text().trim();       

        //If Menu Is Large do something else
        var count = $(ddHTML).find($('<li>')).andSelf().length;

        ddHTML = ddHTML.replace(/<\/li>/g,'</li>,');          
        ddHTMLArray = ddHTML.split(',');


        if ( count > 9 ) { 

          for (var i = 0; i < ddHTMLArray.length; i++) { 

            var practiceAreaTitle = ddHTMLArray[i];
            practiceAreaTitle = practiceAreaTitle.replace(/<\/?[^>]+(>|$)/g, "");
            
            if ( 
              //Always display the most important practice areas if there are a lot
              (practiceAreaTitle !== "Bicycle Accidents") &&
              (practiceAreaTitle !== "Birth Injury") &&
              (practiceAreaTitle !== "Birth Injuries") &&
              (practiceAreaTitle !== "Boating Accidents") &&
              (practiceAreaTitle !== "Car Accidents") &&
              (practiceAreaTitle !== "Construction Accidents") &&
              (practiceAreaTitle !== "Dog Bites") &&
              (practiceAreaTitle !== "Medical Malpractice") &&
              (practiceAreaTitle !== "Motorcycle Accidents") &&
              (practiceAreaTitle !== "Pedestrian Accidents") &&
              (practiceAreaTitle !== "Personal Injury") &&
              (practiceAreaTitle !== "Slip and Fall Accidents") &&
              (practiceAreaTitle !== "Truck Accidents") &&
              (practiceAreaTitle !== "Workers Compensation") &&
              (practiceAreaTitle !== "Workers' Compensation") &&
              (practiceAreaTitle !== "Wrongful Death")              
            
            ){
              //console.log("Removed: " + practiceAreaTitle)
              ddHTMLArray.splice(i, 1); 
              i--;
            }
            
          }

          var viewMore = '<li><a href="' + locationLink + '">View All Practice Areas</a></li>';
          ddHTMLArray.push(viewMore);
          
        }
        
        //Remove Duplicates
        function unique(list) {
          var result = [];
          $.each(list, function(i, e) {
            if ($.inArray(e, result) == -1) result.push(e);
          });
          return result;
        }

        var ddHTMLArrayUnique = unique(ddHTMLArray);
        ddHTML = ddHTMLArrayUnique.toString();
        ddHTML = ddHTML.replace(/<\/li>,/g,'</li>'); 

        //Florida Pages are Odd
        if ( !(locationTitle.includes('Florida')) ) {
          $('.menu-item-100 > a').html(locationTitle);
          $('.menu-item-100 > a').attr("title", locationTitle);
          $('.menu-item-100 > a').attr("href", locationLink);
          $('.menu-item-has-children.menu-item-100 .sub-menu').html(ddHTML);
        }
                
      }
    }

    /*-------------------------------
    Add Header Class on Scroll

    -- TODO: Refactor if time
    ---------------------------------*/

   
    function headerScrolledStatus(window, desktop, mobile){
      
      var header = $("body");
      var scroll = $(document).scrollTop();
      
        //Mobile Resolutions
        if ( window < 768) {
          
          if (scroll > 0) {
            header.addClass("scrolled");
            //header.css('top', mobile);
          } else {
              header.removeClass("scrolled");

              setTimeout(function() {
                  var animatedHeight = $('.mobile-header').outerHeight();
                  header.css('top', animatedHeight );
              }, 210);
          }         
         
        }

        //Desktop Resolutions
        if ( window >= 768) {
          if (scroll > 0) {
              header.addClass("scrolled");
              //header.css('top', desktop - 93);
          } else {
              header.removeClass("scrolled");
              header.css('top', 0);
          }
      }

    }


    //Calling 

    var windowWidth =  $(window).width();
    var headerHeight = $('.site-header').outerHeight();
    var mobileHeaderHeight = $('.mobile-header').outerHeight();

    headerScrolledStatus(windowWidth, headerHeight, mobileHeaderHeight);

    $(window).resize( throttle( function() { 

      var windowWidth =  $(window).width();
      var headerHeight = $('.site-header').outerHeight();
      var mobileHeaderHeight = $('.mobile-header').outerHeight();
      
      headerScrolledStatus(windowWidth, headerHeight, mobileHeaderHeight);
    }, 30));
    
    $(window).scroll( debounce( function() { 
      var windowWidth =  $(window).width();
      var headerHeight = $('.site-header').outerHeight();
      var mobileHeaderHeight = $('.mobile-header').outerHeight();
      var scroll = $(window).scrollTop(); 

      headerScrolledStatus(windowWidth, headerHeight, mobileHeaderHeight);       
       
    }, 100));



    /*--------------------------------------------
    Adding Drop Down Arrows to Menu Items
    ---------------------------------------------*/

    $('.desktop-navigation-wrapper .menu-item-has-children a').append('<span class="desktop-downarrow"></span>');

    
    /*--------------------------------------------
    Mobile Slideout Navigation 
    ---------------------------------------------*/
    

      var slideout = new Slideout({
        'panel':  document.querySelector('.site-container'),
        'menu': document.getElementById('mobile-menu'),
        'padding': 256,
        'tolerance': 70, 
        'side': 'right'
      });
  
      document.querySelector('.navigation-pane-toggle').addEventListener('click', function() {
        slideout.toggle();
      });
      document.querySelector('.navigation-pane-close').addEventListener('click', function() {
        slideout.close();
      });

      var fixed = document.querySelector('.mobile-header');

      slideout.on('translate', function(translated) {
        fixed.style.transform = 'translateX(' + translated + 'px)';
      });

      slideout.on('beforeopen', function () {
        fixed.style.transition = 'transform 300ms ease';
        fixed.style.transform = 'translateX(-256px)';
      });

      slideout.on('beforeclose', function () {
        fixed.style.transition = 'transform 300ms ease';
        fixed.style.transform = 'translateX(0px)';
      });

      slideout.on('open', function () {
        fixed.style.transition = '';
      });

      slideout.on('close', function () {
        fixed.style.transition = '';
      });

    

  


}); //End Document Ready









/*-------------------------------
FAQS
---------------------------------*/

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;

 jQuery(container).each(function() {

   $el = jQuery(this);
   jQuery($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

  var $container = jQuery('.faq-filter-list').isotope({
    itemSelector: '.grid-item',
    layoutMode: 'fitRows',
    getSortData: {
      category: '[data-category]'
    },
    hiddenClass: 'isotope-hidden',
  });

// filter functions
  var filterFns = {

  };

  var itemReveal = Isotope.Item.prototype.reveal;
Isotope.Item.prototype.reveal = function() {
  itemReveal.apply( this, arguments );
  jQuery( this.element ).removeClass('isotope-hidden');
};

var itemHide = Isotope.Item.prototype.hide;
Isotope.Item.prototype.hide = function() {
  itemHide.apply( this, arguments );
  jQuery( this.element ).addClass('isotope-hidden');
};

  // bind filter button click
  jQuery('#filters').on( 'click', 'button', function() {

    var filterValue = jQuery( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $container.isotope({ filter: filterValue });



  });

  // change is-checked class on buttons
  jQuery('.filters-button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = jQuery( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      jQuery( this ).addClass('is-checked');
    });
  });


  $container.on( 'arrangeComplete',  function( event, filteredItems ) {
    //console.log( 'Isotope arrange completed on ' + filteredItems.length + ' items' );
    //equalheight('.faq-filter-listing');
  });

  $container.on( 'layoutComplete',  function( event, filteredItems ) {
    console.log( 'Layout Complete Isotope arrange completed on ' + filteredItems.length + ' items' );
    //jQuery('.faq-filter-listing').addClass('sorted');

     if(jQuery('.isotope-link:nth-of-type(1)').hasClass('is-checked')){
      jQuery('.faq-filter-listing').removeClass('sorted');
     }

     equalheight('.faq-filter-listing:not(.isotope-hidden)');

  });


  //Equal Height Columns
  equalheight('.faq-filter-listing:not(.isotope-hidden)');
