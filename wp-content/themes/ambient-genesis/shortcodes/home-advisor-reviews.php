<?php
/*--------------------------------
Home Advisor Review Display
[home-advisor-review]
---------------------------------*/

function hennesseyHomeAdvisorReviews($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT
?>

<div style="background: #f1f2f2; border: 1px solid #ccc; border-radius: 3px; padding: 0 15px 10px; margin: 0 auto; width: 100%; height: 310px; overflow: hidden; box-sizing: border-box;">
   <div style="border-bottom: 1px solid #d7d7d7; margin: 0 -15px 10px;">
      <img alt="Review Pros" src="//cdn2.homeadvisor.com/images/consumer/home/ha-logo-title.png" width="185" style="float: left; margin: 10px 0 0 20px;">
      <div style="clear: both;"></div>
   </div>


   <iframe src="https://www.homeadvisor.com/ratings/embed/iframe/16226755/?orientation=horizontal&reviewSort=highest" style="width: 100%; height: 215px; background: transparent;" frameborder="0" scrolling="no"></iframe>

   <a href="https://www.homeadvisor.com/rated.AmbientEdgeAir.16226755.html" style="color: #5486a3; float: right; font-size: 11px; font-family: helvetica, arial, san-serif; text-align: center; text-decoration: none;">See More Reviews of Ambient Edge Air Conditioning and Refrigeration, Inc. on HomeAdvisor</a>
</div>


<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('home-advisor-review', 'hennesseyHomeAdvisorReviews');


?>