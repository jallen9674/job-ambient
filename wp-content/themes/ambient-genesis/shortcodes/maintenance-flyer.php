<?php
/*--------------------------------
Maintenance Flyer Shortcode
- August 2018
[hc-maintenance-flyer]
---------------------------------*/

function hcMaintenanceFlyerShortcode($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT

?>

<?php /* UNUSED CURRENTLY

    <h2 class="maintenance-flyer__heading--a">Pick The Right Plan for You!</h2>
    <p>Everything needs maintenance over time. And, like a car getting its oil changed, your home comfort systems need to be inspected and cleaned to make sure everything stays in working order. Neglected maintenance can lead to unwanted leaks, clogs, rising utility costs, reduced equipment eciency, or worse…mechanical failure. When your system fails, needing an emergency repair is never fun and can be costly. Don’t let this happen to you. Get an annual home maintenance plan—and put your mind at ease.</p>
    <h3 class="maintenance-flyer__subheading maintenance-flyer__subheading--script">Key Benefits</h3>
    <h3 class="maintenance-flyer__subheading">Regular Maintenance</h3>
    <ul>
        <li>Prolonged system efficiency</li>
        <li>Promotes healthy air and water</li>
        <li>Lowers utility costs</li>
        <li>Save <b>MONEY</b> on costly repairs</li>
    </ul>


*/ ?>


<div class="maintenance-flyer">
    <div class="maintenance-flyer__inner">

        <h2 class="maintenance-flyer__heading maintenance-flyer__heading--a">Heating and Cooling Plan</h2>      
        <div class="maintenance-flyer__row maintenance-flyer__content maintenance-flyer__content--eap">

            <div class="maintenance-flyer__row-left">

                <h3 class="maintenance-flyer__subheading">Includes</h3>
                <ul>
                    <li>37 Point Inspection</li>
                    <li>Same Day Scheduling</li>
                    <li>No Over Time</li>
                    <li>30 Day Guarantee*</li>
                    <li>Pre-season Scheduling</li>
                    <li>2 Year Inflation Protection</li>
                    <li>$25 Purchase Accrual **</li>
                    <li>Reminder Service</li>
                    <li>24 Hour Scheduling</li>
                    <li>1 Year Repair Warranty</li>
                    <li>2 Visits per Year</li>
                </ul>

            </div> <?php //left ?>

            <div class="maintenance-flyer__row-right">

                <div class="maintenance-flyer__interior-photo">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flyer/ambient-heating-cooling-large.png" alt="Heating and Cooling Plan">
                </div>
                <div class="maintenance-flyer__discount">
                    <strong>Member Discount</strong><br>15% Parts & Labor
                </div>

                <div class="maintenance-flyer__corner-rate">
                    <span class="maintenance-flyer__corner-rate-monthly">$15.99/month</span>
                    <span class="maintenance-flyer__corner-rate-yearly">or<br>$189/year</span>
                </div>

            </div> <?php // right ?>
            
        </div> <?php // end row ?>
        
        <div class="maintenance-flyer__disclaimer">
            <p>Price is for 1st unit. Each additional unit is $11.99/month or $139/year</p>
            <p><small>* 30 Day Guarantee is for service calls only!</small></p>
            <p><small>** Purchase Accrual is annual accrual amount.</small></p>
        </div>


        <h2 class="maintenance-flyer__heading maintenance-flyer__heading--b">Total Home Coverage</h2>
        <div class="maintenance-flyer__row maintenance-flyer__content maintenance-flyer__content--tpp">
           
            <div class="maintenance-flyer__row-left">  

                <h3 class="maintenance-flyer__subheading">Includes</h3>
                <ul>
                    <li>
                        <strong>Heating and Cooling Protection</strong><br>All Benefits of the Edge Assurance Plan
                    </li>
                    <li>
                        <strong>Plumbing Protection</strong><br>All benefits of the Jake's Protection Plan
                    </li>
                </ul>

            </div> <?php //left ?>

            <div class="maintenance-flyer__row-right">

                <div class="maintenance-flyer__interior-photo">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flyer/ambient-pbj-box-a.png" alt="Jakes Protection plan"><br>
                    <a href="https://www.plumbingbyjake.com/kingman-annual-home-plumbing-maintenance-plan/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flyer/ambient-pbj-box-c.png" alt="Edge Protection Plan"></a>
                </div>
                <div class="maintenance-flyer__discount">
                    <strong>Member Discount</strong><br>15% Parts & Labor
                </div>

                <div class="maintenance-flyer__corner-rate">
                    <span class="maintenance-flyer__corner-rate-monthly">$19.99/month</span>
                    <span class="maintenance-flyer__corner-rate-yearly">or<br>$239/year</span>
                </div>

            </div> <?php // right ?>

        </div> <?php // row ?>              

            <div class="maintenance-flyer__disclaimer">
                <p>Price is for 1st unit. Each additional HVAC unit is $11.99/month or $139/year</p>
            </div>            

    </div> <?php // Maintenance Flyer inner ?>
</div> <?php //Maintenance Flyer ?>

<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('hc-maintenance-flyer', 'hcMaintenanceFlyerShortcode');


