<?php
/*--------------------------------
Example Shortcode Wrapper
[scholarship-banner]
---------------------------------*/

function scholarshipBanner($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT
?>

<?php //Output Goes Here ?>

<h2><span>Scholarship Opportunity</span></h2>
<div class="scholarship-banner">
  <a class="scholarship-banner__inner" href="https://www.ambientedge.com/2019-students-affected-by-cancer-scholarship/">
    <img src="https://www.ambientedge.com/wp-content/uploads/2018/01/Ambient_Edge_College_Scholarship.jpg" alt="Ambient Edge College Scholarship Colored" />
    <div class="scholarship-banner__content">
      <h3>Students Affected by Cancer Scholarship</h3>
      <span>Deadline: December 31, 2019</span>
    </div>
  </a>
</div>

<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('scholarship-banner', 'scholarshipBanner');
 