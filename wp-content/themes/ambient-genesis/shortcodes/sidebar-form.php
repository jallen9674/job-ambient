<?php 

/*-------------------------------------
The Contact Form Displayed in Sidebar
[hennessey-sidebar-form]
--------------------------------------*/


function hennesseySidebarForm($atts = null) {

   
    $contact_form = do_shortcode( '[contact-form-7 id="1709" title="Standard Contact Form"]' );

    ob_start();
    //BEGIN OUTPUT

    ?>

        <div class="sidebar-contact-form standard-form">
            
            <span class="sidebar-contact-form__phone-heading">
                Call <a href="tel:<?php echo hennessey_phone_display(); ?>"><?php echo hennessey_phone_display(); ?></a>
            </span>

            <span class="sidebar-contact-form__subheading">
                Free 24/7 Strategy Session
            </span>

            <span class="sidebar-contact-form__cta">
                Request a Call Back
            </span>

            <?php echo $contact_form ?>	
        </div>

    <?php 
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}


add_shortcode('hennessey-sidebar-form', 'hennesseySidebarForm');