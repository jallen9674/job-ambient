<?php
/*--------------------------------
Testimonials Shortcode
[ambient-testimonials]
---------------------------------*/

function ambientTestimonials($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT
?>

<?php //Output Goes Here ?>
    <div class="interior-testimonial-slider-wrapper">
      <div class="interior-testimonial-slider">
					
				<div class="interior-testimonial-slider__slide">
					<span class="interior-testimonial-slider__slide-content">
						I have had Ambient Edge Heating & Air in for service calls on my old unit and was thrilled that they would be the company to install my upgrade. I saw a big improvement right away and was sure that I had made the right decision.
					</span>
					<span class="interior-testimonial-slider__slide-author">
						Carl Stough
					</span>
				</div>

						
				<div class="interior-testimonial-slider__slide">
					<span class="interior-testimonial-slider__slide-content">
						Brian, the service technician, was most helpful diagnosing the problem quickly, isolating the defective part and replacing it. He was very friendly and polite. He was also dressed neatly. Thank you.
					</span>
					<span class="interior-testimonial-slider__slide-author">
						Greg and Ada Guindon
					</span>
				</div>

				<div class="interior-testimonial-slider__slide">
					<span class="interior-testimonial-slider__slide-content">
						Plan to have the same contractor replace the unit with a Trane, when it wears out. Very satisfied with contractor, Ambient Edge.
					</span>
					<span class="interior-testimonial-slider__slide-author">
						Murray Gibson
					</span>
				</div>

      </div>
    </div>
<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('ambient-testimonials', 'ambientTestimonials');
 