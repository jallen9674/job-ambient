<?php
/**
 * Template Name: Media Page
 */

add_filter( 'body_class', 'hennessey_add_body_class' );
function hennessey_add_body_class( $classes ) {
	$classes[] = 'media-page-template';
	return $classes;
}

//Enqueue Styles for Page
$fileVersion =  filemtime($_SERVER["DOCUMENT_ROOT"] . '/wp-content/themes/ambient-genesis/scss/pages/media.scss');
wp_enqueue_style( 'page-media-styles', get_stylesheet_directory_uri() . '/css/pages/media.css', array(), $fileVersion );

//Remove Default the_content()
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//Adding Media Content
add_action( 'genesis_before_entry_content',  'hennessey_media_content');

// Runs the Genesis loop.
genesis();



//General Content
function hennessey_media_content(){
	?>
	
	<div class="media-top-section content">
		<h2>Company Information</h2>
		<div class="media-top-section__inner">

			<img src="<?php echo get_stylesheet_directory_uri();?>/images/media/community-banner.jpg" alt="Awards"  class="media-top-section__image"/>

			<div class="media-top-section__information">
				
				<ul>
					<li><a href="<?php echo site_url(); ?>/about-us/">About Us</a></li>
					<li><a href="<?php echo site_url(); ?>/careers/">Careers</a></li>
					<li><a href="<?php echo site_url(); ?>/reviews/">Reviews</a></li>
				</ul>
			</div>

		</div>
	</div>

	<div class="media-press-release">
		<div class="media-press-release__inner content">
		<h2>Press Releases</h2>
		<?php 
			$args = array(
			'posts_per_page' => 4,
			'post_type' => 'post',
			'category_name' => 'press-releases',
			'order' => 'DSC',
			'orderby' => 'date',
			);

			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
        ?>
        

            <a href="<?php the_permalink(); ?>" class="media-press-release__single">
				<span class="media-press-release__single-title"><?php the_title(); ?></span>
				<span class="media-press-release__single-date"><?php echo get_the_date('F j, Y'); ?></span>
			</a>



          <?php endwhile; else : ?>
            <!-- IF NOTHING FOUND CONTENT HERE -->
          <?php endif; ?>
          <?php wp_reset_query(); ?>
		
		</div>
	</div>

	<div class="media-social content">
		<h2>Social</h2>
		<div class="media-social__inner">
			
			<a href="https://www.facebook.com/AmbientEdgeAC" target="_blank">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/media/facebook-icon.png" alt="facebook" />
			</a>

			<a href="https://mobile.twitter.com/ambientedge" target="_blank">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/media/twitter-icon.png" alt="facebook" />
			</a>
		
		</div>
	</div>


	<div class="media-community content">
		<h2>Community</h2>
		<div class="media-community__inner">
			
			<div class="media-community__we-care">
				<h3>We Care</h3>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/media/community-we-care.png" alt="we care" />
				<p>
					We love the areas we service and strive to give back to help our communities and the organizations that support to help them grow and thrive.
				</p>
			</div>

			<div class="media-community__sponsorships">
				<h3>Sponsorships</h3>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/media/community-sponsorships.png" alt="sponsorships" />
				<p>
					We sponsor community events, school fundraising efforts, an support our community through our sponsorship programs.
				</p>
			</div>

			<div class="media-community__home-depot">
				<h3>Home Depot</h3>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/media/community-home-depot.png" alt="home depot" />
				<p>
					We are an authorized service provider for The Home Depot in Kingman, Bullhead City, and Lake Havasu.
				</p>
			</div>
		
		</div>
	</div>

	<div class="media-inquiry content">
		<h3>For Media Inquiries or Request for Sponsorship please contact:</h3>
		<p>Chris Stansell, Marketing Coordinator<br><a href="mailto:marketing@ambientedge.com">marketing@ambientedge.com</a></p>
	</div>

	<?php 
}
