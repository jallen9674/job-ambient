<?php

// Adding AMP Specific Content

add_action( 'cmb2_init', 'hc_register_example_meta' );

function hc_register_example_meta() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_hc_';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $hc_amp_content = new_cmb2_box( array(
        'id'            => $prefix . 'example_metabox',
        'title'         => __( 'Example Heading', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );

    $hc_amp_content->add_field( array(
        'name'       => __( 'Example field', 'cmb2' ),
        'desc'       => __( 'There are many different field types. ', 'cmb2' ),
        'id'         => $prefix . 'example_wysiwyg',
        'type'       => 'wysiwyg',
        'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );

}