<?php
/**
 * Template Name: Blog Archive
 */


/*------------------------------
Blog Archive Layout
--------------------------------*/

add_action('genesis_before_entry_content', 'hennessey_blog_archive_list');

 function hennessey_blog_archive_list(){

	$args = array(
		'post_type'              => 'post',
		'posts_per_page'         => -1,
		'order'                  => 'ASC',
		'orderby'                => 'date'
	  );
	
	  // The Query
	  $query = new WP_Query( $args );

	?> 

	<ul> 
		<?php 
			if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
			
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
			<?php endwhile; else : ?>
				<p>Nothing found.</p>
		<?php endif; ?>
	</ul>

	<?php 

 }

// Runs the Genesis loop.
genesis();

