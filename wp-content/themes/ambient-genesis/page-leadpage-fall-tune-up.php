<?php
/**
 * Template Name: Fall Tune Up
 */

add_filter( 'body_class', 'hennessey_body_classes' );
function hennessey_body_classes( $classes ) {
	$classes[] = 'fall-tune-up-template';
	return $classes;
}

//Enqueue Styles for Page
$fileVersion =  filemtime($_SERVER["DOCUMENT_ROOT"] . '/wp-content/themes/ambient-genesis/scss/pages/fall-tune-up.scss');
wp_enqueue_style( 'page-fall-tune-up-styles', get_stylesheet_directory_uri() . '/css/pages/fall-tune-up.css', array(), $fileVersion );

//Remove Default the_content()
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//Full Width Layou
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//Adding R22 Page Content
add_action( 'genesis_before_entry_content',  'hennessey_fall_tune_up');

// Runs the Genesis loop.
genesis();



//General Content
function hennessey_fall_tune_up(){
	?>
    
    <?php
    /*-----------------------------
    BEGIN SECTION 1
    ------------------------------*/
    ?>

    <div class="r22-section-one r22-page-section">
        <div class="r22-page-section__inner r22-section-one__inner">

            <span class="r22-section-one__attention-text">
                ATTENTION!
            </span>

            <p style="text-align: center; font-size: 2.1em; margin-bottom: 5px; line-height: 1.2; text-align: center; max-width: 670px; margin-left: auto; margin-right: auto;">
                <span class="big-59-text">$69</span>
                Invest $69 and Get the Absolute Best Furnace Tune-Up Offered Ever!
            </p>

            <p style="margin: 30px auto; text-align: left; max-width: 675px; font-size: 1.4em; clear: both;">
                Plus, get a FREE warranty on your furnace this winter.* This added value includes a Parts and Labor Warranty on your furnace or heat pump for this entire winter season absolutely FREE if you act now. That is a $299 value!
            </p>

            <a href="#schedule-now-top" class="r22-schedule-button">Get Your $69 Tune-Up Now!</a>

        </div>
    </div> <?php //End .r22-section-one ?>

<?php
/*-----------------------------
BEGIN SECTION 2
------------------------------*/
?>

    <div class="r22-section-two r22-page-section">
        <div class="r22-page-section__inner r22-section-two__inner">

            <div class="r22-section-two__left">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tuneup/heating-icon.png" alt="Heating Icon" class="leadpage-heating-icon">

            </div> <?php //End Left ?>

            <div class="r22-section-two__right">
                <span class="r22-section-two__title">
                    Superior Service and Quality
                </span>

                <h2>We Check...</h2>
                <p style="margin-bottom: 0;">&nbsp;</p>

                <div class="r22-section-two__checkbox-content">
                    <span class="r22-section-two__checkbox-content-title" style="font-size: 18px;">
                            Blower and Burners
                    </span>
                </div>
                <div class="r22-section-two__checkbox-content">
                    <span class="r22-section-two__checkbox-content-title" style="font-size: 18px;">
                        Electrical Components
                    </span>
                </div>
                <div class="r22-section-two__checkbox-content">
                    <span class="r22-section-two__checkbox-content-title" style="font-size: 18px;">
                        Filtration
                    </span>
                </div>
                <div class="r22-section-two__checkbox-content">
                    <span class="r22-section-two__checkbox-content-title" style="font-size: 18px;">
                        Overall Condition
                    </span>
                </div>
                <div class="r22-section-two__checkbox-content">
                    <span class="r22-section-two__checkbox-content-title" style="font-size: 18px;">
                        And More....
                    </span>
                </div>

                <div class="r22-section-two__general-content" style="font-size: 18px;">

                    <h2>Don't Wait. Be sure to get your FREE furnace warranty!</h2>

                    <p>
                    Schedule your system renewal tune-up soon to save and take advantage of our free furnace warranty.*
                    </p>
                    <p><i>
                        * Parts & Labor Furnace Warranty is for the entire winter season, expiring <strong>March 20th, 2019</strong>. Unit must pass full tune-up inspection and complete any repairs needed to receive furnace warranty certificate. See dealer for complete details.
                    </i></p>
                </div>



            </div> <?php //End Right ?>

        </div>
    </div> <?php //End .r22-section-two ?>


    <?php
    /*-----------------------------
    BEGIN SECTION 3
    ------------------------------*/
    ?>

        <div class="r22-section-three r22-page-section">
            <div class="r22-page-section__inner r22-section-three__inner">

            <span class="r22-section-three__title">
                <span style="color: #ec2024;">100%</span> Satisfaction Guaranteed!
            </span>

            <p style="text-align: center; margin-bottom: 20px;">
                SATISFACTION MATTERS | If we fail to satisfy you with our quality service or do the job poorly we know you won’t want to use us again and that’s just not the way Ambient Edge does business. That is why we offer a 100% Satisfaction Guarantee that is no risk to you!
            </p>


            <div class="r22-sets-apart-box">

                <div class="r22-sets-apart-box__title">
                    What Sets Us Apart
                </div>

                <p>
                    Any heating and cooling company can promise great results, but what sets use apart is consistency. At Ambient Edge, we offer superior quality and service you can count on.
                </p>

                <div class="r22-sets-apart-box__icons">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/r22/available-24-7.png" alt="Available 24/7">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/r22/satisfaction-guarantee.png" alt="Satisfaction Guarantee">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/r22/flat-rate-pricing.png" alt="Flat Rate Pricing">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/r22/free-estimates.png" alt="Free Estimates">
                </div>

            </div>

            <div class="r22-section-three__call-us-box" id="schedule-now-top">
                Call Us Now! <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/r22/phone-icon.png" alt="Phone Icon"> 888-628-5890
            </div>


            </div>
        </div> <?php //End .r22-page-section-three ?>

    <?php
    /*-----------------------------
    BEGIN SECTION 4 - Contact Form
    ------------------------------*/
    ?>

        <div class="r22-section-four r22-page-section" id="schedule-now">
            <div class="r22-page-section__inner r22-section-four__inner">

                <div class="r22-section-four__left">
                    <div style="max-width: 450px; margin: 0 auto;">
                        <span class="r22-section-four__title">
                            Request a Call
                        </span>
                        <p style="font-size: 1.1em; margin-bottom: 20px; text-align: center;">
                            Call our PRIORITY SCHEDULING LINE at 888-628-5890. This offer expires on soon. We must be able to perform your tune-up early in the season. That’s the only way we can offer our tune-up at these prices and with this FREE Warranty!
                        </p>
                        <p style="text-align: center; font-size: 2em; margin-bottom: 20px;">
                            CALL NOW!<br>888-628-5890
                        </p>
                    </div>
                </div>
                
                <div class="r22-section-four__right">
                    <div class="r22-section-four__form">

                    <span class="r22-section-four__title">Book Online</span>
                    <p style="font-size: 1.1em; margin-bottom: 20px; text-align: center;">
                        Schedule your service online.<br>Be sure to ask for our Fall Tune-Up Special.
                    </p>
                    <ul class="leadpage-blue-bg-form">
                        <li>Pick Your Date</li>
                        <li>Select Your Time Window</li>
                        <li>Fill out our form.</li>
                        <li>Tell us you want the Fall Tune- Up Special</li>
                    </ul>

                    <span class="r22-section-four__title">Its That Easy.</span>

                    <a href="<?php echo site_url();?>/schedule-service/" class="r22-schedule-button">Schedule Now</a>




                    <?php /* Form removed at request of client
                        <?php //echo do_shortcode('[formidable id=6 title=false description=false]'); ?>
                        <?php echo do_shortcode('[gravityform id="14" title="false" description="false" ajax="true"]'); ?>
                    */ ?>

                    </div>
                </div> 

            </div>
        </div> <?php //End .r22-section-four ?>

    <?php
    /*-----------------------------
    BEGIN SECTION 5 - Testimonials
    ------------------------------*/
    ?>

        <div class="r22-section-five r22-page-section">
            <div class="r22-page-section__inner r22-section-five__inner">

                <span class="r22-section-five__title">
                    Testimonials
                </span>

                <div class="r22-single-testimonial">
                    <span class="r22-single-testimonial__title">
                        Better Business, <span style="color: #e00943">Review</span>
                    </span>
                    <div class="r22-single-testimonial__content">
                        "The installers are knowledgeable and fast. They are a great bunch of people at Ambient Edge!"
                    </div>
                </div>

                <div class="r22-single-testimonial">
                    <span class="r22-single-testimonial__title">
                        Facebook, <span style="color: #e00943">Review</span>
                    </span>
                    <div class="r22-single-testimonial__content">
                        "If you have any problems with your heating or cooling - call Ambient edge. They are great to work with, they answer all your questions and give you excellent advice and come when you call."
                    </div>
                </div>

                <a href="https://www.ambientedge.com/reviews/" target="_blank" class="r22-section-five__read-more">Read More Reviews &raquo;</a>

            </div>
        </div> <?php //End .r22-section-five ?>

    <?php
    /*-----------------------------
    BEGIN SECTION 6 - President
    ------------------------------*/
    ?>

        <div class="r22-section-six r22-page-section">
            <div class="r22-page-section__inner">

                <div class="r22-section-six__inner">
                    <div class="r22-section-six__left">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/r22/ambient-edge-president.jpg" alt="Ambient Edge President Steve Lewis">
                    </div>

                    <div class="r22-section-six__right">
                        <span class="r22-section-six__title">
                            President: <span style="color: #e00943">Steven Lewis</span>
                        </span>
                        <p>
                            "Hello! I know that the comfort of your home rests in the quality and efficiency of your home heating and cooling system. That is why at Ambient Edge, you will get quality service from industry trained technicians, I guarantee it."
                        </p>
                    </div>
                </div>

                <a href="<?php echo site_url();?>/schedule-service/" class="r22-schedule-button">Schedule Now</a>

            </div>
        </div> <?php //End .r22-section-six ?>
	

	<?php 
}
