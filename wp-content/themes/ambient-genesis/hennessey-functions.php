<?php 
/*-------------------------------------
Hennessey Consulting Specific Functions
--------------------------------------*/

/*---------------------------------
Cleaning Up WP Head
----------------------------------*/
function removeHeadLinks() {
    remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
    remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
    remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
    remove_action( 'wp_head', 'index_rel_link' ); // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
    remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
  }
  add_action('init', 'removeHeadLinks');
  remove_action('wp_head', 'wp_generator');
  
if (  !is_user_logged_in() ){
  wp_deregister_style( 'dashicons' ); 
}


/*---------------------------------
Adding Google Fonts
----------------------------------*/

add_action('wp_head', 'hennessey_google_fonts');
function hennessey_google_fonts(){
  ?>
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,700,900&display=swap" rel="stylesheet">
  <?php 
}

/*---------------------------------
Enqueue of Optimized Styles
----------------------------------*/



function hennessey_load_styles(){
    $scssVersion =  filemtime($_SERVER["DOCUMENT_ROOT"] . '/wp-content/themes/ambient-genesis/scss/scss-styles.scss');
    wp_enqueue_style( 'hennessey-css', get_stylesheet_directory_uri() .'/css/scss-styles.css?v=' . $scssVersion);
    wp_enqueue_style( 'hennessey-standalone-css', get_stylesheet_directory_uri() .'/css/standalone-styles.css' );
}
add_action( 'wp_enqueue_scripts', 'hennessey_load_styles', 10000);


//Admin CSS
function hennessey_wp_admin_style() {
  wp_register_style( 'hennessey-admin-css', get_stylesheet_directory_uri() . '/css/admin.css', false, '1.0.0' );
  wp_enqueue_style( 'hennessey-admin-css' );
}

add_action( 'admin_enqueue_scripts', 'hennessey_wp_admin_style' );

/*---------------------------------
Enqueue of Optimized Scripts
----------------------------------*/

function hennessey_load_scripts(){
    wp_enqueue_script('hennessey-vendors-js', get_stylesheet_directory_uri() . '/js/vendors.js', array('jquery'), '1.0', true);  
    wp_enqueue_script('hennessey-custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), '1.0', true);
}
add_action( 'wp_enqueue_scripts', 'hennessey_load_scripts', 40);




/*--------------------------------------------
Favicons
---------------------------------------------*/

//Remove Genesis Favicon
remove_action('wp_head', 'genesis_load_favicon');

//Add Hennessey Favicon
function hennessey_add_favicon() {
    ?>
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/favicon-16x16.png">
    <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <?php
  }
  
  add_action('wp_head', 'hennessey_add_favicon');
  


/*---------------------------------
Bringing in Template Parts
----------------------------------*/

require_once('template-parts/footer.php');
require_once('template-parts/header.php');
require_once('template-parts/header-title-section.php');
require_once('template-parts/mobile-navigation-pane.php');


/*---------------------------------
Using Template Parts the Genesis Way
----------------------------------*/

//Removing Edit Link
add_filter ( 'genesis_edit_post_link' , '__return_false' );

//Adding Mobile Navigation Pane
add_action( 'genesis_after', 'hennessey_mobile_nav_pane' );
add_action( 'genesis_before', 'hennessey_mobile_nav_header' );

//Remove Genesis Header Content (Logo and Navigation)
remove_action( 'genesis_header', 'genesis_do_header' );
remove_action( 'genesis_header', 'genesis_do_nav' );

//Add Hennessey Header
add_action( 'genesis_header', 'hennessey_global_header' );

//Remove Genesis Footer
remove_action( 'genesis_footer', 'genesis_footer_markup_open', 5 );
remove_action( 'genesis_footer', 'genesis_do_footer' );
remove_action( 'genesis_footer', 'genesis_footer_markup_close', 15 );

//Add Hennessey Footer
add_action( 'genesis_footer', 'hennessey_global_footer' );


//Removing Interior Page Heading
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

//Adding Hennessey H1 & Breadcrumb Section
add_action('genesis_before_content', 'hennessey_single_interior_header');

/*---------------------------------
Post Types and Taxonomies
----------------------------------*/

require_once('post-types/faqs.php');
require_once('post-types/glossary.php');


/*---------------------------------
Adding Widgets
----------------------------------*/

require_once('widgets/related-pages/related-module.php');


/*---------------------------------
Modules
----------------------------------*/

//JSON-LD Module
require_once('modules/jsonld/init.php');



/*---------------------------------
Adding Metaboxes
----------------------------------*/

require_once('metaboxes/header-phone-number.php');

/*---------------------------------
Adding Shortcodes
----------------------------------*/

require_once('shortcodes/sidebar-form.php');
require_once('shortcodes/random-faq-text.php');
require_once('shortcodes/random-faq.php');
require_once('shortcodes/scholarship-banner.php');
require_once('shortcodes/home-advisor-reviews.php');
require_once('shortcodes/homepage-accolade-slider.php');
require_once('shortcodes/featured-on-section.php');
require_once('shortcodes/city-shortcode.php');
require_once('shortcodes/maintenance-flyer.php');
require_once('shortcodes/searchbox.php');
require_once('shortcodes/random-blog-feed.php');
require_once('shortcodes/testimonials.php');

//Allow Sidebar Shortcodes
add_filter( 'widget_text', 'do_shortcode' );


/*---------------------------------
Image Sizes
----------------------------------*/

//Image Sizes
add_image_size( 'faq-thumb', 300, 200, true );
add_image_size( 'faq-thumb-sidebar', 400, 250, true );
add_image_size( 'blog-featured', 300, 200, true );
add_image_size( 'blog-thumb-large', 920, 400, true );
add_image_size( 'page-hero', 1400, 350, true );

/*---------------------------------
Add Read More Link to Post Excerpts
----------------------------------*/

add_filter('excerpt_more', 'get_read_more_link');
add_filter( 'the_content_more_link', 'get_read_more_link' );
function get_read_more_link() {
   return '... <a href="' . get_permalink() . '">Read More</a>';
}


/*---------------------------------
Adding CMB2
----------------------------------*/

require_once dirname( __FILE__ ) . '/lib/cmb2/init.php';

function update_cmb_meta_box_url( $url ) {
    $url = '/wp-content/themes/ambient-genesis/lib/cmb2/';
    return $url;
}
add_filter( 'cmb2_meta_box_url', 'update_cmb_meta_box_url' );



/*---------------------------------
Setting Template Phone Number
----------------------------------*/

define("PHONE_NUMBER", "888-628-5890");

function hennessey_phone_display(){
  global $post;

  //Get Header And Phone Number Information
  if ( get_post_meta( $post->ID, '_ae_primary_phone_number', true ) ) {
    $phoneNumber = get_post_meta( $post->ID, '_ae_primary_phone_number', true );
  } else {
    $phoneNumber = PHONE_NUMBER;
  }

  return $phoneNumber;
}


/*--------------------------------------------
Adding Userway
---------------------------------------------*/

//add_action('wp_footer', 'hennessey_add_userway');

function hennessey_add_userway(){
  ?>
  <script type="text/javascript">
  var _userway_config = {
   account: 'PgJBLRHmZy'
  };
  </script>
  <script type="text/javascript" src="https://cdn.userway.org/widget.js"></script>
  <?php 
}


/*--------------------------------------------
Adding Google Analytics
---------------------------------------------*/

add_action('wp_head', 'hennessey_add_analytics_head');

function hennessey_add_analytics_head(){
  ?>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-K2BT5SF');</script>
  <!-- End Google Tag Manager -->  
  <?php 
}


add_action('genesis_before_header', 'hennessey_add_analytics_body');
function hennessey_add_analytics_body(){
  ?>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K2BT5SF"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <?php 
}


/*--------------------------------------------
Adding Facebook Pixel 
---------------------------------------------*/

add_action('wp_head', 'hennessey_add_fb');

function hennessey_add_fb(){
  ?>
  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '403160610043658');
    fbq('track', 'PageView');
  </script>
  <noscript>
    <img height="1" width="1" style="display:none" 
        src="https://www.facebook.com/tr?id=403160610043658&ev=PageView&noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->
  <?php
}


/*--------------------------------------------
Adding Bing PPC Snippet
---------------------------------------------*/

function hennessey_add_bing_ppc(){
  ?>
  
    <script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"25024193"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
  
  <?php 
  }
  
  add_action('wp_footer', 'hennessey_add_bing_ppc');

/*--------------------------------------------
Adding Call Tracking Metrics Snippet
-- Note: CTM Added via Plugin
---------------------------------------------*/

//add_action('wp_footer', 'hennessey_add_ctm');

function hennessey_add_ctm() {
  ?>
  <script async src="//#####.tctm.co/t.js"></script>
  <?php
}


/*--------------------------------------------
Adding Apex Chat
---------------------------------------------*/

function hennessey_add_apex() {

  if ( (!($_SERVER['SERVER_NAME']  == 'ambientgenesis.local')) && (!($_SERVER['SERVER_NAME']  == 'ambientgenesis.wpengine.com')) ) {
    
  ?>

  <script src="//www.apex.live/scripts/invitation.ashx?company=ambientedge" async></script>

  <?php
  }
}

add_action('wp_footer', 'hennessey_add_apex');

/*---------------------------------
Add Tag to Meta Description & Category
----------------------------------*/

add_filter( 'wpseo_metadesc', 'hennessey_description_tag', 100, 1 );

function hennessey_description_tag($s){
  if( is_tag() ){
     $s .= single_tag_title(" Posts Tagged as: ", false);
  }
  if( is_category() ){
     $s .= single_cat_title(" Posts Categorized as: ", false);
  }
  return $s;
 }
 
 
 /*------------------------------------------
 Add page number to meta description
 -------------------------------------------*/

 function hc_add_page_number( $s ) {
    global $page;
    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
    ! empty ( $page ) && 1 < $page && $paged = $page;

    $paged > 1 && $s .= ' | ' . sprintf( __( 'Page: %s' ), $paged );

    return $s;
}

add_filter( 'wpseo_title', 'hc_add_page_number', 100, 1 );
add_filter( 'wpseo_metadesc', 'hc_add_page_number', 100, 1 );


 
/*------------------------------------------
Adding Las Vegas Schema
-------------------------------------------*/


function hennessey_add_location_lasvegas_schema(){
  if (is_single(5526) ){
  ?>

   <script type='application/ld+json'>
{
    "@context": "http://www.schema.org",
    "@type": "LocalBusiness",
    "name": "Air Conditioning Repair Company In Las Vegas, NV",
    "url": "https://www.ambientedge.com/las-vegas/air-conditioning-repair-company/",
    "sameAs": [
      "https://facebook.com/AmbientEdgeAC",
      "https://www.linkedin.com/company/ambient-edge",
      "https://www.youtube.com/user/AmbientEdge",
      "https://twitter.com/ambientedge"
    ],
    "logo": "https://www.ambientedge.com/wp-content/uploads/2017/03/ambient-edge.jpg",
    "image": "https://www.ambientedge.com/wp-content/uploads/2018/07/ambient-edge-logo.png",
    "address": {
          "@type": "PostalAddress",
          "streetAddress": "5940 S Rainbow Blvd #213",
          "addressLocality": "Las Vegas",
          "addressRegion": "NV",
          "postalCode": "89118",
          "addressCountry": "United States"
        },
    "openingHoursSpecification" : {
      "@type" : "OpeningHoursSpecification",
      "dayOfWeek" : [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ],
      "opens" : "00:00:00",
      "closes" : "23:59:59"
    },
    "telephone": " 702-928-7201",
    "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "5",
        "ratingCount": "7"
      },
      "geo":{
        "@type":"GeoCoordinates",
        "latitude":"36.0811365",
        "longitude":"-115.24130689999998"
    },
    "review": [{
      "@type": "Review",
      "author": "Vernon J.",
      "datePublished": "2018-08-02",
      "reviewBody": "Chris F. Was were professional and efficient. He was knowledgeable and very fast. Showed up early and got the A/C working quicker than I expected. I would rate his performance and the company at a 5 🌟 Thank you for doing a great job.",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }, {
      "@type": "Review",
      "author": "Daniel W.",
      "datePublished": "2018-05-31",
      "reviewBody": "Brien A. Was awesome! Very professional and explained what was going on in terms that I could understand. I didn't feel like I was dealing with someone who was just trying to sell me something. He was polite and it was overall a great experience, I would definitely recommend these guys!",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }, {
      "@type": "Review",
      "author": "Sally G.",
      "datePublished": "2017-06-15",
      "reviewBody": "Shawn was extremely kind and helpful and efficient at helping us with our air conditioning issues. I highly recommend Ambient Edge as we experienced excellent service!!!!!",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }, {
      "@type": "Review",
      "author": "Marvene H.",
      "datePublished": "2017-03-29",
      "reviewBody": "Professional, on time and excellent job!",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }, {
      "@type": "Review",
      "author": "Micah G.",
      "datePublished": "2018-08-13",
      "reviewBody": "Ambient Edge did a great job on our A/C work. Their staff has great customer service and made sure to check back with us to see if everything was working well.",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }, {
      "@type": "Review",
      "author": "David F.",
      "datePublished": "2018-08-02",
      "reviewBody": "Had problems with ac got me an appointment asap. Then got estimate some day. Great help.",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }]
    }
    }
  </script>

  <?php
  }
}

add_action('wp_head', 'hennessey_add_location_lasvegas_schema');

/*--------------------------------------------
Adding Footer Phone Icon
---------------------------------------------*/

function hennessey_add_phone_icon() {
  global $post;
  //Get Header And Phone Number Information
  if ( get_post_meta( $post->ID, '_ae_primary_phone_number', true ) ) {
    $phoneNumberFooter = get_post_meta( $post->ID, '_ae_primary_phone_number', true );
  } else {
    $phoneNumberFooter = hennessey_phone_display();
  }
  ?>
  <a onclick="ga('send', 'event', 'Mobile Phone Link', 'Mobile Phone Click', 'Mobile Phone Click');" href="tel:<?php echo $phoneNumberFooter; ?>" class="footer-phone-circle mobile-phone-analytics-track"><i class="fa fa-phone"></i></a>
  <?php
}

add_action('wp_footer', 'hennessey_add_phone_icon');

/*--------------------------------------------
Hennessey Add Text Us Box
-- Currently Disabled, Chat provided via Apex
---------------------------------------------*/

//add_action('wp_footer','hennessey_text_overlay');

function hennessey_text_overlay() {
  ?>

  <div class="hc-text-box">
    <div class="hc-text-box__inner">
        <a href="sms:855-258-9236" class="hc-text-box__text-link" onclick="ga('send', 'event', 'Mobile Text Click', 'Mobile Text Click', 'Mobile Text Click');">TEXT US</a>
    </div>
  </div>

  <?php 
}

/*---------------------------------
Adding Footer Popup to Plumbing by Jake
----------------------------------*/

function hennessey_footer_popup_ambient(){
  ?>
    <a href="https://www.plumbingbyjake.com/" class="ambient-footer-popup">
        <div class="ambient-footer-popup__inner">
          <span class="ambient-footer-popup__close-button"></span>
          <span class="ambient-footer-popup__title">Looking for Plumbing Services?</span>          
          <img  width="115" height="101" class="ambient-footer-popup__image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/plumbing-by-jake.png" alt="Visit Plumbing By Jake!">
        </div>
    </a>
  <?php
}

add_action('wp_footer', 'hennessey_footer_popup_ambient');



/*---------------------------------
Adding Related FAQs After Content on Pages & Posts
----------------------------------*/

add_action('genesis_after_entry_content', 'hennessey_add_random_faqs');

function hennessey_add_random_faqs(){

  global $post;

  if ( is_page() || is_singular('post') ) {
    if (!is_front_page() && !is_page(['4558', '4513'])) {
    echo do_shortcode('[dm-random-faq-loop-text amount=5]');
    }
  }

}


/*---------------------------------
Adding Alt Tags to Images
- From  https://sridharkatakam.com/how-to-truncate-alt-text-for-featured-images-on-content-archives-in-genesis/
----------------------------------*/


add_action( 'genesis_entry_header', 'hennessey_post_image' );

function hennessey_post_image() {

	// if we are on a content archive page and featured images are set to be shown on content archives in Genesis theme settings
	if ( ! is_singular() && genesis_get_option( 'content_archive_thumbnail' ) ) {

		// remove the standard featured image output by Genesis per theme settings
		remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );

		// add custom featured image output
		add_action( 'genesis_entry_content', 'hennessey_do_post_image', 8 );

	}

}

// Function to set the alt text of featured image to truncated post title text
function hennessey_do_post_image() {

  global $post;

	$img = genesis_get_image( array(
		'format'  => 'html',
		'size'    => genesis_get_option( 'image_size' ),
		'context' => 'archive',
		'attr'    => genesis_parse_attr( 'entry-image', array ( 'alt' => get_the_title() ) ),
	) );

	if ( ! empty( $img ) ) {
		printf( '<a href="%s" aria-hidden="true">%s</a>', get_permalink(), $img );
	}

}