<?php
/*
Glossary Post Type
*/


// let's create the function for the custom type
function hc_glossary_post_type() {
    // creating (registering) the custom type
    register_post_type( 'hc_glossary', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
        // let's now add all the options for this post type
        array( 'labels' => array(
            'name' => __( 'Glossary', 'bonestheme' ), /* This is the Title of the Group */
            'singular_name' => __( 'Term', 'bonestheme' ), /* This is the individual type */
            'all_items' => __( 'All Glossary Terms', 'bonestheme' ), /* the all items menu item */
            'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
            'add_new_item' => __( 'Add New Term', 'bonestheme' ), /* Add New Display Title */
            'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
            'edit_item' => __( 'Edit Term', 'bonestheme' ), /* Edit Display Title */
            'new_item' => __( 'New Term', 'bonestheme' ), /* New Display Title */
            'view_item' => __( 'View Term', 'bonestheme' ), /* View Display Title */
            'search_items' => __( 'Search Terms', 'bonestheme' ), /* Search Custom Type Title */
            'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */
            'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
            ), /* end of arrays */
            'description' => __( 'These are the items that appear on the resources page', 'bonestheme' ), /* Custom Type Description */
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => true,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon' => 'dashicons-lightbulb',  /* get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png'*/
            'rewrite'   => array( 'slug' => 'glossary', 'with_front' => false ), /* you can specify its url slug */
            'has_archive' => true, /* you can rename the slug here */
            'capability_type' => 'post',
            'hierarchical' => false,
            /* the next one is important, it tells what's enabled in the post editor */
            'supports' => array( 'title', 'editor', 'revisions')
        ) /* end of options */
    ); /* end of register post type */

}

    // adding the function to the Wordpress init
    add_action( 'init', 'hc_glossary_post_type');

    // Adding City Taxonomy
    register_taxonomy( 'hc_glossary_letter',
        array('hc_glossary'),
        array('hierarchical' => true,     /* if this is true, it acts like categories */
            'labels' => array(
                'name' => __( 'First Letter', 'bonestheme' ), /* name of the custom taxonomy */
                'singular_name' => __( 'First Letterty', 'bonestheme' ), /* single taxonomy name */
                'search_items' =>  __( 'Search Letters', 'bonestheme' ), /* search title for taxomony */
                'all_items' => __( 'All Letters', 'bonestheme' ), /* all title for taxonomies */
                'parent_item' => __( 'Parent', 'bonestheme' ), /* parent title for taxonomy */
                'parent_item_colon' => __( 'Parent:', 'bonestheme' ), /* parent taxonomy title */
                'edit_item' => __( 'Edit First Letter', 'bonestheme' ), /* edit custom taxonomy title */
                'update_item' => __( 'Update First Letter', 'bonestheme' ), /* update title for taxonomy */
                'add_new_item' => __( 'Add New First Letter', 'bonestheme' ), /* add new title for taxonomy */
                'new_item_name' => __( 'New First Letter', 'bonestheme' ) /* name title for taxonomy */
            ),
            'show_admin_column' => true,
            'show_ui' => true,
            'query_var' => true,
            'public' => false,
            'rewrite' => array( 'slug' => 'glossary-section', 'with_front' => false ),
        )
    );


add_action( 'cmb2_init', 'bg_glossary_meta' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function bg_glossary_meta() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_bg_';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $citylist_metaboxes = new_cmb2_box( array(
        'id'            => $prefix . 'glossary_metaboxes',
        'title'         => __( 'Additional Glossary Information', 'cmb2' ),
        'object_types'  => array( 'hc_glossary' ), // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );

    $citylist_metaboxes->add_field( array(
        'name'       => __( 'Alternate Listing Title (optional)', 'cmb2' ),
        'desc'       => __( 'A shorter version of the title to display on the general glossary page.', 'cmb2' ),
        'id'         => $prefix . 'glossary_alt_title',
        'type'       => 'text_medium',
        'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
}
