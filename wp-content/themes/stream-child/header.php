<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PZKHTJC');</script>
<!-- End Google Tag Manager -->


<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />

<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'THEME_FRONT_SITE' ), max( $paged, $page ) );

?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo THEME_URI; ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<!--[if IE 7]>
  <link rel="stylesheet" href="<?php echo THEME_URI; ?>/css/fortawesome/font-awesome-ie7.css">
<![endif]-->
<?php if(get_option('atp_custom_favicon')) { ?>
	<link rel="shortcut icon" href="<?php echo get_option('atp_custom_favicon'); ?>" type="image/x-icon" /> 
<?php } ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92188150-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;

n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,

document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '403160610043658'); // Insert your pixel ID here.

fbq('track', 'PageView');

</script>

<noscript><img height="1" width="1" style="display:none"

src="https://www.facebook.com/tr?id=403160610043658&ev=PageView&noscript=1"

/></noscript>

<!-- DO NOT MODIFY -->

<!-- End Facebook Pixel Code -->
</head>

<body <?php body_class('gradient'); ?>>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PZKHTJC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


	<?php if ( get_post_meta($post->ID, 'page_bg_image', true) ) : ?>
	<img id="pagebg" src="<?php echo get_post_meta($post->ID, "page_bg_image", true); ?>" />
	<?php endif; ?>

	<div id="<?php echo get_option('atp_layoutoption');?>" class="<?php echo atp_generator('sidebaroption',$post->ID);?>"><div class="bodyoverlay"></div>
	<?php if(get_option('atp_stickybar') != "on" &&  get_option('atp_stickycontent') !='') { ?>
	<div id="sticky">
	<?php echo  stripslashes(get_option('atp_stickycontent')); ?>
	</div>
	<div id="trigger" class="tarrow"></div>
	<!-- #sticky -->
	<?php }?>
	<div id="wrapper">

		<div class="topbar">
        <div class="inner">
        <div class="proudly-serving">Proudly Serving Kingman, AZ, & the Surrounding Areas, and Las Vegas, NV, & the Surrounding Areas</div>
        <div class="social"><ul class="social">
        <li><a href="https://plus.google.com/+AmbientEdgeKingman" target="_blank" class="google"></a></li>
        <li><a href="https://www.twitter.com/ambientedge" target="_blank" class="twitter"></a></li>
        <li><a href="https://facebook.com/AmbientEdgeAC" target="_blank" class="facebook"></a></li>
        <li><a href="/feed" target="_blank" class="rss"></a></li>
        </ul>
        </div>
        </div>
        </div>
		<!-- /- .topbar -->
		<div id="header">
			<div class="inner">
				<div class="logo">
				<?php atp_generator('logo'); ?>
                <a href="/services/plumbing" title="Ambient Edge: Plumbing by Jake"><img src="http://userinclude.com/zAmbEdgNew/wp-content/themes/stream-child/images/logo-plumbingbyjake-ambientedge.png" alt="Ambient Edge: Plumbing by Jake" class="logo-jake"></a>
			  </div>
				<!-- /logo -->

				<div class="header_right">
				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('headerright') ) : endif;  ?>
				</div>
				<!-- /headright-->
			</div>
			<!-- /inner-->
		</div>
		<!-- /header -->
		<?php if ( get_option('atp_menu_position') != "on" ) { ?>
		<div class="primarymenu"><nav><?php atp_generator('atp_primary_menu'); ?></nav></div>
		<?php } ?>
		<?php 
		$pageslider = get_post_meta($post->ID,'page_slider', true);
		if(is_front_page() || $pageslider != "" ) {
			atp_generator('teaser_option');
		}else{
			echo atp_generator('subheader',$post->ID);
		}?>
		<!-- /- .frontpage_teaser -->
		
		<div id="layout_wrapper">
			<?php if ( get_option('atp_menu_position') == "on" ) { ?><div class="primarymenu"><nav><?php get_option('atp_menu_position') ? atp_generator('atp_primary_menu') : ''; ?></nav></div>	<?php } ?>
			<div class="clear"></div>
		<?php		
		if(is_front_page() || $pageslider != "" ) {
		
			// Get Slider based on the option selected in theme options panel
			if(get_option("atp_slidervisble") != "on") {
				if($pageslider == "") {
					$chooseslider=get_option('atp_slider');
				} else {
					$chooseslider = $pageslider;
				}
				switch ($chooseslider):
					case 'static_image':
										get_template_part( 'slider/static', 'slider' );   	
										break;
					case 'flexslider':
										get_template_part( 'slider/flex', 'slider' );
										break;
					case 'eislider':
										get_template_part('slider/ei','slider'); 	
										break;
					case 'nivoslider':
										get_template_part('slider/nivo','slider');
										break;
					case 'videoslider':
										get_template_part( 'slider/video', 'slider' );   	
										break;
					case 'planbox':
										get_template_part( 'slider/planbox', 'slider' );   	
										break;
					case 'custom_slider':
										get_template_part( 'slider/custom', 'slider' );   	
										break;
					default:
										get_template_part( 'slider/default', 'slider' );
				endswitch;
			}
			wp_reset_query();
		} 
?>