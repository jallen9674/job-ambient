	</div>
	<div class="clear"></div>
	<!-- /- #layout_wrapper -->
	
	<footer id="footer">
		<?php if( get_option( 'atp_footer_sidebar' ) != 'on' ) { ?>
		<div class="inner">
			<?php get_template_part( 'sidebar', 'footer' ); ?>
		</div>
		<?php } ?>
	</footer>
	<!-- /footer -->
	

	<div class="copyright">
		<div class="inner">
			<div class="copyright_left">
				<?php echo stripslashes(do_shortcode(get_option('atp_copy_left')));?>
			</div>
			<div class="copyright_right">
				<?php //echo stripslashes(do_shortcode(get_option('atp_copy_right'))); ?> AZ ROC# 198597, 296317 | NV LIC# 0071575
			</div>
		</div>
		<!-- /inner -->
	</div>
	<!-- /copyright -->

</div>
<!-- /wrapper -->

</div>
<!-- /layout -->
<?php 
	$googleanalytics = get_option( 'atp_googleanalytics' );
	if( $googleanalytics ){
		echo stripslashes($googleanalytics); 
	}
?>

<?php wp_footer();?>
<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '1e3cecee-de98-4777-8b13-f0f976a84a4e', f: true }); done = true; } }; })();</script>
</body>
</html>