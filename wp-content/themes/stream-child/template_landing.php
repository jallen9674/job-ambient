<?php
/*
Template Name: Landing Page
*/

/** 
 * The Header for our theme.
 * Includes the header.php template file. 
 */
 ?>

 <?
get_header(); ?>
<link href='//fonts.googleapis.com/css?family=La Belle Aurore' rel='stylesheet'>
<link href='//fonts.googleapis.com/css?family=Rock Salt' rel='stylesheet'>
<link href='//fonts.googleapis.com/css?family=Arial' rel='stylesheet'>
<style>
.primarymenu, #footer, #header, .topbar {
    clear: both;
    display: none;
}
.fifty {
    float: left;
    text-align: center;
    width: 50%;
}
h2 {
    font-family: arial;
    font-size: 31px !important;
    line-height: 45px;
    padding-left: 25px;
} 
h1 {
    font-family: arial;
    font-size: 50px;
    line-height: 50px;
    padding: 20px;
    text-align: center;
}
body {
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+0,dce9f4+51,0075be+100 */

background: rgb(255,255,255); /* Old browsers */

background: -moz-linear-gradient(-45deg,  rgba(255,255,255,1) 0%, rgba(220,233,244,1) 51%, rgba(0,117,190,1) 100%); /* FF3.6-15 */

background: -webkit-linear-gradient(-45deg,  rgba(255,255,255,1) 0%,rgba(220,233,244,1) 51%,rgba(0,117,190,1) 100%); /* Chrome10-25,Safari5.1-6 */

background: linear-gradient(135deg,  rgba(255,255,255,1) 0%,rgba(220,233,244,1) 51%,rgba(0,117,190,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */

filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#0075be',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */


}
h3 {
    color: red;
    font-family: 'Rock Salt' !important;
    font-size: 30px; 
    padding: 20px;
}
p {
    padding: 0 50px;
    text-align: justify;
}
.topba {
    background-color: #3a53a4;
    display: block;
    height: auto;
    margin: 0;
    min-height: 34px;
    padding: 0;
}
.topimg  {
    background: #fff none repeat scroll 0 0;
	text-align:center;
}
.testimonials {
    background: #fff;
    height: 85px;
    margin: 20px;
    overflow: hidden;
}
.red {
    color: red;
    font-size: 60px;
    font-weight: bold;
    line-height: 60px;
}
@media screen and (max-width: 880px) {
    .fifty {
		width: 100%;
	}
	.red {
    font-size: 45px;
	}
	
}

</style>

<div class="topba" style="background-color: #3A53A4 !important;">
<div class="inner">
<div class="proudly-serving">Call us for all your Heating, Air Conditioning, & Plumbing Needs!</div>
<div class="social">
<ul class="social">
<li>
<a class="google" href="https://plus.google.com/+AmbientEdgeKingman" target="_blank"></a>
</li>
<li>
<a class="twitter" href="https://www.twitter.com/ambientedge" target="_blank"></a>
</li>
<li>
<a class="facebook" href="https://facebook.com/AmbientEdgeAC" target="_blank"></a>
</li>
<li>
<a class="rss" href="/feed" target="_blank"></a>
</li>
</ul>
</div>
</div>
</div>
	
	<div id="primary" class="pagemid">
	

		<div class="topimg">
        
        
        <? if($_GET['l'] == "Las Vegas") {?>
        
        <a href="http://userinclude.com/zAmbEdgNew/local/las-vegas-nv"><img src="http://userinclude.com/zAmbEdgNew/wp-content/uploads/2017/03/Screen-Shot-2017-03-01-at-12.32.56-PM.png" style="margin: -1px auto;
    max-width: 980px; width:100%;" /></a>
    
    	<? } ?>
        
        <? if($_GET['l'] == "Henderson") {?>
        
        <a href="http://userinclude.com/zAmbEdgNew/local/henderson-nv"><img src="http://userinclude.com/zAmbEdgNew/wp-content/uploads/2017/03/Screen-Shot-2017-03-01-at-12.32.56-PM.png" style="margin: -1px auto;
    max-width: 980px; width:100%;" /></a>
    
    	<? } ?>
        
        <? if($_GET['l'] == "Laughlin") {?>
        
        <a href="http://userinclude.com/zAmbEdgNew/local/laughlin-nv"><img src="http://userinclude.com/zAmbEdgNew/wp-content/uploads/2017/03/Screen-Shot-2017-03-01-at-12.32.56-PM.png" style="margin: -1px auto;
    max-width: 980px; width:100%;" /></a>
    
    	<? } ?>
        
        <? if($_GET['l'] == "Kingman") {?>
        
        <a href="http://userinclude.com/zAmbEdgNew/local/kingman-az"><img src="http://userinclude.com/zAmbEdgNew/wp-content/uploads/2017/03/Screen-Shot-2017-03-01-at-12.32.56-PM.png" style="margin: -1px auto;
    max-width: 980px; width:100%;" /></a>
    
    	<? } ?>
        
        <? if($_GET['l'] == "Bullhead City") {?>
        
        <a href="http://userinclude.com/zAmbEdgNew/local/bullhead-city-az"><img src="http://userinclude.com/zAmbEdgNew/wp-content/uploads/2017/03/Screen-Shot-2017-03-01-at-12.32.56-PM.png" style="margin: -1px auto;
    max-width: 980px; width:100%;" /></a>
    
    	<? } ?>
        
        <? if($_GET['l'] == "Lake Havasu City") {?>
        
        <a href="http://userinclude.com/zAmbEdgNew/local/lake-havasu-az"><img src="http://userinclude.com/zAmbEdgNew/wp-content/uploads/2017/03/Screen-Shot-2017-03-01-at-12.32.56-PM.png" style="margin: -1px auto;
    max-width: 980px; width:100%;" /></a>
    
    	<? } ?>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        </div>
        </div>
       
        
        <div style="background:red; color:red;">_</div>
        
        <div class="inner">
        <div class="content-area">
        <div class="toparea">
        
        <div class="inner">
<div class="content-area">

			<h1>Looking For <?=$_GET['kw']?>?</h1>
            <div class="fifty">
            <h3>Make an appointment:</h3>
            <?php echo FrmFormsController::get_form_shortcode( array( 'id' => 6, 'title' => false, 'description' => false ) ); ?>
            </div>
            <div class="fifty">
            <br />
            <h2>	Call now for priority service!<br />
            <b style="font-weight: bold;
    line-height: 30px;">FOR 24-HOUR RESPONSE</b><br />
            <span class="red"><?=$_GET['ph']?></span><br />
            <b style="font-weight: bold;
    line-height: 30px;">100% Satisfaction Guaranteed</b></h2>
    <h3>Testimonials</h3>
            <div style="background: #fff none repeat scroll 0 0;
    margin: 0 0 0 50px;
    padding: 25px;">
            <?=do_shortcode( '[testimonial cat="short-testimonial"][/testimonial]' )?>
            </div>
            </div>
            <div style="clear: both;"></div>
            
             <h3>Why Choose Us?</h3>
            <p>
            Ambient Edge is a hometown heating, air conditioning and plumbing company serving the <?=$_GET['l'];?> area. Our services include the sale of new Lennox equipment, around-the-clock repair and service for all major HVAC brands, duct design, fabrication of custom sheet metal, performing building load calculations, and more. Our mission is to provide quality service and products for both residential and commercial consumers while honoring our 100% satisfaction guarantee. 
            </p>
           <div align="center">
           <img class="lefticon" src="http://shared.mgsites.net/logo-nate-white.png" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img class="lefticon" src="http://shared.mgsites.net/logo-bbb-a-plus-rating.png" style="margin-bottom: 30px !important;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img class="lefticon" src="http://www.homeadvisor.com/images/sp-badges/boha-2014-border.png?sp=16226755&key=207846838001463641136df62da5f43e" / >
</div>
		</div><!-- .content-area -->

		
	</div><!-- inner -->
	</div><!-- #primary.pagemid -->

<?php get_footer(); ?>