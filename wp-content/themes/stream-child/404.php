<?php
	/**
	 * Required Variables
	 * Get variables info from theme options panel
	 */

	$atp_searchformtxt = get_option('atp_searchformtxt') ? get_option('atp_searchformtxt') : 'Search';
?>
<?php get_header(); ?>

<div class="pagemid">
  <div class="inner">
    <div id="main">
      <div class="entry-content">
        <div class="error">
          <h1>404</h1>
          <h2>We're sorry, but the page you were looking for doesn't exist.</h2>
          <a href="/sitemap" class="more-link button medium blue"><span>Visit the Sitemap</span></a>
        </div>
      </div>
      <!-- .content --> 
    </div>
    <!-- main -->
    <?php { get_sidebar(); } ?>
    <!-- /sidebar -->
    
    <div class="clear"></div>
  </div>
  <!-- .inner --> 
</div>
<!-- .pagemid -->
<?php get_footer(); ?>
