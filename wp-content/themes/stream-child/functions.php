<?php

/* Child theme functions.php. */

add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style' );
function enqueue_parent_theme_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

/*------------------------------------------------------------------------------------------------------*/
/* Excerpt function added for List Category Posts with Pagination Plugin
/*------------------------------------------------------------------------------------------------------*/

/*
function get_excerpt(){
$excerpt = get_the_content();
$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
$excerpt = strip_shortcodes($excerpt);
$excerpt = strip_tags($excerpt);
$excerpt = substr($excerpt, 0, 300);
$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
$excerpt = $excerpt.' ... ';
//$excerpt = $excerpt.' <a href="'.$permalink.'">Read more →</a>';
return $excerpt;
}
*/




?>