<!doctype html>
<html amp <?php echo AMP_HTML_Utils::build_attributes_string( $this->get( 'html_tag_attributes' ) ); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <script async custom-element="amp-analytics"  src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <?php do_action( 'amp_post_template_head', $this ); ?>


    <?php  //Setting Template Variables
        $siteName = 'Ambient Edge';
        $phoneNumber = '702-766-5531'; //'702-479-7464';
        $accentColor = '#ec2227';
        $textColor = '#333';
        $titleColor = '#3a53a4';
        $analyticsID =  "UA-92188150-1";
        $titleBackground = '#3a53a4';
        $metaColor = '#777';
        $linkColor = '#ff2a13';
        $linkHover = '#d80f0f';
    ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <style amp-custom>


        body {
            font-family: 'Roboto', Serif;
            font-size: 16px;
            line-height: 1.8;
            background: #fff;
            color: <?php echo $textColor; ?>;
            padding-bottom: 100px;
            margin:  0;
        }

        .wrapper {
            max-width: 600px;
            margin: 0 auto;
            padding: 0 10px;
        }

        .amp-content {
            padding:  0 16px 16px 16px;
            overflow-wrap: break-word;
            word-wrap: break-word;
            font-weight: 400;
            color: <?php echo $textColor; ?>;
        }

        amp-video{
            margin:  20px auto;
            max-width: 568px;
        }

        .amp-wp-article{
            max-width:  600px;
            margin:  20px auto;
            text-align: left;
            overflow: hidden;
        }

        .amp-wp-article amp-img {
            display: block;
            margin: 0 auto 20px auto;
            float: none;
            width: auto;
            max-width: 100%;
            box-sizing: border-box;
        }


        p,
        ol,
        ul,
        figure {
            margin: 0 0 24px 0;
        }

        a,
        a:visited {
            color: <?php echo $linkColor; ?>;
        }

        a:hover,
        a:active,
        a:focus {
            color: <?php echo $linkHover; ?>;
        }

        /* Header Styles */

         .title-bar {
            padding: 10px 0;
            background-image: -webkit-linear-gradient(top,#f3f3f3,#f0f0f0);
            background-image: -moz-linear-gradient(top,#f3f3f3,#f0f0f0);
            background-image: -o-linear-gradient(top,#f3f3f3,#f0f0f0);
            background-image: linear-gradient(top,#f3f3f3,#f0f0f0);
            width:  100%;            
        }

        .title-bar amp-img {
            display: inline-block;
            vertical-align: top;
            padding: 0;
            margin: 0 auto;
        }

        .main-logo {
            height: auto;
            width: 260px;
        }

        @media screen and (max-width: 480px){
            .main-logo {
                height: auto;
                width: 215px;
                margin-top:  20px;
            }
        }

        .page-title {
            margin-top: 15px;
            color:  <?php echo $titleColor; ?>;
        }

        h1, h2, h3 {
            color:  <?php echo $titleColor; ?>;
            font-weight: normal;
            line-height: 1.2;
        }

        .phone-icon-amp{
            color: white;
            height: 40px;
            width: 40px;
            fill: #fff;
            display: block;
            margin: 7px auto;
        }

        .phone-link{
            text-align: center;
            float: right;
            display: block;
            height: 54px;
            width: 54px;
            border-radius: 54px;
            border: 1px solid #fff;
            background: <?php echo $accentColor; ?>;
            font-size: 10px;
            color: white;
            text-decoration: none;
            position: relative;
            top: 2px;
        }

        .phone-link:hover{
            color: #fff;
            text-decoration: none;
        }

        /* Navigation Styles */
        .amp-navigation{
            text-align: center;
            width: 100%;
            margin: 0;
            padding: 0;
            box-shadow: 0 3px 3px rgba(0,0,0,0.3);
            background:  <?php echo $titleColor; ?>;
        }

        .amp-navigation ul{
            margin:  0;
            padding:  0;
        }

        .amp-navigation li{
            display: inline-block;
            margin: 0;
            text-align: center;
        }

        .amp-navigation a{
            text-align: center;
            padding-right: 10px;
            display:  inline-block;
            padding:  5px 10px;
            color:  white;
            transition: all 0.2s;
            text-decoration: none;
        }

        .amp-navigation a:hover{
            background:  <?php echo $accentColor; ?>;
            text-decoration: none;
        }


        /* Footer */
        footer{
            text-align: center;
            color: #777;
            font-size: 13px;
        }

        /* Footer Phone */
        .footer-contact {
            padding-bottom: 10px;
            padding-top: 10px;
            border-bottom: 1px solid #eaeaea;
            border-top: 1px solid #eaeaea;
            display: block;
            max-width: 600px;
            margin: 0 auto 10px auto;
        }
        .footer-phone-content {
            font-size: 20px;
            text-align: center;
            display: block;
            line-height: 1;
            margin: 10px auto;
            padding: 0 10px;
        }

        .footer-phone-number {
            font-size: 28px;
            text-align: center;
            display: block;
            margin: 10px auto;
            line-height: 1;
            padding: 0 10px;
        }

        .footer-phone-number a {
            text-decoration: none;
            color: <?php echo $linkColor ?>;
            text-align: center;
        }

        /* Disclaimer */

        .disclaimer {
            background: #e0e0e0;
            border:  1px solid #ccc;
            padding:  10px;
            margin:  20px auto;
        }

        .disclaimer--title{
            font-size:  20px;
            display:  block;
            margin-bottom:  5px;
        }

        .disclaimer--content{
            font-size:  14px;
        }

        /* General Article */
        .amp-wp-article {
            padding-left: 10px;
            padding-right: 10px;
        }

        .addtoany_content_bottom {
            display: none;
        }

        @media screen and (max-width: 430px){

            .phone-link{
                height: 40px;
                width: 40px;
                border-radius: 40px;               
                position: relative;
                top: 4px;
                background: <?php echo $accentColor; ?>;
            }

            .phone-icon-amp{
                height: 28px;
                width: 28px;
                margin: 6px auto;
            }

            h1{
                font-size:  24px;
            }

            h2{
                font-size:  20px;
            }

        }

        @media screen and (min-width: 550px){
            .two-column{
                -moz-column-count: 2;
                -moz-column-gap: 20px;
                -webkit-column-count: 2;
                -webkit-column-gap: 20px;
                column-count: 2;
                column-gap: 20px;
            }
        }
    </style>
</head>

<body class="<?php echo esc_attr( $this->get( 'body_class' ) ); ?> etst">


<div class="title-bar">
    <div class="wrapper">
        <a href="<?php echo site_url(); ?>" class="logo-link">
            <amp-img
                src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/amp/amp-logo.png"
                alt="Ambient Edge Logo"
                width="260" height="62"
                layout="responsive"
                class="main-logo">
            </amp-img>
        </a>

        <a href="tel:+1-<?php echo  $phoneNumber; ?>" class="phone-link">
            <svg width="1792" height="1792" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg" class="phone-icon-amp"><path d="M1600 1240q0 27-10 70.5t-21 68.5q-21 50-122 106-94 51-186 51-27 0-52.5-3.5t-57.5-12.5-47.5-14.5-55.5-20.5-49-18q-98-35-175-83-128-79-264.5-215.5t-215.5-264.5q-48-77-83-175-3-9-18-49t-20.5-55.5-14.5-47.5-12.5-57.5-3.5-52.5q0-92 51-186 56-101 106-122 25-11 68.5-21t70.5-10q14 0 21 3 18 6 53 76 11 19 30 54t35 63.5 31 53.5q3 4 17.5 25t21.5 35.5 7 28.5q0 20-28.5 50t-62 55-62 53-28.5 46q0 9 5 22.5t8.5 20.5 14 24 11.5 19q76 137 174 235t235 174q2 1 19 11.5t24 14 20.5 8.5 22.5 5q18 0 46-28.5t53-62 55-62 50-28.5q14 0 28.5 7t35.5 21.5 25 17.5q25 15 53.5 31t63.5 35 54 30q70 35 76 53 3 7 3 21z"/></svg>
        </a>
    </div>
</div>

<nav class="amp-navigation">
    <div class="wrapper">
        <?php wp_nav_menu(array(
            'container' => false,                           // remove nav container
            'container_class' => 'menu cf',                 // class of container (should you choose to use it)
            'menu' => __( 'The Amp Menu', 'bonestheme' ),  // nav name
            'menu_class' => 'nav amp-nav cf',               // adding custom nav class
            'theme_location' => 'amp-nav',                 // where it's located in the theme
            'before' => '',                                 // before the menu
            'after' => '',                                  // after the menu
            'link_before' => '',                            // before each link
            'link_after' => '',                             // after each link
            'depth' => 0,                                   // limit the depth of the nav
            'fallback_cb' => ''                             // fallback function (if there is one)
        )); ?>
    </div>
</nav>


<article class="amp-wp-article">

    <header class="amp-wp-article-header">
        <h1 class="amp-wp-title"><?php echo wp_kses_data( $this->get( 'post_title' ) ); ?></h1>
    </header>

    <?php 
        if ($this->load_parts( array( 'featured-image' ) ) ) {
            $this->load_parts( array( 'featured-image' ) );
        }
    ?>

    <div class="amp-wp-article-content">
        <?php 
            function phoneNumberReplace($content, $phone){
                $pattern = '((\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4})mi';
                $content = preg_replace($pattern, '$1 '. $phone . '$6', $content);
                return $content;
            }
            //Update Phone Number for Unified Phone Number
            $pageContent = $this->get( 'post_amp_content' ); 
            echo phoneNumberReplace( $pageContent, $phoneNumber );
        ?>
        <?php //echo $this->get( 'post_amp_content' ); // amphtml content; no kses ?>
    </div>

</article>

<div class="footer-contact">
    <span class="footer-phone-content">Call Now to Speak with One of Our Representatives</span>
    <span class="footer-phone-number"><a href="tel:+1-<?php echo  $phoneNumber; ?>"><?php echo  $phoneNumber; ?></a></span>
</div>

<footer>
    Copyright <?php echo date('Y') ?> Ambient Edge <br> All Rights Reserved.
</footer>

<?php //$this->load_parts( array( 'footer' ) ); ?>

<?php do_action( 'amp_post_template_footer', $this ); ?>

<amp-analytics type="googleanalytics" id="analytics1">
<script type="application/json">
{
  "vars": {
    "account": "<?php echo $analyticsID; ?>"
  },
  "triggers": {
  "trackPageviewWithAmpdocUrl": {
      "on": "visible",
      "request": "pageview",
      "vars": {
          "ampdocUrl": "<?php echo get_the_permalink() . 'amp/'; ?>"
      }
     }
  }
}
</script>
</amp-analytics>

</body>
</html>
