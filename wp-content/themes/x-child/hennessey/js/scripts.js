jQuery(document).ready(function($) {


	jQuery('.x-slider-container span:contains("GET A QUOTE")').each(function(){
		$(this).text('Book Online Now')
		$(this).css('min-width', '200px')
		$(this).css('letter-spacing', '-1px')
	});

	$(window).on('load', function(){
		jQuery('[data-fancybox]').off('touchend click')
		jQuery('[data-fancybox]').fancybox()
	})

	$('.rs-sidebar-faq a').click(function(e){
		e.preventDefault();
//		$(this).parent().find('a + div').slideUp();
		$(this).next('div').slideToggle();

	})

	if($(window).width() < 768) {
		$('ul.random-faq-loop-text li').each(function(i){
			if(i >= 5) {
				$(this).remove()
			}
		})

		$('.page-id-5221 .h-landmark.entry-title span').text('Commercial Credit Application')

		$('.page-id-5238 iframe').before('<div style="text-align: center; margin: 25px 0; font-size: 20px;"><a class="rs-booking-faq" href="#">Booking FAQ</a></a>');
		$('.page-id-5238 .rs-sidebar-faq').after('<div style="text-align: center; margin: 25px 0; font-size: 20px;"><a class="rs-book-now" href="#">Book Now</a></a>');

		$('.rs-booking-faq').on('click', function(e){
			e.preventDefault();
			$('html,body').animate({scrollTop: $('.page-id-5238 .rs-sidebar-faq').offset() ? $('.page-id-5238 .rs-sidebar-faq').offset().top - 200 : 0}, 500)
		})
		$('.rs-book-now').on('click', function(e){
			e.preventDefault();
			$('html,body').animate({scrollTop: 0}, 500)
		})
	}

	if(jQuery('header.x-header-landmark.test2').offset() && jQuery('header.x-header-landmark.test2').offset().top < 150) {
		jQuery('header.x-header-landmark.test2').css('margin-top', '61px');
	}

	/* ---- Removing because of phone swap issues
	setTimeout(function(){
		$('a[href="tel:+17023235982"]').html('<span style="font-size: 14px; color: #b6bac5;" class="contact-text">702-948-7201</span>');
		$('a[href="tel:+17023235982"]').attr('href', 'tel:+17029487201')
	}, 1500) ----*/

	$('.rs-close-button').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		$('.rs-left-block').fadeOut();
	});

	if($('.x-header-landmark').length && $('.x-header-landmark').outerHeight() > 130) {
		$('aside.x-sidebar').css('margin-top', 0 - $('.x-header-landmark').outerHeight())
	}


    /*****************************************
    Displaying Mobile Contact Button on Scroll
    *****************************************/
    $(window).on( 'scroll', function(){
        if ($(window).scrollTop() >= 300) {
            $('.footer-phone-circle').addClass('visible');
        } else {
            $('.footer-phone-circle').removeClass('visible');
        }
    });

    /*-----------------------------------------------
    Scroll to Top When Menu Toggle is Pressed
    -----------------------------------------------*/
    $('.vegas-header-menu-toggle').on('click',function(e){

        $("html, body").animate({ scrollTop: 0 }, 200);

    });

 /*-----------------------------------------------
    Homepage Awards/Accolades Slider
-----------------------------------------------*/
 $('.homepage-awards-slider').flexslider({
    animation: "slide",
    controlNav: "false",
    directionNav: "true",
    minItems: 4,
    maxItems: 8,
    itemWidth: 160,
    itemMargin: 20
  });

/*-----------------------------------------------
Lennox Fall Promotion Popup Activation
- Magnific-popup.js
-----------------------------------------------*/
$('.popup-with-zoom-anim').unbind();
$('.popup-with-zoom-anim').on('mousedown touchstart', function(e){
    e.preventDefault();
});


$('.popup-with-zoom-anim').magnificPopup({
    type: 'inline',

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,
    fixedBgPos: true, fixedContentPos: true,
        
    midClick: true,
    removalDelay: 300,
    disableOn: false,
    mainClass: 'my-mfp-zoom-in',
    callbacks: {
        open: function() {
         console.log('popup open');
        },
        close: function() {
            console.log('popup close');
        }
        
      }
});

/*****************************************
Get Initial Referrer and Set in Cookie
*****************************************/

//Cookie Helper Functions

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getQueryVariable(variable){
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

//Getting Referrer Info and Setting Cookie

if ( getCookie('hennessey-referrer') !== '' ) {
  console.log('Existing Cookie: ' + getCookie('hennessey-referrer'));
  console.log('Current Referrer: ' + document.referrer);

  //Update TextArea in Form
  $('.your-info textarea').html(getCookie('hennessey-referrer'));
} else {

  //Putting Together Cookie Info
  var utmSource = getQueryVariable('utm_source');
  var utmCampaign = getQueryVariable('utm_campaign');
  var utmMedium = getQueryVariable('utm_medium');
  var initialReferrer = document.referrer;

  //Pretty Values for No Info
  if ( utmSource == false ){
    utmSource = 'Not Set';
  }

  if ( utmCampaign == false ){
    utmCampaign = 'Not Set';
  }

  if ( utmMedium == false ){
    utmMedium = 'Not Set';
  }

  if ( initialReferrer == false ){
    initialReferrer = 'Direct/Not Set';
  }

  //Setting up Cookie Value
  var cookieSourceValue =   '  Tracking Information - Source: ' + utmSource +
                            ', Campaign: ' + utmCampaign +
                            ', Medium: ' + utmMedium +
                            ', Entry Referrer: ' + initialReferrer;

  //Set Cookie
  setCookie('hennessey-referrer',cookieSourceValue,1);

  //Debugging
  console.log('New Cookie: ' + getCookie('hennessey-referrer'));
  console.log('Current Referrer: ' + document.referrer);

  //Update TextArea in Form
  $('.your-info textarea').html(getCookie('hennessey-referrer'));
}


/*-----------------------------------------------
Display Ambient Footer Popup
-----------------------------------------------*/
$('.ambient-footer-popup').addClass('visible');
$('.ambient-footer-popup__close-button').on('click', function(e){
    e.preventDefault();
    $('.ambient-footer-popup').addClass('closed');
});

}); //End Document Ready





/*-----------------------------------------------
Initializing things that rely on full CSS load
to render correctly (Isotope etc.)
-----------------------------------------------*/

jQuery(window).bind('load', function ($) {

    //Adding Option to Auto-Open Lennox Promo with URL Parameter

var lennoxOpen = 'lennox-promotion';
var url = window.location.href;
if(url.indexOf('?' + lennoxOpen) != -1) {

    //console.log('parameter found!');
    jQuery.fancybox.open({
        src  : '#lennox-popup',
        type : 'inline',
        opts : {
            afterShow : function( instance, current ) {
                //console.info( 'done!' );
            }
        }
    });
} else {
    //console.log('parameter missing');
}

    /*-----------------------------------------------
  Isotope Video FAQ Filtering
  -----------------------------------------------*/

    var $container = jQuery('.faq-filter-list').isotope({
        itemSelector: '.grid-item',
        layoutMode: 'fitRows',
        getSortData: {
            category: '[data-category]'
        },
        hiddenClass: 'isotope-hidden',
    });

    // filter functions
    var filterFns = {

    };

    var itemReveal = Isotope.Item.prototype.reveal;
    Isotope.Item.prototype.reveal = function () {
        itemReveal.apply(this, arguments);
        jQuery(this.element).removeClass('isotope-hidden');
    };

    var itemHide = Isotope.Item.prototype.hide;
    Isotope.Item.prototype.hide = function () {
        itemHide.apply(this, arguments);
        jQuery(this.element).addClass('isotope-hidden');
    };

    // bind filter button click
    jQuery('#filters').on('click', 'button', function () {

        var filterValue = jQuery(this).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;
        $container.isotope({ filter: filterValue });



    });

    // change is-checked class on buttons
    jQuery('.filters-button-group').each(function (i, buttonGroup) {
        var $buttonGroup = jQuery(buttonGroup);
        $buttonGroup.on('click', 'button', function () {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            jQuery(this).addClass('is-checked');
        });
    });


    $container.on('arrangeComplete', function (event, filteredItems) {
        //console.log( 'Isotope arrange completed on ' + filteredItems.length + ' items' );
        //equalheight('.faq-filter-listing');
    });

    $container.on('layoutComplete', function (event, filteredItems) {
        console.log('Layout Complete Isotope arrange completed on ' + filteredItems.length + ' items');
        //jQuery('.faq-filter-listing').addClass('sorted');

        if (jQuery('.isotope-link:nth-of-type(1)').hasClass('is-checked')) {
            jQuery('.faq-filter-listing').removeClass('sorted');
        }

        equalheight('.faq-filter-listing:not(.isotope-hidden)');

    });


    //Equal Height Columns
    equalheight('.faq-filter-listing:not(.isotope-hidden)');

}); //End Window Load Scripts

equalheight = function (container) {

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;

    jQuery(container).each(function () {

        $el = jQuery(this);
        jQuery($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}



