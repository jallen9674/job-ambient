<?php
/*--------------------------------
Example Shortcode Wrapper
[shortcode-name]
---------------------------------*/

function hennesseyExampleShortcode($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT
?>

<?php //Output Goes Here ?>

<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('shortcode-name', 'hennesseyExampleShortcode');


?>