<?php
/*--------------------------------
Nearby City Shortcode
[hc-city-shortcode]
---------------------------------*/

function hcCityWidgetShortcode($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT

?>


<div class="city-widget">
    <div class="city-widget__inner">

        <span class="city-widget__title">Locations Served</span>

        <ul class="city-widget__list">
            <li><a href="<?php echo site_url(); ?>/locations/las-vegas-air-conditioning-repair-company/"><span>Las Vegas, NV AC Repair</span></a></li>
            <li><a href="<?php echo site_url(); ?>/las-vegas/heating-and-furnace-repair-company/"><span>Las Vegas, NV Heating Repair</span></a></li>
            <li><a href="<?php echo site_url(); ?>/locations/north-las-vegas-air-conditioning-repair-company/"><span>North Las Vegas, NV AC Repair</span></a></li>
            <li><a href="<?php echo site_url(); ?>/north-las-vegas/heating-and-furnace-repair-company/"><span>North Las Vegas, NV Heating Repair</span></a></li>
            <li><a href="<?php echo site_url(); ?>/locations/henderson-air-conditioning-repair-company/"><span>Henderson, NV AC Repair</span></a></li>
            <li><a href="<?php echo site_url(); ?>/henderson/heating-and-furnace-repair-company/">Henderson, NV Heating Repair</a></li>
            <li><a href="<?php echo site_url(); ?>/locations/laughlin-air-conditioning-repair-company/"><span>Laughlin, NV AC Repair</span></a></li>
            <li><a href="<?php echo site_url(); ?>/locations/summerlin-air-conditioning-repair-company/"><span>Summerlin, NV AC Repair</span></a></li>
            <li><a href="<?php echo site_url(); ?>/locations/kingman-air-conditioning-repair-company/"><span>Kingman, AZ AC Repair</span></a></li>
            <li><a href="<?php echo site_url(); ?>/locations/bullhead-city-air-conditioning-repair-company/"><span>Bullhead City, AZ AC Repair</span></a></li>
            <li><a href="<?php echo site_url(); ?>/locations/lake-havasu-air-conditioning-repair-company/"><span>Lake Havasu, AZ AC Repair</span></a></li>
        </ul>

    </div>
</div>


<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('hc-city-shortcode', 'hcCityWidgetShortcode');


