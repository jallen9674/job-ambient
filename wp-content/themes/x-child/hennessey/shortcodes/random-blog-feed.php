<?php
/*--------------------------------
Display 4 Blog Posts
[hc-random-blog-feed]
---------------------------------*/

function hcRandomBlogFeed($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT

?>

    <div class="hc-blog-feed">
        <div class="hc-blog-feed__inner">

            <h2 class="hc-blog-feed__title">
                <span>Featured Articles</span>
            </h2>

            <div class="hc-blog-feed__feed">
            
            <?php
                $args = array(
                    'posts_per_page' => 4,
                    'post_type' => 'post',
                    'order' => 'DSC',
                    'orderby' => 'rand',
                );

                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
            ?>
                <a href="<?php the_permalink(); ?>" class="hc-blog-feed__single-post">
                    <?php 
                         echo '<div class="hc-blog-feed__single-image">';
                            if ( has_post_thumbnail() ) {                            
                                the_post_thumbnail('blog-featured', array('alt'=> get_the_title() ));
                            } else {
                                echo '<img width="273" height="175" class="hc-blog-feed__placeholder-image" src="' . get_stylesheet_directory_uri() . '/hennessey/images/default-faq-thumb.jpg" alt="'. get_the_title() . '">';
                            }
                        echo '</div>';
                    ?>
                    <h3 class="hc-blog-feed__single-post-title"><?php the_title(); ?></h3>
                </a>
            
            <?php endwhile; else : ?>
                <p>No blog posts found.</p>
            <?php endif; ?>
            <?php wp_reset_query(); ?>

            </div>

        </div>
    </div>


<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('hc-random-blog-feed', 'hcRandomBlogFeed');


