<?php
/*--------------------------------
Search Form
[hc-search]
---------------------------------*/

function hcSearchForm($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT

?>

<div class="search-form-wrapper">
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">		
		<input type="text" class="search-form__input" name="s" id="s" placeholder="Search our site..." />
        <button type="submit" class="search-form__submit" name="submit" id="searchsubmit" value="Submit">
        <i class="fa fa-search"></i>
     </button>
	</form>
</div>

<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('hc-search', 'hcSearchForm');


