<?php
/*--------------------------------
Homepage Accolade Slider
[homepage-accolade-slider]
---------------------------------*/

function hennesseyExampleShortcode($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT
?>

<div class="homepage-awards-slider">
    <div class="homepage-awards-slider__inner">
        <div class="flexslider">
            <ul class="slides">
                <li>
                    <img src="<?php  echo get_stylesheet_directory_uri(); ?>/hennessey/images/awards-slider/bbb_a_home.png" alt="Better Business Bureau" width="120" height="84">
                </li>
                 <li>
                    <img src="<?php  echo get_stylesheet_directory_uri(); ?>/hennessey/images/awards-slider/dave_lennox_premier-png.png" alt="Lennox Premier Dealer" width="120" height="37">
                </li>
                <li>
                    <img src="<?php  echo get_stylesheet_directory_uri(); ?>/hennessey/images/awards-slider/nate.png" alt="NATE - North American Technician Excellence HVAC Repair" width="120" height="120">
                </li>
                <li>
                    <img src="<?php  echo get_stylesheet_directory_uri(); ?>/hennessey/images/awards-slider/boha-2017.jpg" alt="Home Advisor Best of 2017 Winner" width="120" height="106">
                </li>
                <li>
                    <img class="portrait-logo" src="<?php  echo get_stylesheet_directory_uri(); ?>/hennessey/images/awards-slider/Kingman%20Chamber%20Logo.jpg" alt="Kingman, AZ Chamber of Commerce Logo" width="90" height="150">
                </li>
                <li>
                    <img  src="<?php  echo get_stylesheet_directory_uri(); ?>/hennessey/images/awards-slider/DC_Logo_Lockup_DC_RGB.png" alt="Mitsubishi Electric Diamond Contractor" width="120" height="30">
                </li>
                 <li>
                    <img src="<?php  echo get_stylesheet_directory_uri(); ?>/hennessey/images/awards-slider/elite.jpg" alt="Home Advisor Elite Service Award" width="120" height="114">
                </li>
                <li>
                    <img src="<?php  echo get_stylesheet_directory_uri(); ?>/hennessey/images/awards-slider/HCC%20Logo.png" alt="Henderson, NV Chamber of Commerce" width="120" height="94">
                </li>
                <li>
                    <img src="<?php  echo get_stylesheet_directory_uri(); ?>/hennessey/images/awards-slider/Proud-Member-Logo-jpg-.jpg" alt="Las Vegas Metro Chamber of Commerce" width="104" height="120">
                </li>
                <li>
                    <img src="<?php  echo get_stylesheet_directory_uri(); ?>/hennessey/images/awards-slider/toprated.jpg" alt="Home Advisor Top Rated" width="104" height="120">
                </li>
            </ul>
        </div>
    </div>
</div>

<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('homepage-accolade-slider', 'hennesseyExampleShortcode');


?>