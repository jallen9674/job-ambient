<?php
/*--------------------------------
Example Shortcode Wrapper
[scholarship-banner]
---------------------------------*/

function scholarshipBanner($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT
?>

<?php //Output Goes Here ?>

<h2 class="h-custom-headline h4 accent"><span>Scholarship Opportunity</span></h2>
<div class="x-recent-posts cf horizontal">
  <a class="x-recent-post1 with-image" href="https://www.ambientedge.com/2019-students-affected-by-cancer-scholarship/">
    <img src="https://www.ambientedge.com/wp-content/uploads/2018/01/Ambient_Edge_College_Scholarship.jpg" alt="Ambient Edge College Scholarship Colored" />
    <div class="x-recent-posts-content">
      <h3 class="h-recent-posts">Students Affected by Cancer Scholarship</h3>
      <span class="x-recent-posts-date">Deadline: December 31, 2019</span>
    </div>
  </a>
</div>

<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('scholarship-banner', 'scholarshipBanner');


?>
