<?php

// [hc-featured-on-logos]

function featuredOnLogos($atts = null) {

    global $post;

    extract(shortcode_atts(array(
      'amount' => '',
   ), $atts));

    $queryAmount = $amount;

    ob_start();
    //BEGIN OUTPUT
?>

<div class="featured-on-logos">
  <div class="featured-on-logos__inner">
    <h2 class="featured-on-logos__title">As Featured On</h2>
      <div class="featured-on-logos__image-list">
        <img width="100" height="47" src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/images/trust-logos/inc-magazine-white.png" alt="Inc. Magazine" class="featured-on-logos__image featured-on-logos__image--inc">
        <img width="79" height="65" src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/images/trust-logos/today-show-white.png" alt="Today Show" class="featured-on-logos__image featured-on-logos__image--today">
        <img width="200" height="43" src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/images/trust-logos/huffington-post-white.png" alt="Huffpost" class="featured-on-logos__image featured-on-logos__image--huffpo">
      </div>
  </div>
</div>

<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}


add_shortcode('hc-featured-on-logos', 'featuredOnLogos');

?>