<?php
/************************************
**
** BEGIN Optimizations
** OF Hennessey Consulting
**
************************************/


/*---------------------------------
Dequeue Scripts and Styles
----------------------------------*/

function hennessey_enqueue_cleanup() {
    wp_dequeue_style( 'x-google-fonts' );
    wp_deregister_style( 'x-google-fonts' );
}
add_action( 'wp_enqueue_scripts', 'hennessey_enqueue_cleanup', 2000 );

function hennessey_scripts_enqueue_cleanup() {
    //wp_dequeue_script( 'cu3er' );
    //wp_deregister_script( 'cu3er' );
}
add_action( 'wp_print_scripts', 'hennessey_scripts_enqueue_cleanup', 2000 );

function hennessey_add_related_faqs() {
  echo do_shortcode('[dm-random-faq-loop-text]');
}
add_action('x_after_the_content_end', 'hennessey_add_related_faqs');

function hennessey_add_featured_logos() {
  echo do_shortcode('[hc-featured-on-logos]');
}

//add_action('x_before_site_end', 'hennessey_add_featured_logos', 0, 1);

/*---------------------------------
Enqueue of Optimized Styles
----------------------------------*/
function hennessey_load_styles(){
    wp_enqueue_style( 'hennessey-css', get_stylesheet_directory_uri() .'/hennessey/css/styles.css?v=1.06' );
    wp_enqueue_style( 'fancybox-css', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css' );
}
add_action( 'wp_enqueue_scripts', 'hennessey_load_styles', 10000);


/*---------------------------------
Enqueue of Optimized Scripts
----------------------------------*/

function hennessey_load_scripts(){
  wp_enqueue_script('isotope-js', get_stylesheet_directory_uri() . '/hennessey/js/vendor/isotope.js', array('jquery'), '1.0', true);
  wp_enqueue_script('fancybox-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js', array('jquery'), '1.0', true);
  wp_enqueue_script('magnific-js', get_stylesheet_directory_uri() . '/hennessey/js/vendor/magnific-popup.js', array('jquery'), '1.0', true);
  wp_enqueue_script('flexslider-js', get_stylesheet_directory_uri() . '/hennessey/js/vendor/jquery.flexslider.js', array('jquery'), '1.0', true);
  wp_enqueue_script('hennessey-js', get_stylesheet_directory_uri() . '/hennessey/js/scripts.js?v=1.23', array('jquery'), '1.0', true);
  
}
add_action( 'wp_enqueue_scripts', 'hennessey_load_scripts', 999999);


/*---------------------------------
Remove Emoji
----------------------------------*/
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


/*----------------------------------------------
- TEMP: To find all script handles
- Should be disabled for production
-----------------------------------------------*/
function hennessey_inspect_scripts() {
    global $wp_scripts;
    foreach( $wp_scripts->queue as $handle ) :
        echo $handle,' ';
    endforeach;
}
//add_action( 'wp_print_scripts', 'hennessey_inspect_scripts' );


/*---------------------------------
Cleaning Up WP Head
----------------------------------*/
function removeHeadLinks() {
  remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
  remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
  remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
  remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
  remove_action( 'wp_head', 'index_rel_link' ); // index link
  remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
  remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
  remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
  remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
}
add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

/*---------------------------------
Adding CMB2
----------------------------------*/

require_once dirname( __FILE__ ) . '/cmb2/init.php';

function update_cmb_meta_box_url( $url ) {
    $url = '/wp-content/themes/x-child/hennessey/cmb2';
    return $url;
}
add_filter( 'cmb2_meta_box_url', 'update_cmb_meta_box_url' );

/*---------------------------------
Adding Additional Image Sizes
----------------------------------*/

add_image_size( 'faq-thumb', 300, 200, true );
add_image_size( 'blog-featured', 300, 200, true );
add_image_size( 'faq-thumb-sidebar', 400, 250, true );
add_image_size( 'blog-thumb-large', 920, 400, true );

/*---------------------------------
Adding Metaboxes
----------------------------------*/
require_once('metaboxes/header-phone-number.php');
 
/*---------------------------------
Adding Shortcodes
----------------------------------*/

//require_once('shortcodes/example-shortcode.php');
require_once('shortcodes/random-faq-image.php');
require_once('shortcodes/random-faq-text.php');
require_once('shortcodes/scholarship-banner.php');
require_once('shortcodes/home-advisor-reviews.php');
require_once('shortcodes/homepage-accolade-slider.php');
require_once('shortcodes/featured-on-section.php');
require_once('shortcodes/city-shortcode.php');
require_once('shortcodes/maintenance-flyer.php');
require_once('shortcodes/searchbox.php');
require_once('shortcodes/random-blog-feed.php');

/*---------------------------------
Adding Taxonomies
----------------------------------*/

require_once('taxonomy/faqs.php');

/*---------------------------------
Adding Class to Body for Alternate Header Pages
----------------------------------*/
add_filter( 'body_class','alternateHeaderBodyClass' );
function alternateHeaderBodyClass( $classes ) {

    global $post;

    if (get_post_meta( $post->ID, '_ae_header_selection', true ) == true) {
        $classes[] = 'alternate-header';
    }

    return $classes;
}


/*--------------------------------------------
Adding CSS/JS Admin Panel
---------------------------------------------*/

function load_custom_wp_admin_style() {
        wp_register_style( 'hennessey_wp_admin_css', get_template_directory_uri() . '/hennessey/css/admin.css', false, '1.0.0' );
        wp_enqueue_style( 'hennessey_wp_admin_css' );
        wp_enqueue_script( 'hennessey_wp_admin_js', get_template_directory_uri() . '/hennessey/js/admin.js', false, '1.0.0' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


/*---------------------------------
Add Tag to Meta Description & Category
----------------------------------*/
function hennessey_description_tag($s){
 if( is_tag() ){
    $s .= single_tag_title(" Posts Tagged as: ", false);
 }
 if( is_category() ){
    $s .= single_cat_title(" Posts Categorized as: ", false);
 }
 return $s;
}
add_filter( 'wpseo_metadesc', 'hennessey_description_tag', 100, 1 );

/*------------------------------------------
Add page number to meta description
- Should be placed in functions.php
-------------------------------------------*/

function hennessey_add_page_number_titles( $s ) {
    global $page;
    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
    ! empty ( $page ) && 1 < $page && $paged = $page;

    $paged > 1 && $s .= ' | ' . sprintf( __( 'Page: %s' ), $paged );

    return $s;
}

add_filter( 'wpseo_title', 'hennessey_add_page_number_titles', 100, 1 );
add_filter( 'wpseo_metadesc', 'hennessey_add_page_number_titles', 100, 1 );


/*------------------------------------------
Diable Autoptimize on Some URLs
-------------------------------------------*/

add_filter('autoptimize_filter_noptimize','my_ao_noptimize',10,0);
function my_ao_noptimize() {
  if (  (strpos($_SERVER['REQUEST_URI'],'temp')!==false) ||
        (strpos($_SERVER['REQUEST_URI'],'las-vegas')!==false) ||
        (strpos($_SERVER['REQUEST_URI'],'amp')!==false)
    ) {
    return true;
  } else {
    return false;
  }
}

/*--------------------------------------------
Adding General Schema to Site
---------------------------------------------*/

function hennessey_add_business_schema(){
//If this isn't a location page add our schema
if( !is_singular('wpseo_locations') && !is_page(4514) ) {
    ?>

    <script type='application/ld+json'>
  {
    "@context": "http://www.schema.org",
    "@type": "LocalBusiness",
    <?php //Swap Out Name Per Location
      if ( is_page(4569) ) {
        //Henderson
        echo '"name": "Ambient Edge Heating and Air Conditioning",';
      } else if ( is_page(4571) ) {
        //Kingman
        echo '"name": "Ambient Edge Air Conditioning and Refrigeration, Inc.",';
      } else if ( is_page(4514) ) {
        //Las Vegas
        echo '"name": "Ambient Edge Heating and Air Conditioning",';
      } else {
        echo '"name": "Ambient Edge Heating and Air Conditioning",';
      }
    ?>
    "url": "<?php echo site_url(); ?>",
    "sameAs": [
      "https://facebook.com/AmbientEdgeAC",
      "https://www.twitter.com/ambientedge",
      "https://plus.google.com/+AmbientEdgeKingman",
      "https://www.linkedin.com/company/ambient-edge"
    ],
    "logo": "<?php echo site_url(); ?>/wp-content/uploads/2015/03/logo-ambientedge1.png",
    "image": "<?php echo site_url(); ?>/wp-content/uploads/2014/03/locations-header.jpg",

    "address": [
        {
          "@type": "PostalAddress",
          "streetAddress": "110 Corporate Park Dr #111",
          "addressLocality": "Henderson",
          "addressRegion": "NV",
          "postalCode": "89074",
          "addressCountry": "United States"
        },
        {
          "@type": "PostalAddress",
          "streetAddress": "5940 S Rainbow Blvd #213 ",
          "addressLocality": "Las Vegas",
          "addressRegion": "NV",
          "postalCode": "89118",
          "addressCountry": "United States"
        },
      {
        "@type": "PostalAddress",
        "streetAddress": "3270 Kino Avenue",
        "addressLocality": "Kingman",
        "addressRegion": "AZ",
        "postalCode": "86409",
        "addressCountry": "United States"
      }
    ],
    "openingHoursSpecification" : {
      "@type" : "OpeningHoursSpecification",
      "dayOfWeek" : [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ],
      "opens" : "00:00:00",
      "closes" : "23:59:59"
    },
    "telephone": ["(702) 723-4704", "(928) 263-8698", "(702) 928-7201"],
    "priceRange": "$$"
  }

   </script>
    <?php
  }
}

//add_action('wp_head', 'hennessey_add_business_schema');


function hennessey_add_location_lasvegas_schema(){
  if (is_single(5526) ){
  ?>

   <script type='application/ld+json'>
{
    "@context": "http://www.schema.org",
    "@type": "LocalBusiness",
    "name": "Air Conditioning Repair Company In Las Vegas, NV",
    "url": "https://www.ambientedge.com/las-vegas/air-conditioning-repair-company/",
    "sameAs": [
      "https://facebook.com/AmbientEdgeAC",
      "https://www.linkedin.com/company/ambient-edge",
      "https://www.youtube.com/user/AmbientEdge",
      "https://twitter.com/ambientedge"
    ],
    "logo": "https://www.ambientedge.com/wp-content/uploads/2017/03/ambient-edge.jpg",
    "image": "https://www.ambientedge.com/wp-content/uploads/2018/07/ambient-edge-logo.png",
    "address": {
          "@type": "PostalAddress",
          "streetAddress": "5940 S Rainbow Blvd #213",
          "addressLocality": "Las Vegas",
          "addressRegion": "NV",
          "postalCode": "89118",
          "addressCountry": "United States"
        },
    "openingHoursSpecification" : {
      "@type" : "OpeningHoursSpecification",
      "dayOfWeek" : [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ],
      "opens" : "00:00:00",
      "closes" : "23:59:59"
    },
    "telephone": " 702-928-7201",
    "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "5",
        "ratingCount": "7"
      },
      "geo":{
        "@type":"GeoCoordinates",
        "latitude":"36.0811365",
        "longitude":"-115.24130689999998"
    },
    "review": [{
      "@type": "Review",
      "author": "Vernon J.",
      "datePublished": "2018-08-02",
      "reviewBody": "Chris F. Was were professional and efficient. He was knowledgeable and very fast. Showed up early and got the A/C working quicker than I expected. I would rate his performance and the company at a 5 🌟 Thank you for doing a great job.",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }, {
      "@type": "Review",
      "author": "Daniel W.",
      "datePublished": "2018-05-31",
      "reviewBody": "Brien A. Was awesome! Very professional and explained what was going on in terms that I could understand. I didn't feel like I was dealing with someone who was just trying to sell me something. He was polite and it was overall a great experience, I would definitely recommend these guys!",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }, {
      "@type": "Review",
      "author": "Sally G.",
      "datePublished": "2017-06-15",
      "reviewBody": "Shawn was extremely kind and helpful and efficient at helping us with our air conditioning issues. I highly recommend Ambient Edge as we experienced excellent service!!!!!",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }, {
      "@type": "Review",
      "author": "Marvene H.",
      "datePublished": "2017-03-29",
      "reviewBody": "Professional, on time and excellent job!",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }, {
      "@type": "Review",
      "author": "Micah G.",
      "datePublished": "2018-08-13",
      "reviewBody": "Ambient Edge did a great job on our A/C work. Their staff has great customer service and made sure to check back with us to see if everything was working well.",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }, {
      "@type": "Review",
      "author": "David F.",
      "datePublished": "2018-08-02",
      "reviewBody": "Had problems with ac got me an appointment asap. Then got estimate some day. Great help.",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "5",
        "worstRating": "1"
      }
    }]
    }
    }
  </script>

  <?php
  }
}

add_action('wp_head', 'hennessey_add_location_lasvegas_schema');


/*---------------------------------
Modifying Yoast's Default Robots Behavior for
Archive, Category, Tag Pages
----------------------------------*/

function hennessey_yoast_robots_directives($robots){

  if ( is_archive() || is_category() || is_tag() ) {
      //Noindexing & Nofollowing all Archive related pages
      $robots =  'noindex,nofollow';
      return $robots;

  } else {
    //Remove noodp tag sitewide
    $robots = str_replace('noodp', '', $robots);
    return $robots;
   }
}

add_filter( 'wpseo_robots', 'hennessey_yoast_robots_directives', 10, 1 );



/*--------------------------------------------
Removing hentry Class to Prevent Search Console Schema Errors
---------------------------------------------*/

function hennessey_remove_hentry( $classes ) {
    if ( is_page() || is_single() ) {
        $classes = array_diff( $classes, array( 'hentry' ) );
    }
    return $classes;
}
add_filter( 'post_class','hennessey_remove_hentry' );


/*--------------------------------------------
Adding Custom AMP Template
---------------------------------------------*/

add_filter( 'amp_post_template_file', 'hennessey_set_amp_template', 10, 3 );

function hennessey_set_amp_template( $file, $type, $post ) {

  if ( ('single' === $type) || ('page' === $type) ) {
    $file = dirname( __FILE__ ) . '/amp/template.php';
  }
  return $file;
}

/*--------------------------------------------
Adding AMP Menu
---------------------------------------------*/

  register_nav_menus(
    array(
      'amp-nav' => __( 'The Amp Menu', 'bonestheme' )
    )
  );

/*--------------------------------------------
Adding Geographic Meta Information
---------------------------------------------*/


function hennessey_add_geo(){
?>

<meta name="zipcode" content="89074">
<meta name="city" content="Henderson" />
<meta name="state" content="Nevada" />
<meta name="country" content="United States of America">


<meta name="geo.region" content="US-NV" />
<meta name="geo.placename" content="Henderson" />
<meta name="geo.position" content="36.036199;-115.034011" />
<meta name="ICBM" content="36.036199, -115.034011" />


<meta name="zipcode" content="86409">
<meta name="city" content="Kingman" />
<meta name="state" content="Arizona" />
<meta name="country" content="United States of America">

<meta name="geo.region" content="US-AZ" />
<meta name="geo.placename" content="Kingman" />
<meta name="geo.position" content="35.232124;-114.000964" />
<meta name="ICBM" content="35.232124, -114.000964" />


<?php
}

//add_action('wp_head','hennessey_add_geo', 10 );


/*--------------------------------------------
Adding Lucky Orange to Site
---------------------------------------------*/
function hennessey_add_lucky_orange() {
  ?>
    <script type='text/javascript'>
    window.__lo_site_id = 99496;
    (function() {
        var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
        wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
      })();
    </script>
  <?php
}
//add_action('wp_footer', 'hennessey_add_lucky_orange' );

/*--------------------------------------------
Adding APEX Chat to Site
---------------------------------------------*/
function hennessey_add_apex() {
  ?>

  <script src="//www.apex.live/scripts/invitation.ashx?company=ambientedge" async></script>

  <?php
}

add_action('wp_footer', 'hennessey_add_apex');

/*--------------------------------------------
Adding Footer Phone Icon
---------------------------------------------*/

function hennessey_add_phone_icon() {
  global $post;
  //Get Header And Phone Number Information
  if ( get_post_meta( $post->ID, '_ae_primary_phone_number', true ) ) {
    $phoneNumberFooter = get_post_meta( $post->ID, '_ae_primary_phone_number', true );
  } else {
    $phoneNumberFooter = '888-628-5890';
  }
  ?>
  <a onclick="ga('send', 'event', 'Mobile Phone Link', 'Mobile Phone Click', 'Mobile Phone Click');" href="tel:<?php echo $phoneNumberFooter; ?>" class="footer-phone-circle mobile-phone-analytics-track"><i class="fa fa-phone"></i></a>
  <?php
}

add_action('wp_footer', 'hennessey_add_phone_icon');

/*--------------------------------------------
Hennessey Add Text Us Box
---------------------------------------------*/

add_action('wp_footer','hennessey_text_overlay');

function hennessey_text_overlay() {
  ?>

  <div class="hc-text-box">
    <div class="hc-text-box__inner">
        <a href="sms:855-258-9236" class="hc-text-box__text-link" onclick="ga('send', 'event', 'Mobile Text Click', 'Mobile Text Click', 'Mobile Text Click');">TEXT US</a>
    </div>
  </div>

  <?php 
}


/*--------------------------------------------
Adding CTM
---------------------------------------------*/

function hennessey_add_ctm() {
  ?>
  <script async src="//142137.tctm.co/t.js"></script>
  <?php
}

//add_action('wp_footer', 'hennessey_add_ctm');

/*--------------------------------------------
Adding Bing PPC Snippet
---------------------------------------------*/

function hennessey_add_bing_ppc(){
?>

  <script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"25024193"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>

<?php 
}

add_action('wp_footer', 'hennessey_add_bing_ppc');

/*--------------------------------------------
Adding Text Request
= Disabled, APEX takes chats now
--------------------------------------------*/

function hennessey_add_textrequest() {
  ?>
  <script src="https://www.textrequest.com/integrations/c2t.js?cbg=000&cb=FFF&btntxt=Text&txtc=FFF&pn=17027234704" async defer></script>
  <?php
}

//add_action('wp_footer', 'hennessey_add_textrequest');

/*--------------------------------------------
Adding Link to Search Results
---------------------------------------------*/

function hennessey_search_link() {
  
  if ( is_search() ) {
  ?>

  <a class="search-permalink" href="<?php the_permalink(); ?>"><?php the_permalink(); ?></a>

  <?php 
  }
}
add_action('x_after_the_excerpt_begin', 'hennessey_search_link');


/*--------------------------------------------
Yoast Breadcrumb Filter for Locations
- To address page/location post type mixing
---------------------------------------------*/

function hennessey_yoast_faq_breadcrumb_mod($crumb){

  


  //Summerlin Location Updates
  $crumb = str_replace('/summerlin/air-conditioning-repair-company/','/locations/summerlin-air-conditioning-repair-company/', $crumb);
  $crumb = str_replace('Air Conditioning Repair Company In Summerlin, NV', 'Summerlin, NV', $crumb);

  //Vegas
  $crumb = str_replace('/las-vegas/air-conditioning-repair-company/','/locations/las-vegas-air-conditioning-repair-company/', $crumb);
  $crumb = str_replace('Air Conditioning Repair Company In Las Vegas, NV', 'Las Vegas, NV', $crumb);

  //Henderson
  $crumb = str_replace('/henderson/air-conditioning-repair-company/','/locations/henderson-air-conditioning-repair-company/', $crumb);
  $crumb = str_replace('Air Conditioning Repair Company In Henderson, NV', 'Henderson, NV', $crumb);

  //Laughlin
  $crumb = str_replace('/laughlin/air-conditioning-repair-company/','/locations/laughlin-air-conditioning-repair-company/', $crumb);
  $crumb = str_replace('Air Conditioning Repair Company In Laughlin, NV', 'Laughlin, NV', $crumb);

  //Kingman
  $crumb = str_replace('/kingman/air-conditioning-repair-company/','/locations/kingman-air-conditioning-repair-company/', $crumb);
  $crumb = str_replace('Air Conditioning Repair Company In Kingman, AZ', 'Kingman, AZ', $crumb);

  //Lake Havasu
  $crumb = str_replace('/lake-havasu/air-conditioning-repair-company/','/locations/lake-havasu-air-conditioning-repair-company/', $crumb);
  $crumb = str_replace('Air Conditioning Repair Company In Lake Havasu, AZ', 'Lake Havasu, AZ', $crumb);

  //Bullhead City
  $crumb = str_replace('/bullhead-city/air-conditioning-repair-company/','/locations/bullhead-city-air-conditioning-repair-company/', $crumb);
  $crumb = str_replace('Air Conditioning Repair Company In Bullhead City, AZ', 'Bullhead City, AZ', $crumb);

  //North Las Vegas
  $crumb = str_replace('/north-las-vegas-air-conditioning-repair-company/','/locations/north-las-vegas-air-conditioning-repair-company/', $crumb);
  $crumb = str_replace('Air Conditioning Repair Company in North Las Vegas, NV', 'North Las Vegas, NV', $crumb);


  return $crumb;

}
add_filter('wpseo_breadcrumb_output', 'hennessey_yoast_faq_breadcrumb_mod', 10, 1);



/*--------------------------------------------
Improved Page Navigation
---------------------------------------------*/

function numeric_posts_nav() {

  if( is_singular() )
    return;

  global $wp_query;

  /** Stop execution if there's only 1 page */
  if( $wp_query->max_num_pages <= 1 )
    return;

  $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
  $max   = intval( $wp_query->max_num_pages );

  /** Add current page to the array */
  if ( $paged >= 1 )
    $links[] = $paged;

  /** Add the pages around the current page to the array */
  if ( $paged >= 3 ) {
    $links[] = $paged - 1;
    $links[] = $paged - 2;
  }

  if ( ( $paged + 2 ) <= $max ) {
    $links[] = $paged + 2;
    $links[] = $paged + 1;
  }

  echo '<div class="pagination"><ul>' . "\n";

  /** Previous Post Link */
  if ( get_previous_posts_link() )
    printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

  /** Link to first page, plus ellipses if necessary */
  if ( ! in_array( 1, $links ) ) {
    $class = 1 == $paged ? ' class="active"' : '';

    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

    if ( ! in_array( 2, $links ) )
      echo '<li><span>...</span></li>';
  }

  /** Link to current page, plus 2 pages in either direction if necessary */
  sort( $links );
  foreach ( (array) $links as $link ) {
    $class = $paged == $link ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
  }

  /** Link to last page, plus ellipses if necessary */
  if ( ! in_array( $max, $links ) ) {
    if ( ! in_array( $max - 1, $links ) )
      echo '<li><span>...</span></li>' . "\n";

    $class = $paged == $max ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
  }

  /** Next Post Link */
  if ( get_next_posts_link() )
    printf( '<li>%s</li>' . "\n", get_next_posts_link() );

  echo '</ul></div>' . "\n";

}


/*--------------------------------------------
Add Lennox Promotion Popup
---------------------------------------------*/

//Popup
function hennessey_fall_lennox_promotion_popup() {

  global $post;
  //Get Header And Phone Number Information
  if ( get_post_meta( $post->ID, '_ae_primary_phone_number', true ) ) {
    $promoPhoneNumber = get_post_meta( $post->ID, '_ae_primary_phone_number', true );
  } else {
    $promoPhoneNumber = '888-628-5890';
  }
  ?>

    <div id="lennox-popup" class="lennox-promo-popup" style="display: none;">
      <div class="lennox-promo-popup__inner">
        <h2 class="lennox-promo-popup__title">The Lennox Fall Promotion is Here!</h2>

        <div class="row">
          <div class="lennox-left">
            <h2 class="lennox-promo-popup__intro">START WITH UP TO $1,250 IN SYSTEM REBATES</h2>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/images/lennox-promo/lennox-popup-system.jpg" width="300" height="189" alt="Lennox Fall Promotion" class="lennox-promo-popup__img">
            <p><small>*Save on a qualifying Lennox® home comfort system that includes an indoor unit, an outdoor unit and a qualifying “System Add-On” or “Thermostat.” Must include a qualifying “System Add-On” or “Thermostat” to be eligible for the $1,250 rebate.</small></p>
          </div>
          <div class="lennox-right">
            <h2 class="lennox-promo-popup__intro">GET UP TO $350 IN ADDITIONAL REBATES WITH THESE ITEMS</h2>
            <img width="300" height="169" src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/images/lennox-promo/lennox-popup-panel.png?v=1" alt="Lennox Fall Promotion" class="lennox-promo-popup__img">
            <p><small>Enjoy additional rebates when you purchase an Ultimate Comfort Indoor System + Ultimate Comfort Outdoor System + iComfort® S30 + PureAirS™ + iHarmony®.</small></p>
          </div>
        </div> <?php //End .row ?>

        <div class="lennox-promo-popup__cta-wrapper">
          <h2 class="lennox-promo-popup__cta">Schedule your appointment with a comfort specialist by calling <a href="tel:<?php echo $promoPhoneNumber; ?>"><?php echo $promoPhoneNumber; ?></a></h2>
          <span class="lennox-promo-popup__cta-divider">or by</span>
          <h2 class="lennox-promo-popup__cta">
            <a href="<?php echo site_url(); ?>/schedule-service/" class="aiwp_button current" style="font-size: 20px;">
              <span class="red" style="min-width: 200px; letter-spacing: -1px;">Booking Online Today</span>
              <span class="blue">&gt;</span>
            </a>
          </h2>
          <a href="<?php echo site_url(); ?>/products/lennox/" class="lennox-promo-popup__info-link">Learn more about why we are a Lennox® Premier Dealer™</a>
          <div class="lennox-promo-popup__disclaimer">
            <p>*Rebate requires purchase of qualifying items by 11/23/18 and submission of a completed rebate form (with proof of purchase) to <a href="https://www.lennoxconsumerrebates.com" rel="nofollow" target="_blank">www.lennoxconsumerrebates.com</a> no later than 12/14/18. Rebate is paid in the form of a Lennox Visa® prepaid debit card. Card is subject to terms and conditions found or referenced on card and expires 12 months after issuance. Conditions apply. See <a href="https://www.lennox.com" target="_blank" rel="nofollow">www.lennox.com</a> for complete rebate terms and conditions.</p>
            <p>**Offer available 9/3/18 – 11/23/18. Requires purchase of qualifying system.</p>
          </div>
        
        </div>

      </div>
    </div>

  <?php
}

add_action('wp_footer', 'hennessey_fall_lennox_promotion_popup');

//Shortcode for Use
function hennesseyLennoxPromotion($atts = null) {
  global $post;
  ob_start();
  //BEGIN OUTPUT
?>

    <a data-src="#lennox-popup" href="javascript:;" class="lennox-promo-widget" data-fancybox>
      <span class="lennox-promo-widget__inner">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/images/lennox-promo/lennox-sidebar-promotion.jpg"  width="300" height="250" alt="Lennox Fall Promotion" class="lennox-promo-widget__img">
      </span>
    </a>

<?php
  //END OUTPUT
  $output = ob_get_contents();
  ob_end_clean();
  return  $output;
}
add_shortcode('hc-lennox-promotion', 'hennesseyLennoxPromotion');


// MODIFY TITLE ON AMP PAGES

add_filter('wpseo_title', 'filter_amp_wpseo_title');
function filter_amp_wpseo_title($title) {
    if( strpos($_SERVER['REQUEST_URI'], "/amp/") !== false ) {
        $title = $title . ' | Ambient Edge';
    }
    return $title;
}

// ------- //


/*---------------------------------
Adding Footer Popup to Plumbing by Jake
----------------------------------*/

function hennessey_footer_popup_ambient(){
  ?>
    <a href="https://www.plumbingbyjake.com/" class="ambient-footer-popup">
        <div class="ambient-footer-popup__inner">
          <span class="ambient-footer-popup__close-button"></span>
          <span class="ambient-footer-popup__title">Looking for Plumbing Services?</span>          
          <img  width="115" height="101" class="ambient-footer-popup__image" src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/images/plumbing-by-jake.png" alt="Visit Plumbing By Jake!">
        </div>
    </a>
  <?php
}

add_action('wp_footer', 'hennessey_footer_popup_ambient');



wp_deregister_script('revmin-migration');



/*---------------------------------
Adding Google Optimize
----------------------------------*/

//add_action('wp_head', 'hennessey_add_google_optimize');

function hennessey_add_google_optimize(){
  ?>

<script>
gtag('config', 'UA-92188150-1', { 'optimize_id': 'GTM-5NKGXDW'});
</script>

  <?php 
}

/************************************
**
** Hennessey Consulting
**
************************************/