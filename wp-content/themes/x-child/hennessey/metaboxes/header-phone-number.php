<?php

add_action( 'cmb2_init', 'ambient_header_phone_information' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function ambient_header_phone_information() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_ae_';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $ambientPhoneInformation = new_cmb2_box( array(
        'id'            => $prefix . 'phone_information',
        'title'         => __( 'Phone & Header Information', 'cmb2' ),
        'object_types'  => array( 'page', 'wpseo_locations'), // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );

    $ambientPhoneInformation->add_field( array(
        'name' => __( 'Phone Number to Display', 'cmb2' ),
        'desc' => __( 'Enter the phone number to display on the page. If no number is entered, the toll free number will be displayed. (AZ: 928-263-8698, NV: 702-723-4704 ,Toll Free: 888-628-5890)', 'cmb2' ),
        'id'   => $prefix . 'primary_phone_number',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    $ambientPhoneInformation->add_field( array(
        'name' => __( 'Alternate Link for Emergency Service Button', 'cmb2' ),
        'desc' => __( 'Enter a link to override the default emergency service button link.', 'cmb2' ),
        'id'   => $prefix . 'emergency_button_link',
        'type' => 'text_url',
        // 'repeatable' => true,
    ) );



    $ambientPhoneInformation->add_field( array(
        'name'             => esc_html__( 'Use Alternate Header', 'cmb2' ),
        'desc'             => esc_html__( 'Select if the page should use the alternate version of the header.', 'cmb2' ),
        'id'               => $prefix . 'header_selection',
        'type'             => 'radio_inline',
        'show_option_none' => 'False',
        'options'          => array(
            'true' => esc_html__( 'True', 'cmb2' ),
        ),
    ) );

}