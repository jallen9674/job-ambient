<?php get_header(); ?>

  <div class="x-container max width offset">
    <div class="x-main full" role="main">

    
        <div class="search-header-wrap">
            <h1 class="entry-title">Search Results for: "<?php echo esc_attr(get_search_query()); ?>"</h1>
            <?php echo do_shortcode('[hc-search]'); ?>
        </div>

            <div class="search-contents">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" class="cf">

                    <header class="article-header">

                        <h3 class="search-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

                        <a href="<?php echo the_permalink(); ?>" class="search-link linebreak"><?php echo the_permalink(); ?></a>
                    </header>

                    <section class="entry-content"></section>
                        <?php the_excerpt( '<span class="read-more">' . __( 'Read more &raquo;', 'bonestheme' ) . '</span>' ); ?>
                    </section>

                </article>

                <?php endwhile; ?>
                   
<?php else: ?> <p>No results found.</p> <?php endif;  ?>
                <?php numeric_posts_nav(); ?>
            
        

            </div>


    </div>
  </div>

<?php get_footer(); ?>
