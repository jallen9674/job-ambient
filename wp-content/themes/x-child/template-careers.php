<?php //Template Name: Careers Page?>

<?php get_header(); ?>

  <div class="x-container max width offset">
    <div class="x-main left" role="main">

      <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
      <?php endwhile; ?>       

    </div>
    <div class="x-sidebar right">
        <?php echo do_shortcode('[x_custom_headline level="h4" looks_like="h5" accent="false"]<span> Featured Job Positions</span>[/x_custom_headline]'); ?>
        <?php echo do_shortcode('[x_image type="none" src="https://www.ambientedge.com/wp-content/uploads/2017/11/Home-Page-Banners_Trained-Techs-GalleryHeader.jpg" alt="Ambient Edge HVAC Trained Technicians" link="false" info="none" info_place="top" info_trigger="hover" info_content=""]'); ?>
        <div id='bzOpeningsContainer'></div><script src='https://ambient-edge.breezy.hr/embed/js?inline=true&group_by=loc'></script>
        <hr>
        <a class="aiwp_button current" style="font-size: 15px;" href="https://ambient-edge.breezy.hr/">
            <span class="red">See All Job Openings</span> <span class="blue">&gt;</span>
        </a>
    </div>
  </div>

<?php get_footer(); ?>
