<?php //Template Name: Leadpage - Spring Tuneup ?>

<?php get_header(); ?>

  <div class="x-container max width offset">
    <div class="x-main full" role="main">

    <?php
    /*-----------------------------
    BEGIN SECTION 1
    ------------------------------*/
    ?>

        <div class="r22-section-one r22-page-section">
            <div class="r22-page-section__inner r22-section-one__inner">

                <span class="r22-section-one__attention-text">
                    ATTENTION!
                </span>

                <p style="text-align: center; font-size: 2.1em; margin-bottom: 5px; line-height: 1.2; text-align: center; max-width: 670px; margin-left: auto; margin-right: auto;">
                    <span class="big-59-text">$89</span>
                    Invest $89 and Get the Absolute Best Air Conditioning Tune-Up Offered Ever!
                </p>

                <p style="margin: 30px auto; text-align: left; max-width: 675px; font-size: 1.4em; clear: both;">
                    <img src"<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/tuneup/teflon-icon.png" class="teflon-lead-icon"> Plus, we will treat your A/C unit with a Teflon spray to protect it from the harsh summer heat,  ABSOLUTELY FREE (a $100 value)!
                </p>

                <a href="#schedule-now" class="r22-schedule-button">Get Your $89 Tune-Up Now!</a>

            </div>
        </div> <?php //End .r22-section-one ?>

    <?php
    /*-----------------------------
    BEGIN SECTION 2
    ------------------------------*/
    ?>

        <div class="r22-section-two r22-page-section">
            <div class="r22-page-section__inner r22-section-two__inner">

                <div class="r22-section-two__left">

                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/tuneup/cooling-icon.png" alt="Cooling Icon">

                </div> <?php //End Left ?>

                <div class="r22-section-two__right">
                    <span class="r22-section-two__title">
                        Superior Service and Quality
                    </span>

                    <div class="r22-section-two__general-content" style="font-size: 18px;">
                        <p>
                            Our tune up is far superior to other tune-up offers you’ll get. We include expensive “extras” free of charge that others don’t. We’ve designed our tune-up to include what we consider to be the most important procedures to be included in a tune-up and that’s why it is the Absolute Best Tune-Up ever offered.
                        </p>
                        <p>Here is an example of what we do:</p>
                    </div>

                    <div class="r22-section-two__checkbox-content">
                        <span class="r22-section-two__checkbox-content-title" style="font-size: 18px;">
                            We clean the condenser coils in your outdoor unit with a cleaning solution and not simply rinse them off with a garden hose. While water is good for removing debris, it fails to remove that oily or greasy residue that clings to the surface.
                        </span>
                    </div>

                    <div class="r22-section-two__checkbox-content">
                        <span class="r22-section-two__checkbox-content-title" style="font-size: 18px;">
                            Once cleaned, we treat the coils by spraying them with Teflon at NO ADDITIONAL COST. (a $100 value) As you know, Teflon reduces dirt, grime, and everything else from sticking to treated surfaces. This keeps your coils clean longer and helps to keep your electric bill down.
                        </span>
                    </div>

                    <div class="r22-section-two__checkbox-content">
                      <span class="r22-section-two__checkbox-content-title" style="font-size: 18px;">
                            We take the time to clean the blower in your furnace or air handler. This helps maintain maximum air flow which results in more efficient operation.
                        </span>
                    </div>

                    <div class="r22-section-two__general-content" style="font-size: 18px;">
                        <p>
                            These extra steps add costs as well as more time to do your tune-up right! But, even with the extra cost, we are still offering you the Best Air Conditioning Tune-Up Ever for only $89. Normally, the cost to provide a service of this type along with the cost of the Teflon would run more than $219 but is yours for only $89 if you act now. Plus, our 100% Satisfaction Guarantee includes the following:
                        </p>
                    </div>



                </div> <?php //End Right ?>

            </div>
        </div> <?php //End .r22-section-two ?>


        <?php
        /*-----------------------------
        BEGIN SECTION 3
        ------------------------------*/
        ?>

            <div class="r22-section-three r22-page-section">
                <div class="r22-page-section__inner r22-section-three__inner">

                <span class="r22-section-three__title">
                    <span style="color: #ec2024;">100%</span> Satisfaction Guaranteed!
                </span>

                <p style="text-align: center; margin-bottom: 20px;">
                    SATISFACTION MATTERS | We’re supremely confident that our service and product quality will surpass what you expect, and that’s reflected in how our service contracts are written. We put our 100% satisfaction guarantee right there for everyone to see!
                </p>


                <div class="tune-up-blue-list">
                    <ol>
                        <li>If our tune-up doesn’t reduce your electric bill we will refund your $89 tune-up investment. Just compare last year’s electric usage with this year’s electric usage.</li>
                        <li>If your air conditioner breaks down within 12 months from the date of the tune-up we will apply the entire tune-up investment of $89 towards the repair.</li>
                        <li>If you decide to have us replace your air conditioner (for any reason) within one (1) year we will apply triple the cost of the tune-up (total of $207) towards the replacement cost.
                        </li>
                    </ol>
                </div>

                <div class="r22-sets-apart-box">

                    <div class="r22-sets-apart-box__title">
                        What Sets Us Apart
                    </div>

                    <p>
                        Any heating and cooling company can promise great results, but what sets use apart is consistency. At Ambient Edge, we offer superior quality and service you can count on.
                    </p>

                    <div class="r22-sets-apart-box__icons">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/available-24-7.png" alt="Available 24/7">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/satisfaction-guarantee.png" alt="Satisfaction Guarantee">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/flat-rate-pricing.png" alt="Flat Rate Pricing">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/free-estimates.png" alt="Free Estimates">
                    </div>

                </div>

                <div class="r22-section-three__call-us-box">
                    Call Us Now! <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/phone-icon.png" alt="Phone Icon"> 888-628-5890
                </div>


                </div>
            </div> <?php //End .r22-page-section-three ?>

        <?php
        /*-----------------------------
        BEGIN SECTION 4 - Contact Form
        ------------------------------*/
        ?>

            <div class="r22-section-four r22-page-section" id="schedule-now">
                <div class="r22-page-section__inner r22-section-four__inner">

                    <div class="r22-section-four__left">
                        <div style="max-width: 450px; margin: 0 auto;">
                            <span class="r22-section-four__title">
                                Request a Call
                            </span>
                            <p style="font-size: 1.1em; margin-bottom: 20px; text-align: center;">
                                Call our PRIORITY SCHEDULING LINE at 888-628-5890. This offer won't last long, call today! We must be able to perform your tune-up before the hot weather arrives. That’s the only way we can offer our tune-up at these prices and with all these guarantees.
                            </p>
                            <p style="text-align: center; font-size: 2em; margin-bottom: 20px;">
                                CALL NOW!<br>888-628-5890
                            </p>
                        </div>
                    </div>

                    <div class="r22-section-four__right">
                        <div class="r22-section-four__form">
                            <?php //echo do_shortcode('[formidable id=6 title=false description=false]'); ?>
                            <?php echo do_shortcode('[gravityform id="13" title="false" description="false" ajax="true"]'); ?>
                        </div>
                    </div>

                </div>
            </div> <?php //End .r22-section-four ?>

        <?php
        /*-----------------------------
        BEGIN SECTION 5 - Testimonials
        ------------------------------*/
        ?>

            <div class="r22-section-five r22-page-section">
                <div class="r22-page-section__inner r22-section-five__inner">

                    <span class="r22-section-five__title">
                        Testimonials
                    </span>

                    <div class="r22-single-testimonial">
                        <span class="r22-single-testimonial__title">
                            Better Business, <span style="color: #e00943">Review</span>
                        </span>
                        <div class="r22-single-testimonial__content">
                            "The installers are knowledgeable and fast. They are a great bunch of people at Ambient Edge!"
                        </div>
                    </div>

                    <div class="r22-single-testimonial">
                        <span class="r22-single-testimonial__title">
                            Facebook, <span style="color: #e00943">Review</span>
                        </span>
                        <div class="r22-single-testimonial__content">
                            "If you have any problems with your heating or cooling - call Ambient edge. They are great to work with, they answer all your questions and give you excellent advice and come when you call."
                        </div>
                    </div>

                    <a href="https://www.ambientedge.com/reviews/" target="_blank" class="r22-section-five__read-more">Read More Reviews &raquo;</a>

                </div>
            </div> <?php //End .r22-section-five ?>

        <?php
        /*-----------------------------
        BEGIN SECTION 6 - President
        ------------------------------*/
        ?>

            <div class="r22-section-six r22-page-section">
                <div class="r22-page-section__inner">

                    <div class="r22-section-six__inner">
                        <div class="r22-section-six__left">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/ambient-edge-president.jpg" alt="Ambient Edge President Steve Lewis">
                        </div>

                        <div class="r22-section-six__right">
                            <span class="r22-section-six__title">
                                President: <span style="color: #e00943">Steven Lewis</span>
                            </span>
                            <p>
                                "Hello! I know that the comfort of your home rests in the quality and efficiency of your home heating and cooling system. That is why at Ambient Edge, you will get quality service from industry trained technicians, I guarantee it."
                            </p>
                        </div>
                    </div>

                    <a href="#schedule-now" class="r22-schedule-button">Schedule Now</a>

                </div>
            </div> <?php //End .r22-section-six ?>

    </div>
  </div>

<?php get_footer(); ?>