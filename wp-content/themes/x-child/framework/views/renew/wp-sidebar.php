<?php

// =============================================================================
// VIEWS/RENEW/WP-SIDEBAR.PHP
// -----------------------------------------------------------------------------
// Sidebar output for Renew.
// =============================================================================

?>
<?php if($post->ID === 5238){ ?>
  <aside class="<?php x_sidebar_class(); ?>" role="complementary">
	<h3>Booking Online FAQ</h3>
	<div class="rs-sidebar-faq">

	<a href="#"><strong>Is my appointment Guaranteed?</strong></a>
	<div>
		<p>No, your appointment is not confirmed until you receive an email/text confirmation. If we have any issues, one of our service specialists will contact you with questions and to book your appointment.</p>
	</div>
	<a href="#"><strong>Can I schedule Emergency Service Request?</strong></a>
	<div>
	<p>No, please call <a href="tel:+1-888-221-4702">888-221-4702</a> to speak with our live on-call team.</p>	</div>
	<a href="#"><strong>What types of services can I request online? </strong></a>
	<div>
	<ul>
	<li>Non-emergency repair or service calls</li>
	<li>Maintenance and tune-up Request</li>
	<li>Estimate for a new system</li>
	<li>Edge Assurance Plan maintenances</li>
	<li>Commercial service requests</li>
	</ul>
	</div>
	<a href="#"><strong>Do you charge a service fee?</strong></a>
	<div>
	<p>Yes, we do have a service fee to come out for repair calls. For questions regarding service fees please call our on-call team at <a href="tel:+1-888-221-4702">888-221-4702</a>.</p>
	</div>
	<a href="#"><strong>Is there an additional charge for after-hours? </strong></a>
	<div>
		<p>Yes, calls booked from 5:00 PM – 10:00 PM are subject to extended hours service fees. For questions regarding service fees please call our on-call team at <a href="tel:+1-888-221-4702">888-221-4702</a>.</p>
	</div>
</aside>
<?php } else { ?>


<?php if ( x_get_content_layout() != 'full-width' ) : ?>
<!-- sidebar test -->
  <aside class="<?php x_sidebar_class(); ?>" role="complementary">
    <?php if ( get_option( 'ups_sidebars' ) != array() ) : ?>
      <?php dynamic_sidebar( apply_filters( 'ups_sidebar', 'sidebar-main' ) ); ?>
    <?php else : ?>
      <?php dynamic_sidebar( 'sidebar-main' ); ?>
    <?php endif; ?>
  </aside>

<?php endif; ?>

<?php } ?>