<?php

// =============================================================================
// VIEWS/GLOBAL/_TOPBAR.PHP
// -----------------------------------------------------------------------------
// Includes topbar output.
// =============================================================================

global $page;

//Get Header And Phone Number Information
if ( get_post_meta( $post->ID, '_ae_primary_phone_number', true ) ) {
    $phoneNumber = get_post_meta( $post->ID, '_ae_primary_phone_number', true );
} else {
    $phoneNumber = '888-628-5890';
}

if ( get_post_meta( $post->ID, '_ae_emergency_button_link', true ) ) {
  $emergencyButtonLink = get_post_meta( $post->ID, '_ae_emergency_button_link', true );
} else {
  $emergencyButtonLink =  site_url() . '/about-us/emergency-services/';
}

$alternateHeader = get_post_meta( $post->ID, '_ae_header_selection', true );

?>

<?php if ( x_get_option( 'x_topbar_display' ) == '1' ) : ?>

  <div class="x-topbar vegas-header x-navbar-fixed-top">
    <div class="x-topbar-inner x-container max width">

     <?php
        //Alternate Header for Las Vegas Page
       {
            ?>
            <div class="vegas-header__left">
              <a rel="nofollow" href="https://facebook.com/AmbientEdgeAC" target="_blank" class="vegas-header__social-icon"><i class="fa fa-facebook"></i></a>
              <a rel="nofollow" href="https://www.twitter.com/ambientedge" target="_blank" class="vegas-header__social-icon"><i class="fa fa-twitter"></i></a>
            </div>

            <div class="vegas-header__right vegas-header__right--desktop">

              <div class="awards-section">
                <span class="awards-section__title">
                  As Featured On:
                </span>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/images/trust-logos/inc-magazine-white.png" width="63" height="17" alt="Inc Magazine" class="awards-section__trust-logo">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/images/trust-logos/today-show-white.png" width="48" height="27" alt="Huffington Post" class="awards-section__trust-logo awards-section__trust-logo--today-show">
              </div>

                <a href="<?php echo site_url(); ?>/schedule-service/" class="vegas-header__contact-button">Schedule Appointment</a>
                <a href="<?php echo $emergencyButtonLink; ?>" class="vegas-header__emergency-button">24/7 Emergency Service</a>
                <a href="tel:+1- <?php echo $phoneNumber; ?>" class="vegas-header__telephone"> <?php echo $phoneNumber; ?></a>
            </div>

            <div class="vegas-header__right vegas-header__right--mobile">

              <a href="#" class="x-btn-navbar vegas-header-menu-toggle collapsed" data-toggle="collapse" data-target=".x-nav-wrap.mobile" style="outline: none;">
                <i class="fa fa-bars"></i>
                <span class="visually-hidden">Navigation</span>
              </a>

              <a href="tel:+1- <?php echo $phoneNumber; ?>" class="vegas-header__emergency-button">24/7 Emergency Service</a>

				<a class="rs-book-online-header button" href="https://www.ambientedge.com/schedule-service/">Book Online</a>
              
				<a href="tel:+1- <?php echo $phoneNumber; ?>" class="vegas-header__telephone-button"> <?php echo $phoneNumber; ?></a>
				
				

            </div>

     <?php /* Old Header Display --- Disabled
       else {
    ?>
      <?php if ( x_get_option( 'x_topbar_content' ) != '' ) : ?>
      <p class="p-info"><?php echo x_get_option( 'x_topbar_content' ); ?></p>
      <?php endif; ?>
      <?php x_social_global(); ?>
    <?php } //End Else ?>
    */ ?>
  <?php } ?>
    </div>
  </div>

<?php endif; ?>