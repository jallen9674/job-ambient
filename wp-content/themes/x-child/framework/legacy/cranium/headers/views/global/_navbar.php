<?php

// =============================================================================
// VIEWS/GLOBAL/_NAVBAR.PHP
// -----------------------------------------------------------------------------
// Outputs the navbar.
// =============================================================================

global $post;
$navbar_position = x_get_navbar_positioning();
$logo_nav_layout = x_get_logo_navigation_layout();
$is_one_page_nav = x_is_one_page_navigation();
$alternateHeader = get_post_meta( $post->ID, '_ae_header_selection', true );


?>

<?php if ( ( $navbar_position == 'static-top' || $navbar_position == 'fixed-top' || $is_one_page_nav ) && $logo_nav_layout == 'stacked' ) : ?>

  <div class="x-logobar">
    <div class="x-logobar-inner">
      <div class="x-container max width">
        <?php x_get_view( 'global', '_brand' ); ?>
      </div>
    </div>
  </div>

  <div class="x-navbar-wrap">
    <div class="<?php x_navbar_class(); ?>">
      <div class="x-navbar-inner">
        <div class="x-container max width">
          <?php x_get_view( 'global', '_nav', 'primary' ); ?>
        </div>
      </div>
    </div>
  </div>

<?php else : ?>

  <div class="x-navbar-wrap <?php if ($alternateHeader == 'true') { echo 'alternate-header'; } ?>">
    <div class="<?php x_navbar_class(); ?>">
      <div class="x-navbar-inner">
        <div class="x-container max width">
          <?php x_get_view( 'global', '_brand' ); ?>

	<div id="plumbing-by-jake">
		<a href="<?php echo site_url(); ?>/services/plumbing/" title="Plumbing by Jake">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/images/plumbing-by-jake.png" height="101" width="115" alt="Plumbing by Jake" />
		</a>
	</div>

          <?php x_get_view( 'global', '_nav', 'primary' ); ?>
        </div>
      </div>
    </div>
  </div>

<?php endif; ?>