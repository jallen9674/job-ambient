<?php
/*
 Template Name: Blog Archive
 *
 * A custom page template -- for duplication.
 *
*/

$args = array(
    'post_type'              => 'post',
    'posts_per_page'         => -1,
    'order'                  => 'ASC',
    'orderby'                => 'date'
  );

  // The Query
  $query = new WP_Query( $args );
?>
<?php get_header(); ?>

  <div class="x-container max width offset">
    <div class="x-main left" role="main">
		<ul>
			<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
				<li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
			<?php endwhile; endif; ?>
		</ul>     
    </div>
    <div class="x-sidebar right">
        <?php echo do_shortcode('[x_custom_headline level="h4" looks_like="h5" accent="false"]<span> Featured Job Positions</span>[/x_custom_headline]'); ?>
        <?php echo do_shortcode('[x_image type="none" src="https://www.ambientedge.com/wp-content/uploads/2017/11/Home-Page-Banners_Trained-Techs-GalleryHeader.jpg" alt="Ambient Edge HVAC Trained Technicians" link="false" info="none" info_place="top" info_trigger="hover" info_content=""]'); ?>
        <div id='bzOpeningsContainer'></div><script src='https://ambient-edge.breezy.hr/embed/js?inline=true&group_by=loc'></script>
        <hr>
        <a class="aiwp_button current" style="font-size: 15px;" href="https://ambient-edge.breezy.hr/">
            <span class="red">See All Job Openings</span> <span class="blue">&gt;</span>
        </a>
    </div>
  </div>

<?php get_footer(); ?>
