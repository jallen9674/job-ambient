<?php //Template Name: FAQ Listings ?>

<?php get_header(); ?>

  <div class="x-container max width offset">
    <div class="x-main full" role="main">

      <?php while ( have_posts() ) : the_post(); ?>
        <div class="faq-header-wrap">
            <h1 class="entry-title">Frequently Asked Questions</h1>
            <?php echo do_shortcode('[hc-search]'); ?>
        </div>
      <?php endwhile; ?>

        <?php //BEGIN FAQ DISPLAY ?>
            <div class="faq-filter-portfolio-wrapper">

                <div id="filters" class="faq-filter-category-list filters-button-group">
                    <?php //Output all Categories
                    echo '<button class="isotope-link is-checked" data-filter="*">All</button>';
                        $terms = get_terms( 'ambient_faqs');
                        if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){

                            foreach ( $terms as $term ) {
                            echo '<button class="isotope-link" data-filter=".' . $term->slug . '"><span>' . $term->name . '</span></button>';

                            }

                        }
                    ?>
                </div>

                <div class="faq-filter-list">
                    <?php
                        $faqTerms = get_terms( 'ambient_faqs' );
                        // convert array of term objects to array of term IDs
                        $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

                        $args = array(
                            'posts_per_page' => -1,
                            'post_type' => 'page',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'ambient_faqs',
                                    'field' => 'term_id',
                                    'terms' => $faqTermIDs
                                ),
                            ),
                            'order' => 'DSC',
                        );

                        $the_query = new WP_Query( $args );
                        if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                        ?>
                        <?php //Getting Category for Filtering
                            $postTerms =  wp_get_object_terms($post->ID, 'ambient_faqs');
                            $categoryFilterSlug = '';
                            $categoryPrettyName = '';
                            if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
                                    foreach ( $postTerms as $term ) {
                                    $categoryFilterSlug .= ' ' . $term->slug;
                                    $categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
                                    }
                                }
                            ?>

                        <div class="faq-filter-listing  grid-item <?php echo $categoryFilterSlug; ?> ">

                            <a href="<?php the_permalink(); ?>" class="faq-filter-image-link">
                            <?php
                                if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                    the_post_thumbnail('faq-thumb');
                                } else{
                                    echo '<img alt="faq thumb" src="' . get_stylesheet_directory_uri() . '/hennessey/images/default-faq-thumb.jpg" />';
                                }
                            ?>
                            <?php /* <span class="faq-date"><?php echo get_the_date('m-j-Y');  ?></span> */ ?>
                            </a>
                            <div class="faq-meta">
                                <span class="faq-category"><?php echo $categoryPrettyName; ?></span>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="faq-filter-title-link"><?php the_title(); ?></a>

                            <a href="<?php the_permalink(); ?>" class="continue-reading-button">Continue Reading &raquo;</a>

                        </div>
                    <?php endwhile; else : ?>
                    <!-- IF NOTHING FOUND CONTENT HERE -->
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>

            </div>
        <?php //END FAQ DISPLAY ?>

        <div class="resources-page-link">
            <a href="<?php echo site_url(); ?>/resources/">Have more questions? Click here to view additional answers and resources.</a>
        </div>

    </div>
  </div>

<?php get_footer(); ?>
