<?php //Template Name: Leadpage - R22 ?>

<?php get_header(); ?>

  <div class="x-container max width offset">
    <div class="x-main full" role="main">

      <?php while ( have_posts() ) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class('faq-top-content'); ?>>
          <?php x_get_view( 'global', '_content', 'the-content' ); ?>
        </article>

      <?php endwhile; ?>

    <?php
    /*-----------------------------
    BEGIN SECTION 1
    ------------------------------*/
    ?>

        <div class="r22-section-one r22-page-section">
            <div class="r22-page-section__inner r22-section-one__inner">

                <span class="r22-section-one__attention-text">
                    ATTENTION!
                </span>
                <p style="text-align: center; font-size: 1.2em;">
                    Your air conditioning system's refrigerant may soon be obsolete.
                </p>
                <p style="text-align: center; font-size: 2.1em; margin-bottom: 5px; line-height: 1.2;">
                    Important Information Regarding the R-22 PHASE OUT
                </p>
                <p style="text-align: center; font-size: 1.8em; margin-bottom: 5px; line-height: 1.2;">
                    Is your air conditioning unit affected?
                </p>
                <p style="text-align: center; font-size: 1em; margin-bottom: 5px; line-height: 1.2;">
                    If so...
                </p>

                <ul style="margin: 20px auto; text-align: left; max-width: 675px; font-size: 1.4em;">
                    <li>The cost to maintain your system will continue to increase.</li>
                    <li>You may have to rely soley on recycled or reclaimed R-22 after 2020.</li>
                </ul>

                <p style="text-align: center; font-size: 1em; margin-bottom: 5px; line-height: 1.2; color: #ec2024; font-size: 1.8em;">
                    Schedule a FREE in-home evaluation to learn more about your options.
                </p>

                <a href="#schedule-now" class="r22-schedule-button">Schedule Now</a>

            </div>
        </div> <?php //End .r22-section-one ?>

    <?php
    /*-----------------------------
    BEGIN SECTION 2
    ------------------------------*/
    ?>

        <div class="r22-section-two r22-page-section">
            <div class="r22-page-section__inner r22-section-two__inner">

                <div class="r22-section-two__left">

                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/arrow-cost-image.jpg" alt="R22 Costs are Skyrocketing" style="border-bottom: 3px solid #000;">

                    <div style="padding: 10px;">

                        <p style="font-size: 1.43em; margin-bottom: 10px;">
                            Our Recommendations
                        </p>

                        <p style="margin-bottom: 10px;">
                           Due to the number of homes affected by this phase out, we recommend you start early evaluating your options and to consider replacing your unit to meet the EPA standards. As the phase out progresses, the cost of a recharge of R22 will significantly increase over the next 2 years. New refrigerant will not be produced after 2019 and the cost of a recharge is expected to skyrocket even further.
                        </p>

                        <p style="font-size: 1.1em; color: #ec2024; text-align: center;">
                            Schedule a FREE in-home evaluation to learn more and go over your options.
                        </p>

                        <a href="#schedule-now" class="r22-schedule-button">Schedule Now</a>

                    </div>

                </div> <?php //End Left ?>

                <div class="r22-section-two__right">
                    <span class="r22-section-two__title">
                        What you need to know...
                    </span>

                    <div class="r22-section-two__checkbox-content">
                        <span class="r22-section-two__checkbox-content-title">
                            What is R-22 Phase Out
                        </span>
                        <p>
                            The Environmental Protection Agency (EPA), through Clean Air Act Regulations is phasing out the production and import of the ozone-depleting substance R-22 Refrigerant that is common in air conditioning systems manufactured before January 2010.
                        </p>
                    </div>

                    <div class="r22-section-two__checkbox-content">
                        <span class="r22-section-two__checkbox-content-title">
                            How does this affect homeowners?
                        </span>
                        <ul>
                            <li>For units that were made prior to 2010, by 2020, R-22 will be completely phased out and homeowners will need to rely solely on recycled or reclaimed R-22 refrigerant to service their unit.</li>
                            <li>Over the next few years, the cost of R22 refrigerant will continue to rise during the phase-out period increasing the cost of upkeep on units that are still using this class II substance.  </li>
                            <li>After phasing out, it will become extremely costly and more difficult to maintain your air conditioning system.</li>
                            <li>If your unit was manufactured after 2010, your system should not be affected by this change. But for units that were made prior to 2010, you will need to replace your system by 2020 to avoid the high service costs that will be associated with R-22 units.</li>
                        </ul>
                    </div>

                    <div class="r22-section-two__checkbox-content">
                        <span class="r22-section-two__checkbox-content-title">
                            Do you have to replace your system?
                        </span>
                        <p>
                            No, but cost of recharge will continue to rise and we do recommend evaluating options early.
                        </p>
                    </div>

                    <div class="r22-section-two__checkbox-content">
                        <span class="r22-section-two__checkbox-content-title">
                            We're are here to help!
                        </span>
                        <p>
                            Ambient Edge is here to help you ensure compliance with the EPA standards and can provide you with a free-no obligation evaluation and estimate for a new system. Plus, we can provide you with up to $500 off select systems, and 0% Financing for up to 60 months*.
                        </p>
                    </div>

                </div> <?php //End Right ?>

            </div>
        </div> <?php //End .r22-section-two ?>


        <?php
        /*-----------------------------
        BEGIN SECTION 3
        ------------------------------*/
        ?>

            <div class="r22-section-three r22-page-section">
                <div class="r22-page-section__inner r22-section-three__inner">

                <span class="r22-section-three__title">
                    <span style="color: #ec2024;">100%</span> Satisfaction Guaranteed!
                </span>

                <p style="text-align: center; margin-bottom: 20px;">
                    SATISFACTION MATTERS | We’re supremely confident that our service and product quality will surpass what you expect, and that’s reflected in how our service contracts are written. We put our 100% satisfaction guarantee right there for everyone to see!
                </p>

                <div class="r22-sets-apart-box">

                    <div class="r22-sets-apart-box__title">
                        What Sets Us Apart
                    </div>

                    <p>
                        Any heating and cooling company can promise great results, but what sets use apart is consistency. At Ambient Edge, we offer superior quality and service you can count on.
                    </p>

                    <div class="r22-sets-apart-box__icons">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/available-24-7.png" alt="Available 24/7">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/satisfaction-guarantee.png" alt="Satisfaction Guarantee">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/flat-rate-pricing.png" alt="Flat Rate Pricing">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/free-estimates.png" alt="Free Estimates">
                    </div>

                </div>

                <div class="r22-section-three__call-us-box">
                    Call Us Now! <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/phone-icon.png" alt="Phone Icon"> 888-628-5890
                </div>


                </div>
            </div> <?php //End .r22-page-section-three ?>

        <?php
        /*-----------------------------
        BEGIN SECTION 4 - Contact Form
        ------------------------------*/
        ?>

            <div class="r22-section-four r22-page-section" id="schedule-now">
                <div class="r22-page-section__inner r22-section-four__inner">

                    <div class="r22-section-four__left">
                        <div style="max-width: 450px; margin: 0 auto;">
                            <span class="r22-section-four__title">
                                Request a Call
                            </span>
                            <p style="font-size: 1.1em; margin-bottom: 20px; text-align: center;">
                                Call our PRIORITY SCHEDULING LINE at 888-628-5890 to schedule your free in-home evaluatation and to learn more about the R-22 Phase Out.
                            </p>
                            <p style="text-align: center; font-size: 2em; margin-bottom: 20px;">
                                CALL NOW! 888-628-5890
                            </p>
                        </div>
                    </div>

                    <div class="r22-section-four__right">
                        <div class="r22-section-four__form">
                            <?php //echo do_shortcode('[formidable id=6 title=false description=false]'); ?>
                            <?php echo do_shortcode('[gravityform id="11" title="false" description="false" ajax="true"]'); ?>
                        </div>
                    </div>

                </div>
            </div> <?php //End .r22-section-four ?>

        <?php
        /*-----------------------------
        BEGIN SECTION 5 - Testimonials
        ------------------------------*/
        ?>

            <div class="r22-section-five r22-page-section">
                <div class="r22-page-section__inner r22-section-five__inner">

                    <span class="r22-section-five__title">
                        Testimonials
                    </span>

                    <div class="r22-single-testimonial">
                        <span class="r22-single-testimonial__title">
                            Better Business, <span style="color: #e00943">Review</span>
                        </span>
                        <div class="r22-single-testimonial__content">
                            "The installers are knowledgeable and fast. They are a great bunch of people at Ambient Edge!"
                        </div>
                    </div>

                    <div class="r22-single-testimonial">
                        <span class="r22-single-testimonial__title">
                            Facebook, <span style="color: #e00943">Review</span>
                        </span>
                        <div class="r22-single-testimonial__content">
                            "If you have any problems with your heating or cooling - call Ambient edge. They are great to work with, they answer all your questions and give you excellent advice and come when you call."
                        </div>
                    </div>

                    <a href="https://www.reviewbuzz.com/AmbientEdge-LasVegas/" target="_blank" class="r22-section-five__read-more">Read More Reviews &raquo;</a>

                </div>
            </div> <?php //End .r22-section-five ?>

        <?php
        /*-----------------------------
        BEGIN SECTION 6 - President
        ------------------------------*/
        ?>

            <div class="r22-section-six r22-page-section">
                <div class="r22-page-section__inner">

                    <div class="r22-section-six__inner">
                        <div class="r22-section-six__left">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/hennessey/leadpages/r22/ambient-edge-president.jpg" alt="Ambient Edge President Steve Lewis">
                        </div>

                        <div class="r22-section-six__right">
                            <span class="r22-section-six__title">
                                President: <span style="color: #e00943">Steven Lewis</span>
                            </span>
                            <p>
                                "Hello! I know that the comfort of your home rests in the quality and efficiency of your home heating and cooling system. That is why at Ambient Edge, you will get quality service from industry trained technicians, I guarantee it."
                            </p>
                        </div>
                    </div>

                    <a href="#schedule-now" class="r22-schedule-button">Schedule Now</a>

                </div>
            </div> <?php //End .r22-section-six ?>

    </div>
  </div>

<?php get_footer(); ?>