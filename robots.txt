User-agent: *
Disallow: /wp-admin/
Disallow: /blog/page/*
Disallow: /tag/*
Disallow: /category/*
Allow: /wp-admin/admin-ajax.php
Sitemap: https://www.ambientedge.com/sitemap_index.xml
